#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-29 06:44+0000\n"
"PO-Revision-Date: 2024-08-26 16:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/kde_kf5-apps_gwenview/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the gwenview-download-http entity
#: blfs-en/kde/kf5-apps/gwenview.xml:7
#, xml-text
#| msgid "&kf5apps-download-http;/gwenview-&kf5apps-version;.tar.xz"
msgid "&kf6apps-download-http;/gwenview-&kf6apps-version;.tar.xz"
msgstr "&kf6apps-download-http;/gwenview-&kf6apps-version;.tar.xz"

#. type: Content of the gwenview-md5sum entity
#: blfs-en/kde/kf5-apps/gwenview.xml:9
#, xml-text
msgid "77571ff82bc6869ae6698d9e6c4d0ea2"
msgstr "77571ff82bc6869ae6698d9e6c4d0ea2"

#. type: Content of the gwenview-size entity
#: blfs-en/kde/kf5-apps/gwenview.xml:10
#, xml-text
msgid "6.2 MB"
msgstr "6,2 MB"

#. type: Content of the gwenview-buildsize entity
#: blfs-en/kde/kf5-apps/gwenview.xml:11
#, xml-text
msgid "55 MB"
msgstr "55 MB"

#. type: Content of the gwenview-time entity
#: blfs-en/kde/kf5-apps/gwenview.xml:12
#, xml-text
#| msgid "0.5 SBU (using parallelism=4)"
msgid "0.6 SBU (using parallelism=4)"
msgstr "0,6UPC (usando paralelismo = 4)"

#. type: Content of: <sect1><title>
#: blfs-en/kde/kf5-apps/gwenview.xml:15 blfs-en/kde/kf5-apps/gwenview.xml:19
#, xml-text
#| msgid "Gwenview-&kf5apps-version;"
msgid "gwenview-&kf6apps-version;"
msgstr "gwenview-&kf6apps-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/kde/kf5-apps/gwenview.xml:22
#, xml-text
msgid "Gwenview"
msgstr "Gwenview"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/kde/kf5-apps/gwenview.xml:26
#, xml-text
msgid "Introduction to Gwenview"
msgstr "Introdução ao Gwenview"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:29
#, xml-text
msgid ""
"<application>Gwenview</application> is a fast and easy-to-use image viewer "
"for KDE."
msgstr ""
"<application>Gwenview</application> é um visualizador de imagens rápido e "
"fácil de usar para o KDE."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf5-apps/gwenview.xml:35
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:39
#, xml-text
#| msgid "Download (HTTP): <ulink url=\"&gwenview5-download-http;\"/>"
msgid "Download (HTTP): <ulink url=\"&gwenview-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&gwenview-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:44
#, xml-text
#| msgid "Download (FTP): <ulink url=\"&gwenview5-download-ftp;\"/>"
msgid "Download (FTP): <ulink url=\"&gwenview-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&gwenview-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:49
#, xml-text
#| msgid "Download MD5 sum: &gwenview5-md5sum;"
msgid "Download MD5 sum: &gwenview-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &gwenview-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:54
#, xml-text
#| msgid "Download size: &gwenview5-size;"
msgid "Download size: &gwenview-size;"
msgstr "Tamanho da transferência: &gwenview-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:59
#, xml-text
#| msgid "Estimated disk space required: &gwenview5-buildsize;"
msgid "Estimated disk space required: &gwenview-buildsize;"
msgstr "Espaço em disco estimado exigido: &gwenview-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:64
#, xml-text
#| msgid "Estimated build time: &gwenview5-time;"
msgid "Estimated build time: &gwenview-time;"
msgstr "Tempo de construção estimado: &gwenview-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf5-apps/gwenview.xml:69
#, xml-text
msgid "Gwenview Dependencies"
msgstr "Dependências do &quot;Gwenview&quot;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf5-apps/gwenview.xml:71
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:73
#, xml-text
msgid ""
"<xref linkend=\"exiv2\"/>, <xref linkend=\"kimageannotator\"/>, <xref "
"linkend=\"kf6-frameworks\"/>, and <xref linkend=\"lcms2\"/>"
msgstr ""
"<xref linkend=\"exiv2\"/>, <xref linkend=\"kimageannotator\"/>, <xref "
"linkend=\"kf6-frameworks\"/> e <xref linkend=\"lcms2\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf5-apps/gwenview.xml:79
#, xml-text
msgid "Recommended"
msgstr "Recomendadas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:81
#, xml-text
msgid "<xref linkend=\"libkdcraw\"/>"
msgstr "<xref linkend=\"libkdcraw\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf5-apps/gwenview.xml:84
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:86
#, xml-text
msgid ""
"<xref linkend=\"plasma-activities\"/> and <ulink "
"url=\"https://heasarc.gsfc.nasa.gov/fitsio/fitsio.html\">CFitsio</ulink>"
msgstr ""
"<xref linkend=\"plasma-activities\"/> e <ulink "
"url=\"https://heasarc.gsfc.nasa.gov/fitsio/fitsio.html\">CFitsio</ulink>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/kde/kf5-apps/gwenview.xml:93
#, xml-text
msgid "Installation of Gwenview"
msgstr "Instalação do Gwenview"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:96
#, xml-text
msgid ""
"Install <application>Gwenview</application> by running the following "
"commands:"
msgstr ""
"Instale o &quot;<application>Gwenview</application>&quot; executando os "
"seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/kde/kf5-apps/gwenview.xml:100
#, no-wrap, xml-text
#| msgid ""
#| "<userinput>mkdir build &amp;&amp;\n"
#| "cd    build &amp;&amp;\n"
#| "\n"
#| "cmake -DCMAKE_INSTALL_PREFIX=$KF5_PREFIX \\\n"
#| "      -DCMAKE_BUILD_TYPE=Release         \\\n"
#| "      -DBUILD_TESTING=OFF                \\\n"
#| "      -Wno-dev .. &amp;&amp;\n"
#| "make</userinput>"
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=$KF6_PREFIX \\\n"
"      -D CMAKE_BUILD_TYPE=Release         \\\n"
"      -D BUILD_TESTING=OFF                \\\n"
"      -W no-dev .. &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=$KF6_PREFIX \\\n"
"      -D CMAKE_BUILD_TYPE=Release         \\\n"
"      -D BUILD_TESTING=OFF                \\\n"
"      -W no-dev .. &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:110
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:114
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/kde/kf5-apps/gwenview.xml:117
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/kde/kf5-apps/gwenview.xml:122
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/kde/kf5-apps/gwenview.xml:125
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/kde/kf5-apps/gwenview.xml:126
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/kde/kf5-apps/gwenview.xml:127
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/kde/kf5-apps/gwenview.xml:131
#, xml-text
msgid "gwenview and gwenview_importer"
msgstr "gwenview e gwenview_importer"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/kde/kf5-apps/gwenview.xml:134
#, xml-text
msgid "libgwenviewlib.so and gvpart.so"
msgstr "libgwenviewlib.so e gvpart.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/kde/kf5-apps/gwenview.xml:138
#, xml-text
#| msgid ""
#| "$KF5_PREFIX/share/{kxmlgui5/gvpart,gwenview,doc/HTML/*/gwenview}"
msgid "$KF6_PREFIX/share/{gwenview,doc/HTML/*/gwenview}"
msgstr "$KF6_PREFIX/share/{gwenview,doc/HTML/*/gwenview}"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/kde/kf5-apps/gwenview.xml:144
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/kde/kf5-apps/gwenview.xml:145
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/kde/kf5-apps/gwenview.xml:149
#, xml-text
msgid "<command>gwenview</command>"
msgstr "<command>gwenview</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:152
#, xml-text
msgid "is the KDE image viewer"
msgstr "é o visualizador de imagens do KDE"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/kde/kf5-apps/gwenview.xml:155
#, xml-text
msgid "gwenview"
msgstr "gwenview"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/kde/kf5-apps/gwenview.xml:161
#, xml-text
msgid "<command>gwenview_importer</command>"
msgstr "<command>gwenview_importer</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/kde/kf5-apps/gwenview.xml:164
#, xml-text
msgid "is a Photo importer"
msgstr "é um importador de Fotografias"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/kde/kf5-apps/gwenview.xml:167
#, xml-text
msgid "gwenview_importer"
msgstr "gwenview_importer"

#, xml-text
#~ msgid "dded6bc13018cb19276257158a428be0"
#~ msgstr "dded6bc13018cb19276257158a428be0"

#, xml-text
#~ msgid "6.6 MB"
#~ msgstr "6,6 MB"

#, xml-text
#~ msgid "50 MB"
#~ msgstr "50 MB"

#, xml-text
#~ msgid "Additional Downloads"
#~ msgstr "Transferências Adicionais"

#, xml-text
#~ msgid ""
#~ "Required patch for building against <xref linkend=\"kimageannotator\"/>: "
#~ "<ulink url=\"&patch-root;/gwenview-&kf5apps-version;-build_fixes-1.patch\"/>"
#~ msgstr ""
#~ "Remendo exigido para construir contra <xref linkend=\"kimageannotator\"/>: "
#~ "<ulink url=\"&patch-root;/gwenview-&kf5apps-version;-build_fixes-1.patch\"/>"

#, xml-text
#~ msgid ""
#~ "<ulink url=\"https://download.kde.org/stable/release-service/&kf5apps-"
#~ "version;/src/\">KF5Kipi</ulink>"
#~ msgstr ""
#~ "<ulink url=\"https://download.kde.org/stable/release-service/&kf5apps-"
#~ "version;/src/\">KF5Kipi</ulink>"

#, xml-text
#~ msgid ""
#~ "First, work around an incompatibility with recent changes in "
#~ "kImageAnnotator:"
#~ msgstr ""
#~ "Primeiro, solucione uma incompatibilidade com mudanças recentes no "
#~ "kImageAnnotator:"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput remap=\"pre\">patch -Np1 -i ../gwenview-&kf5apps-"
#~ "version;-build_fixes-1.patch</userinput>"
#~ msgstr ""
#~ "<userinput remap=\"pre\">patch -Np1 -i ../gwenview-&kf5apps-"
#~ "version;-build_fixes-1.patch</userinput>"

#, xml-text
#~ msgid "f57eec9534df6f1f738ae9803552f27b"
#~ msgstr "f57eec9534df6f1f738ae9803552f27b"

#, xml-text
#~ msgid "51 MB"
#~ msgstr "51 MB"

#, xml-text
#~ msgid "08f84c2bdfffe46f06032fe39fb38128"
#~ msgstr "08f84c2bdfffe46f06032fe39fb38128"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/gwenview\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/gwenview\"/>"
