#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-04 19:02+0000\n"
"PO-Revision-Date: 2025-03-08 16:07+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/kde_kf-apps_plasma-activities-stats/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.10\n"

#. type: Content of the plasma-activities-stats-download-http entity
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:7
#, xml-text
msgid ""
"https://download.kde.org/stable/plasma/&plasma-version;/plasma-activities-"
"stats-&plasma-version;.tar.xz"
msgstr ""
"https://download.kde.org/stable/plasma/&plasma-version;/plasma-activities-"
"stats-&plasma-version;.tar.xz"

#. type: Content of the plasma-activities-stats-md5sum entity
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:9
#, xml-text
msgid "b59f54a9c59f5633d4141f9acd6369ce"
msgstr "b59f54a9c59f5633d4141f9acd6369ce"

#. type: Content of the plasma-activities-stats-size entity
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:10
#, xml-text
msgid "84 KB"
msgstr "84 KB"

#. type: Content of the plasma-activities-stats-buildsize entity
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:11
#, xml-text
msgid "3.8 MB"
msgstr "3,8 MB"

#. type: Content of the plasma-activities-stats-time entity
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:12
#, xml-text
msgid "0.2 SBU"
msgstr "0,2 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:15
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:19
#, xml-text
msgid "plasma-activities-stats-&plasma-version;"
msgstr "plasma-activities-stats-&plasma-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:22
#, xml-text
msgid "plasma-activities-stats"
msgstr "plasma-activities-stats"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:26
#, xml-text
msgid "Introduction to plasma-activities-stats-&plasma-version;"
msgstr "Introdução ao plasma-activities-stats-&plasma-version;"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:29
#, xml-text
msgid ""
"The <application>plasma-activities-stats-&plasma-version;</application> "
"library provides access to the usage data collected by the KDE Activities "
"system. It is normally built with <xref linkend=\"plasma-build\"/> but is "
"included here because it is needed for <xref linkend=\"kio-extras\"/>."
msgstr ""
"A biblioteca <application>plasma-activities-stats-&plasma-"
"version;</application> fornece acesso aos dados de uso coletados pelo "
"sistema KDE Activities. Ela normalmente é construída com <xref "
"linkend=\"plasma-build\"/>, mas está incluída aqui porque é necessária para "
"<xref linkend=\"kio-extras\"/>."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:37
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:41
#, xml-text
msgid "Download (HTTP): <ulink url=\"&plasma-activities-stats-download-http;\"/>"
msgstr ""
"Transferência (HTTP): <ulink url=\"&plasma-activities-stats-download-"
"http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:46
#, xml-text
msgid "Download (FTP): <ulink url=\"&plasma-activities-stats-download-ftp;\"/>"
msgstr ""
"Transferência (FTP): <ulink url=\"&plasma-activities-stats-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:51
#, xml-text
msgid "Download MD5 sum: &plasma-activities-stats-md5sum;"
msgstr ""
"Soma de verificação MD5 da transferência: &plasma-activities-stats-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:56
#, xml-text
msgid "Download size: &plasma-activities-stats-size;"
msgstr "Tamanho da transferência: &plasma-activities-stats-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:61
#, xml-text
msgid "Estimated disk space required: &plasma-activities-stats-buildsize;"
msgstr "Espaço em disco estimado exigido: &plasma-activities-stats-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:66
#, xml-text
msgid "Estimated build time: &plasma-activities-stats-time;"
msgstr "Tempo de construção estimado: &plasma-activities-stats-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:71
#, xml-text
msgid "plasma-activities Dependencies"
msgstr "Dependências do plasma-activities"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:73
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:75
#, xml-text
msgid "<xref linkend=\"plasma-activities\"/>"
msgstr "<xref linkend=\"plasma-activities\"/>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:81
#, xml-text
msgid "Installation of plasma-activities-stats"
msgstr "Instalação do plasma-activities-stats"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:84
#, xml-text
msgid ""
"Install <application>plasma-activities-stats</application> by running the "
"following commands:"
msgstr ""
"Instale <application>plasma-activities-stats</application> executando os "
"seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:88
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=$KF6_PREFIX \\\n"
"      -D CMAKE_BUILD_TYPE=Release         \\\n"
"      -D BUILD_TESTING=OFF                \\\n"
"      -W no-dev .. &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=$KF6_PREFIX \\\n"
"      -D CMAKE_BUILD_TYPE=Release         \\\n"
"      -D BUILD_TESTING=OFF                \\\n"
"      -W no-dev .. &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:98
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:102
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:105
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:110
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:113
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:114
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:115
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:119
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:122
#, xml-text
msgid "libPlasmaActivitiesStats.so"
msgstr "libPlasmaActivitiesStats.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/kde/kf-apps/plasma-activities-stats.xml:125
#, xml-text
msgid ""
"$KF6_PREFIX/include/PlasmaActivitiesStats and "
"$KF6_PREFIX/lib/cmake/PlasmaActivitiesStats"
msgstr ""
"$KF6_PREFIX/include/PlasmaActivitiesStats e "
"$KF6_PREFIX/lib/cmake/PlasmaActivitiesStats"

#, xml-text
#~ msgid "c003c98ec0e7dc9be7eb755e83c2f810"
#~ msgstr "c003c98ec0e7dc9be7eb755e83c2f810"

#, xml-text
#~ msgid "adf34da687444af8e1b2816887fd0249"
#~ msgstr "adf34da687444af8e1b2816887fd0249"

#, xml-text
#~ msgid "3.7 MB"
#~ msgstr "3,7 MB"

#, xml-text
#~ msgid "4334541dcd6c6a6b825e94cf2d4ed273"
#~ msgstr "4334541dcd6c6a6b825e94cf2d4ed273"

#, xml-text
#~ msgid "4.2 MB"
#~ msgstr "4,2 MB"

#, xml-text
#~ msgid "0.1 SBU (using parallelism=4)"
#~ msgstr "0,1 UPC (usando paralelismo = 4)"
