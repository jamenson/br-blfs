#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-08 12:22+0000\n"
"PO-Revision-Date: 2024-09-06 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/multimedia_libdriv_mlt/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the mlt-download-http entity
#: blfs-en/multimedia/libdriv/mlt.xml:7
#, xml-text
msgid ""
"https://github.com/mltframework/mlt/releases/download/v&mlt-"
"version;/mlt-&mlt-version;.tar.gz"
msgstr ""
"https://github.com/mltframework/mlt/releases/download/v&mlt-"
"version;/mlt-&mlt-version;.tar.gz"

#. type: Content of the mlt-md5sum entity
#: blfs-en/multimedia/libdriv/mlt.xml:9
#, xml-text
msgid "ea1ac3e412fe182f6cb5e0e92f7eb116"
msgstr "ea1ac3e412fe182f6cb5e0e92f7eb116"

#. type: Content of the mlt-size entity
#: blfs-en/multimedia/libdriv/mlt.xml:10
#, xml-text
msgid "1.6 MB"
msgstr "1,6 MB"

#. type: Content of the mlt-buildsize entity
#: blfs-en/multimedia/libdriv/mlt.xml:11
#, xml-text
msgid "29 MB"
msgstr "29 MB"

#. type: Content of the mlt-time entity
#: blfs-en/multimedia/libdriv/mlt.xml:12
#, xml-text
msgid "0.1 SBU (Using parallelism=4)"
msgstr "0,1 UPC (Usando paralelismo=4)"

#. type: Content of: <sect1><title>
#: blfs-en/multimedia/libdriv/mlt.xml:15 blfs-en/multimedia/libdriv/mlt.xml:19
#, xml-text
msgid "mlt-&mlt-version;"
msgstr "mlt-&mlt-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/multimedia/libdriv/mlt.xml:22
#, xml-text
msgid "mlt"
msgstr "mlt"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/multimedia/libdriv/mlt.xml:26
#, xml-text
msgid "Introduction to mlt"
msgstr "Introdução ao mlt"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/multimedia/libdriv/mlt.xml:29
#, xml-text
msgid ""
"The <application>mlt</application> package is the Media Lovin Toolkit.  It "
"is an open source multimedia framework, designed and developed for "
"television broadcasting. It provides a toolkit for broadcasters, video "
"editors, media players, transcoders, web streamers and many more types of "
"applications."
msgstr ""
"O pacote <application>mlt</application> é o Media Lovin Toolkit. Ele é uma "
"estrutura multimídia de fonte aberto, projetada e desenvolvida para difusão "
"televisiva. Ela fornece um kit de ferramentas para emissoras, editores de "
"vídeo, reprodutores de mídia, transcodificadores, transmissores da web e "
"muitos mais tipos de aplicativos."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/multimedia/libdriv/mlt.xml:38
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:42
#, xml-text
msgid "Download (HTTP): <ulink url=\"&mlt-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&mlt-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:47
#, xml-text
msgid "Download (FTP): <ulink url=\"&mlt-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&mlt-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:52
#, xml-text
msgid "Download MD5 sum: &mlt-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &mlt-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:57
#, xml-text
msgid "Download size: &mlt-size;"
msgstr "Tamanho da transferência: &mlt-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:62
#, xml-text
msgid "Estimated disk space required: &mlt-buildsize;"
msgstr "Espaço em disco estimado exigido: &mlt-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:67
#, xml-text
msgid "Estimated build time: &mlt-time;"
msgstr "Tempo de construção estimado: &mlt-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/multimedia/libdriv/mlt.xml:72
#, xml-text
msgid "mlt Dependencies"
msgstr "Dependências de mlt"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/multimedia/libdriv/mlt.xml:74
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/multimedia/libdriv/mlt.xml:76
#, xml-text
msgid "<xref linkend='frei0r'/> and <xref linkend='qt6'/>"
msgstr "<xref linkend='frei0r'/> e <xref linkend='qt6'/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/multimedia/libdriv/mlt.xml:80
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/multimedia/libdriv/mlt.xml:82
#, xml-text
msgid ""
"<xref linkend='doxygen'/>, <xref linkend='fftw'/>, <xref "
"linkend=\"libexif\"/>, <xref linkend=\"sdl2\"/>, <ulink "
"url=\"https://jackaudio.org\">JACK</ulink>, <ulink "
"url=\"https://www.ipswitch.com/moveit/\">MOVEit</ulink>, <ulink "
"url=\"https://sox.sourceforge.net/\">SoX</ulink>, and <ulink "
"url=\"http://public.hronopik.de/vid.stab/\">vid.stab</ulink>"
msgstr ""
"<xref linkend='doxygen'/>, <xref linkend='fftw'/>, <xref "
"linkend=\"libexif\"/>, <xref linkend=\"sdl2\"/>, <ulink "
"url=\"https://jackaudio.org\">JACK</ulink>, <ulink "
"url=\"https://www.ipswitch.com/moveit/\">MOVEit</ulink>, <ulink "
"url=\"https://sox.sourceforge.net/\">SoX</ulink> e <ulink "
"url=\"http://public.hronopik.de/vid.stab/\">vid.stab</ulink>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/multimedia/libdriv/mlt.xml:97
#, xml-text
msgid "Installation of mlt"
msgstr "Instalação do mlt"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/multimedia/libdriv/mlt.xml:100
#, xml-text
msgid ""
"Install <application>mlt</application> by running the following commands:"
msgstr ""
"Instale <application>mlt</application> executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/multimedia/libdriv/mlt.xml:103
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=/usr \\\n"
"      -D CMAKE_BUILD_TYPE=Release  \\\n"
"      -D MOD_QT=OFF                \\\n"
"      -D MOD_QT6=ON                \\\n"
"      -W no-dev .. &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=/usr \\\n"
"      -D CMAKE_BUILD_TYPE=Release  \\\n"
"      -D MOD_QT=OFF                \\\n"
"      -D MOD_QT6=ON                \\\n"
"      -W no-dev .. &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/multimedia/libdriv/mlt.xml:114
#, xml-text
msgid ""
"This package does not come with a test suite. However a test .mp4 file can "
"be played in a local graphical environment with <command>./out/bin/melt "
"&lt;filename&gt;.mp4</command>."
msgstr ""
"Esse pacote não vem com uma suíte de teste. No entanto, um arquivo .mp4 de "
"teste pode ser reproduzido em um ambiente gráfico local com "
"<command>./out/bin/melt &lt;nome_arquivo&gt;.mp4</command>."

#. type: Content of: <sect1><sect2><note><para>
#: blfs-en/multimedia/libdriv/mlt.xml:121
#, xml-text
msgid ""
"This application uses advanced graphical capabilities.  In some cases, "
"firmware for your specific graphics adaptor may be needed.  See <xref "
"linkend=\"video-firmware\"/> for more information."
msgstr ""
"Esse aplicativo usa recursos gráficos avançados. Em alguns casos, firmware "
"para o teu adaptador gráfico específico possivelmente seja necessário. Veja-"
"se <xref linkend=\"video-firmware\"/> para mais informações."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/multimedia/libdriv/mlt.xml:128
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/multimedia/libdriv/mlt.xml:131
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/multimedia/libdriv/mlt.xml:136
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/multimedia/libdriv/mlt.xml:139
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/multimedia/libdriv/mlt.xml:140
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/multimedia/libdriv/mlt.xml:141
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/multimedia/libdriv/mlt.xml:144
#, xml-text
msgid "melt-7 and melt (symlink to melt-7)"
msgstr "melt-7 e melt (link simbólico para melt-7)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/multimedia/libdriv/mlt.xml:146
#, xml-text
msgid "libmlt-7.so, libmlt++-7.so, and over twenty plugins"
msgstr "libmlt-7.so, libmlt++-7.so e mais que vinte plugins"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/multimedia/libdriv/mlt.xml:149
#, xml-text
msgid ""
"/usr/include/mlt-7, /usr/lib/mlt-7, /usr/lib/cmake/Mlt7, and "
"/usr/share/mlt-7"
msgstr ""
"/usr/include/mlt-7, /usr/lib/mlt-7, /usr/lib/cmake/Mlt7 e /usr/share/mlt-7"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/multimedia/libdriv/mlt.xml:158
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/multimedia/libdriv/mlt.xml:159
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/multimedia/libdriv/mlt.xml:163
#, xml-text
msgid "<command>melt</command>"
msgstr "<command>melt</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/multimedia/libdriv/mlt.xml:166
#, xml-text
msgid "is a test tool for mlt"
msgstr "é uma ferramenta de teste para mlt"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/multimedia/libdriv/mlt.xml:169
#, xml-text
msgid "melt"
msgstr "melt"

#, xml-text
#~ msgid "dab096e465078ac5a1d5e1145c14461c"
#~ msgstr "dab096e465078ac5a1d5e1145c14461c"

#, xml-text
#~ msgid "14b822bc386db5c0fd2c9e5116c04d73"
#~ msgstr "14b822bc386db5c0fd2c9e5116c04d73"

#, xml-text
#~ msgid "08b3604ad071a13fd172f7a18abd610b"
#~ msgstr "08b3604ad071a13fd172f7a18abd610b"

#, xml-text
#~ msgid "28 MB"
#~ msgstr "28 MB"

#, xml-text
#~ msgid "9f747ae1b7388937be6328b4c234431b"
#~ msgstr "9f747ae1b7388937be6328b4c234431b"

#, xml-text
#~ msgid "30 MB"
#~ msgstr "30 MB"

#, xml-text
#~ msgid "MLT-&mlt-version;"
#~ msgstr "MLT-&mlt-version;"

#, xml-text
#~ msgid "MLT"
#~ msgstr "MLT"

#, xml-text
#~ msgid "89daf7bbce3f10fa6dfc1db5ae1d314c"
#~ msgstr "89daf7bbce3f10fa6dfc1db5ae1d314c"

#, xml-text
#~ msgid "1.5 MB"
#~ msgstr "1,5 MB"

#, xml-text
#~ msgid "27 MB"
#~ msgstr "27 MB"

#, xml-text
#~ msgid "0.4 SBU"
#~ msgstr "0,4 UPC"

#, xml-text
#~ msgid "63e863f7653c098ece96ac8f6785fd38"
#~ msgstr "63e863f7653c098ece96ac8f6785fd38"

#, xml-text
#~ msgid "1.4 MB"
#~ msgstr "1,4 MB"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/mlt\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/mlt\"/>"
