# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-22 06:43+0000\n"
"PO-Revision-Date: 2024-08-13 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_sysutils_zip/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the zip-download-http entity
#: blfs-en/general/sysutils/zip.xml:7
#, xml-text
msgid "&sourceforge-dl;/infozip/zip30.tar.gz"
msgstr "&sourceforge-dl;/infozip/zip30.tar.gz"

#. type: Content of the zip-md5sum entity
#: blfs-en/general/sysutils/zip.xml:9
#, xml-text
msgid "7b74551e63f8ee6aab6fbc86676c0d37"
msgstr "7b74551e63f8ee6aab6fbc86676c0d37"

#. type: Content of the zip-size entity
#: blfs-en/general/sysutils/zip.xml:10
#, xml-text
msgid "1.1 MB"
msgstr "1,1 MB"

#. type: Content of the zip-buildsize entity
#: blfs-en/general/sysutils/zip.xml:11
#, xml-text
msgid "6.4 MB"
msgstr "6,4 MB"

#. type: Content of the zip-time entity
#: blfs-en/general/sysutils/zip.xml:12
#, xml-text
msgid "0.1 SBU"
msgstr "0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/general/sysutils/zip.xml:15 blfs-en/general/sysutils/zip.xml:19
#, xml-text
msgid "Zip-&zip-version;"
msgstr "Zip-&zip-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/general/sysutils/zip.xml:22
#, xml-text
msgid "Zip"
msgstr "Zip"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/zip.xml:26
#, xml-text
msgid "Introduction to Zip"
msgstr "Introdução ao \"Zip\""

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/zip.xml:29
#, xml-text
msgid ""
"The <application>Zip</application> package contains "
"<application>Zip</application> utilities. These are useful for compressing "
"files into <filename>ZIP</filename> archives."
msgstr ""
"O pacote <application>Zip</application> contém utilitários "
"<application>Zip</application>. Eles são úteis para comprimir arquivos em "
"arquivamentos <filename>ZIP</filename>."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/sysutils/zip.xml:37
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/zip.xml:41
#, xml-text
msgid "Download (HTTP): <ulink url=\"&zip-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&zip-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/zip.xml:46
#, xml-text
msgid "Download (FTP): <ulink url=\"&zip-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&zip-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/zip.xml:51
#, xml-text
msgid "Download MD5 sum: &zip-md5sum;"
msgstr "Soma de verificação MD5da transferência: &zip-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/zip.xml:56
#, xml-text
msgid "Download size: &zip-size;"
msgstr "Tamanho da transferência: &zip-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/zip.xml:61
#, xml-text
msgid "Estimated disk space required: &zip-buildsize;"
msgstr "Espaço em disco estimado exigido: &zip-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/zip.xml:66
#, xml-text
msgid "Estimated build time: &zip-time;"
msgstr "Tempo de construção estimado: &zip-time;"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/zip.xml:74
#, xml-text
msgid "Installation of Zip"
msgstr "Instalação do \"Zip\""

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/zip.xml:77
#, xml-text
msgid ""
"Install <application>Zip</application> by running the following commands:"
msgstr ""
"Instale <application>Zip</application> executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/sysutils/zip.xml:81
#, no-wrap, xml-text
msgid "<userinput>make -f unix/Makefile generic CC=\"gcc -std=gnu89\"</userinput>"
msgstr ""
"<userinput>make -f unix/Makefile generic CC=\"gcc -std=gnu89\"</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/zip.xml:84
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/zip.xml:88
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/sysutils/zip.xml:91
#, no-wrap, xml-text
msgid ""
"<userinput>make prefix=/usr MANDIR=/usr/share/man/man1 -f unix/Makefile "
"install</userinput>"
msgstr ""
"<userinput>make prefix=/usr MANDIR=/usr/share/man/man1 -f unix/Makefile "
"install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/zip.xml:96
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/zip.xml:99
#, xml-text
msgid ""
"<parameter>CC=\"gcc -std=gnu89\"</parameter>: This parameter overrides the "
"<varname>CC</varname> variable that is set to <command>cc</command> in the "
"<filename>unix/Makefile</filename> file.  On LFS <command>cc</command> is a "
"symlink to <command>gcc</command>, and it uses <option>-std=gnu17</option> "
"(ISO 9899:2017 with GNU extensions) as the default but Zip is a "
"<quote>legacy</quote> package where some grammar constructs are invalid in "
"ISO 9899:1999 and newer standards."
msgstr ""
"<parameter>CC=\"gcc -std=gnu89\"</parameter>: Esse parâmetro substitui a "
"variável <varname>CC</varname> que é configurada como <command>cc</command> "
"no arquivo <filename>unix/Makefile</filename>. No LFS, <command>cc</command>"
" é um link simbólico para <command>gcc</command> e usa "
"<option>-std=gnu17</option> (ISO 9899:2017 com extensões GNU) como padrão, "
"mas Zip é um pacote <quote>legado</quote> onde algumas construções "
"gramaticais são inválidas no ISO 9899:1999 e usos comuns mais recentes."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/zip.xml:110
#, xml-text
msgid ""
"<parameter>prefix=/usr</parameter>: This parameter overrides the "
"<varname>prefix</varname> variable that is set to <filename "
"class='directory'>/usr/local</filename> in the "
"<filename>unix/Makefile</filename> file."
msgstr ""
"<parameter>prefix=/usr</parameter>: Esse parâmetro substitui a variável "
"<varname>prefix</varname> que está configurada como <filename "
"class='directory'>/usr/local</filename> no arquivo "
"<filename>unix/Makefile</filename>."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/zip.xml:119
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/sysutils/zip.xml:122
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/sysutils/zip.xml:123
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/sysutils/zip.xml:124
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/sysutils/zip.xml:127
#, xml-text
msgid "zip, zipcloak, zipnote, and zipsplit"
msgstr "zip, zipcloak, zipnote e zipsplit"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/sysutils/zip.xml:128 blfs-en/general/sysutils/zip.xml:129
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/general/sysutils/zip.xml:134
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/general/sysutils/zip.xml:135
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/sysutils/zip.xml:139
#, xml-text
msgid "<command>zip</command>"
msgstr "<command>zip</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/sysutils/zip.xml:142
#, xml-text
msgid "compresses files into a <filename>ZIP</filename> archive"
msgstr "comprime arquivos em um arquivamento <filename>ZIP</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/sysutils/zip.xml:145
#, xml-text
msgid "zip"
msgstr "zip"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/sysutils/zip.xml:151
#, xml-text
msgid "<command>zipcloak</command>"
msgstr "<command>zipcloak</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/sysutils/zip.xml:154
#, xml-text
msgid "is a utility to encrypt and decrypt a <filename>ZIP</filename> archive"
msgstr ""
"é um utilitário para encriptar e desencriptar um arquivamento "
"<filename>ZIP</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/sysutils/zip.xml:158
#, xml-text
msgid "zipcloak"
msgstr "zipcloak"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/sysutils/zip.xml:164
#, xml-text
msgid "<command>zipnote</command>"
msgstr "<command>zipnote</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/sysutils/zip.xml:167
#, xml-text
msgid "reads or writes comments stored in a <filename>ZIP</filename> file"
msgstr ""
"lê ou escreve comentários armazenados em um arquivo <filename>ZIP</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/sysutils/zip.xml:171
#, xml-text
msgid "zipnote"
msgstr "zipnote"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/sysutils/zip.xml:177
#, xml-text
msgid "<command>zipsplit</command>"
msgstr "<command>zipsplit</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/sysutils/zip.xml:180
#, xml-text
msgid ""
"is a utility to split <filename>ZIP</filename> files into smaller files"
msgstr ""
"é um utilitário para dividir arquivos <filename>ZIP</filename> em arquivos "
"menores"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/sysutils/zip.xml:184
#, xml-text
msgid "zipsplit"
msgstr "zipsplit"

#, xml-text
#~ msgid "First, make the package compatible with gcc-14:"
#~ msgstr "Primeiro, torne o pacote compatível com o gcc-14:"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>sed -i '205 s/CC=gcc/CC=\"gcc -std=gnu89\"/' "
#~ "unix/Makefile</userinput>"
#~ msgstr ""
#~ "<userinput>sed -i '205 s/CC=gcc/CC=\"gcc -std=gnu89\"/' "
#~ "unix/Makefile</userinput>"

#, xml-text
#~ msgid "ftp://ftp.info-zip.org/pub/infozip/src/zip30.tgz"
#~ msgstr "ftp://ftp.info-zip.org/pub/infozip/src/zip30.tgz"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/zip\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/zip\"/>"
