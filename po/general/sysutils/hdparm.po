#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-07 18:59+0000\n"
"PO-Revision-Date: 2023-09-07 18:59+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-11-3/general_sysutils_hdparm/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.18.2\n"

#. type: Content of the hdparm-download-http entity
#: blfs-en/general/sysutils/hdparm.xml:7
#, xml-text
msgid "&sourceforge-dl;/hdparm/hdparm-&hdparm-version;.tar.gz"
msgstr "&sourceforge-dl;/hdparm/hdparm-&hdparm-version;.tar.gz"

#. type: Content of the hdparm-md5sum entity
#: blfs-en/general/sysutils/hdparm.xml:9
#, xml-text
msgid "6d6d039d61ec995b1ec72ddce0b1853b"
msgstr "6d6d039d61ec995b1ec72ddce0b1853b"

#. type: Content of the hdparm-size entity
#: blfs-en/general/sysutils/hdparm.xml:10
#, xml-text
msgid "140 KB"
msgstr "140 KB"

#. type: Content of the hdparm-buildsize entity
#: blfs-en/general/sysutils/hdparm.xml:11
#, xml-text
msgid "1.0 MB"
msgstr "1,0 MB"

#. type: Content of the hdparm-time entity
#: blfs-en/general/sysutils/hdparm.xml:12
#, xml-text
msgid "less than 0.1 SBU"
msgstr "menos que 0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/general/sysutils/hdparm.xml:15
#: blfs-en/general/sysutils/hdparm.xml:19
#, xml-text
msgid "Hdparm-&hdparm-version;"
msgstr "Hdparm-&hdparm-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/general/sysutils/hdparm.xml:22
#, xml-text
msgid "Hdparm"
msgstr "Hdparm"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/hdparm.xml:26
#, xml-text
msgid "Introduction to Hdparm"
msgstr "Introdução ao \"Hdparm\""

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/hdparm.xml:29
#, xml-text
msgid ""
"The <application>Hdparm</application> package contains a utility that is "
"useful for obtaining information about, and controlling ATA/IDE controllers "
"and hard drives. It allows to increase performance and sometimes to increase"
" stability."
msgstr ""
"O pacote <application>Hdparm</application> contém um utilitário que é útil "
"para obter informações e controlar controladores \"ATA\"/\"IDE\" e unidades "
"rígidas. Permite aumentar o desempenho e, às vezes, aumentar a estabilidade."

#. type: Content of: <sect1><sect2><warning><para>
#: blfs-en/general/sysutils/hdparm.xml:39
#, xml-text
msgid ""
"As well as being useful, incorrect usage of "
"<application>Hdparm</application> can destroy your information and in rare "
"cases, drives. Use with caution and make sure you know what you are doing. "
"If in doubt, it is recommended that you leave the default kernel parameters "
"alone."
msgstr ""
"Além de ser útil, o uso incorreto do <application>Hdparm</application> pode "
"destruir suas informações e, em casos raros, unidades. Use com cuidado e "
"certifique-se de saber o que está fazendo. Se em dúvida, [então] é "
"recomendado que você deixe os parâmetros padrão do núcleo em paz."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/sysutils/hdparm.xml:47
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:51
#, xml-text
msgid "Download (HTTP): <ulink url=\"&hdparm-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&hdparm-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:56
#, xml-text
msgid "Download (FTP): <ulink url=\"&hdparm-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&hdparm-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:61
#, xml-text
msgid "Download MD5 sum: &hdparm-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &hdparm-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:66
#, xml-text
msgid "Download size: &hdparm-size;"
msgstr "Tamanho da transferência: &hdparm-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:71
#, xml-text
msgid "Estimated disk space required: &hdparm-buildsize;"
msgstr "Espaço em disco estimado exigido: &hdparm-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:76
#, xml-text
msgid "Estimated build time: &hdparm-time;"
msgstr "Tempo de construção estimado: &hdparm-time;"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/hdparm.xml:84
#, xml-text
msgid "Installation of Hdparm"
msgstr "Instalação do \"Hdparm\""

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/hdparm.xml:87
#, xml-text
msgid ""
"Build <application>Hdparm</application> by running the following command:"
msgstr ""
"Construa o <application>Hdparm</application> executando o seguinte comando:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/sysutils/hdparm.xml:91
#, no-wrap, xml-text
msgid "<userinput>make</userinput>"
msgstr "<userinput>make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/hdparm.xml:94
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/sysutils/hdparm.xml:98
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/sysutils/hdparm.xml:101
#, no-wrap, xml-text
msgid "<userinput>make binprefix=/usr install</userinput>"
msgstr "<userinput>make binprefix=/usr install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/sysutils/hdparm.xml:119
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/sysutils/hdparm.xml:122
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/sysutils/hdparm.xml:123
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/sysutils/hdparm.xml:124
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/sysutils/hdparm.xml:128
#: blfs-en/general/sysutils/hdparm.xml:152
#, xml-text
msgid "hdparm"
msgstr "hdparm"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/sysutils/hdparm.xml:131
#: blfs-en/general/sysutils/hdparm.xml:134
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/general/sysutils/hdparm.xml:140
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/general/sysutils/hdparm.xml:141
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/sysutils/hdparm.xml:145
#, xml-text
msgid "<command>hdparm</command>"
msgstr "<command>hdparm</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/sysutils/hdparm.xml:148
#, xml-text
msgid ""
"provides a command-line interface to various hard disk ioctls supported by "
"the stock Linux ATA/IDE device driver subsystem"
msgstr ""
"fornece uma interface de linha de comando para vários \"ioctls\" de disco "
"rígido suportados pelo subsistema padrão de controlador de dispositivo "
"\"ATA\"/\"IDE\" do Linux"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/hdparm\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/hdparm\"/>"
