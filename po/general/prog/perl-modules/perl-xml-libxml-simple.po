# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-22 06:42+0000\n"
"PO-Revision-Date: 2024-05-15 03:15+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-0/general_prog_perl-modules_perl-xml-libxml-simple/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.4.2\n"

#. type: Content of the my-download-http entity
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:7
#, xml-text
msgid ""
"&perl_authors;/id/M/MA/MARKOV/XML-LibXML-Simple-&XML-LibXML-Simple-"
"version;.tar.gz"
msgstr ""
"&perl_authors;/id/M/MA/MARKOV/XML-LibXML-Simple-&XML-LibXML-Simple-"
"version;.tar.gz"

#. type: Content of the my-md5sum entity
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:8
#, xml-text
msgid "faad5ed26cd83998f6514be199c56c38"
msgstr "faad5ed26cd83998f6514be199c56c38"

#. type: Attribute 'xreflabel' of: <sect2>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:13
#, xml-text
msgid "XML-LibXML-Simple-&XML-LibXML-Simple-version;"
msgstr "XML-LibXML-Simple-&XML-LibXML-Simple-version;"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:15
#, xml-text
msgid "XML::LibXML::Simple-&XML-LibXML-Simple-version;"
msgstr "XML::LibXML::Simple-&XML-LibXML-Simple-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:18
#, xml-text
msgid "XML::LibXML::Simple"
msgstr "XML::LibXML::Simple"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:22
#, xml-text
msgid "Introduction to XML::LibXML::Simple"
msgstr "Introdução ao \"XML::LibXML::Simple\""

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:25
#, xml-text
msgid ""
"The XML::LibXML::Simple module is a rewrite of XML::Simple to use the "
"XML::LibXML parser for XML structures, instead of the plain "
"<application>Perl</application> or SAX parsers."
msgstr ""
"O módulo XML::LibXML::Simple é uma reescrita de XML::Simple para usar o "
"analisador XML::LibXML para estruturas XML, em vez dos analisadores simples "
"<application>Perl</application> ou SAX."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:32
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:36
#, xml-text
msgid "Download (HTTP): <ulink url=\"&my-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&my-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:41
#, xml-text
msgid "Download MD5 sum: &my-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &my-md5sum;"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:46
#, xml-text
msgid "XML::LibXML::Simple Dependencies"
msgstr "Dependências do \"XML::LibXML::Simple\""

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:48
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:50
#, xml-text
msgid "<xref linkend=\"perl-xml-libxml\"/>"
msgstr "<xref linkend=\"perl-xml-libxml\"/>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-modules/perl-xml-libxml-simple.xml:56
#, xml-text
msgid "Installation of XML::LibXML::Simple"
msgstr "Instalação do \"XML::LibXML::Simple\""
