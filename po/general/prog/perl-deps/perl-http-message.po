# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-13 16:19+0000\n"
"PO-Revision-Date: 2024-06-25 03:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/general_prog_perl-deps_perl-http-message/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the my-download-http entity
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:7
#, xml-text
msgid ""
"&metacpan_authors;/O/OA/OALDERS/HTTP-Message-&HTTP-Message-version;.tar.gz"
msgstr ""
"&metacpan_authors;/O/OA/OALDERS/HTTP-Message-&HTTP-Message-version;.tar.gz"

#. type: Content of the my-md5sum entity
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:8
#, xml-text
msgid "01b29ce55d79a774420bc768bb905354"
msgstr "01b29ce55d79a774420bc768bb905354"

#. type: Attribute 'xreflabel' of: <sect2>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:13
#, xml-text
msgid "HTTP-Message-&HTTP-Message-version;"
msgstr "HTTP-Message-&HTTP-Message-version;"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:15
#, xml-text
msgid "HTTP::Message-&HTTP-Message-version;"
msgstr "HTTP::Message-&HTTP-Message-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:18
#, xml-text
msgid "HTTP::Message"
msgstr "HTTP::Message"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:22
#, xml-text
msgid "Introduction to HTTP::Message"
msgstr "Introdução ao HTTP::Message"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:25
#, xml-text
msgid "HTTP::Message provides a base class for HTTP style message objects."
msgstr ""
"\"HTTP::Message\" fornece uma classe base para objetos de mensagem de estilo"
" \"HTTP\"."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:30
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:34
#, xml-text
msgid "Download (HTTP): <ulink url=\"&my-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&my-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:39
#, xml-text
msgid "Download MD5 sum: &my-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &my-md5sum;"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:44
#, xml-text
msgid "HTTP::Message Dependencies"
msgstr "Dependências do HTTP::Message"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:46
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:48
#, xml-text
msgid ""
"<xref linkend=\"perl-clone\"/>, <xref linkend=\"perl-encode-locale\"/>, "
"<xref linkend=\"perl-http-date\"/>, <xref linkend=\"perl-io-html\"/>, <xref "
"linkend=\"perl-lwp-mediatypes\"/>, and <xref linkend=\"perl-uri\"/>"
msgstr ""
"<xref linkend=\"perl-clone\"/>, <xref linkend=\"perl-encode-locale\"/>, "
"<xref linkend=\"perl-http-date\"/>, <xref linkend=\"perl-io-html\"/>, <xref "
"linkend=\"perl-lwp-mediatypes\"/> e <xref linkend=\"perl-uri\"/>"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:56
#, xml-text
msgid "Recommended (required for the test suite)"
msgstr "Recomendadas (exigidas para a suíte de teste)"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:58
#, xml-text
msgid "<xref linkend=\"perl-test-needs\"/> and <xref linkend=\"perl-try-tiny\"/>"
msgstr "<xref linkend=\"perl-test-needs\"/> e <xref linkend=\"perl-try-tiny\"/>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-deps/perl-http-message.xml:65
#, xml-text
msgid "Installation of HTTP::Message"
msgstr "Instalação do HTTP::Message"

#, xml-text
#~ msgid "12a4bf7d993ba7b231df9a24f8bf3ec5"
#~ msgstr "12a4bf7d993ba7b231df9a24f8bf3ec5"

#, xml-text
#~ msgid "86c386bcc85a63c8908e6ae9967b34ee"
#~ msgstr "86c386bcc85a63c8908e6ae9967b34ee"

#, xml-text
#~ msgid "926a077669a7828c5ca39b5cf7735625"
#~ msgstr "926a077669a7828c5ca39b5cf7735625"
