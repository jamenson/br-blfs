# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-04-25 20:21+0000\n"
"PO-Revision-Date: 2024-06-25 03:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/general_prog_perl-deps_perl-sub-info/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the my-download-http entity
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:7
#, xml-text
msgid "&metacpan_authors;/E/EX/EXODIST/Sub-Info-&Sub-Info-version;.tar.gz"
msgstr "&metacpan_authors;/E/EX/EXODIST/Sub-Info-&Sub-Info-version;.tar.gz"

#. type: Content of the my-md5sum entity
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:8
#, xml-text
msgid "335345b534fc0539c894050f7814cbda"
msgstr "335345b534fc0539c894050f7814cbda"

#. type: Attribute 'xreflabel' of: <sect2>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:13
#, xml-text
msgid "Sub-Info-&Sub-Info-version;"
msgstr "Sub-Info-&Sub-Info-version;"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:15
#, xml-text
msgid "Sub::Info-&Sub-Info-version;"
msgstr "Sub::Info-&Sub-Info-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:18
#, xml-text
msgid "Sub::Info"
msgstr "Sub::Info"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:22
#, xml-text
msgid "Introduction to Sub::Info"
msgstr "Introdução ao Sub::Info"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:25
#, xml-text
msgid "Sub::Info is a tool for inspecting subroutines."
msgstr "\"Sub::Info\" é uma ferramenta para inspecionar sub-rotinas."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:30
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:34
#, xml-text
msgid "Download (HTTP): <ulink url=\"&my-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&my-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:39
#, xml-text
msgid "Download MD5 sum: &my-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &my-md5sum;"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:44
#, xml-text
msgid "Sub::Info Dependencies"
msgstr "Dependências do Sub::Info"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:46
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:48
#, xml-text
msgid "<xref linkend=\"perl-importer\"/>"
msgstr "<xref linkend=\"perl-importer\"/>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-deps/perl-sub-info.xml:54
#, xml-text
msgid "Installation of Sub::Info"
msgstr "Instalação do Sub::Info"
