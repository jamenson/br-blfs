# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-18 09:07+0000\n"
"PO-Revision-Date: 2024-06-09 07:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/general_prog_perl-deps_perl-text-glob/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the my-download-http entity
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:7
#, xml-text
msgid "&metacpan_authors;/R/RC/RCLAMP/Text-Glob-&Text-Glob-version;.tar.gz"
msgstr "&metacpan_authors;/R/RC/RCLAMP/Text-Glob-&Text-Glob-version;.tar.gz"

#. type: Content of the my-md5sum entity
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:8
#, xml-text
msgid "d001559c504a2625dd117bd1558f07f7"
msgstr "d001559c504a2625dd117bd1558f07f7"

#. type: Attribute 'xreflabel' of: <sect2>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:13
#, xml-text
msgid "Text-Glob-&Text-Glob-version;"
msgstr "Text-Glob-&Text-Glob-version;"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:15
#, xml-text
msgid "Text::Glob-&Text-Glob-version;"
msgstr "Text::Glob-&Text-Glob-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:18
#, xml-text
msgid "Text::Glob"
msgstr "Text::Glob"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:22
#, xml-text
msgid "Introduction to Text::Glob"
msgstr "Introdução ao Text::Glob"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:25
#, xml-text
msgid ""
"Text::Glob implements <ulink role='man' url='&man;glob.3'>glob(3)</ulink> "
"style matching that can be used to match against text, rather than fetching "
"names from a filesystem."
msgstr ""
"Text::Glob implementa correspondência de estilo <ulink role='man' "
"url='&man;glob.3'>glob(3)</ulink> que pode ser usada para corresponder a "
"texto, em vez de buscar nomes a partir de um sistema de arquivos."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:32
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:36
#, xml-text
msgid "Download (HTTP): <ulink url=\"&my-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&my-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:41
#, xml-text
msgid "Download MD5 sum: &my-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &my-md5sum;"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/perl-deps/perl-text-glob.xml:49
#, xml-text
msgid "Installation of Text::Glob"
msgstr "Instalação do Text::Glob"
