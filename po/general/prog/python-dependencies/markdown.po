#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-02 05:26+0000\n"
"PO-Revision-Date: 2024-10-10 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_prog_python-dependencies_markdown/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the markdown-download-http entity
#: blfs-en/general/prog/python-dependencies/markdown.xml:7
#, xml-text
#| msgid ""
#| "https://files.pythonhosted.org/packages/source/M/Markdown/Markdown-&markdown-"
#| "version;.tar.gz"
msgid ""
"https://files.pythonhosted.org/packages/source/M/Markdown/markdown-&markdown-"
"version;.tar.gz"
msgstr ""
"https://files.pythonhosted.org/packages/source/M/Markdown/markdown-&markdown-"
"version;.tar.gz"

#. type: Content of the markdown-md5sum entity
#: blfs-en/general/prog/python-dependencies/markdown.xml:9
#, xml-text
msgid "&markdown-md5sum;"
msgstr "&markdown-md5sum;"

#. type: Content of the markdown-size entity
#: blfs-en/general/prog/python-dependencies/markdown.xml:10
#, xml-text
msgid "348 KB"
msgstr "348 KB"

#. type: Content of the markdown-buildsize entity
#: blfs-en/general/prog/python-dependencies/markdown.xml:11
#, xml-text
msgid "4.1 MB (add 27 MB for tests)"
msgstr "4,1 MB (adicionar 27 MB para os testes)"

#. type: Content of the markdown-time entity
#: blfs-en/general/prog/python-dependencies/markdown.xml:12
#, xml-text
msgid "less than 0.1 SBU (with tests)"
msgstr "menos que 0,1 UPC (com testes)"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/python-dependencies/markdown.xml:15
#: blfs-en/general/prog/python-dependencies/markdown.xml:17
#, xml-text
msgid "Markdown-&markdown-version;"
msgstr "Markdown-&markdown-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/python-dependencies/markdown.xml:20
#, xml-text
msgid "Markdown"
msgstr "Markdown"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/markdown.xml:24
#, xml-text
msgid "Introduction to Markdown Module"
msgstr "Introdução ao Módulo Markdown"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:27
#, xml-text
msgid ""
"<application>Markdown</application> is a Python parser for John Gruber's "
"Markdown specification."
msgstr ""
"&quot;<application>Markdown</application>&quot; é um analisador "
"&quot;Python&quot; para a especificação &quot;Markdown&quot; de John Gruber."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/markdown.xml:33
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:37
#, xml-text
msgid "Download (HTTP): <ulink url=\"&markdown-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&markdown-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:42
#, xml-text
msgid "Download (FTP): <ulink url=\"&markdown-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&markdown-download-ftp;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:47
#, xml-text
msgid "Download MD5 sum: &markdown-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &markdown-md5sum;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:52
#, xml-text
msgid "Download size: &markdown-size;"
msgstr "Tamanho da transferência: &markdown-size;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:57
#, xml-text
msgid "Estimated disk space required: &markdown-buildsize;"
msgstr "Espaço em disco estimado exigido: &markdown-buildsize;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:62
#, xml-text
msgid "Estimated build time: &markdown-time;"
msgstr "Tempo de construção estimado: &markdown-time;"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/markdown.xml:67
#, xml-text
msgid "Markdown Dependencies"
msgstr "Dependências do Markdown"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/markdown.xml:69
#, xml-text
msgid "Optional (for testing)"
msgstr "Opcionais (para testagem)"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:71
#, xml-text
msgid ""
"<xref linkend=\"pytest\"/>, <xref linkend=\"PyYAML\"/>, and <ulink "
"url=\"https://pypi.org/project/coverage/\">coverage</ulink>"
msgstr ""
"<xref linkend=\"pytest\"/>, <xref linkend=\"PyYAML\"/> e <ulink "
"url=\"https://pypi.org/project/coverage/\">coverage</ulink>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/markdown.xml:80
#, xml-text
msgid "Installation of Markdown"
msgstr "Instalação do Markdown"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:82
#, xml-text
msgid "Build the module:"
msgstr "Construa o módulo:"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:87
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect2><sect3><screen>
#: blfs-en/general/prog/python-dependencies/markdown.xml:90
#, no-wrap, xml-text
msgid "<userinput>&install-wheel; Markdown</userinput>"
msgstr "<userinput>&install-wheel; Markdown</userinput>"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:93
#, xml-text
msgid ""
"Assuming <xref linkend=\"pytest\"/> and <xref linkend='PyYAML'/> are "
"installed and the other optional dependency is not, the installation can be "
"tested with:"
msgstr ""
"Supondo que <xref linkend=\"pytest\"/> e <xref linkend='PyYAML'/> estejam "
"instalados e a outra dependência opcional não esteja, a instalação pode ser "
"testada com:"

#.  no && because of a possible error
#. type: Content of: <sect2><sect3><screen>
#: blfs-en/general/prog/python-dependencies/markdown.xml:98
#, no-wrap, xml-text
msgid ""
"<userinput>python3 -m venv --system-site-packages testenv &amp;&amp;\n"
"source testenv/bin/activate                    &amp;&amp;\n"
"pip3 install coverage                          &amp;&amp;\n"
"python3 /usr/bin/pytest --ignore=tests/test_syntax/extensions/test_md_in_html.py\n"
"deactivate</userinput>"
msgstr ""
"<userinput>python3 -m venv --system-site-packages testenv &amp;&amp;\n"
"source testenv/bin/activate                   &amp;&amp;\n"
"pip3 install coverage                          &amp;&amp;\n"
"python3 /usr/bin/pytest --ignore=tests/test_syntax/extensions/test_md_in_html.py\n"
"deactivate</userinput>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/markdown.xml:110
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/markdown.xml:113
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/markdown.xml:114
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/markdown.xml:115
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of:
#. <sect2><sect3><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/prog/python-dependencies/markdown.xml:118
#: blfs-en/general/prog/python-dependencies/markdown.xml:139
#, xml-text
msgid "markdown_py"
msgstr "markdown_py"

#. type: Content of: <sect2><sect3><segmentedlist><seglistitem><seg>
#: blfs-en/general/prog/python-dependencies/markdown.xml:119
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect2><sect3><segmentedlist><seglistitem><seg>
#: blfs-en/general/prog/python-dependencies/markdown.xml:121
#, xml-text
msgid ""
"/usr/lib/python&python3-majorver;/site-packages/markdown and "
"/usr/lib/python&python3-majorver;/site-packages/Markdown-&markdown-"
"version;.dist-info"
msgstr ""
"/usr/lib/python&python3-majorver;/site-packages/markdown e "
"/usr/lib/python&python3-majorver;/site-packages/Markdown-&markdown-"
"version;.dist-info"

#. type: Content of: <sect2><sect3><variablelist><bridgehead>
#: blfs-en/general/prog/python-dependencies/markdown.xml:128
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect2><sect3><variablelist>
#: blfs-en/general/prog/python-dependencies/markdown.xml:129
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect2><sect3><variablelist><varlistentry><term>
#: blfs-en/general/prog/python-dependencies/markdown.xml:133
#, xml-text
msgid "<command>markdown_py</command>"
msgstr "<command>markdown_py</command>"

#. type: Content of:
#. <sect2><sect3><variablelist><varlistentry><listitem><para>
#: blfs-en/general/prog/python-dependencies/markdown.xml:136
#, xml-text
msgid "converts markdown files to (x)html"
msgstr "converte arquivos &quot;markdown&quot; para &quot;(x)html&quot;"

#, xml-text
#~ msgid "315 KB"
#~ msgstr "315 KB"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>pip3 wheel -w dist --no-build-isolation --no-deps "
#~ "$PWD</userinput>"
#~ msgstr ""
#~ "<userinput>pip3 wheel -w dist --no-build-isolation --no-deps "
#~ "$PWD</userinput>"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>pip3 install --no-index --find-links dist --no-cache-dir --no-"
#~ "user Markdown</userinput>"
#~ msgstr ""
#~ "<userinput>pip3 install --no-index --find-links dist --no-cache-dir --no-"
#~ "user Markdown</userinput>"
