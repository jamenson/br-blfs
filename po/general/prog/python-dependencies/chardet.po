#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-01 03:26+0000\n"
"PO-Revision-Date: 2024-10-10 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_prog_python-dependencies_chardet/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the chardet-download-http entity
#: blfs-en/general/prog/python-dependencies/chardet.xml:7
#, xml-text
msgid ""
"https://files.pythonhosted.org/packages/source/c/chardet/chardet-&chardet-"
"version;.tar.gz"
msgstr ""
"https://files.pythonhosted.org/packages/source/c/chardet/chardet-&chardet-"
"version;.tar.gz"

#. type: Content of the chardet-md5sum entity
#: blfs-en/general/prog/python-dependencies/chardet.xml:9
#, xml-text
msgid "&chardet-md5sum;"
msgstr "&chardet-md5sum;"

#. type: Content of the chardet-size entity
#: blfs-en/general/prog/python-dependencies/chardet.xml:10
#, xml-text
msgid "2 MB"
msgstr "2 MB"

#. type: Content of the chardet-buildsize entity
#: blfs-en/general/prog/python-dependencies/chardet.xml:11
#, xml-text
msgid "12 MB (add 1.1 MB for tests)"
msgstr "12 MB (adicionar 1,1 MB para os testes)"

#. type: Content of the chardet-time entity
#: blfs-en/general/prog/python-dependencies/chardet.xml:12
#, xml-text
msgid "less than 0.1 SBU (0.3 SBU for tests)"
msgstr "menos que 0,1 UPC (0,3 UPC para os testes)"

#. type: Attribute 'xreflabel' of: <sect2>
#: blfs-en/general/prog/python-dependencies/chardet.xml:15
#, xml-text
msgid "chardet-&chardet-version;"
msgstr "chardet-&chardet-version;"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/python-dependencies/chardet.xml:17
#, xml-text
msgid "Chardet-&chardet-version;"
msgstr "Chardet-&chardet-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/python-dependencies/chardet.xml:20
#, xml-text
msgid "chardet"
msgstr "chardet"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/chardet.xml:24
#, xml-text
msgid "Introduction to chardet Module"
msgstr "Introdução ao Módulo chardet"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:27
#, xml-text
msgid ""
"<application>Chardet</application> is a universal character encoding "
"detector."
msgstr ""
"&quot;<application>Chardet</application>&quot; é um detector universal de "
"codificação de caracteres."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/chardet.xml:33
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:37
#, xml-text
msgid "Download (HTTP): <ulink url=\"&chardet-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&chardet-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:42
#, xml-text
msgid "Download (FTP): <ulink url=\"&chardet-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&chardet-download-ftp;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:47
#, xml-text
msgid "Download MD5 sum: &chardet-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &chardet-md5sum;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:52
#, xml-text
msgid "Download size: &chardet-size;"
msgstr "Tamanho da transferência: &chardet-size;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:57
#, xml-text
msgid "Estimated disk space required: &chardet-buildsize;"
msgstr "Espaço em disco estimado exigido: &chardet-buildsize;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:62
#, xml-text
msgid "Estimated build time: &chardet-time;"
msgstr "Tempo de construção estimado: &chardet-time;"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/chardet.xml:67
#, xml-text
msgid "Chardet Dependencies"
msgstr "Dependências do Chardet"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/chardet.xml:69
#, xml-text
msgid "Optional (for testing)"
msgstr "Opcionais (para testagem)"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:71
#, xml-text
msgid "<xref linkend=\"pytest\"/>"
msgstr "<xref linkend=\"pytest\"/>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/chardet.xml:77
#, xml-text
msgid "Installation of Chardet"
msgstr "Instalação do Chardet"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:79
#, xml-text
msgid "Build the module:"
msgstr "Construa o módulo:"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:84
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect2><sect3><screen>
#: blfs-en/general/prog/python-dependencies/chardet.xml:87
#, no-wrap, xml-text
msgid "<userinput>&install-wheel; chardet</userinput>"
msgstr "<userinput>&install-wheel; chardet</userinput>"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:90
#, xml-text
msgid "To test the installation issue <command>pytest</command>."
msgstr "Para testar a instalação, emita <command>pytest</command>."

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/chardet.xml:99
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/chardet.xml:102
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/chardet.xml:103
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/chardet.xml:104
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of:
#. <sect2><sect3><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/prog/python-dependencies/chardet.xml:107
#: blfs-en/general/prog/python-dependencies/chardet.xml:128
#, xml-text
msgid "chardetect"
msgstr "chardetect"

#. type: Content of: <sect2><sect3><segmentedlist><seglistitem><seg>
#: blfs-en/general/prog/python-dependencies/chardet.xml:108
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect2><sect3><segmentedlist><seglistitem><seg>
#: blfs-en/general/prog/python-dependencies/chardet.xml:110
#, xml-text
msgid ""
"/usr/lib/python&python3-majorver;/site-packages/chardet and "
"/usr/lib/python&python3-majorver;/site-packages/chardet-&chardet-"
"version;.dist-info"
msgstr ""
"/usr/lib/python&python3-majorver;/site-packages/chardet e "
"/usr/lib/python&python3-majorver;/site-packages/chardet-&chardet-"
"version;.dist-info"

#. type: Content of: <sect2><sect3><variablelist><bridgehead>
#: blfs-en/general/prog/python-dependencies/chardet.xml:117
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect2><sect3><variablelist>
#: blfs-en/general/prog/python-dependencies/chardet.xml:118
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect2><sect3><variablelist><varlistentry><term>
#: blfs-en/general/prog/python-dependencies/chardet.xml:122
#, xml-text
msgid "<command>chardetect</command>"
msgstr "<command>chardetect</command>"

#. type: Content of:
#. <sect2><sect3><variablelist><varlistentry><listitem><para>
#: blfs-en/general/prog/python-dependencies/chardet.xml:125
#, xml-text
msgid "is a Universal Character Encoding Detector"
msgstr "é um detector universal de codificação de caracteres"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>pip3 wheel -w dist --no-build-isolation --no-deps "
#~ "$PWD</userinput>"
#~ msgstr ""
#~ "<userinput>pip3 wheel -w dist --no-build-isolation --no-deps "
#~ "$PWD</userinput>"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>pip3 install --no-index --find-links dist --no-cache-dir --no-"
#~ "user chardet</userinput>"
#~ msgstr ""
#~ "<userinput>pip3 install --no-index --find-links dist --no-cache-dir --no-"
#~ "user chardet</userinput>"
