#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-02 05:26+0000\n"
"PO-Revision-Date: 2024-10-10 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_prog_python-dependencies_sphinxcontrib-htmlhelp/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the sc-htmlhelp-download-http entity
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:7
#, xml-text
msgid ""
"https://files.pythonhosted.org/packages/source/s/sphinxcontrib-"
"htmlhelp/sphinxcontrib_htmlhelp-&sc-htmlhelp-version;.tar.gz"
msgstr ""
"https://files.pythonhosted.org/packages/source/s/sphinxcontrib-"
"htmlhelp/sphinxcontrib_htmlhelp-&sc-htmlhelp-version;.tar.gz"

#. type: Content of the sc-htmlhelp-md5sum entity
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:9
#, xml-text
msgid "&sc-htmlhelp-md5sum;"
msgstr "&sc-htmlhelp-md5sum;"

#. type: Content of the sc-htmlhelp-size entity
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:10
#, xml-text
msgid "24 KB"
msgstr "24 KB"

#. type: Content of the sc-htmlhelp-buildsize entity
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:11
#, xml-text
msgid "1.2 MB (with tests)"
msgstr "1,2 MB (com testes)"

#. type: Content of the sc-htmlhelp-time entity
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:12
#, xml-text
msgid "less than 0.1 SBU (with tests)"
msgstr "menos que 0,1 UPC (com testes)"

#. type: Attribute 'xreflabel' of: <sect2>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:15
#, xml-text
msgid "sphinxcontrib-htmlhelp-&sc-htmlhelp-version;"
msgstr "sphinxcontrib-htmlhelp-&sc-htmlhelp-version;"

#. type: Content of: <sect2><title>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:17
#, xml-text
msgid "Sphinxcontrib-htmlhelp-&sc-htmlhelp-version;"
msgstr "Sphinxcontrib-htmlhelp-&sc-htmlhelp-version;"

#. type: Content of: <sect2><indexterm><primary>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:20
#, xml-text
msgid "sphinxcontrib-htmlhelp"
msgstr "sphinxcontrib-htmlhelp"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:24
#, xml-text
msgid "Introduction to Sphinxcontrib-htmlhelp Module"
msgstr "Introdução ao Módulo Sphinxcontrib-htmlhelp"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:27
#, xml-text
msgid ""
"The <application>Sphinxcontrib-htmlhelp</application> package is a "
"<application>Sphinx</application> extension which renders HTML help files."
msgstr ""
"O pacote &quot;<application>Sphinxcontrib-htmlhelp</application>&quot; é uma"
" extensão &quot;<application>Sphinx</application>&quot; que renderiza "
"arquivos de ajuda &quot;HTML&quot;."

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:34
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:38
#, xml-text
msgid "Download (HTTP): <ulink url=\"&sc-htmlhelp-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&sc-htmlhelp-download-http;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:43
#, xml-text
msgid "Download (FTP): <ulink url=\"&sc-htmlhelp-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&sc-htmlhelp-download-ftp;\"/>"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:48
#, xml-text
msgid "Download MD5 sum: &sc-htmlhelp-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &sc-htmlhelp-md5sum;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:53
#, xml-text
msgid "Download size: &sc-htmlhelp-size;"
msgstr "Tamanho da transferência: &sc-htmlhelp-size;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:58
#, xml-text
msgid "Estimated disk space required: &sc-htmlhelp-buildsize;"
msgstr "Espaço em disco estimado exigido: &sc-htmlhelp-buildsize;"

#. type: Content of: <sect2><sect3><itemizedlist><listitem><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:63
#, xml-text
msgid "Estimated build time: &sc-htmlhelp-time;"
msgstr "Tempo de construção estimado: &sc-htmlhelp-time;"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:68
#, xml-text
msgid "Sphinxcontrib-htmlhelp Dependencies"
msgstr "Dependências do Sphinxcontrib-htmlhelp"

#. type: Content of: <sect2><sect3><bridgehead>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:70
#, xml-text
msgid "Optional (for testing)"
msgstr "Opcionais (para testagem)"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:72
#, xml-text
msgid ""
"<xref linkend=\"pytest\"/>, <xref linkend=\"sphinx\"/> (circular "
"dependency), and <xref linkend=\"html5lib\"/>"
msgstr ""
"<xref linkend=\"pytest\"/>, <xref linkend=\"sphinx\"/> (dependência "
"circular) e <xref linkend=\"html5lib\"/>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:80
#, xml-text
msgid "Installation of Sphinxcontrib-htmlhelp"
msgstr "Instalação do Sphinxcontrib-htmlhelp"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:82
#, xml-text
msgid "Build the module:"
msgstr "Construa o módulo:"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:87
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect2><sect3><screen>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:90
#, no-wrap, xml-text
msgid "<userinput>&install-wheel; sphinxcontrib-htmlhelp</userinput>"
msgstr "<userinput>&install-wheel; sphinxcontrib-htmlhelp</userinput>"

#. type: Content of: <sect2><sect3><para>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:93
#, xml-text
msgid ""
"If the optional dependencies are installed, the package can be tested with:"
msgstr ""
"Se as dependências opcionais estiverem instaladas, o pacote pode ser testado"
" com:"

#. type: Content of: <sect2><sect3><screen>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:96
#, no-wrap, xml-text
msgid "<userinput>pytest</userinput>"
msgstr "<userinput>pytest</userinput>"

#. type: Content of: <sect2><sect3><title>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:104
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:107
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:108
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect2><sect3><segmentedlist><segtitle>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:109
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect2><sect3><segmentedlist><seglistitem><seg>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:112
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:113
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect2><sect3><segmentedlist><seglistitem><seg>
#: blfs-en/general/prog/python-dependencies/sphinxcontrib-htmlhelp.xml:115
#, xml-text
msgid ""
"/usr/lib/python&python3-majorver;/site-packages/sphinxcontrib and "
"/usr/lib/python&python3-majorver;/site-packages/sphinxcontrib_htmlhelp-&sc-"
"htmlhelp-version;.dist-info"
msgstr ""
"/usr/lib/python&python3-majorver;/site-packages/sphinxcontrib e "
"/usr/lib/python&python3-majorver;/site-packages/sphinxcontrib_htmlhelp-&sc-"
"htmlhelp-version;.dist-info"

#, xml-text
#~ msgid "28 KB"
#~ msgstr "28 KB"

#, xml-text
#~ msgid "3.2 MB (add 26 MB for tests)"
#~ msgstr "3,2 MB (adicionar 26 MB para testes)"

#, xml-text
#~ msgid ""
#~ "Assuming <xref linkend=\"pytest\"/> is installed, but the other optional "
#~ "dependency is not, the installation can be tested with the following "
#~ "commands:"
#~ msgstr ""
#~ "Supondo que &quot;<xref linkend=\"pytest\"/>&quot; esteja instalado, mas a "
#~ "outra dependência opcional não esteja, a instalação pode ser testada com os "
#~ "seguintes comandos:"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>python3 -m venv --system-site-packages testenv   &amp;&amp;\n"
#~ "source testenv/bin/activate                      &amp;&amp;\n"
#~ "pip3 install html5lib                            &amp;&amp;\n"
#~ "python3 /usr/bin/pytest\n"
#~ "deactivate</userinput>"
#~ msgstr ""
#~ "<userinput>python3 -m venv --system-site-packages testenv   &amp;&amp;\n"
#~ "source testenv/bin/activate                      &amp;&amp;\n"
#~ "pip3 install html5lib                            &amp;&amp;\n"
#~ "python3 /usr/bin/pytest\n"
#~ "deactivate</userinput>"

#, no-wrap, xml-text
#~ msgid ""
#~ "<userinput>pip3 wheel -w dist --no-build-isolation --no-deps "
#~ "$PWD</userinput>"
#~ msgstr ""
#~ "<userinput>pip3 wheel -w dist --no-build-isolation --no-deps "
#~ "$PWD</userinput>"

#, xml-text
#~ msgid "27 KB"
#~ msgstr "27 KB"

#, xml-text
#~ msgid ""
#~ "the <command>sed ...</command> command is needed because of a change in "
#~ "<application>Sphinx</application> API for versions greater than 5.0."
#~ msgstr ""
#~ "o comando &quot;<command>sed ...</command>&quot; é necessário devido a uma "
#~ "mudança na &quot;API&quot; &quot;<application>Sphinx</application>&quot; "
#~ "para versões superiores a 5.0."
