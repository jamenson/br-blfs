# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-22 06:44+0000\n"
"PO-Revision-Date: 2024-12-12 18:45+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_genlib_json-glib/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.7.2\n"

#. type: Content of the json-glib-download-http entity
#: blfs-en/general/genlib/json-glib.xml:7
#, xml-text
msgid ""
"&gnome-download-http;/json-glib/1.10/json-glib-&json-glib-version;.tar.xz"
msgstr ""
"&gnome-download-http;/json-glib/1.10/json-glib-&json-glib-version;.tar.xz"

#. type: Content of the json-glib-md5sum entity
#: blfs-en/general/genlib/json-glib.xml:9
#, xml-text
msgid "d4bf13ddd1e6d607d039d39286f9e3d0"
msgstr "d4bf13ddd1e6d607d039d39286f9e3d0"

#. type: Content of the json-glib-size entity
#: blfs-en/general/genlib/json-glib.xml:10
#, xml-text
msgid "248 KB"
msgstr "248 KB"

#. type: Content of the json-glib-buildsize entity
#: blfs-en/general/genlib/json-glib.xml:11
#, xml-text
msgid "14 MB (with tests)"
msgstr "14 MB (com testes)"

#. type: Content of the json-glib-time entity
#: blfs-en/general/genlib/json-glib.xml:12
#, xml-text
msgid "0.1 SBU (with tests)"
msgstr "0,1 UPC (com testes)"

#. type: Content of: <sect1><title>
#: blfs-en/general/genlib/json-glib.xml:15
#: blfs-en/general/genlib/json-glib.xml:19
#, xml-text
msgid "JSON-GLib-&json-glib-version;"
msgstr "JSON-GLib-&json-glib-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/general/genlib/json-glib.xml:22
#, xml-text
msgid "JSON-GLib"
msgstr "JSON-GLib"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-glib.xml:26
#, xml-text
msgid "Introduction to JSON GLib"
msgstr "Introdução ao JSON GLib"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:29
#, xml-text
msgid ""
"The <application>JSON GLib</application> package is a library providing "
"serialization and deserialization support for the JavaScript Object Notation"
" (JSON) format described by RFC 4627."
msgstr ""
"O pacote <application>JSON GLib</application> é uma biblioteca que oferece "
"suporte à serialização e desserialização para o formato JavaScript Object "
"Notation (JSON) descrito pela RFC 4627."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-glib.xml:36
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:40
#, xml-text
msgid "Download (HTTP): <ulink url=\"&json-glib-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&json-glib-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:45
#, xml-text
msgid "Download (FTP): <ulink url=\"&json-glib-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&json-glib-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:50
#, xml-text
msgid "Download MD5 sum: &json-glib-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &json-glib-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:55
#, xml-text
msgid "Download size: &json-glib-size;"
msgstr "Tamanho da transferência: &json-glib-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:60
#, xml-text
msgid "Estimated disk space required: &json-glib-buildsize;"
msgstr "Espaço em disco estimado exigido: &json-glib-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:65
#, xml-text
msgid "Estimated build time: &json-glib-time;"
msgstr "Tempo de construção estimado: &json-glib-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-glib.xml:70
#, xml-text
msgid "JSON-GLib Dependencies"
msgstr "Dependências do JSON-GLib"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-glib.xml:72
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:74
#, xml-text
msgid ""
"<xref linkend=\"glib2\"/> (GObject Introspection required if building GNOME)"
msgstr ""
"<xref linkend=\"glib2\"/> (GObject Introspection exigido se construir GNOME)"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-glib.xml:78
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:80
#, xml-text
msgid "<xref linkend=\"gtk-doc\"/>"
msgstr "<xref linkend=\"gtk-doc\"/>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-glib.xml:86
#, xml-text
msgid "Installation of JSON GLib"
msgstr "Instalação do JSON GLib"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:89
#, xml-text
msgid ""
"Install <application>JSON GLib</application> by running the following "
"commands:"
msgstr ""
"Instale o <application>JSON GLib</application> executando os seguintes "
"comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/json-glib.xml:93
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr --buildtype=release .. &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr --buildtype=release .. &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:100
#, xml-text
msgid ""
"To test the results, issue: <command>ninja test</command>.  One test, node, "
"is known to fail."
msgstr ""
"Para testar os resultados, emita: <command>ninja test</command>. Um teste, "
"node, é conhecido por falhar."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:105
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/json-glib.xml:108
#, no-wrap, xml-text
msgid "<userinput>ninja install</userinput>"
msgstr "<userinput>ninja install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-glib.xml:113
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-glib.xml:119
#, xml-text
msgid ""
"<option>-D gtk_doc=disabled</option>: Add this option if you have <xref "
"role=\"nodep\" linkend=\"gtk-doc\"/> installed and do not wish to generate "
"the API documentation."
msgstr ""
"<option>-D gtk_doc=disabled</option>: Adicione essa opção se você tiver "
"<xref role=\"nodep\" linkend=\"gtk-doc\"/> instalado e não desejar gerar a "
"documentação da API."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-glib.xml:127
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/json-glib.xml:130
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/json-glib.xml:131
#, xml-text
msgid "Installed Library"
msgstr "Biblioteca Instalada"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/json-glib.xml:132
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/json-glib.xml:136
#, xml-text
msgid "json-glib-format and json-glib-validate"
msgstr "json-glib-format e json-glib-validate"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/json-glib.xml:139
#: blfs-en/general/genlib/json-glib.xml:184
#, xml-text
msgid "libjson-glib-1.0.so"
msgstr "libjson-glib-1.0.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/json-glib.xml:142
#, xml-text
msgid ""
"/usr/{include,libexec,share{,/installed-tests}/json-glib-1.0} and "
"/usr/share/gtk-doc/html/json-glib"
msgstr ""
"/usr/{include,libexec,share{,/installed-tests}/json-glib-1.0} e "
"/usr/share/gtk-doc/html/json-glib"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/general/genlib/json-glib.xml:149
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/general/genlib/json-glib.xml:150
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/json-glib.xml:154
#, xml-text
msgid "<command>json-glib-format</command>"
msgstr "<command>json-glib-format</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:157
#, xml-text
msgid "is a simple command line interface to format JSON data"
msgstr "é uma interface de linha de comando simples para formatar dados JSON"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/json-glib.xml:160
#, xml-text
msgid "json-glib-format"
msgstr "json-glib-format"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/json-glib.xml:166
#, xml-text
msgid "<command>json-glib-validate</command>"
msgstr "<command>json-glib-validate</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:169
#, xml-text
msgid "is a simple command line interface to validate JSON data"
msgstr "é uma interface de linha de comando simples para validar dados JSON"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/json-glib.xml:172
#, xml-text
msgid "json-glib-validate"
msgstr "json-glib-validate"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/json-glib.xml:178
#, xml-text
msgid "<filename class=\"libraryfile\">libjson-glib-1.0.so</filename>"
msgstr "<filename class=\"libraryfile\">libjson-glib-1.0.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/json-glib.xml:181
#, xml-text
msgid "contains the <application>JSON GLib</application> API functions"
msgstr "contém as funções da API da <application>JSON GLib</application>"

#, xml-text
#~ msgid "4cff0304873cdc4064da2c8881e72b1b"
#~ msgstr "4cff0304873cdc4064da2c8881e72b1b"

#, xml-text
#~ msgid "f1aac2b8a17fd68646653cc4d8426486"
#~ msgstr "f1aac2b8a17fd68646653cc4d8426486"

#, xml-text
#~ msgid "156 KB"
#~ msgstr "156 KB"

#, xml-text
#~ msgid "<xref linkend=\"glib2\"/>"
#~ msgstr "<xref linkend=\"glib2\"/>"

#, xml-text
#~ msgid "Optional (Required if building GNOME)"
#~ msgstr "Opcionais (Exigidas se construir GNOME)"

#, xml-text
#~ msgid "<xref linkend=\"gobject-introspection\"/>"
#~ msgstr "<xref linkend=\"gobject-introspection\"/>"

#, xml-text
#~ msgid ""
#~ "&gnome-download-ftp;/json-glib/1.6/json-glib-&json-glib-version;.tar.xz"
#~ msgstr ""
#~ "&gnome-download-ftp;/json-glib/1.6/json-glib-&json-glib-version;.tar.xz"

#, xml-text
#~ msgid "9c40fcd8cdbf484dd1704480afefae14"
#~ msgstr "9c40fcd8cdbf484dd1704480afefae14"

#, xml-text
#~ msgid "1.2 MB"
#~ msgstr "1,2 MB"

#, xml-text
#~ msgid "less than 0.1 SBU (with tests)"
#~ msgstr "menos que 0,1 UPC (com testes)"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/json-glib\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/json-glib\"/>"
