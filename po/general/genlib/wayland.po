#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-24 20:00+0000\n"
"PO-Revision-Date: 2024-08-25 16:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_genlib_wayland/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the wayland-download-http entity
#: blfs-en/general/genlib/wayland.xml:7
#, xml-text
msgid ""
"https://gitlab.freedesktop.org/wayland/wayland/-/releases/&wayland-"
"version;/downloads/wayland-&wayland-version;.tar.xz"
msgstr ""
"https://gitlab.freedesktop.org/wayland/wayland/-/releases/&wayland-"
"version;/downloads/wayland-&wayland-version;.tar.xz"

#. type: Content of the wayland-md5sum entity
#: blfs-en/general/genlib/wayland.xml:9
#, xml-text
msgid "23ad991e776ec8cf7e58b34cbd2efa75"
msgstr "23ad991e776ec8cf7e58b34cbd2efa75"

#. type: Content of the wayland-size entity
#: blfs-en/general/genlib/wayland.xml:10
#, xml-text
msgid "236 KB"
msgstr "236 KB"

#. type: Content of the wayland-buildsize entity
#: blfs-en/general/genlib/wayland.xml:11
#, xml-text
msgid "6.8 MB (with tests)"
msgstr "6,8 MB (com testes)"

#. type: Content of the wayland-time entity
#: blfs-en/general/genlib/wayland.xml:12
#, xml-text
msgid "0.1 SBU (with tests)"
msgstr "0,1 UPC (com testes)"

#. type: Content of: <sect1><title>
#: blfs-en/general/genlib/wayland.xml:15 blfs-en/general/genlib/wayland.xml:19
#, xml-text
msgid "Wayland-&wayland-version;"
msgstr "Wayland-&wayland-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/general/genlib/wayland.xml:22
#, xml-text
msgid "Wayland"
msgstr "Wayland"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/wayland.xml:26
#, xml-text
msgid "Introduction to Wayland"
msgstr "Introdução ao Wayland"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:29
#, xml-text
msgid ""
"<application>Wayland</application> is a project to define a protocol for a "
"compositor to talk to its clients as well as a library implementation of the"
" protocol."
msgstr ""
"<application>Wayland</application> é um projeto para definir um protocolo "
"para um compositor falar com os clientes dele, bem como uma biblioteca de "
"implementação do protocolo."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/wayland.xml:36
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/wayland.xml:40
#, xml-text
msgid "Download (HTTP): <ulink url=\"&wayland-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&wayland-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/wayland.xml:45
#, xml-text
msgid "Download (FTP): <ulink url=\"&wayland-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&wayland-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/wayland.xml:50
#, xml-text
msgid "Download MD5 sum: &wayland-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &wayland-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/wayland.xml:55
#, xml-text
msgid "Download size: &wayland-size;"
msgstr "Tamanho da transferência: &wayland-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/wayland.xml:60
#, xml-text
msgid "Estimated disk space required: &wayland-buildsize;"
msgstr "Espaço em disco estimado exigido: &wayland-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/wayland.xml:65
#, xml-text
msgid "Estimated build time: &wayland-time;"
msgstr "Tempo de construção estimado: &wayland-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/wayland.xml:70
#, xml-text
msgid "Wayland Dependencies"
msgstr "Dependências de Wayland"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/wayland.xml:72
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:74
#, xml-text
msgid "<xref linkend=\"libxml2\"/>"
msgstr "<xref linkend=\"libxml2\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/wayland.xml:77
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:79
#, xml-text
msgid ""
"<xref linkend=\"doxygen\"/>, <xref linkend=\"graphviz\"/> and <xref "
"linkend=\"xmlto\"/> (to build the API documentation) and <xref "
"linkend=\"DocBook\"/>, <xref linkend=\"docbook-xsl\"/> and <xref "
"linkend=\"libxslt\"/> (to build the manual pages)"
msgstr ""
"<xref linkend=\"doxygen\"/>, <xref linkend=\"graphviz\"/> e <xref "
"linkend=\"xmlto\"/> (para construir a documentação da API) e <xref "
"linkend=\"DocBook\"/>, <xref linkend=\"docbook-xsl\"/> e <xref "
"linkend=\"libxslt\"/> (para construir as páginas de manual)"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/wayland.xml:90
#, xml-text
msgid "Installation of Wayland"
msgstr "Instalação do Wayland"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:93
#, xml-text
msgid ""
"Install <application>Wayland</application> by running the following "
"commands:"
msgstr ""
"Instale <application>Wayland</application> executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/wayland.xml:97
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup ..            \\\n"
"      --prefix=/usr       \\\n"
"      --buildtype=release \\\n"
"      -D documentation=false &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup ..            \\\n"
"      --prefix=/usr       \\\n"
"      --buildtype=release \\\n"
"      -D documentation=false &amp;&amp;\n"
"ninja</userinput>"

#.  If XDG_RUNTIME_DIR is set but the value is a non-exist path,
#.            19 tests will fail.  Unfortunately BLFS /etc/profile sets
#.            XDG_RUNTIME_DIR but does not create it.  For now simply unset
#.            the variable.
#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:111
#, xml-text
msgid ""
"To test the results, issue: <command>env -u XDG_RUNTIME_DIR ninja "
"test</command>."
msgstr ""
"Para testar os resultados, emita: <command>env -u XDG_RUNTIME_DIR ninja "
"test</command>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:116
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/wayland.xml:119
#, no-wrap, xml-text
msgid "<userinput>ninja install</userinput>"
msgstr "<userinput>ninja install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/wayland.xml:124
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/wayland.xml:127
#, xml-text
msgid ""
"<parameter>-D documentation=false</parameter>: This switch is used to "
"disable building of the API documentation. Remove it if you have installed "
"optional dependencies."
msgstr ""
"<parameter>-D documentation=false</parameter>: Essa chave é usada para "
"desabilitar a construção da documentação da API. Remova-a se tiver instalado"
" as dependências opcionais."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/wayland.xml:135
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/wayland.xml:138
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/wayland.xml:139
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/wayland.xml:140
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/wayland.xml:144
#: blfs-en/general/genlib/wayland.xml:171
#, xml-text
msgid "wayland-scanner"
msgstr "wayland-scanner"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/wayland.xml:147
#, xml-text
msgid ""
"libwayland-client.so, libwayland-cursor.so, libwayland-egl.so, and "
"libwayland-server.so"
msgstr ""
"libwayland-client.so, libwayland-cursor.so, libwayland-egl.so e libwayland-"
"server.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/wayland.xml:153
#, xml-text
msgid "/usr/share/wayland"
msgstr "/usr/share/wayland"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/general/genlib/wayland.xml:159
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/general/genlib/wayland.xml:160
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/wayland.xml:164
#, xml-text
msgid "<command>wayland-scanner</command>"
msgstr "<command>wayland-scanner</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/wayland.xml:167
#, xml-text
msgid ""
"is a tool to generate proxy methods in wayland-client-protocol.h and "
"wayland-server-protocol.h"
msgstr ""
"é uma ferramenta para gerar métodos de proxy em wayland-client-protocol.h e "
"wayland-server-protocol.h"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/wayland.xml:177
#, xml-text
msgid "<filename class=\"libraryfile\">libwayland-client.so</filename>"
msgstr "<filename class=\"libraryfile\">libwayland-client.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/wayland.xml:180
#, xml-text
msgid ""
"contains API functions for writing <application>Wayland</application> "
"applications"
msgstr ""
"contém funções de API para escrever aplicativos "
"<application>Wayland</application>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/wayland.xml:184
#, xml-text
msgid "libwayland-client.so"
msgstr "libwayland-client.so"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/wayland.xml:190
#, xml-text
msgid "<filename class=\"libraryfile\">libwayland-cursor.so</filename>"
msgstr "<filename class=\"libraryfile\">libwayland-cursor.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/wayland.xml:193
#, xml-text
msgid ""
"contains API functions for managing cursors in "
"<application>Wayland</application> applications"
msgstr ""
"contém funções de API para gerenciar cursores em aplicativos "
"<application>Wayland</application>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/wayland.xml:197
#, xml-text
msgid "libwayland-cursor.so"
msgstr "libwayland-cursor.so"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/wayland.xml:203
#, xml-text
msgid "<filename class=\"libraryfile\">libwayland-egl.so</filename>"
msgstr "<filename class=\"libraryfile\">libwayland-egl.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/wayland.xml:206
#, xml-text
msgid ""
"contains API functions for handling OpenGL calls in "
"<application>Wayland</application> applications"
msgstr ""
"contém funções de API para lidar com chamadas OpenGL em aplicativos "
"<application>Wayland</application>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/wayland.xml:210
#, xml-text
msgid "libwayland-egl.so"
msgstr "libwayland-egl.so"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/wayland.xml:216
#, xml-text
msgid "<filename class=\"libraryfile\">libwayland-server.so</filename>"
msgstr "<filename class=\"libraryfile\">libwayland-server.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/wayland.xml:219
#, xml-text
msgid ""
"contains API functions for writing <application>Wayland</application> "
"compositors"
msgstr ""
"contém funções de API para escrever compositores "
"<application>Wayland</application>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/wayland.xml:223
#, xml-text
msgid "libwayland-server.so"
msgstr "libwayland-server.so"

#, xml-text
#~ msgid "7410ab549e3928fce9381455b17b0803"
#~ msgstr "7410ab549e3928fce9381455b17b0803"

#, xml-text
#~ msgid "228 KB"
#~ msgstr "228 KB"

#, xml-text
#~ msgid "f2653a2293bcd882d756c6a83d278903"
#~ msgstr "f2653a2293bcd882d756c6a83d278903"

#, xml-text
#~ msgid "224 KB"
#~ msgstr "224 KB"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/wayland\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/wayland\"/>"
