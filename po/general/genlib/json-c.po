#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-24 20:00+0000\n"
"PO-Revision-Date: 2024-07-10 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/general_genlib_json-c/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the json-c-download-http entity
#: blfs-en/general/genlib/json-c.xml:7
#, xml-text
msgid ""
"https://s3.amazonaws.com/json-c_releases/releases/json-c-&json-c-"
"version;.tar.gz"
msgstr ""
"https://s3.amazonaws.com/json-c_releases/releases/json-c-&json-c-"
"version;.tar.gz"

#. type: Content of the json-c-md5sum entity
#: blfs-en/general/genlib/json-c.xml:9
#, xml-text
msgid "e6593766de7d8aa6e3a7e67ebf1e522f"
msgstr "e6593766de7d8aa6e3a7e67ebf1e522f"

#. type: Content of the json-c-size entity
#: blfs-en/general/genlib/json-c.xml:10
#, xml-text
msgid "396 KB"
msgstr "396 KB"

#. type: Content of the json-c-buildsize entity
#: blfs-en/general/genlib/json-c.xml:11
#, xml-text
msgid "7.9 MB"
msgstr "7,9 MB"

#. type: Content of the json-c-time entity
#: blfs-en/general/genlib/json-c.xml:12
#, xml-text
msgid "0.2 SBU (with tests)"
msgstr "0,2 UPC (com testes)"

#. type: Content of: <sect1><title>
#: blfs-en/general/genlib/json-c.xml:15 blfs-en/general/genlib/json-c.xml:19
#, xml-text
msgid "JSON-C-&json-c-version;"
msgstr "JSON-C-&json-c-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/general/genlib/json-c.xml:22
#, xml-text
msgid "JSON-C"
msgstr "JSON-C"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-c.xml:26
#, xml-text
msgid "Introduction to JSON-C"
msgstr "Introdução ao JSON-C"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:29
#, xml-text
msgid ""
"The <application>JSON-C</application> implements a reference counting object"
" model that allows you to easily construct JSON objects in C, output them as"
" JSON formatted strings and parse JSON formatted strings back into the C "
"representation of JSON objects."
msgstr ""
"O <application>JSON-C</application> implementa um modelo de objeto de "
"contagem de referência que permite construir facilmente objetos JSON em C, "
"produzi-los como sequências de caracteres formatadas JSON e analisar "
"sequências de caracteres formatadas JSON de volta na representação C de "
"objetos JSON."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-c.xml:37
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-c.xml:41
#, xml-text
msgid "Download (HTTP): <ulink url=\"&json-c-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&json-c-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-c.xml:46
#, xml-text
msgid "Download (FTP): <ulink url=\"&json-c-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&json-c-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-c.xml:51
#, xml-text
msgid "Download MD5 sum: &json-c-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &json-c-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-c.xml:56
#, xml-text
msgid "Download size: &json-c-size;"
msgstr "Tamanho da transferência: &json-c-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-c.xml:61
#, xml-text
msgid "Estimated disk space required: &json-c-buildsize;"
msgstr "Espaço em disco estimado exigido: &json-c-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/json-c.xml:66
#, xml-text
msgid "Estimated build time: &json-c-time;"
msgstr "Tempo de construção estimado: &json-c-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-c.xml:71
#, xml-text
msgid "JSON-C Dependencies"
msgstr "Dependências do JSON-C"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-c.xml:73
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:75
#, xml-text
msgid "<xref linkend=\"cmake\"/>"
msgstr "<xref linkend=\"cmake\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/json-c.xml:78
#, xml-text
msgid "Optional (for documentation)"
msgstr "Opcionais (para documentação)"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:80
#, xml-text
msgid "<xref linkend=\"doxygen\"/> and <xref linkend=\"graphviz\"/> (for dot tool)"
msgstr ""
"<xref linkend=\"doxygen\"/> e <xref linkend=\"graphviz\"/> (para ferramenta "
"dot)"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-c.xml:87
#, xml-text
msgid "Installation of JSON-C"
msgstr "Instalação do JSON-C"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:96
#, xml-text
msgid ""
"Install <application>JSON-C</application> by running the following commands:"
msgstr ""
"Instale <application>JSON-C</application> executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/json-c.xml:100
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=/usr \\\n"
"      -D CMAKE_BUILD_TYPE=Release  \\\n"
"      -D BUILD_STATIC_LIBS=OFF     \\\n"
"      .. &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"cmake -D CMAKE_INSTALL_PREFIX=/usr \\\n"
"      -D CMAKE_BUILD_TYPE=Release  \\\n"
"      -D BUILD_STATIC_LIBS=OFF     \\\n"
"      .. &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:110
#, xml-text
msgid ""
"If you have installed <xref linkend=\"doxygen\" role=\"nodep\"/> and <xref "
"linkend=\"graphviz\" role=\"nodep\"/>, you can build the documentation by "
"running the following command:"
msgstr ""
"Se tiver instalado <xref linkend=\"doxygen\" role=\"nodep\"/> e <xref "
"linkend=\"graphviz\" role=\"nodep\"/>, você consegue construir a "
"documentação executando o seguinte comando:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/json-c.xml:115
#, no-wrap, xml-text
msgid "<userinput>doxygen doc/Doxyfile</userinput>"
msgstr "<userinput>doxygen doc/Doxyfile</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:118
#, xml-text
msgid "To test the results, issue: <command>make test</command>."
msgstr "Para testar os resultados, emita: <command>make test</command>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:122
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/json-c.xml:125
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:128
#, xml-text
msgid ""
"If you built the documentation, install it by running the following commands"
" as the &root; user:"
msgstr ""
"Se você construiu a documentação, instale-a executando os seguintes comandos"
" como o(a) usuário(a) &root;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/json-c.xml:132
#, no-wrap, xml-text
msgid ""
"<userinput>install -d -vm755 /usr/share/doc/json-c-&json-c-version; &amp;&amp;\n"
"install -v -m644 doc/html/* /usr/share/doc/json-c-&json-c-version;</userinput>"
msgstr ""
"<userinput>install -d -vm755 /usr/share/doc/json-c-&json-c-version; &amp;&amp;\n"
"install -v -m644 doc/html/* /usr/share/doc/json-c-&json-c-version;</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-c.xml:138
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/json-c.xml:141
#, xml-text
msgid ""
"<parameter>-D CMAKE_BUILD_TYPE=Release</parameter>: This switch is used to "
"apply a higher level of compiler optimizations."
msgstr ""
"<parameter>-D CMAKE_BUILD_TYPE=Release</parameter>: Essa chave é usada para "
"aplicar um nível mais alto de otimização à compilação."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/json-c.xml:148
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/json-c.xml:151
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/json-c.xml:152
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/json-c.xml:153
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/json-c.xml:157
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/json-c.xml:160 blfs-en/general/genlib/json-c.xml:180
#, xml-text
msgid "libjson-c.so"
msgstr "libjson-c.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/json-c.xml:163
#, xml-text
msgid "/usr/include/json-c"
msgstr "/usr/include/json-c"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/general/genlib/json-c.xml:169
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/general/genlib/json-c.xml:170
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/json-c.xml:174
#, xml-text
msgid "<filename class=\"libraryfile\">libjson-c.so</filename>"
msgstr "<filename class=\"libraryfile\">libjson-c.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/json-c.xml:177
#, xml-text
msgid "contains the <application>JSON-C</application> API functions"
msgstr "contém as funções da API <application>JSON-C</application>"

#, xml-text
#~ msgid "bad8f5e91b7b2563ee2d507054c70eb2"
#~ msgstr "bad8f5e91b7b2563ee2d507054c70eb2"

#, xml-text
#~ msgid "384 KB"
#~ msgstr "384 KB"

#, xml-text
#~ msgid "8110782cb2a996da5517f1f27a4bed8e"
#~ msgstr "8110782cb2a996da5517f1f27a4bed8e"

#, xml-text
#~ msgid "7.7 MB"
#~ msgstr "7,7 MB"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/json-c\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/json-c\"/>"
