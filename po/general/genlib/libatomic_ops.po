#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-24 20:00+0000\n"
"PO-Revision-Date: 2024-06-11 15:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/general_genlib_libatomic_ops/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the libatomic_ops-download-http entity
#: blfs-en/general/genlib/libatomic_ops.xml:7
#, xml-text
msgid ""
"https://github.com/ivmai/libatomic_ops/releases/download/v&libatomic_ops-"
"version;/libatomic_ops-&libatomic_ops-version;.tar.gz"
msgstr ""
"https://github.com/ivmai/libatomic_ops/releases/download/v&libatomic_ops-"
"version;/libatomic_ops-&libatomic_ops-version;.tar.gz"

#. type: Content of the libatomic_ops-md5sum entity
#: blfs-en/general/genlib/libatomic_ops.xml:9
#, xml-text
msgid "d07b3d8369d7f9efdca59f7501dd1117"
msgstr "d07b3d8369d7f9efdca59f7501dd1117"

#. type: Content of the libatomic_ops-size entity
#: blfs-en/general/genlib/libatomic_ops.xml:10
#, xml-text
msgid "516 KB"
msgstr "516 KB"

#. type: Content of the libatomic_ops-buildsize entity
#: blfs-en/general/genlib/libatomic_ops.xml:11
#, xml-text
msgid "6.8 MB (with tests)"
msgstr "6,8 MB (com testes)"

#. type: Content of the libatomic_ops-time entity
#: blfs-en/general/genlib/libatomic_ops.xml:12
#, xml-text
msgid "0.2 SBU (with tests)"
msgstr "0,2 UPC (com testes)"

#. type: Content of: <sect1><title>
#: blfs-en/general/genlib/libatomic_ops.xml:15
#: blfs-en/general/genlib/libatomic_ops.xml:19
#, xml-text
msgid "libatomic_ops-&libatomic_ops-version;"
msgstr "libatomic_ops-&libatomic_ops-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/general/genlib/libatomic_ops.xml:22
#, xml-text
msgid "libatomic_ops"
msgstr "libatomic_ops"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/libatomic_ops.xml:26
#, xml-text
msgid "Introduction to libatomic_ops"
msgstr "Introdução ao libatomic_ops"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/libatomic_ops.xml:29
#, xml-text
msgid ""
"<application>libatomic_ops</application> provides implementations for atomic"
" memory update operations on a number of architectures. This allows direct "
"use of these in reasonably portable code. Unlike earlier similar packages, "
"this one explicitly considers memory barrier semantics, and allows the "
"construction of code that involves minimum overhead across a variety of "
"architectures."
msgstr ""
"<application>libatomic_ops</application> fornece implementações para "
"operações atômicas de atualização de memória em várias arquiteturas. Isso "
"permite o uso direto delas em código razoavelmente portátil. Ao contrário de"
" pacotes semelhantes anteriores, esse considera explicitamente a semântica "
"de barreira de memória e permite a construção de código que envolve "
"sobrecarga mínima ao longo de uma variedade de arquiteturas."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/general/genlib/libatomic_ops.xml:39
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:43
#, xml-text
msgid "Download (HTTP): <ulink url=\"&libatomic_ops-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&libatomic_ops-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:48
#, xml-text
msgid "Download (FTP): <ulink url=\"&libatomic_ops-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&libatomic_ops-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:53
#, xml-text
msgid "Download MD5 sum: &libatomic_ops-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &libatomic_ops-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:58
#, xml-text
msgid "Download size: &libatomic_ops-size;"
msgstr "Tamanho da transferência: &libatomic_ops-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:63
#, xml-text
msgid "Estimated disk space required: &libatomic_ops-buildsize;"
msgstr "Espaço em disco estimado exigido: &libatomic_ops-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:68
#, xml-text
msgid "Estimated build time: &libatomic_ops-time;"
msgstr "Tempo de construção estimado: &libatomic_ops-time;"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/libatomic_ops.xml:76
#, xml-text
msgid "Installation of libatomic_ops"
msgstr "Instalação do libatomic_ops"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/libatomic_ops.xml:79
#, xml-text
msgid ""
"Install <application>libatomic_ops</application> by running the following "
"commands:"
msgstr ""
"Instale <application>libatomic_ops</application> executando os seguintes "
"comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/libatomic_ops.xml:83
#, no-wrap, xml-text
msgid ""
"<userinput>./configure --prefix=/usr    \\\n"
"            --enable-shared  \\\n"
"            --disable-static \\\n"
"            --docdir=/usr/share/doc/libatomic_ops-&libatomic_ops-version; &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>./configure --prefix=/usr  \\\n"
"            --enable-shared  \\\n"
"            --disable-static \\\n"
"            --docdir=/usr/share/doc/libatomic_ops-&libatomic_ops-version; &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/libatomic_ops.xml:90
#, xml-text
msgid "To check the results, issue <command>make check</command>."
msgstr "Para verificar os resultados, emita <command>make check</command>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/libatomic_ops.xml:94
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/general/genlib/libatomic_ops.xml:97
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/libatomic_ops.xml:102
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/general/genlib/libatomic_ops.xml:105
#, xml-text
msgid ""
"<parameter>--enable-shared</parameter>: This switch enables building of the "
"<filename class=\"libraryfile\">libatomic_ops</filename> shared libraries."
msgstr ""
"<parameter>--enable-shared</parameter>: Essa chave habilita construir as "
"bibliotecas compartilhadas <filename "
"class=\"libraryfile\">libatomic_ops</filename>."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/general/genlib/libatomic_ops.xml:115
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/libatomic_ops.xml:118
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/libatomic_ops.xml:119
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/general/genlib/libatomic_ops.xml:120
#, xml-text
msgid "Installed Directory"
msgstr "Diretório Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/libatomic_ops.xml:124
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/libatomic_ops.xml:127
#, xml-text
msgid "libatomic_ops.so and libatomic_ops_gpl.so"
msgstr "libatomic_ops.so e libatomic_ops_gpl.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/general/genlib/libatomic_ops.xml:130
#, xml-text
msgid ""
"/usr/include/libatomic_ops and /usr/share/doc/libatomic_ops-&libatomic_ops-"
"version;"
msgstr ""
"/usr/include/libatomic_ops e /usr/share/doc/libatomic_ops-&libatomic_ops-"
"version;"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/general/genlib/libatomic_ops.xml:137
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/general/genlib/libatomic_ops.xml:138
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/general/genlib/libatomic_ops.xml:142
#, xml-text
msgid "<filename class=\"libraryfile\">libatomic_ops.so</filename>"
msgstr "<filename class=\"libraryfile\">libatomic_ops.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/general/genlib/libatomic_ops.xml:145
#, xml-text
msgid "contains functions for atomic memory operations"
msgstr "contém funções para operações atômicas de memória"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/general/genlib/libatomic_ops.xml:148
#, xml-text
msgid "libatomic_ops.so"
msgstr "libatomic_ops.so"

#, xml-text
#~ msgid "a7e51e8041c3e60c298c037b2789c3fa"
#~ msgstr "a7e51e8041c3e60c298c037b2789c3fa"

#, xml-text
#~ msgid "ee8251f5091b7938d18be4dda843a515"
#~ msgstr "ee8251f5091b7938d18be4dda843a515"

#, xml-text
#~ msgid "492 KB"
#~ msgstr "492 KB"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/libatomic_ops\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/libatomic_ops\"/>"
