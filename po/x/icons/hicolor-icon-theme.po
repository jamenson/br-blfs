#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-23 21:59+0000\n"
"PO-Revision-Date: 2024-06-24 01:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/x_icons_hicolor-icon-theme/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the hicolor-icon-theme-download-http entity
#: blfs-en/x/icons/hicolor-icon-theme.xml:7
#, xml-text
msgid ""
"https://icon-theme.freedesktop.org/releases/hicolor-icon-theme-&hicolor-"
"icon-theme-version;.tar.xz"
msgstr ""
"https://icon-theme.freedesktop.org/releases/hicolor-icon-theme-&hicolor-"
"icon-theme-version;.tar.xz"

#. type: Content of the hicolor-icon-theme-md5sum entity
#: blfs-en/x/icons/hicolor-icon-theme.xml:9
#, xml-text
msgid "ef14f3af03bcde9ed134aad626bdbaad"
msgstr "ef14f3af03bcde9ed134aad626bdbaad"

#. type: Content of the hicolor-icon-theme-size entity
#: blfs-en/x/icons/hicolor-icon-theme.xml:10
#, xml-text
msgid "32 KB"
msgstr "32 KB"

#. type: Content of the hicolor-icon-theme-buildsize entity
#: blfs-en/x/icons/hicolor-icon-theme.xml:11
#, xml-text
msgid "644 KB"
msgstr "644 KB"

#. type: Content of the hicolor-icon-theme-time entity
#: blfs-en/x/icons/hicolor-icon-theme.xml:12
#, xml-text
msgid "less than 0.1 SBU"
msgstr "menos que 0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/x/icons/hicolor-icon-theme.xml:16
#: blfs-en/x/icons/hicolor-icon-theme.xml:20
#, xml-text
msgid "hicolor-icon-theme-&hicolor-icon-theme-version;"
msgstr "hicolor-icon-theme-&hicolor-icon-theme-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/x/icons/hicolor-icon-theme.xml:23
#, xml-text
msgid "Hicolor-icon-theme"
msgstr "Hicolor-icon-theme"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/icons/hicolor-icon-theme.xml:27
#, xml-text
msgid "Introduction to hicolor-icon-theme"
msgstr "Introdução ao hicolor-icon-theme"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:30
#, xml-text
msgid ""
"The <application>hicolor-icon-theme</application> package contains a default"
" fallback theme for implementations of the icon theme specification."
msgstr ""
"O pacote &quot;<application>hicolor-icon-theme</application>&quot; contém um"
" tema substituto padrão para implementações da especificação do tema de "
"ícones."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/icons/hicolor-icon-theme.xml:37
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:41
#, xml-text
msgid "Download (HTTP): <ulink url=\"&hicolor-icon-theme-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&hicolor-icon-theme-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:46
#, xml-text
msgid "Download (FTP): <ulink url=\"&hicolor-icon-theme-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&hicolor-icon-theme-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:51
#, xml-text
msgid "Download MD5 sum: &hicolor-icon-theme-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &hicolor-icon-theme-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:56
#, xml-text
msgid "Download size: &hicolor-icon-theme-size;"
msgstr "Tamanho da transferência: &hicolor-icon-theme-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:61
#, xml-text
msgid "Estimated disk space required: &hicolor-icon-theme-buildsize;"
msgstr "Espaço em disco estimado exigido: &hicolor-icon-theme-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:66
#, xml-text
msgid "Estimated build time: &hicolor-icon-theme-time;"
msgstr "Tempo de construção estimado: &hicolor-icon-theme-time;"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/icons/hicolor-icon-theme.xml:75
#, xml-text
msgid "Installation of hicolor-icon-theme"
msgstr "Instalação do hicolor-icon-theme"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:78
#, xml-text
msgid ""
"Install <application>hicolor-icon-theme</application> by running the "
"following commands:"
msgstr ""
"Instale o &quot;<application>hicolor-icon-theme</application>&quot; "
"executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/x/icons/hicolor-icon-theme.xml:82
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr --buildtype=release .. &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr --buildtype=release .. &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:89
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:93
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/x/icons/hicolor-icon-theme.xml:96
#, no-wrap, xml-text
msgid "<userinput>ninja install</userinput>"
msgstr "<userinput>ninja install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/icons/hicolor-icon-theme.xml:101
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/icons/hicolor-icon-theme.xml:104
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/icons/hicolor-icon-theme.xml:105
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/icons/hicolor-icon-theme.xml:106
#, xml-text
msgid "Installed Directory"
msgstr "Diretório Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/x/icons/hicolor-icon-theme.xml:109
#: blfs-en/x/icons/hicolor-icon-theme.xml:110
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/x/icons/hicolor-icon-theme.xml:111
#, xml-text
msgid "/usr/share/icons/hicolor"
msgstr "/usr/share/icons/hicolor"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/x/icons/hicolor-icon-theme.xml:116
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/x/icons/hicolor-icon-theme.xml:117
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/x/icons/hicolor-icon-theme.xml:121
#, xml-text
msgid "/usr/share/icons/hicolor/*"
msgstr "/usr/share/icons/hicolor/*"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/x/icons/hicolor-icon-theme.xml:124
#, xml-text
msgid "contains icon definitions used as defaults"
msgstr "contém definições de ícones usadas como padrão"

#, xml-text
#~ msgid "84eec8d6f810240a069c731f1870b474"
#~ msgstr "84eec8d6f810240a069c731f1870b474"

#, xml-text
#~ msgid "340 KB"
#~ msgstr "340 KB"

#, no-wrap, xml-text
#~ msgid "<userinput>./configure --prefix=/usr</userinput>"
#~ msgstr "<userinput>./configure --prefix=/usr</userinput>"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/hicolor-icon-theme\"/>"
#~ msgstr ""
#~ "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/hicolor-icon-theme\"/>"
