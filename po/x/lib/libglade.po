# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-22 06:38+0000\n"
"PO-Revision-Date: 2024-06-25 03:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/x_lib_libglade/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the libglade-download-http entity
#: blfs-en/x/lib/libglade.xml:7
#, xml-text
msgid "&gnome-download-http;/libglade/2.6/libglade-&libglade-version;.tar.bz2"
msgstr ""
"&gnome-download-http;/libglade/2.6/libglade-&libglade-version;.tar.bz2"

#. type: Content of the libglade-md5sum entity
#: blfs-en/x/lib/libglade.xml:9
#, xml-text
msgid "d1776b40f4e166b5e9c107f1c8fe4139"
msgstr "d1776b40f4e166b5e9c107f1c8fe4139"

#. type: Content of the libglade-size entity
#: blfs-en/x/lib/libglade.xml:10
#, xml-text
msgid "348 KB"
msgstr "348 KB"

#. type: Content of the libglade-buildsize entity
#: blfs-en/x/lib/libglade.xml:11
#, xml-text
msgid "5 MB"
msgstr "5 MB"

#. type: Content of the libglade-time entity
#: blfs-en/x/lib/libglade.xml:12
#, xml-text
msgid "0.1 SBU"
msgstr "0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/x/lib/libglade.xml:15 blfs-en/x/lib/libglade.xml:19
#, xml-text
msgid "libglade-&libglade-version;"
msgstr "libglade-&libglade-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/x/lib/libglade.xml:22
#, xml-text
msgid "Libglade"
msgstr "Libglade"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/libglade.xml:26
#, xml-text
msgid "Introduction to libglade"
msgstr "Introdução ao libglade"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:29
#, xml-text
msgid ""
"The <application>libglade</application> package contains <filename "
"class=\"libraryfile\">libglade</filename> libraries. These are useful for "
"loading Glade interface files in a program at runtime."
msgstr ""
"O pacote &quot;<application>libglade</application>&quot; contém bibliotecas "
"&quot;<filename class=\"libraryfile\">libglade</filename>&quot;. Elas são "
"úteis para carregar arquivos de interface do &quot;Glade&quot; em um "
"aplicativo em tempo de execução."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/libglade.xml:36
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/libglade.xml:40
#, xml-text
msgid "Download (HTTP): <ulink url=\"&libglade-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&libglade-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/libglade.xml:45
#, xml-text
msgid "Download (FTP): <ulink url=\"&libglade-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&libglade-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/libglade.xml:50
#, xml-text
msgid "Download MD5 sum: &libglade-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &libglade-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/libglade.xml:55
#, xml-text
msgid "Download size: &libglade-size;"
msgstr "Tamanho da transferência: &libglade-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/libglade.xml:60
#, xml-text
msgid "Estimated disk space required: &libglade-buildsize;"
msgstr "Espaço em disco estimado exigido: &libglade-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/libglade.xml:65
#, xml-text
msgid "Estimated build time: &libglade-time;"
msgstr "Tempo de construção estimado: &libglade-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/libglade.xml:70
#, xml-text
msgid "libglade Dependencies"
msgstr "Dependências do &quot;libglade&quot;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/libglade.xml:72
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:74
#, xml-text
msgid "<xref linkend=\"libxml2\"/> and <xref linkend=\"gtk2\"/>"
msgstr "<xref linkend=\"libxml2\"/> e <xref linkend=\"gtk2\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/libglade.xml:78
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:80
#, xml-text
msgid "<xref linkend=\"python2\"/> and <xref linkend=\"gtk-doc\"/>"
msgstr "<xref linkend=\"python2\"/> e <xref linkend=\"gtk-doc\"/>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/libglade.xml:87
#, xml-text
msgid "Installation of libglade"
msgstr "Instalação do libglade"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:90
#, xml-text
msgid ""
"Install <application>libglade</application> by running the following "
"commands:"
msgstr ""
"Instale o &quot;<application>libglade</application>&quot; executando os "
"seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/x/lib/libglade.xml:94
#, no-wrap, xml-text
msgid ""
"<userinput>sed -i '/DG_DISABLE_DEPRECATED/d' glade/Makefile.in &amp;&amp;\n"
"./configure --prefix=/usr --disable-static &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>sed -i '/DG_DISABLE_DEPRECATED/d' glade/Makefile.in &amp;&amp;\n"
"./configure --prefix=/usr --disable-static &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:99
#, xml-text
msgid ""
"To test the results, issue: <command>make check</command>.  One of the "
"tests, test-convert, is known to fail."
msgstr ""
"Para testar os resultados, emita: &quot;<command>make check</command>&quot;."
" Um dos testes, &quot;test-convert&quot;, é conhecido por falhar."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:106
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/x/lib/libglade.xml:109
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/libglade.xml:114
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/libglade.xml:117
#, xml-text
msgid ""
"<command>sed -i '/DG_DISABLE_DEPRECATED/d'</command>: Some of the "
"<application>glib</application> functions that "
"<application>libglade</application> uses were declared deprecated in "
"glib-2.30. This sed removes the G_DISABLE_DEPRECATED CFLAG."
msgstr ""
"<command>sed -i '/DG_DISABLE_DEPRECATED/d'</command>: Algumas das funções do"
" &quot;<application>glib</application>&quot; que a "
"&quot;<application>libglade</application>&quot; usa foram declaradas "
"obsoletas no &quot;glib-2.30&quot;. Esse &quot;sed&quot; remove o "
"&quot;CFLAG&quot; &quot;G_DISABLE_DEPRECATED&quot;."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/libglade.xml:132
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/lib/libglade.xml:135
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/lib/libglade.xml:136
#, xml-text
msgid "Installed Library"
msgstr "Biblioteca Instalada"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/lib/libglade.xml:137
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/x/lib/libglade.xml:140
#, xml-text
msgid "libglade-convert (requires <command>python</command>)"
msgstr "libglade-convert (exige o &quot;<command>python</command>&quot;)"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/x/lib/libglade.xml:141 blfs-en/x/lib/libglade.xml:172
#, xml-text
msgid "libglade-2.0.so"
msgstr "libglade-2.0.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/x/lib/libglade.xml:142
#, xml-text
msgid ""
"/usr/{include/libglade-2.0/glade,share/{gtk-doc/html/libglade, "
"xml/libglade}}"
msgstr ""
"/usr/{include/libglade-2.0/glade,share/{gtk-doc/html/libglade, "
"xml/libglade}}"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/x/lib/libglade.xml:148
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/x/lib/libglade.xml:149
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/x/lib/libglade.xml:153
#, xml-text
msgid "<command>libglade-convert</command>"
msgstr "<command>libglade-convert</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/x/lib/libglade.xml:156
#, xml-text
msgid "is used to convert old Glade interface files to Glade-2.0 standards"
msgstr ""
"é usado para converter arquivos antigos da interface do &quot;Glade&quot; "
"para os padrões do &quot;Glade-2.0&quot;"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/x/lib/libglade.xml:160
#, xml-text
msgid "libglade-convert"
msgstr "libglade-convert"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/x/lib/libglade.xml:166
#, xml-text
msgid "<filename class=\"libraryfile\">libglade-2.0.so</filename>"
msgstr "<filename class=\"libraryfile\">libglade-2.0.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/x/lib/libglade.xml:169
#, xml-text
msgid "contains the functions necessary to load Glade interface files"
msgstr ""
"contém as funções necessárias para carregar os arquivos de interface do "
"&quot;Glade&quot;"

#, xml-text
#~ msgid "&gnome-download-ftp;/libglade/2.6/libglade-&libglade-version;.tar.bz2"
#~ msgstr "&gnome-download-ftp;/libglade/2.6/libglade-&libglade-version;.tar.bz2"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/libglade\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/libglade\"/>"
