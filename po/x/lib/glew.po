#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-24 19:58+0000\n"
"PO-Revision-Date: 2024-06-24 01:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/x_lib_glew/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the glew-download-http entity
#: blfs-en/x/lib/glew.xml:7
#, xml-text
msgid "https://downloads.sourceforge.net/glew/glew-&glew-version;.tgz"
msgstr "https://downloads.sourceforge.net/glew/glew-&glew-version;.tgz"

#. type: Content of the glew-md5sum entity
#: blfs-en/x/lib/glew.xml:9
#, xml-text
msgid "3579164bccaef09e36c0af7f4fd5c7c7"
msgstr "3579164bccaef09e36c0af7f4fd5c7c7"

#. type: Content of the glew-size entity
#: blfs-en/x/lib/glew.xml:10
#, xml-text
msgid "820 KB"
msgstr "820 KB"

#. type: Content of the glew-buildsize entity
#: blfs-en/x/lib/glew.xml:11
#, xml-text
msgid "16 MB"
msgstr "16 MB"

#. type: Content of the glew-time entity
#: blfs-en/x/lib/glew.xml:12
#, xml-text
msgid "less than 0.1 SBU"
msgstr "menos que 0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/x/lib/glew.xml:15 blfs-en/x/lib/glew.xml:19
#, xml-text
msgid "GLEW-&glew-version;"
msgstr "GLEW-&glew-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/x/lib/glew.xml:22
#, xml-text
msgid "GLEW"
msgstr "GLEW"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/glew.xml:26
#, xml-text
msgid "Introduction to GLEW"
msgstr "Introdução ao GLEW"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:29
#, xml-text
msgid ""
"<application>GLEW</application> is the OpenGL Extension Wrangler Library."
msgstr ""
"&quot;<application>GLEW</application>&quot; é a Biblioteca &quot;OpenGL "
"Extension Wrangler&quot;."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/glew.xml:34
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/glew.xml:38
#, xml-text
msgid "Download (HTTP): <ulink url=\"&glew-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&glew-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/glew.xml:43
#, xml-text
msgid "Download (FTP): <ulink url=\"&glew-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&glew-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/glew.xml:48
#, xml-text
msgid "Download MD5 sum: &glew-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &glew-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/glew.xml:53
#, xml-text
msgid "Download size: &glew-size;"
msgstr "Tamanho da transferência: &glew-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/glew.xml:58
#, xml-text
msgid "Estimated disk space required: &glew-buildsize;"
msgstr "Espaço em disco estimado exigido: &glew-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/x/lib/glew.xml:63
#, xml-text
msgid "Estimated build time: &glew-time;"
msgstr "Tempo de construção estimado: &glew-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/glew.xml:68
#, xml-text
msgid "glew Dependencies"
msgstr "Dependências do &quot;glew&quot;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/x/lib/glew.xml:70
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:72
#, xml-text
msgid "<xref linkend=\"mesa\"/>"
msgstr "<xref linkend=\"mesa\"/>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/glew.xml:79
#, xml-text
msgid "Installation of GLEW"
msgstr "Instalação do GLEW"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:82
#, xml-text
msgid ""
"Install <application>GLEW</application> by running the following commands:"
msgstr ""
"Instale o &quot;<application>GLEW</application>&quot; executando os "
"seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/x/lib/glew.xml:86
#, no-wrap, xml-text
msgid ""
"<userinput>sed -i 's%lib64%lib%g' config/Makefile.linux &amp;&amp;\n"
"sed -i -e '/glew.lib.static:/d' \\\n"
"       -e '/0644 .*STATIC/d'    \\\n"
"       -e 's/glew.lib.static//' Makefile     &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>sed -i 's%lib64%lib%g' config/Makefile.linux &amp;&amp;\n"
"sed -i -e '/glew.lib.static:/d' \\\n"
"       -e '/0644 .*STATIC/d'     \\\n"
"       -e 's/glew.lib.static//' Makefile     &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:93
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:97
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/x/lib/glew.xml:100
#, no-wrap, xml-text
msgid "<userinput>make install.all</userinput>"
msgstr "<userinput>make install.all</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/glew.xml:105
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:108
#, xml-text
msgid ""
"<command>sed -i 's%lib64%lib%g' ...</command>: This ensures that the library"
" is installed in <filename class=\"directory\">/usr/lib</filename>."
msgstr ""
"<command>sed -i 's%lib64%lib%g' ...</command>: Isso garante que a biblioteca"
" seja instalada em &quot;<filename "
"class=\"directory\">/usr/lib</filename>&quot;."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:113
#, xml-text
msgid ""
"<command>sed -i -e '/glew.lib.static:/d' ...</command>: This suppresses the "
"static library."
msgstr ""
"<command>sed -i -e '/glew.lib.static:/d' ...</command>: Isso suprime a "
"biblioteca estática."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/x/lib/glew.xml:118
#, xml-text
msgid ""
"<command>make install.all</command>: This installs the programs as well as "
"the library."
msgstr ""
"<command>make install.all</command>: Isso instala os aplicativos bem como a "
"biblioteca."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/x/lib/glew.xml:126
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/lib/glew.xml:129
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/lib/glew.xml:130
#, xml-text
msgid "Installed Library"
msgstr "Biblioteca Instalada"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/x/lib/glew.xml:131
#, xml-text
msgid "Installed Directory"
msgstr "Diretório Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/x/lib/glew.xml:135
#, xml-text
msgid "glewinfo and visualinfo"
msgstr "glewinfo e visualinfo"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/x/lib/glew.xml:138 blfs-en/x/lib/glew.xml:186
#, xml-text
msgid "libGLEW.so"
msgstr "libGLEW.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/x/lib/glew.xml:141
#, xml-text
msgid "/usr/include/GL"
msgstr "/usr/include/GL"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/x/lib/glew.xml:147
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#.  If the program or library name conflicts (is the same) as the
#.       package name, add -prog or -lib to the varlistentry entity id
#.       and the 2nd entry of the indexterm zone entity
#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/x/lib/glew.xml:148
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/x/lib/glew.xml:156
#, xml-text
msgid "<command>glewinfo</command>"
msgstr "<command>glewinfo</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/x/lib/glew.xml:159
#, xml-text
msgid "provides information about the supported extensions"
msgstr "fornece informações a respeito das extensões suportadas"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/x/lib/glew.xml:162
#, xml-text
msgid "glewinfo"
msgstr "glewinfo"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/x/lib/glew.xml:168
#, xml-text
msgid "<command>visualinfo</command>"
msgstr "<command>visualinfo</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/x/lib/glew.xml:171
#, xml-text
msgid "is an extended version of glxinfo"
msgstr "é uma versão estendida do &quot;glxinfo&quot;"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/x/lib/glew.xml:174
#, xml-text
msgid "visualinfo"
msgstr "visualinfo"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/x/lib/glew.xml:180
#, xml-text
msgid "<filename class=\"libraryfile\">libGLEW.so</filename>"
msgstr "<filename class=\"libraryfile\">libGLEW.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/x/lib/glew.xml:183
#, xml-text
msgid "provides functions to access OpenGL extensions"
msgstr "fornece funções para acessar extensões &quot;OpenGL&quot;"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/GLEW\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/GLEW\"/>"
