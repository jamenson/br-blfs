#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-24 04:18+0000\n"
"PO-Revision-Date: 2024-12-11 21:14+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/gnome_platform_tinysparql/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.7.2\n"

#. type: Content of the tinysparql-md5sum entity
#: blfs-en/gnome/platform/tinysparql.xml:10
#, xml-text
msgid "9c3ab085fe15b55acfd956c039819a00"
msgstr "9c3ab085fe15b55acfd956c039819a00"

#. type: Content of the tinysparql-size entity
#: blfs-en/gnome/platform/tinysparql.xml:11
#, xml-text
msgid "2.0 MB"
msgstr "2,0 MB"

#. type: Content of the tinysparql-buildsize entity
#: blfs-en/gnome/platform/tinysparql.xml:12
#, xml-text
msgid "53 MB (with tests)"
msgstr "53 MB (com testes)"

#. type: Content of the tinysparql-time entity
#: blfs-en/gnome/platform/tinysparql.xml:13
#, xml-text
msgid "0.4 SBU (with tests)"
msgstr "0,4 UPC (com testes)"

#. type: Content of: <sect1><title>
#: blfs-en/gnome/platform/tinysparql.xml:16
#: blfs-en/gnome/platform/tinysparql.xml:20
#, xml-text
msgid "tinysparql-&tinysparql-version;"
msgstr "tinysparql-&tinysparql-version;"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/gnome/platform/tinysparql.xml:23
#: blfs-en/gnome/platform/tinysparql.xml:201
#: blfs-en/gnome/platform/tinysparql.xml:226
#, xml-text
msgid "tinysparql"
msgstr "tinysparql"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tinysparql.xml:27
#, xml-text
msgid "Introduction to Tinysparql"
msgstr "Introdução ao Tinysparql"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:30
#, xml-text
msgid ""
"<application>Tinysparql</application> is a low-footprint RDF triple store "
"with a SPARQL 1.1 interface."
msgstr ""
"<application>Tinysparql</application> é um armazenamento triplo RDF de baixo"
" custo com uma interface SPARQL 1.1."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tinysparql.xml:36
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:40
#, xml-text
msgid "Download (HTTP): <ulink url=\"&tinysparql-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&tinysparql-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:45
#, xml-text
msgid "Download (FTP): <ulink url=\"&tinysparql-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&tinysparql-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:50
#, xml-text
msgid "Download MD5 sum: &tinysparql-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &tinysparql-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:55
#, xml-text
msgid "Download size: &tinysparql-size;"
msgstr "Tamanho da transferência: &tinysparql-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:60
#, xml-text
msgid "Estimated disk space required: &tinysparql-buildsize;"
msgstr "Espaço em disco estimado exigido: &tinysparql-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:65
#, xml-text
msgid "Estimated build time: &tinysparql-time;"
msgstr "Tempo de construção estimado: &tinysparql-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tinysparql.xml:70
#, xml-text
msgid "Tinysparql Dependencies"
msgstr "Dependências do Tinysparql"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tinysparql.xml:72
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:74
#, xml-text
msgid "<xref linkend=\"json-glib\"/> and <xref linkend=\"vala\"/>"
msgstr "<xref linkend=\"json-glib\"/> e <xref linkend=\"vala\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tinysparql.xml:78
#, xml-text
msgid "Recommended"
msgstr "Recomendadas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:80
#, xml-text
msgid ""
"&gobject-introspection;, <xref linkend=\"icu\"/>, <xref "
"linkend=\"libsoup3\"/>, <xref linkend=\"localsearch\"/> (runtime), <xref "
"linkend=\"pygobject3\"/>, and <xref linkend=\"sqlite\"/>"
msgstr ""
"&gobject-introspection;, <xref linkend=\"icu\"/>, <xref "
"linkend=\"libsoup3\"/>, <xref linkend=\"localsearch\"/> (tempo de execução),"
" <xref linkend=\"pygobject3\"/> e <xref linkend=\"sqlite\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tinysparql.xml:88
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#.   It looks like not used even if docs=true,
#.       the pre-built doc is shipped in tarball and will be installed
#.       <xref linkend="gi-docgen"/>
#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:90
#, xml-text
msgid ""
"<xref linkend=\"asciidoc\"/>, <xref linkend=\"avahi\"/>, <xref "
"linkend=\"graphviz\"/>, and <ulink url=\"https://github.com/scop/bash-"
"completion/\">bash-completion</ulink>"
msgstr ""
"<xref linkend=\"asciidoc\"/>, <xref linkend=\"avahi\"/>, <xref "
"linkend=\"graphviz\"/> e <ulink url=\"https://github.com/scop/bash-"
"completion/\">bash-completion</ulink>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tinysparql.xml:103
#, xml-text
msgid "Installation of Tinysparql"
msgstr "Instalação do Tinysparql"

#. type: Content of: <sect1><sect2><warning><para>
#: blfs-en/gnome/platform/tinysparql.xml:107
#, xml-text
msgid ""
"If you are upgrading this package from a version that was from when this "
"package was known as <application>Tracker</application>, remove a file that "
"will cause a conflict as the &root; user:"
msgstr ""
"Se você estiver atualizando esse pacote a partir de uma versão de quando "
"esse pacote era conhecido como <application>Tracker</application>, remova um"
" arquivo que causará um conflito como o(a) usuário(a) &root;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tinysparql.xml:113
#, no-wrap, xml-text
msgid ""
"<userinput>rm -fv /usr/lib/systemd/user/tracker-xdg-"
"portal-3.service</userinput>"
msgstr ""
"<userinput>rm -fv /usr/lib/systemd/user/tracker-xdg-"
"portal-3.service</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:116
#, xml-text
msgid "Fix the location to install the documentation into:"
msgstr "Corrija o local para instalar a documentação:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tinysparql.xml:119
#, no-wrap, xml-text
msgid ""
"<userinput>mv docs/reference/libtracker-sparql/doc/{Tsparql-3.0,tinysparql-&tinysparql-version;} &amp;&amp;\n"
"sed '/docs_name/s/Tsparql-3.0/tinysparql-&tinysparql-version;/' \\\n"
"    -i docs/reference/libtracker-sparql/meson.build</userinput>"
msgstr ""
"<userinput>mv docs/reference/libtracker-sparql/doc/{Tsparql-3.0,tinysparql-&tinysparql-version;} &amp;&amp;\n"
"sed '/docs_name/s/Tsparql-3.0/tinysparql-&tinysparql-version;/' \\\n"
"    -i docs/reference/libtracker-sparql/meson.build</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:124
#, xml-text
msgid ""
"Install <application>Tinysparql</application> by running the following "
"commands:"
msgstr ""
"Instale <application>Tinysparql</application> executando os seguintes "
"comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tinysparql.xml:128
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr       \\\n"
"            --buildtype=release \\\n"
"            -D man=false        \\\n"
"            ..                  &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr       \\\n"
"            --buildtype=release \\\n"
"            -D man=false        \\\n"
"            ..                  &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tinysparql.xml:137
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr                  \\\n"
"            --buildtype=release            \\\n"
"            -D man=false                   \\\n"
"            -D systemd_user_services=false \\\n"
"            ..                            &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr                  \\\n"
"            --buildtype=release            \\\n"
"            -D man=false                   \\\n"
"            -D systemd_user_services=false \\\n"
"            ..                            &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:148
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tinysparql.xml:151
#, no-wrap, xml-text
msgid "<userinput>ninja install</userinput>"
msgstr "<userinput>ninja install</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:154
#, xml-text
msgid ""
"To test the results, issue: <command>meson configure -D debug=true "
"&amp;&amp; LC_ALL=C ninja test</command>.  The test suite should be run from"
" a graphical session. One test is known to fail due to the manual pages not "
"being generated."
msgstr ""
"Para testar os resultados, emita: <command>meson configure -D debug=true "
"&amp;&amp; LC_ALL=C ninja test</command>. A suíte de teste deveria ser "
"executada a partir de uma sessão gráfica. Um teste é conhecido por falhar "
"devido às páginas de manual não serem geradas."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tinysparql.xml:163
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:169
#, xml-text
msgid ""
"<parameter>-D man=false</parameter>: This switch prevents the build process "
"from generating man pages. Omit this switch if you have <xref "
"linkend=\"asciidoc\" role=\"nodep\"/> installed and wish to generate and "
"install the man pages."
msgstr ""
"<parameter>-D man=false</parameter>: Essa chave impede o processo de "
"construção de gerar páginas de manual. Omita essa chave se você tiver <xref "
"linkend=\"asciidoc\" role=\"nodep\"/> instalado e desejar gerar e instalar "
"as páginas de manual."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:176
#, xml-text
msgid ""
"<parameter>-D systemd_user_services=false</parameter>: This switch prevents "
"the build process from installing systemd user services since they are "
"useless on SysV systems."
msgstr ""
"<parameter>-D systemd_user_services=false</parameter>: Essa chave impede o "
"processo de construção de instalar serviços de usuário(a) do systemd, pois "
"eles são inúteis em sistemas SysV."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tinysparql.xml:182
#, xml-text
msgid ""
"<command>meson configure -D debug=true</command>: This command enables some "
"debug checks necessary for the test suite.  We don't want to enable them for"
" the installed tinysparql libraries and programs, so we run the test suite "
"after installation."
msgstr ""
"<command>meson configure -D debug=true</command>: Esse comando habilita "
"algumas verificações de depuração necessárias para a suíte de teste. Nós não"
" queremos habilitá-las para as bibliotecas e programas tinysparql "
"instaladas, de forma que executamos a suíte de teste depois da instalação."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tinysparql.xml:192
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/tinysparql.xml:195
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/tinysparql.xml:196
#, xml-text
msgid "Installed Library"
msgstr "Biblioteca Instalada"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/tinysparql.xml:197
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/gnome/platform/tinysparql.xml:204
#, xml-text
msgid "libtinysparql-3.0.so and libtracker-sparql-3.0.so"
msgstr "libtinysparql-3.0.so e libtracker-sparql-3.0.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/gnome/platform/tinysparql.xml:208
#, xml-text
msgid ""
"/usr/{include,lib}/tinysparql-3.0 and /usr/share/doc/tinysparql-&tinysparql-"
"version; (optional)"
msgstr ""
"/usr/{include,lib}/tinysparql-3.0 e /usr/share/doc/tinysparql-&tinysparql-"
"version; (opcional)"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/gnome/platform/tinysparql.xml:215
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/gnome/platform/tinysparql.xml:216
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/gnome/platform/tinysparql.xml:220
#, xml-text
msgid "<command>tinysparql</command>"
msgstr "<command>tinysparql</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:223
#, xml-text
msgid "is a control program for TinySPARQL databases"
msgstr "é um programa de controle para bases de dados TinySPARQL"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/gnome/platform/tinysparql.xml:232
#, xml-text
msgid "<filename class=\"libraryfile\">libtinysparql-3.0.so</filename>"
msgstr "<filename class=\"libraryfile\">libtinysparql-3.0.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:235
#, xml-text
msgid "contains functions for administering TinySPARQL databases"
msgstr "contém funções para administrar bases de dados TinySPARQL"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/gnome/platform/tinysparql.xml:238
#, xml-text
msgid "libtinysparql-3.0.so"
msgstr "libtinysparql-3.0.so"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/gnome/platform/tinysparql.xml:244
#, xml-text
msgid "<filename class=\"libraryfile\">libtracker-sparql-3.0.so</filename>"
msgstr "<filename class=\"libraryfile\">libtracker-sparql-3.0.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/gnome/platform/tinysparql.xml:247
#, xml-text
msgid "contains resource management and database functions"
msgstr "contém funções de gerenciamento de recursos e de base de dados"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/gnome/platform/tinysparql.xml:250
#, xml-text
msgid "libtracker-sparql-3.0.so"
msgstr "libtracker-sparql-3.0.so"

#, xml-text
#~ msgid "2bbdce836d0207ebdb508fe676a1ac7e"
#~ msgstr "2bbdce836d0207ebdb508fe676a1ac7e"

#, xml-text
#~ msgid "b265db81d1292d405945dbeb168a361b"
#~ msgstr "b265db81d1292d405945dbeb168a361b"
