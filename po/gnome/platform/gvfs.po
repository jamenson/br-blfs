# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-22 06:38+0000\n"
"PO-Revision-Date: 2024-08-25 16:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/gnome_platform_gvfs/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of the gvfs-download-http entity
#: blfs-en/gnome/platform/gvfs.xml:7
#, xml-text
msgid "&gnome-download-http;/gvfs/1.56/gvfs-&gvfs-version;.tar.xz"
msgstr "&gnome-download-http;/gvfs/1.56/gvfs-&gvfs-version;.tar.xz"

#. type: Content of the gvfs-md5sum entity
#: blfs-en/gnome/platform/gvfs.xml:9
#, xml-text
msgid "5ba12cd02b17c058aa71adc39c5eb6cf"
msgstr "5ba12cd02b17c058aa71adc39c5eb6cf"

#. type: Content of the gvfs-size entity
#: blfs-en/gnome/platform/gvfs.xml:10
#, xml-text
msgid "1.2 MB"
msgstr "1,2 MB"

#. type: Content of the gvfs-buildsize entity
#: blfs-en/gnome/platform/gvfs.xml:11
#, xml-text
msgid "29 MB"
msgstr "29 MB"

#. type: Content of the gvfs-time entity
#: blfs-en/gnome/platform/gvfs.xml:12
#, xml-text
msgid "0.1 SBU (using parallelism=4)"
msgstr "0,1 UPC (usando paralelismo = 4)"

#. type: Content of: <sect1><title>
#: blfs-en/gnome/platform/gvfs.xml:15 blfs-en/gnome/platform/gvfs.xml:19
#, xml-text
msgid "Gvfs-&gvfs-version;"
msgstr "Gvfs-&gvfs-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/gnome/platform/gvfs.xml:22
#, xml-text
msgid "Gvfs"
msgstr "Gvfs"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/gvfs.xml:26
#, xml-text
msgid "Introduction to Gvfs"
msgstr "Introdução ao Gvfs"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:29
#, xml-text
msgid ""
"The <application>Gvfs</application> package is a userspace virtual "
"filesystem designed to work with the I/O abstractions of GLib's GIO library."
msgstr ""
"O pacote <application>Gvfs</application> é um sistema de arquivos virtual de"
" espaço de usuário(a) projetado para funcionar com as abstrações de E/S da "
"biblioteca GIO da GLib."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/gvfs.xml:36
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:40
#, xml-text
msgid "Download (HTTP): <ulink url=\"&gvfs-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&gvfs-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:45
#, xml-text
msgid "Download (FTP): <ulink url=\"&gvfs-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&gvfs-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:50
#, xml-text
msgid "Download MD5 sum: &gvfs-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &gvfs-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:55
#, xml-text
msgid "Download size: &gvfs-size;"
msgstr "Tamanho da transferência: &gvfs-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:60
#, xml-text
msgid "Estimated disk space required: &gvfs-buildsize;"
msgstr "Espaço em disco estimado exigido: &gvfs-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:65
#, xml-text
msgid "Estimated build time: &gvfs-time;"
msgstr "Tempo de construção estimado: &gvfs-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/gvfs.xml:80
#, xml-text
msgid "Gvfs Dependencies"
msgstr "Dependências do Gvfs"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/gvfs.xml:82
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:84
#, xml-text
msgid ""
"<xref linkend=\"dbus\"/>, <xref linkend=\"glib2\"/>, <xref "
"linkend=\"gcr4\"/>, <xref linkend=\"libusb\"/>, and <xref "
"linkend=\"libsecret\"/>"
msgstr ""
"<xref linkend=\"dbus\"/>, <xref linkend=\"glib2\"/>, <xref "
"linkend=\"gcr4\"/>, <xref linkend=\"libusb\"/> e <xref "
"linkend=\"libsecret\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/gvfs.xml:91
#, xml-text
msgid "Recommended"
msgstr "Recomendadas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:93
#, xml-text
msgid ""
"<xref linkend=\"gtk3\"/>, <xref linkend=\"libcdio\"/>, <xref "
"linkend=\"libgudev\"/>, <xref linkend=\"libsoup3\"/>, <phrase "
"revision=\"sysv\"><xref linkend=\"elogind\"/>,</phrase> <phrase "
"revision=\"systemd\"><xref role='runtime' linkend=\"systemd\"/> "
"(runtime),</phrase> and <xref linkend=\"udisks2\"/>"
msgstr ""
"<xref linkend=\"gtk3\"/>, <xref linkend=\"libcdio\"/>, <xref "
"linkend=\"libgudev\"/>, <xref linkend=\"libsoup3\"/>, <phrase "
"revision=\"sysv\"><xref linkend=\"elogind\"/></phrase> <phrase "
"revision=\"systemd\"><xref role='runtime' linkend=\"systemd\"/> (tempo de "
"execução)</phrase> e <xref linkend=\"udisks2\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/gvfs.xml:103
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:105
#, xml-text
msgid ""
"<xref linkend=\"apache\"/>, <xref linkend=\"avahi\"/>, <xref "
"linkend=\"bluez\"/>, <xref linkend=\"fuse3\"/>, <xref linkend=\"gnome-"
"online-accounts\"/>, <xref linkend=\"gtk-doc\"/>, <xref "
"linkend=\"libarchive\"/>, <xref linkend=\"libgcrypt\"/>, <xref "
"linkend=\"libgdata\"/>, <xref linkend=\"libxml2\"/>, <xref "
"linkend=\"libxslt\"/>, <xref linkend=\"openssh\"/>, <xref "
"linkend=\"samba\"/>, <ulink url=\"&gnome-download-http;/gnome-desktop-"
"testing/\">gnome-desktop-testing</ulink> (for tests), <ulink "
"url=\"https://www.videolan.org/developers/libbluray.html\">libbluray</ulink>,"
" <ulink url=\"http://www.gphoto.org/\">libgphoto2</ulink>, <ulink "
"url=\"https://www.libimobiledevice.org/\">libimobiledevice</ulink>, <ulink "
"url=\"https://gitlab.gnome.org/GNOME/msgraph\">libmsgraph</ulink>, <ulink "
"url=\"https://libmtp.sourceforge.net/\">libmtp</ulink>, <ulink "
"url=\"https://github.com/sahlberg/libnfs/\">libnfs</ulink>, and <ulink "
"url=\"https://twisted.org/\">Twisted</ulink>"
msgstr ""
"<xref linkend=\"apache\"/>, <xref linkend=\"avahi\"/>, <xref "
"linkend=\"bluez\"/>, <xref linkend=\"fuse3\"/>, <xref linkend=\"gnome-"
"online-accounts\"/>, <xref linkend=\"gtk-doc\"/>, <xref "
"linkend=\"libarchive\"/>, <xref linkend=\"libgcrypt\"/>, <xref "
"linkend=\"libgdata\"/>, <xref linkend=\"libxml2\"/>, <xref "
"linkend=\"libxslt\"/>, <xref linkend=\"openssh\"/>, <xref "
"linkend=\"samba\"/>, <ulink url=\"&gnome-download-http;/gnome-desktop-"
"testing/\">gnome-desktop-testing</ulink> (para testes), <ulink "
"url=\"https://www.videolan.org/developers/libbluray.html\">libbluray</ulink>,"
" <ulink url=\"http://www.gphoto.org/\">libgphoto2</ulink>, <ulink "
"url=\"https://www.libimobiledevice.org/\">libimobiledevice</ulink>, <ulink "
"url=\"https://gitlab.gnome.org/GNOME/msgraph\">libmsgraph</ulink>, <ulink "
"url=\"https://libmtp.sourceforge.net/\">libmtp</ulink>, <ulink "
"url=\"https://github.com/sahlberg/libnfs/\">libnfs</ulink> e <ulink "
"url=\"https://twisted.org/\">Twisted</ulink>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/gvfs.xml:131
#, xml-text
msgid "Installation of Gvfs"
msgstr "Instalação do Gvfs"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:134
#, xml-text
msgid ""
"Install <application>Gvfs</application> by running the following commands:"
msgstr ""
"Instale o <application>Gvfs</application> executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/gvfs.xml:138
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup               \\\n"
"      --prefix=/usr       \\\n"
"      --buildtype=release \\\n"
"      -D onedrive=false   \\\n"
"      -D fuse=false       \\\n"
"      -D gphoto2=false    \\\n"
"      -D afc=false        \\\n"
"      -D bluray=false     \\\n"
"      -D nfs=false        \\\n"
"      -D mtp=false        \\\n"
"      -D smb=false        \\\n"
"      -D tmpfilesdir=no   \\\n"
"      -D dnssd=false      \\\n"
"      -D goa=false        \\\n"
"      -D google=false     \\\n"
"      -D systemduserunitdir=no .. &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup               \\\n"
"      --prefix=/usr       \\\n"
"      --buildtype=release \\\n"
"      -D onedrive=false   \\\n"
"      -D fuse=false       \\\n"
"      -D gphoto2=false    \\\n"
"      -D afc=false        \\\n"
"      -D bluray=false     \\\n"
"      -D nfs=false        \\\n"
"      -D mtp=false        \\\n"
"      -D smb=false        \\\n"
"      -D tmpfilesdir=no   \\\n"
"      -D dnssd=false      \\\n"
"      -D goa=false        \\\n"
"      -D google=false     \\\n"
"      -D systemduserunitdir=no .. &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/gvfs.xml:159
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup               \\\n"
"      --prefix=/usr       \\\n"
"      --buildtype=release \\\n"
"      -D onedrive=false   \\\n"
"      -D fuse=false       \\\n"
"      -D gphoto2=false    \\\n"
"      -D afc=false        \\\n"
"      -D bluray=false     \\\n"
"      -D nfs=false        \\\n"
"      -D mtp=false        \\\n"
"      -D smb=false        \\\n"
"      -D dnssd=false      \\\n"
"      -D goa=false        \\\n"
"      -D google=false     .. &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup               \\\n"
"      --prefix=/usr       \\\n"
"      --buildtype=release \\\n"
"      -D onedrive=false   \\\n"
"      -D fuse=false       \\\n"
"      -D gphoto2=false    \\\n"
"      -D afc=false        \\\n"
"      -D bluray=false     \\\n"
"      -D nfs=false        \\\n"
"      -D mtp=false        \\\n"
"      -D smb=false        \\\n"
"      -D dnssd=false      \\\n"
"      -D goa=false        \\\n"
"      -D google=false     .. &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:179
#, xml-text
msgid ""
"The test suite requires <application>gnome-desktop-testing</application>, "
"which is beyond the scope of BLFS."
msgstr ""
"A suíte de teste exige <application>gnome-desktop-testing</application>, que"
" está além do escopo do BLFS."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:184
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/gvfs.xml:187
#, no-wrap, xml-text
msgid "<userinput>ninja install</userinput>"
msgstr "<userinput>ninja install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/gvfs.xml:194
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:200
#, xml-text
msgid ""
"<parameter>-D &lt;option&gt;=false</parameter>: These switches are required "
"if the corresponding dependency is not installed.  Remove those where you "
"have installed the corresponding application and wish to use it with "
"<application>Gvfs</application>. The dnssd option requires avahi and both "
"goa and google require GNOME Online Accounts.  The google option also "
"requires libgdata. The onedrive support requires the libmsgraph package."
msgstr ""
"<parameter>-D &lt;option&gt;=false</parameter>: Essas chaves são exigidas se"
" a dependência correspondente não estiver instalada. Remova aquelas onde "
"você tiver instalado o aplicativo correspondente e desejar usá-lo com o "
"<application>Gvfs</application>. A opção dnssd exige avahi e, ambos, goa e "
"google exigem GNOME Online Accounts. A opção google também exige libgdata. A"
" opção google também exige libgdata. O suporte a onedrive exige o pacote "
"libmsgraph."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:210
#, xml-text
msgid ""
"<parameter>-D tmpfilesdir=no</parameter> <parameter>-D "
"systemduserunitdir=no</parameter>: These switches provide systemd directory "
"locations. Setting them to <quote>no</quote> disables the dependency on "
"systemd and logind."
msgstr ""
"<parameter>-D tmpfilesdir=no</parameter> <parameter>-D "
"systemduserunitdir=no</parameter>: Essas chaves fornecem locais de diretório"
" do systemd. Configurá-las para <quote>no</quote> desabilita a dependência "
"do systemd e do logind."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/gvfs.xml:217
#, xml-text
msgid ""
"<option>-D cdda=false</option>: This switch is required if libcdio is not "
"installed. The cdda backend is useless on machines without a CDROM/DVD "
"drive."
msgstr ""
"<option>-D cdda=false</option>: Essa chave é exigida se a libcdio não "
"estiver instalada. A estrutura de retaguarda cdda é inútil em máquinas sem "
"uma unidade de CDROM/DVD."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/gvfs.xml:225
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/gvfs.xml:228
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/gvfs.xml:229
#, xml-text
msgid "Installed Library"
msgstr "Biblioteca Instalada"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/gvfs.xml:230
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/gnome/platform/gvfs.xml:234
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/gnome/platform/gvfs.xml:237
#, xml-text
msgid ""
"libgvfscommon.so, libgvfsdaemon.so and some under /usr/lib/gio/modules/"
msgstr ""
"libgvfscommon.so, libgvfsdaemon.so e algumas sob /usr/lib/gio/modules/"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/gnome/platform/gvfs.xml:242
#, xml-text
msgid "/usr/include/gvfs-client and /usr/{lib,share}/gvfs"
msgstr "/usr/include/gvfs-client e /usr/{lib,share}/gvfs"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/gnome/platform/gvfs.xml:249
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/gnome/platform/gvfs.xml:250
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/gnome/platform/gvfs.xml:254
#, xml-text
msgid "<filename class=\"libraryfile\">libgvfscommon.so</filename>"
msgstr "<filename class=\"libraryfile\">libgvfscommon.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/gnome/platform/gvfs.xml:257
#, xml-text
msgid ""
"contains the common API functions used in <application>Gvfs</application> "
"programs"
msgstr ""
"contém as funções comuns de API usadas em aplicativos "
"<application>Gvfs</application>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/gnome/platform/gvfs.xml:261
#, xml-text
msgid "libgvfscommon.so"
msgstr "libgvfscommon.so"

#, xml-text
#~ msgid "587c5b279ec3020c597f3ab3f6a73bbd"
#~ msgstr "587c5b279ec3020c597f3ab3f6a73bbd"

#, xml-text
#~ msgid "30 MB"
#~ msgstr "30 MB"

#, xml-text
#~ msgid "4deb7730bcbf87e3aa89d92cb9fd352a"
#~ msgstr "4deb7730bcbf87e3aa89d92cb9fd352a"

#, xml-text
#~ msgid "1.3 MB"
#~ msgstr "1,3 MB"

#, xml-text
#~ msgid "87c087d868352e702e5dcaf42eb729ea"
#~ msgstr "87c087d868352e702e5dcaf42eb729ea"

#, xml-text
#~ msgid "&gnome-download-ftp;/gvfs/1.50/gvfs-&gvfs-version;.tar.xz"
#~ msgstr "&gnome-download-ftp;/gvfs/1.50/gvfs-&gvfs-version;.tar.xz"

#, xml-text
#~ msgid "e780a221d643d2daf9f694a692d23006"
#~ msgstr "e780a221d643d2daf9f694a692d23006"

#, xml-text
#~ msgid "44 MB"
#~ msgstr "44 MB"

#, xml-text
#~ msgid "052ef17215d1ff52640713297d4d4f5e"
#~ msgstr "052ef17215d1ff52640713297d4d4f5e"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/gvfs\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/gvfs\"/>"

#, xml-text
#~ msgid "This package does not come with a test suite."
#~ msgstr "Esse pacote não vem com uma suíte de teste."
