#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-03 02:29+0000\n"
"PO-Revision-Date: 2024-06-26 04:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/gnome_platform_tecla/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the tecla-download-http entity
#: blfs-en/gnome/platform/tecla.xml:7
#, xml-text
msgid "&gnome-download-http;/tecla/&gnome-47;/tecla-&tecla-version;.tar.xz"
msgstr "&gnome-download-http;/tecla/&gnome-47;/tecla-&tecla-version;.tar.xz"

#. type: Content of the tecla-md5sum entity
#: blfs-en/gnome/platform/tecla.xml:9
#, xml-text
msgid "68233c486bfe8b86f759fdd9ae1be00a"
msgstr "68233c486bfe8b86f759fdd9ae1be00a"

#. type: Content of the tecla-size entity
#: blfs-en/gnome/platform/tecla.xml:10
#, xml-text
msgid "40 KB"
msgstr "40 KB"

#. type: Content of the tecla-buildsize entity
#: blfs-en/gnome/platform/tecla.xml:11
#, xml-text
msgid "1.8 MB"
msgstr "1,8 MB"

#. type: Content of the tecla-time entity
#: blfs-en/gnome/platform/tecla.xml:12
#, xml-text
msgid "less than 0.1 SBU"
msgstr "menos que 0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/gnome/platform/tecla.xml:15 blfs-en/gnome/platform/tecla.xml:18
#, xml-text
msgid "Tecla-&tecla-version;"
msgstr "Tecla-&tecla-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/gnome/platform/tecla.xml:21
#, xml-text
msgid "Tecla"
msgstr "Tecla"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tecla.xml:25
#, xml-text
msgid "Introduction to Tecla"
msgstr "Introdução ao Tecla"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tecla.xml:28
#, xml-text
msgid ""
"The <application>Tecla</application> package contains a keyboard layout "
"viewer."
msgstr ""
"O pacote <application>Tecla</application> contém um visualizador de esquema "
"de teclado."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tecla.xml:34
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:38
#, xml-text
msgid "Download (HTTP): <ulink url=\"&tecla-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&tecla-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:43
#, xml-text
msgid "Download (FTP): <ulink url=\"&tecla-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&tecla-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:48
#, xml-text
msgid "Download MD5 sum: &tecla-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &tecla-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:53
#, xml-text
msgid "Download size: &tecla-size;"
msgstr "Tamanho da transferência: &tecla-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:58
#, xml-text
msgid "Estimated disk space required: &tecla-buildsize;"
msgstr "Espaço em disco estimado exigido: &tecla-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:63
#, xml-text
msgid "Estimated build time: &tecla-time;"
msgstr "Tempo de construção estimado: &tecla-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tecla.xml:68
#, xml-text
msgid "Tecla Dependencies"
msgstr "Dependências do Tecla"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/gnome/platform/tecla.xml:70
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tecla.xml:72
#, xml-text
msgid "<xref linkend=\"libadwaita1\"/> and <xref linkend=\"libxkbcommon\"/>"
msgstr "<xref linkend=\"libadwaita1\"/> e <xref linkend=\"libxkbcommon\"/>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tecla.xml:79
#, xml-text
msgid "Installation of Tecla"
msgstr "Instalação do Tecla"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tecla.xml:82
#, xml-text
msgid ""
"Install <application>Tecla</application> by running the following commands:"
msgstr ""
"Instale <application>Tecla</application> executando os seguintes comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tecla.xml:86
#, no-wrap, xml-text
msgid ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr --buildtype=release .. &amp;&amp;\n"
"ninja</userinput>"
msgstr ""
"<userinput>mkdir build &amp;&amp;\n"
"cd    build &amp;&amp;\n"
"\n"
"meson setup --prefix=/usr --buildtype=release .. &amp;&amp;\n"
"ninja</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tecla.xml:93
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/gnome/platform/tecla.xml:97
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/gnome/platform/tecla.xml:100
#, no-wrap, xml-text
msgid "<userinput>ninja install</userinput>"
msgstr "<userinput>ninja install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tecla.xml:105
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/gnome/platform/tecla.xml:113
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/tecla.xml:116
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/tecla.xml:117
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/gnome/platform/tecla.xml:118
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/gnome/platform/tecla.xml:122 blfs-en/gnome/platform/tecla.xml:149
#, xml-text
msgid "tecla"
msgstr "tecla"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/gnome/platform/tecla.xml:125 blfs-en/gnome/platform/tecla.xml:128
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/gnome/platform/tecla.xml:134
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#.  If the program or library name conflicts with (is the same as) the
#.       package name, add -prog or -lib to the varlistentry entity id
#.       and the 2nd entry of the indexterm zone entity
#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/gnome/platform/tecla.xml:135
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/gnome/platform/tecla.xml:143
#, xml-text
msgid "<command>tecla</command>"
msgstr "<command>tecla</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/gnome/platform/tecla.xml:146
#, xml-text
msgid "is a keyboard layout viewer"
msgstr "é um visualizador de esquema de teclado"

#, xml-text
#~ msgid "8e3ecd44ed17dab85aa281df19357395"
#~ msgstr "8e3ecd44ed17dab85aa281df19357395"

#, xml-text
#~ msgid "36 KB"
#~ msgstr "36 KB"

#, xml-text
#~ msgid "2.2 MB"
#~ msgstr "2,2 MB"

#, xml-text
#~ msgid "68a44119c5c76ca952a4cb6ca2e0fd22"
#~ msgstr "68a44119c5c76ca952a4cb6ca2e0fd22"

#, xml-text
#~ msgid "2.0 MB"
#~ msgstr "2,0 MB"
