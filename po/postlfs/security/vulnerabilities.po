# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-05 19:36+0000\n"
"PO-Revision-Date: 2024-07-11 16:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/postlfs_security_vulnerabilities/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Attribute 'xreflabel' of: <sect1>
#: blfs-en/postlfs/security/vulnerabilities.xml:8
#, xml-text
msgid "vulnerabilities"
msgstr "vulnerabilidades"

#. type: Content of: <sect1><title>
#: blfs-en/postlfs/security/vulnerabilities.xml:12
#, xml-text
msgid "Vulnerabilities"
msgstr "Vulnerabilidades"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/postlfs/security/vulnerabilities.xml:16
#, xml-text
msgid "vulnerability links"
msgstr "links vulnerabilidade"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/security/vulnerabilities.xml:20
#, xml-text
msgid "About vulnerabilities"
msgstr "Acerca de vulnerabilidades"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:23
#, xml-text
msgid ""
"All software has bugs. Sometimes, a bug can be exploited, for example to "
"allow users to gain enhanced privileges (perhaps gaining a root shell, or "
"simply accessing or deleting other user&apos;s files), or to allow a remote "
"site to crash an application (denial of service), or for theft of data. "
"These bugs are labelled as vulnerabilities."
msgstr ""
"Todo software tem defeitos. De vez em quando, um defeito cosegue ser "
"explorado, por exemplo para permitir que os(as) usuários(as) ganhem "
"privilégios melhorados (talvez ganhando um shell do(a) root; ou simplesmente"
" acessando ou deletando os arquivos dos(as) outros(as) usuários(as)); ou "
"para permitir que um sítio remoto quebre um aplicativo (negação de serviço);"
" ou para roubo de dados. Esses defeitos são rotulados como vulnerabilidades."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:31
#, xml-text
msgid ""
"The main place where vulnerabilities get logged is <ulink "
"url=\"https://cve.mitre.org\">cve.mitre.org</ulink>. Unfortunately, many "
"vulnerability numbers (CVE-yyyy-nnnn) are initially only labelled as "
"\"reserved\" when distributions start issuing fixes.  Also, some "
"vulnerabilities apply to particular combinations of "
"<command>configure</command> options, or only apply to old versions of "
"packages which have long since been updated in BLFS."
msgstr ""
"O lugar principal onde as vulnerabilidades são registradas é <ulink "
"url=\"https://cve.mitre.org\">cve.mitre.org</ulink>. Infelizmente, muitos "
"números de vulnerabilidade (\"CVE-yyyy-nnnn\") inicialmente são rotulados "
"somente como \"reservado\" quando as distribuições iniciam a emitir "
"correções. Também, algumas vulnerabilidades se aplicam a combinações "
"particulares das opções do <command>configure</command>; ou se aplicam "
"somente a versões antigas de pacotes que foram há muito tempo atualizados no"
" BLFS."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:41
#, xml-text
msgid ""
"BLFS differs from distributions&mdash;there is no BLFS security team, and "
"the editors only become aware of vulnerabilities after they are public "
"knowledge. Sometimes, a package with a vulnerability will not be updated in "
"the book for a long time.  Issues can be logged in the Trac system, which "
"might speed up resolution."
msgstr ""
"O BLFS se diferencia das distribuições&mdash;não existe equipe de segurança "
"do BLFS e os(as) editores(as) somente se tornam cientes das vulnerabilidades"
" depois que elas são de conhecimento público. De vez em quando, um pacote "
"com uma vulnerabilidade não será atualizado no livro por um tempo longo. Os "
"problemas podem ser registrados no sistema Trac, o que poderia acelerar a "
"resolução."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:49
#, xml-text
msgid ""
"The normal way for BLFS to fix a vulnerability is, ideally, to update the "
"book to a new fixed release of the package.  Sometimes that happens even "
"before the vulnerability is public knowledge, so there is no guarantee that "
"it will be shown as a vulnerability fix in the Changelog.  Alternatively, a "
"<command>sed</command> command, or a patch taken from a distribution, may be"
" appropriate."
msgstr ""
"A maneira normal para o BLFS corrigir uma vulnerabilidade é, idealmente, a "
"de atualizar o livro para um novo lançamento corrigido do pacote. De vez em "
"quando isso acontece mesmo antes da vulnerabilidade ser de conhecimento "
"público, de forma que não existe a garantia de que será mostrada como uma "
"correção de vulnerabilidade no Registro das Mudanças. Alternativamente, um "
"comando <command>sed</command> ou um remendo tomado a partir de uma "
"distribuição, possivelmente seja apropriado."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:58
#, xml-text
msgid ""
"The bottom line is that you are responsible for your own security, and for "
"assessing the potential impact of any problems."
msgstr ""
"O ponto principal é o de que você é o(a) responsável pela sua própria "
"segurança e por avaliar o impacto potencial de quaisquer problemas."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:63
#, xml-text
msgid ""
"The editors now issue Security Advisories for packages in BLFS (and LFS), "
"which can be found at <ulink "
"url=\"https://www.linuxfromscratch.org/blfs/advisories/\">BLFS Security "
"Advisories</ulink>, and grade the severity according to what upstream "
"reports, or to what is shown at <ulink "
"url=\"https://nvd.nist.gov/\">nvd.nist.gov</ulink> if that has details."
msgstr ""
"Os(As) editores(as) agora emitem Avisos de Segurança para pacotes no BLFS (e"
" no LFS), os quais podem ser encontrados em <ulink "
"url=\"https://www.linuxfromscratch.org/blfs/advisories/\">Avisos de "
"Segurança do BLFS</ulink>, e graduam a gravidade de acordo com o que o(a) "
"desenvolvedor(a) informa; ou com o que for mostrado em <ulink "
"url=\"https://nvd.nist.gov/\">nvd.nist.gov</ulink>, se isso tiver detalhes."

#.  To editors: the https version redirects back to the non-https
#.            version.  Not sure why they must do this, but please check
#.            before turning this to https.
#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:72
#, xml-text
msgid ""
"To keep track of what is being discovered, you may wish to follow the "
"security announcements of one or more distributions. For example, Debian has"
" <ulink url=\"https://www.debian.org/security\">Debian security</ulink>.  "
"Fedora's links on security are at <ulink "
"url=\"https://fedoraproject.org/wiki/category:Security\">the Fedora "
"wiki</ulink>.  Details of Gentoo linux security announcements are discussed "
"at <ulink url=\"https://security.gentoo.org\">Gentoo security</ulink>.  "
"Finally, the Slackware archives of security announcements are at <ulink "
"url=\"http://slackware.com/security/\">Slackware security</ulink>."
msgstr ""
"Para acompanhar o que está sendo descoberto, você possivelmente deseje "
"seguir os anúncios de segurança de uma ou mais distribuições. Por exemplo, o"
" Debian tem o <ulink url=\"https://www.debian.org/security\">Segurança do "
"Debian</ulink>. Os links do Fedora acerca de segurança estão em <ulink "
"url=\"https://fedoraproject.org/wiki/category:Security\">o wiki do "
"Fedora</ulink>. Os detalhes dos anúncios de segurança do Linux do Gentoo são"
" discutidos em <ulink url=\"https://security.gentoo.org\">Segurança do "
"Gentoo</ulink>. Finalmente, os arquivamentos do Slackware dos anúncios de "
"segurança estão em <ulink url=\"http://slackware.com/security/\">Segurança "
"do Slackware</ulink>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:87
#, xml-text
msgid ""
"The most general English source is perhaps <ulink "
"url=\"https://seclists.org/fulldisclosure\">the Full Disclosure Mailing "
"List</ulink>, but please read the comment on that page. If you use other "
"languages you may prefer other sites such as <ulink "
"url=\"https://www.heise.de/security\">heise.de</ulink> (German) or <ulink "
"url=\"https://www.cert.hr\">cert.hr</ulink> (Croatian). These are not linux-"
"specific. There is also a daily update at lwn.net for subscribers (free "
"access to the data after 2 weeks, but their vulnerabilities database at "
"<ulink url=\"https://lwn.net/Alerts/\">lwn.net/Alerts</ulink> is "
"unrestricted)."
msgstr ""
"A fonte mais genérica no idioma inglês é talvez <ulink "
"url=\"https://seclists.org/fulldisclosure\">a Lista de Discussão de "
"Divulgação Completa</ulink>; porém, por favor, leia o comentário naquela "
"página. Se usar outros idiomas, [então] você possivelmente prefira outros "
"sítios, tais como o <ulink "
"url=\"https://www.heise.de/security\">heise.de</ulink> (alemão); ou o <ulink"
" url=\"https://www.cert.hr\">cert.hr</ulink> (croata). Não existe um "
"específico para Linux. Existe também uma atualização diária em \"lwn.net\" "
"para assinantes (acesso livre aos dados depois de duas semanas, porém a base"
" de dados de vulnerabilidades deles em <ulink "
"url=\"https://lwn.net/Alerts/\">lwn.net/Alerts</ulink> é irrestrita)."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/vulnerabilities.xml:101
#, xml-text
msgid ""
"For some packages, subscribing to their &apos;announce&apos; lists will "
"provide prompt news of newer versions."
msgstr ""
"Para alguns pacotes, assinar as listas de &apos;anúncio&apos; deles "
"fornecerá notícias imediatas das versões mais recentes."

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/vulnerabilities\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/vulnerabilities\"/>"

#, xml-text
#~ msgid "<date>$Date$</date>"
#~ msgstr "<date>$Date$</date>"
