#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-24 19:59+0000\n"
"PO-Revision-Date: 2024-11-08 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/postlfs_security_cracklib/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.7.2\n"

#. type: Content of the cracklib-url entity
#: blfs-en/postlfs/security/cracklib.xml:7
#, xml-text
msgid "https://github.com/cracklib/cracklib/releases/download"
msgstr "https://github.com/cracklib/cracklib/releases/download"

#. type: Content of the cracklib-download-http entity
#: blfs-en/postlfs/security/cracklib.xml:9
#, xml-text
msgid "&cracklib-url;/v&cracklib-version;/cracklib-&cracklib-version;.tar.xz"
msgstr "&cracklib-url;/v&cracklib-version;/cracklib-&cracklib-version;.tar.xz"

#. type: Content of the cracklib-md5sum entity
#: blfs-en/postlfs/security/cracklib.xml:11
#, xml-text
msgid "e8ea2b86de774fc09fdd0f2829680b19"
msgstr "e8ea2b86de774fc09fdd0f2829680b19"

#. type: Content of the cracklib-size entity
#: blfs-en/postlfs/security/cracklib.xml:12
#, xml-text
msgid "456 KB"
msgstr "456 KB"

#. type: Content of the cracklib-buildsize entity
#: blfs-en/postlfs/security/cracklib.xml:13
#, xml-text
msgid "5.0 MB"
msgstr "5,0 MB"

#. type: Content of the cracklib-time entity
#: blfs-en/postlfs/security/cracklib.xml:14
#, xml-text
msgid "less than 0.1 SBU"
msgstr "menos que 0,1 UPC"

#. type: Content of the crackdict-download entity
#: blfs-en/postlfs/security/cracklib.xml:16
#, xml-text
msgid ""
"&cracklib-url;/v&cracklib-version;/cracklib-words-&cracklib-version;.xz"
msgstr ""
"&cracklib-url;/v&cracklib-version;/cracklib-words-&cracklib-version;.xz"

#. type: Content of the crackdict-size entity
#: blfs-en/postlfs/security/cracklib.xml:17
#, xml-text
msgid "4.0 MB"
msgstr "4,0 MB"

#. type: Content of the crackdict-md5sum entity
#: blfs-en/postlfs/security/cracklib.xml:18
#, xml-text
msgid "f27804022dbf2682a7f7c353317f9a53"
msgstr "f27804022dbf2682a7f7c353317f9a53"

#. type: Content of: <sect1><title>
#: blfs-en/postlfs/security/cracklib.xml:21
#: blfs-en/postlfs/security/cracklib.xml:25
#, xml-text
msgid "CrackLib-&cracklib-version;"
msgstr "CrackLib-&cracklib-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/postlfs/security/cracklib.xml:28
#, xml-text
msgid "CrackLib"
msgstr "CrackLib"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/security/cracklib.xml:32
#, xml-text
msgid "Introduction to CrackLib"
msgstr "Introdução ao CrackLib"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:35
#, xml-text
msgid ""
"The <application>CrackLib</application> package contains a library used to "
"enforce strong passwords by comparing user selected passwords to words in "
"chosen word lists."
msgstr ""
"O pacote <application>CrackLib</application> contém uma biblioteca usada "
"para impor senhas fortes comparando senhas selecionadas pelo(a) usuário(a) a"
" palavras em listas de palavras escolhidas."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/security/cracklib.xml:42
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:46
#, xml-text
msgid "Download (HTTP): <ulink url=\"&cracklib-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&cracklib-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:51
#, xml-text
msgid "Download (FTP): <ulink url=\"&cracklib-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&cracklib-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:56
#, xml-text
msgid "Download MD5 sum: &cracklib-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &cracklib-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:61
#, xml-text
msgid "Download size: &cracklib-size;"
msgstr "Tamanho da transferência: &cracklib-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:66
#, xml-text
msgid "Estimated disk space required: &cracklib-buildsize;"
msgstr "Espaço em disco estimado exigido: &cracklib-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:71
#, xml-text
msgid "Estimated build time: &cracklib-time;"
msgstr "Tempo de construção estimado: &cracklib-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/security/cracklib.xml:76
#, xml-text
msgid "Additional Downloads"
msgstr "Transferências Adicionais"

#. type: Content of: <sect1><sect2><itemizedlist><para>
#: blfs-en/postlfs/security/cracklib.xml:78
#, xml-text
msgid "Recommended word list for English-speaking countries:"
msgstr "Lista de palavras recomendadas para países de idioma inglês:"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:81
#, xml-text
msgid "Download (HTTP): <ulink url=\"&crackdict-download;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&crackdict-download;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:86
#, xml-text
msgid "Download MD5 sum: &crackdict-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &crackdict-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:91
#, xml-text
msgid "Download size: &crackdict-size;"
msgstr "Tamanho da transferência: &crackdict-size;"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:97
#, xml-text
msgid ""
"There are additional word lists available for download, e.g., from <ulink "
"url=\"https://wiki.skullsecurity.org/index.php/Passwords\"/>.  "
"<application>CrackLib</application> can utilize as many, or as few word "
"lists you choose to install."
msgstr ""
"Existem listas adicionais de palavras disponíveis para download, por "
"exemplo, em <ulink "
"url=\"https://wiki.skullsecurity.org/index.php/Passwords\"/>. O "
"<application>CrackLib</application> consegue utilizar o máximo ou o mínimo "
"possível das listas de palavras que você escolher instalar."

#. type: Content of: <sect1><sect2><important><para>
#: blfs-en/postlfs/security/cracklib.xml:105
#, xml-text
msgid ""
"Users tend to base their passwords on regular words of the spoken language, "
"and crackers know that. <application>CrackLib</application> is intended to "
"filter out such bad passwords at the source using a dictionary created from "
"word lists. To accomplish this, the word list(s) for use with "
"<application>CrackLib</application> must be an exhaustive list of words and "
"word-based keystroke combinations likely to be chosen by users of the system"
" as (guessable) passwords."
msgstr ""
"Os(As) usuários(as) tendem a basear as senhas deles(as) em palavras comuns "
"do idioma falado e os crackers sabem disso. "
"<application>CrackLib</application> destina-se a filtrar essas senhas ruins "
"na fonte usando um dicionário criado a partir de listas de palavras. Para "
"conseguir isso, a(s) lista(s) de palavras para uso com "
"<application>CrackLib</application> precisa ser uma lista exaustiva de "
"palavras e combinações de teclas baseadas em palavras que provavelmente "
"serão escolhidas pelos(as) usuários(as) do sistema como senhas "
"(adivinháveis)."

#. type: Content of: <sect1><sect2><important><para>
#: blfs-en/postlfs/security/cracklib.xml:115
#, xml-text
msgid ""
"The default word list recommended above for downloading mostly satisfies "
"this role in English-speaking countries. In other situations, it may be "
"necessary to download (or even create) additional word lists."
msgstr ""
"A lista de palavras padrão recomendada acima para download atende "
"principalmente a essa função em países de língua inglesa. Em outras "
"situações, possivelmente seja necessário baixar (ou mesmo criar) listas "
"adicionais de palavras."

#. type: Content of: <sect1><sect2><important><para>
#: blfs-en/postlfs/security/cracklib.xml:121
#, xml-text
msgid ""
"Note that word lists suitable for spell-checking are not usable as "
"<application>CrackLib</application> word lists in countries with non-Latin "
"based alphabets, because of <quote>word-based keystroke combinations</quote>"
" that make bad passwords."
msgstr ""
"Observe que as listas de palavras adequadas para verificação ortográfica não"
" podem ser usadas como listas de palavras do "
"<application>CrackLib</application> em países com alfabetos não latinos, "
"devido às <quote>combinações de teclas baseadas em palavras</quote> que "
"tornam as senhas incorretas ."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/security/cracklib.xml:131
#, xml-text
msgid "Installation of CrackLib"
msgstr "Instalação do CrackLib"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:134
#, xml-text
msgid ""
"Install <application>CrackLib</application> by running the following "
"commands:"
msgstr ""
"Instale o <application>CrackLib</application> executando os seguintes "
"comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/security/cracklib.xml:138
#, no-wrap, xml-text
msgid ""
"<userinput>CPPFLAGS+=' -I /usr/include/python3.13' \\\n"
"./configure --prefix=/usr               \\\n"
"            --disable-static            \\\n"
"            --with-default-dict=/usr/lib/cracklib/pw_dict &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>CPPFLAGS+=' -I /usr/include/python3.13' \\\n"
"./configure --prefix=/usr               \\\n"
"            --disable-static            \\\n"
"            --with-default-dict=/usr/lib/cracklib/pw_dict &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:145
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/security/cracklib.xml:148
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:151
#, xml-text
msgid ""
"Issue the following commands as the <systemitem "
"class=\"username\">root</systemitem> user to install the recommended word "
"list and create the <application>CrackLib</application> dictionary. Other "
"word lists (text based, one word per line) can also be used by simply "
"installing them into <filename "
"class=\"directory\">/usr/share/dict</filename> and adding them to the "
"<command>create-cracklib-dict</command> command."
msgstr ""
"Emita os seguintes comandos como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem> para instalar a lista de palavras "
"recomendadas e criar o dicionário <application>CrackLib</application>. "
"Outras listas de palavras (baseadas em texto, uma palavra por linha) também "
"podem ser usadas simplesmente instalando-as em <filename "
"class=\"directory\">/usr/share/dict</filename> e adicionando-as ao comando "
"<command>create-cracklib -dict</command>."

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/security/cracklib.xml:160
#, no-wrap, xml-text
msgid ""
"<userinput>xzcat ../cracklib-words-&cracklib-version;.xz \\\n"
"                       &gt; /usr/share/dict/cracklib-words       &amp;&amp;\n"
"ln -v -sf cracklib-words /usr/share/dict/words                &amp;&amp;\n"
"echo $(hostname) >>      /usr/share/dict/cracklib-extra-words &amp;&amp;\n"
"install -v -m755 -d      /usr/lib/cracklib                    &amp;&amp;\n"
"\n"
"create-cracklib-dict     /usr/share/dict/cracklib-words \\\n"
"                         /usr/share/dict/cracklib-extra-words</userinput>"
msgstr ""
"<userinput>xzcat ../cracklib-words-&cracklib-version;.xz \\\n"
"                       &gt; /usr/share/dict/cracklib-words       &amp;&amp;\n"
"ln -v -sf cracklib-words /usr/share/dict/words                &amp;&amp;\n"
"echo $(hostname) >>      /usr/share/dict/cracklib-extra-words &amp;&amp;\n"
"install -v -m755 -d      /usr/lib/cracklib                    &amp;&amp;\n"
"\n"
"create-cracklib-dict     /usr/share/dict/cracklib-words \\\n"
"                         /usr/share/dict/cracklib-extra-words</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:170
#, xml-text
msgid ""
"If desired, check the proper operation of the library as an unprivileged "
"user by issuing the following command:"
msgstr ""
"Se desejado, verifique a operação adequada da biblioteca como um(a) "
"usuário(a) não privilegiado(a) emitindo o seguinte comando:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/security/cracklib.xml:174
#, no-wrap, xml-text
msgid "<userinput>make test</userinput>"
msgstr "<userinput>make test</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:177
#, xml-text
msgid "If desired, test the Python module with:"
msgstr "Se desejado, teste o módulo Python com:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/security/cracklib.xml:180
#, no-wrap, xml-text
msgid "<userinput>python3 -c 'import cracklib; cracklib.test()'</userinput>"
msgstr "<userinput>python3 -c 'import cracklib; cracklib.test()'</userinput>"

#. type: Content of: <sect1><sect2><important><para>
#: blfs-en/postlfs/security/cracklib.xml:184
#, xml-text
msgid ""
"If you are installing <application>CrackLib</application> after your LFS "
"system has been completed and you have the <application>Shadow</application>"
" package installed, you must reinstall <xref linkend=\"shadow\"/> if you "
"wish to provide strong password support on your system. If you are now going"
" to install the <xref linkend=\"linux-pam\"/> package, you may disregard "
"this note as <application>Shadow</application> will be reinstalled after the"
" <application>Linux-PAM</application> installation."
msgstr ""
"Se você estiver instalando o <application>CrackLib</application> depois que "
"seu sistema LFS tiver sido concluído e você tiver o pacote "
"<application>Shadow</application> instalado, [então] você precisa reinstalar"
" o <xref linkend=\"shadow\"/> se desejar fornecer suporte de senha forte em "
"seu sistema. Se for instalar agora o pacote <xref linkend=\"linux-pam\"/>, "
"você pode desconsiderar essa observação, pois o "
"<application>Shadow</application> será reinstalado depois da instalação do "
"<application>Linux-PAM</application>."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/security/cracklib.xml:198
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:206
#, xml-text
msgid ""
"<envar>CPPFLAGS+=' -I /usr/include/python3.13'</envar>: This environment "
"variable is needed to allow the package to find the "
"<application>Python-3.13</application> include files."
msgstr ""
"<envar>CPPFLAGS+=' -I /usr/include/python3.13'</envar>: Essa variável de "
"ambiente é necessária para permitir que o pacote encontre os arquivos de "
"inclusão do <application>Python-3.13</application>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:213
#, xml-text
msgid ""
"<parameter>--with-default-dict=/usr/lib/cracklib/pw_dict</parameter>: This "
"parameter forces the installation of the <application>CrackLib</application>"
" dictionary to the <filename class=\"directory\">/lib</filename> hierarchy."
msgstr ""
"<parameter>--with-default-dict=/usr/lib/cracklib/pw_dict</parameter>: Esse "
"parâmetro força a instalação do dicionário "
"<application>CrackLib</application> na hierarquia <filename "
"class=\"directory\">/lib</filename>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:223
#, xml-text
msgid ""
"<command>sed ... ./python/test_cracklib.py</command>: This command updates "
"the build procedure for the Python module for "
"<application>Python-3.13</application> and later.."
msgstr ""
"<command>sed ... ./python/test_cracklib.py</command>: Esse comando atualiza "
"o procedimento de construção para o módulo Python para "
"<application>Python-3.13</application> e posteriores."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:229
#, xml-text
msgid ""
"<command>install -v -m644 -D ...</command>: This command creates the "
"<filename class=\"directory\">/usr/share/dict</filename> directory (if it "
"doesn't already exist) and installs the compressed word list there."
msgstr ""
"<command>install -v -m644 -D ...</command>: Esse comando cria o diretório "
"<filename class=\"directory\">/usr/share/dict</filename> (se já não existir)"
" e instala a lista compactada de palavras lá."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:235
#, xml-text
msgid ""
"<command>ln -v -s cracklib-words /usr/share/dict/words</command>: The word "
"list is linked to <filename>/usr/share/dict/words</filename> as "
"historically, <filename>words</filename> is the primary word list in the "
"<filename class=\"directory\">/usr/share/dict</filename> directory. Omit "
"this command if you already have a "
"<filename>/usr/share/dict/words</filename> file installed on your system."
msgstr ""
"<command>ln -v -s cracklib-words /usr/share/dict/words</command>: A lista de"
" palavras está vinculada a <filename>/usr/share/dict/words</filename> como "
"historicamente; <filename>words</filename> é a lista principal de palavras "
"no diretório <filename class=\"directory\">/usr/share/dict</filename>. Omita"
" esse comando se você já tiver um arquivo "
"<filename>/usr/share/dict/words</filename> instalado em seu sistema."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:244
#, xml-text
msgid ""
"<command>echo $(hostname) >>...</command>: The value of "
"<command>hostname</command> is echoed to a file called <filename>cracklib-"
"extra-words</filename>. This extra file is intended to be a site specific "
"list which includes easy to guess passwords such as company or department "
"names, user names, product names, computer names, domain names, etc."
msgstr ""
"<command>echo $(hostname) >>...</command>: O valor de "
"<command>hostname</command> é ecoado para um arquivo chamado "
"<filename>cracklib-extra-words</filename>. Esse arquivo extra destina-se a "
"ser uma lista específica do sítio que inclui senhas fáceis de adivinhar, "
"como nomes de organizações empresariais ou departamentos, nomes de "
"usuários(as), nomes de produtos, nomes de computadores, nomes de domínio, "
"etc."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/security/cracklib.xml:253
#, xml-text
msgid ""
"<command>create-cracklib-dict ...</command>: This command creates the "
"<application>CrackLib</application> dictionary from the word lists.  Modify "
"the command to add any additional word lists you have installed."
msgstr ""
"<command>create-cracklib-dict ...</command>: Esse comando cria o dicionário "
"<application>CrackLib</application> a partir das listas de palavras. "
"Modifique o comando para adicionar quaisquer listas adicionais de palavras "
"que você tiver instalado."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/security/cracklib.xml:261
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/postlfs/security/cracklib.xml:264
#, xml-text
msgid "Installed Programs"
msgstr "Aplicativos Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/postlfs/security/cracklib.xml:265
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/postlfs/security/cracklib.xml:266
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/postlfs/security/cracklib.xml:269
#, xml-text
msgid ""
"cracklib-check, cracklib-format, cracklib-packer, cracklib-unpacker, "
"cracklib-update, and create-cracklib-dict"
msgstr ""
"cracklib-check, cracklib-format, cracklib-packer, cracklib-unpacker, "
"cracklib-update e create-cracklib-dict"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/postlfs/security/cracklib.xml:272
#, xml-text
msgid "libcrack.so and _cracklib.so (Python module)"
msgstr "libcrack.so e _cracklib.so (módulo Python)"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/postlfs/security/cracklib.xml:274
#, xml-text
msgid "/usr/lib/cracklib, /usr/share/dict, and /usr/share/cracklib"
msgstr "/usr/lib/cracklib, /usr/share/dict e /usr/share/cracklib"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/postlfs/security/cracklib.xml:279
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/postlfs/security/cracklib.xml:280
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/security/cracklib.xml:284
#, xml-text
msgid "<command>cracklib-check</command>"
msgstr "<command>cracklib-check</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:287
#, xml-text
msgid "is used to determine if a password is strong"
msgstr "é usado para determinar se uma senha é forte"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/postlfs/security/cracklib.xml:290
#, xml-text
msgid "cracklib-check"
msgstr "cracklib-check"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/security/cracklib.xml:296
#, xml-text
msgid "<command>cracklib-format</command>"
msgstr "<command>cracklib-format</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:299
#, xml-text
msgid ""
"is used to format text files (lowercases all words, removes control "
"characters and sorts the lists)"
msgstr ""
"é usado para formatar arquivos de texto (minuscula todas as palavras, remove"
" caracteres de controle e ordena as listas)"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/postlfs/security/cracklib.xml:303
#, xml-text
msgid "cracklib-format"
msgstr "cracklib-format"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/security/cracklib.xml:309
#, xml-text
msgid "<command>cracklib-packer</command>"
msgstr "<command>cracklib-packer</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:312
#, xml-text
msgid "creates a database with words read from standard input"
msgstr ""
"cria uma base de dados com palavras lidas a partir da entrada gerada padrão"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/postlfs/security/cracklib.xml:315
#: blfs-en/postlfs/security/cracklib.xml:327
#, xml-text
msgid "cracklib-packer"
msgstr "cracklib-packer"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/security/cracklib.xml:321
#, xml-text
msgid "<command>cracklib-unpacker</command>"
msgstr "<command>cracklib-unpacker</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:324
#, xml-text
msgid "displays on standard output the database specified"
msgstr "exibe na saída gerada padrão a base de dados especificada"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/security/cracklib.xml:333
#, xml-text
msgid "<command>create-cracklib-dict</command>"
msgstr "<command>create-cracklib-dict</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:336
#, xml-text
msgid ""
"is used to create the <application>CrackLib</application> dictionary from "
"the given word list(s)"
msgstr ""
"é usado para criar o dicionário <application>CrackLib</application> a partir"
" da(s) lista(s) fornecida(s) de palavras"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/postlfs/security/cracklib.xml:340
#, xml-text
msgid "create-cracklib-dict"
msgstr "create-cracklib-dict"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/security/cracklib.xml:346
#, xml-text
msgid "<filename class=\"libraryfile\">libcrack.so</filename>"
msgstr "<filename class=\"libraryfile\">libcrack.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/security/cracklib.xml:349
#, xml-text
msgid ""
"provides a fast dictionary lookup method for strong password enforcement"
msgstr ""
"fornece um método rápido de pesquisa de dicionário para imposição de senha "
"forte"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/postlfs/security/cracklib.xml:353
#, xml-text
msgid "libcrack.so"
msgstr "libcrack.so"

#, xml-text
#~ msgid "a99e0aef4c677df7063624690b634988"
#~ msgstr "a99e0aef4c677df7063624690b634988"

#, xml-text
#~ msgid ""
#~ "<command>autoreconf</command>: This command resets the configuration to "
#~ "recognize <application>Python-3.13</application>."
#~ msgstr ""
#~ "<command>autoreconf</command>: Esse comando redefine a configuração para "
#~ "reconhecer <application>Python-3.13</application>."

#, xml-text
#~ msgid ""
#~ "<command>autoreconf -fiv</command>: The configure script shipped with the "
#~ "package is too old to get the right version string of Python 3.10 or later. "
#~ "This command regenerates it with a more recent version of autotools, which "
#~ "fixes the issue."
#~ msgstr ""
#~ "<command>autoreconf -fiv</command>: O conjunto de comandos sequenciais de "
#~ "configuração fornecido com o pacote é muito antigo para obter a sequência "
#~ "correta de caracteres de versão do Python 3.10 ou posterior. Esse comando o "
#~ "regenera com uma versão mais recente do autotools, que corrige o problema."

#, xml-text
#~ msgid ""
#~ "<envar>PYTHON=python3</envar>: This forces the installation of python "
#~ "bindings for Python 3, even if Python 2 is installed."
#~ msgstr ""
#~ "<envar>PYTHON=python3</envar>: Isso força a instalação de vínculos python "
#~ "para Python 3, mesmo se o Python 2 estiver instalado."

#, xml-text
#~ msgid "7830a17c41ab732838161f91b122fffd"
#~ msgstr "7830a17c41ab732838161f91b122fffd"

#, xml-text
#~ msgid "77dcff93b42fa07aa5d43b159612e320"
#~ msgstr "77dcff93b42fa07aa5d43b159612e320"

#, xml-text
#~ msgid "464 KB"
#~ msgstr "464 KB"

#, xml-text
#~ msgid "4.3 MB"
#~ msgstr "4,3 MB"

#, xml-text
#~ msgid "a6dfb1766aab43a54e1cbd78abf0a20a"
#~ msgstr "a6dfb1766aab43a54e1cbd78abf0a20a"

#, xml-text
#~ msgid "452 KB"
#~ msgstr "452 KB"

#, xml-text
#~ msgid "6.8 MB"
#~ msgstr "6,8 MB"

#, xml-text
#~ msgid "0.1 SBU"
#~ msgstr "0,1 UPC"

#, xml-text
#~ msgid "a242301bad13421476db8eecbbc9536a"
#~ msgstr "a242301bad13421476db8eecbbc9536a"

#, xml-text
#~ msgid "600 KB"
#~ msgstr "600 KB"

#, xml-text
#~ msgid "6.7 MB"
#~ msgstr "6,7 MB"

#, xml-text
#~ msgid "94e9963e4786294f7fb0f2efd7618551"
#~ msgstr "94e9963e4786294f7fb0f2efd7618551"

#, xml-text
#~ msgid ""
#~ "Recommended word list for English-speaking countries (size: &crackdict-"
#~ "size;; md5sum: &crackdict-md5sum;): <ulink url=\"&crackdict-download;\"/>"
#~ msgstr ""
#~ "Lista das palavras recomendadas para países de língua inglesa (tamanho: "
#~ "&crackdict-size;; soma de verificação md5: &crackdict-md5sum;): <ulink "
#~ "url=\"&crackdict-download;\"/>"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/cracklib\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/cracklib\"/>"

#, xml-text
#~ msgid ""
#~ "<command>sed -i '/skipping/d' util/packer.c</command>: Remove a meaningless "
#~ "warning."
#~ msgstr ""
#~ "<command>sed -i '/skipping/d' util/packer.c</command>: Remove um aviso sem "
#~ "significado."
