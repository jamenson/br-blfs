#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-24 19:59+0000\n"
"PO-Revision-Date: 2025-02-01 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/postlfs_editors_mousepad/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.7.2\n"

#. type: Content of the mousepad-md5sum entity
#: blfs-en/postlfs/editors/mousepad.xml:10
#, xml-text
msgid "6e46d6a22e0656fbddf2655d1e9dfc1f"
msgstr "6e46d6a22e0656fbddf2655d1e9dfc1f"

#. type: Content of the mousepad-size entity
#: blfs-en/postlfs/editors/mousepad.xml:11
#, xml-text
msgid "1.4 MB"
msgstr "1,4 MB"

#. type: Content of the mousepad-buildsize entity
#: blfs-en/postlfs/editors/mousepad.xml:12
#, xml-text
msgid "16 MB"
msgstr "16 MB"

#. type: Content of the mousepad-time entity
#: blfs-en/postlfs/editors/mousepad.xml:13
#, xml-text
msgid "0.1 SBU"
msgstr "0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/postlfs/editors/mousepad.xml:16
#: blfs-en/postlfs/editors/mousepad.xml:20
#, xml-text
msgid "Mousepad-&mousepad-version;"
msgstr "Mousepad-&mousepad-version;"

#. type: Content of: <sect1><indexterm><primary>
#: blfs-en/postlfs/editors/mousepad.xml:23
#, xml-text
msgid "Mousepad"
msgstr "Mousepad"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/editors/mousepad.xml:27
#, xml-text
msgid "Introduction to Mousepad"
msgstr "Introdução ao Mousepad"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:30
#, xml-text
msgid ""
"<application>Mousepad</application> is a simple <application>GTK+ "
"3</application> text editor for the <application>Xfce</application> desktop "
"environment."
msgstr ""
"<application>Mousepad</application> é um editor simples de texto "
"<application>GTK+ 3</application> para o ambiente de área de trabalho "
"<application>Xfce</application>."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/editors/mousepad.xml:37
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:41
#, xml-text
msgid "Download (HTTP): <ulink url=\"&mousepad-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&mousepad-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:46
#, xml-text
msgid "Download (FTP): <ulink url=\"&mousepad-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&mousepad-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:51
#, xml-text
msgid "Download MD5 sum: &mousepad-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &mousepad-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:56
#, xml-text
msgid "Download size: &mousepad-size;"
msgstr "Tamanho da transferência: &mousepad-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:61
#, xml-text
msgid "Estimated disk space required: &mousepad-buildsize;"
msgstr "Espaço em disco estimado exigido: &mousepad-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:66
#, xml-text
msgid "Estimated build time: &mousepad-time;"
msgstr "Tempo de construção estimado: &mousepad-time;"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/editors/mousepad.xml:71
#, xml-text
msgid "Mousepad Dependencies"
msgstr "Dependências do Mousepad"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/editors/mousepad.xml:73
#, xml-text
msgid "Required"
msgstr "Exigidas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:75
#, xml-text
msgid "<xref linkend=\"gtksourceview4\"/> and <xref linkend=\"libxfce4ui\"/>"
msgstr "<xref linkend=\"gtksourceview4\"/> e <xref linkend=\"libxfce4ui\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/editors/mousepad.xml:79
#, xml-text
msgid "Recommended"
msgstr "Recomendadas"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:81
#, xml-text
msgid "<xref linkend=\"gspell\"/>"
msgstr "<xref linkend=\"gspell\"/>"

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/postlfs/editors/mousepad.xml:84
#, xml-text
msgid "Optional"
msgstr "Opcionais"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:86
#, xml-text
msgid ""
"<xref role=\"runtime\" linkend='dconf'/> (runtime) and <xref linkend=\"dbus-"
"glib\"/>"
msgstr ""
"<xref role=\"runtime\" linkend='dconf'/> (tempo de execução) e <xref "
"linkend=\"dbus-glib\"/>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/editors/mousepad.xml:93
#, xml-text
msgid "Installation of Mousepad"
msgstr "Instalação do Mousepad"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:96
#, xml-text
msgid ""
"Install <application>Mousepad</application> by running the following "
"commands:"
msgstr ""
"Instale <application>Mousepad</application> executando os seguintes "
"comandos:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/editors/mousepad.xml:100
#, no-wrap, xml-text
msgid ""
"<userinput>./configure --prefix=/usr           \\\n"
"            --enable-gtksourceview4 \\\n"
"            --enable-keyfile-settings &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>./configure --prefix=/usr           \\\n"
"            --enable-gtksourceview4 \\\n"
"            --enable-keyfile-settings &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:106
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:110
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) <systemitem "
"class=\"username\">root</systemitem>:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/postlfs/editors/mousepad.xml:113
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/editors/mousepad.xml:118
#, xml-text
msgid "Command Explanations"
msgstr "Explicações do Comando"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:121
#, xml-text
msgid ""
"<parameter>--enable-keyfile-settings</parameter>: Use the GSettings keyfile "
"backend rather than the default <xref linkend='dconf'/>."
msgstr ""
"<parameter>--enable-keyfile-settings</parameter>: Use a estrutura de "
"retaguarda do arquivo de chaves GSettings em vez do padrão <xref "
"linkend='dconf'/>."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/postlfs/editors/mousepad.xml:126
#, xml-text
msgid ""
"<option>--disable-plugin-gspell</option>: Use this option to disable "
"building the <xref linkend=\"gspell\"/> plugin if you have not installed "
"<application>gspell</application>."
msgstr ""
"<option>--disable-plugin-gspell</option>: Use essa opção para desabilitar "
"construir o plugin <xref linkend=\"gspell\"/> se você não tiver instalado o "
"<application>gspell</application>."

#. type: Content of: <sect1><sect2><title>
#: blfs-en/postlfs/editors/mousepad.xml:134
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/postlfs/editors/mousepad.xml:137
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/postlfs/editors/mousepad.xml:138
#, xml-text
msgid "Installed Libraries"
msgstr "Bibliotecas Instaladas"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/postlfs/editors/mousepad.xml:139
#, xml-text
msgid "Installed Directories"
msgstr "Diretórios Instalados"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/postlfs/editors/mousepad.xml:143
#: blfs-en/postlfs/editors/mousepad.xml:166
#, xml-text
msgid "mousepad"
msgstr "mousepad"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/postlfs/editors/mousepad.xml:146
#: blfs-en/postlfs/editors/mousepad.xml:149
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/postlfs/editors/mousepad.xml:155
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/postlfs/editors/mousepad.xml:156
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/postlfs/editors/mousepad.xml:160
#, xml-text
msgid "<command>mousepad</command>"
msgstr "<command>mousepad</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/postlfs/editors/mousepad.xml:163
#, xml-text
msgid "is a simple <application>GTK+ 3</application> text editor"
msgstr "é um editor simples <application>GTK+ 3</application> de texto"

#, xml-text
#~ msgid "3ad46198202d2696cac27d5a0f08bab0"
#~ msgstr "3ad46198202d2696cac27d5a0f08bab0"

#, xml-text
#~ msgid "1.3 MB"
#~ msgstr "1,3 MB"

#, xml-text
#~ msgid "4c6cde9a05c6f8048133d8d4137edca9"
#~ msgstr "4c6cde9a05c6f8048133d8d4137edca9"

#, xml-text
#~ msgid "937.2 KB"
#~ msgstr "937,2 KB"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/mousepad\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/mousepad\"/>"
