# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-07-01 16:38+0000\n"
"PO-Revision-Date: 2024-07-11 16:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch/networking_netprogs_wireless-kernel/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Content of: <sect1><title>
#: blfs-en/networking/netprogs/wireless-kernel.xml:8
#: blfs-en/networking/netprogs/wireless-kernel.xml:12
#, xml-text
msgid "Configuring the Linux Kernel for Wireless"
msgstr "Configurando o Núcleo Linux para Sem Fios"

#. type: Content of: <sect1><para>
#: blfs-en/networking/netprogs/wireless-kernel.xml:15
#, xml-text
msgid ""
"Before using any userspace tools for connecting to a wireless AP, the Linux "
"kernel must be configured to drive the wireless NIC properly.  Enable the "
"following options in the kernel configuration as well as specific device "
"drivers for your hardware and recompile the kernel if necessary:"
msgstr ""
"Antes de usar qualquer ferramenta de espaço do(a) usuário(a) para conectar-"
"se a um ponto de acesso sem fio, o núcleo Linux precisa ser configurado para"
" controlar a NIC sem fio corretamente. Habilite as seguintes opções na "
"configuração do núcleo, bem como controladores específicos de dispositivos "
"para o teu hardware e recompile o núcleo, se necessário:"

#. type: Content of: <sect1><para>
#: blfs-en/networking/netprogs/wireless-kernel.xml:26
#, xml-text
msgid ""
"Open the <quote>Wireless LAN</quote> submenu and select the options that "
"support your hardware. <command>lspci</command> from <xref "
"linkend=\"pciutils\"/> or <command>lsusb</command> from <xref "
"linkend=\"usbutils\"/> can be used to view your hardware configuration.  "
"Note that many (though not all)  options for the wireless NICs depend on "
"<parameter>CONFIG_MAC80211</parameter>. After the correct drivers are "
"loaded, the interface will appear in <filename>/sys/class/net</filename>, or"
" in the output of the <command>ip link</command> command."
msgstr ""
"Abra o submenu <quote>Wireless LAN</quote> e selecione as opções que "
"suportam teu hardware. <command>lspci</command> oriundo de <xref "
"linkend=\"pciutils\"/> ou <command>lsusb</command> oriundo de <xref "
"linkend=\"usbutils\"/> podem ser usados para visualizar tua configuração de "
"hardware. Observe que muitas (embora nem todas) opções para as NICs sem fio "
"dependem de <parameter>CONFIG_MAC80211</parameter>. Depois que os "
"controladores corretos forem carregados, a interface aparecerá em "
"<filename>/sys/class/net</filename> ou na saída gerada do comando "
"<command>ip link</command>."

#. type: Content of: <sect1><para>
#: blfs-en/networking/netprogs/wireless-kernel.xml:39
#, xml-text
msgid ""
"Many wireless NIC drivers require firmware. If you've enabled the correct "
"driver in the kernel configuration but it fails to load (with messages like "
"<computeroutput>Direct firmware load for "
"<replaceable>&lt;filename</replaceable>&gt; failed with error "
"-2</computeroutput>, it means that you need to install the firmware or the "
"wireless NIC won't work. Read <xref linkend='postlfs-firmware'/> for more "
"details."
msgstr ""
"Muitos controladores de NIC sem fio exigem firmware. Se você tiver "
"habilitado o controlador correto na configuração do núcleo, mas ele falhar "
"para carregar (com mensagens como <computeroutput>Direct firmware load for "
"<replaceable>&lt;nome_arquivo</replaceable>&gt; failed with error "
"-2</computeroutput>, significa que você precisa instalar o firmware ou a NIC"
" sem fio não funcionará. Leia-se <xref linkend='postlfs-firmware'/> para "
"mais detalhes."
