#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-01 14:57+0000\n"
"PO-Revision-Date: 2024-06-24 01:48+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/beyond-linux-from-scratch-12-1/networking_netlibs_libndp/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.5.5\n"

#. type: Content of the libndp-download-http entity
#: blfs-en/networking/netlibs/libndp.xml:7
#, xml-text
msgid "http://libndp.org/files/libndp-&libndp-version;.tar.gz"
msgstr "http://libndp.org/files/libndp-&libndp-version;.tar.gz"

#. type: Content of the libndp-md5sum entity
#: blfs-en/networking/netlibs/libndp.xml:9
#, xml-text
msgid "9d486750569e7025e5d0afdcc509b93c"
msgstr "9d486750569e7025e5d0afdcc509b93c"

#. type: Content of the libndp-size entity
#: blfs-en/networking/netlibs/libndp.xml:10
#, xml-text
msgid "368 KB"
msgstr "368 KB"

#. type: Content of the libndp-buildsize entity
#: blfs-en/networking/netlibs/libndp.xml:11
#, xml-text
msgid "2.5 MB"
msgstr "2,5 MB"

#. type: Content of the libndp-time entity
#: blfs-en/networking/netlibs/libndp.xml:12
#, xml-text
msgid "less than 0.1 SBU"
msgstr "menos que 0,1 UPC"

#. type: Content of: <sect1><title>
#: blfs-en/networking/netlibs/libndp.xml:15
#: blfs-en/networking/netlibs/libndp.xml:19
#, xml-text
msgid "libndp-&libndp-version;"
msgstr "libndp-&libndp-version;"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/networking/netlibs/libndp.xml:22
#: blfs-en/networking/netlibs/libndp.xml:137
#, xml-text
msgid "libndp"
msgstr "libndp"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/networking/netlibs/libndp.xml:26
#, xml-text
msgid "Introduction to libndp"
msgstr "Introdução ao libndp"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/networking/netlibs/libndp.xml:29
#, xml-text
msgid ""
"The <application>libndp</application> package provides a wrapper for IPv6 "
"Neighbor Discovery Protocol. It also provides a tool named ndptool for "
"sending and receiving NDP messages."
msgstr ""
"O pacote &quot;<application>libndp</application>&quot; fornece um envólucro "
"para o &quot;Neighbor Discovery Protocol&quot; do &quot;IPv6&quot;. Ele "
"também fornece uma ferramenta chamada &quot;ndptool&quot; para enviar e "
"receber mensagens &quot;NDP&quot;."

#. type: Content of: <sect1><sect2><bridgehead>
#: blfs-en/networking/netlibs/libndp.xml:36
#, xml-text
msgid "Package Information"
msgstr "Informação do Pacote"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:40
#, xml-text
msgid "Download (HTTP): <ulink url=\"&libndp-download-http;\"/>"
msgstr "Transferência (HTTP): <ulink url=\"&libndp-download-http;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:45
#, xml-text
msgid "Download (FTP): <ulink url=\"&libndp-download-ftp;\"/>"
msgstr "Transferência (FTP): <ulink url=\"&libndp-download-ftp;\"/>"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:50
#, xml-text
msgid "Download MD5 sum: &libndp-md5sum;"
msgstr "Soma de verificação MD5 da transferência: &libndp-md5sum;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:55
#, xml-text
msgid "Download size: &libndp-size;"
msgstr "Tamanho da transferência: &libndp-size;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:60
#, xml-text
msgid "Estimated disk space required: &libndp-buildsize;"
msgstr "Espaço em disco estimado exigido: &libndp-buildsize;"

#. type: Content of: <sect1><sect2><itemizedlist><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:65
#, xml-text
msgid "Estimated build time: &libndp-time;"
msgstr "Tempo de construção estimado: &libndp-time;"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/networking/netlibs/libndp.xml:73
#, xml-text
msgid "Installation of libndp"
msgstr "Instalação do libndp"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/networking/netlibs/libndp.xml:76
#, xml-text
msgid ""
"Install <application>libndp</application> by running the following command:"
msgstr ""
"Instale o &quot;<application>libndp</application>&quot; executando o "
"seguinte comando:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/networking/netlibs/libndp.xml:80
#, no-wrap, xml-text
msgid ""
"<userinput>./configure --prefix=/usr        \\\n"
"            --sysconfdir=/etc    \\\n"
"            --localstatedir=/var \\\n"
"            --disable-static     &amp;&amp;\n"
"make</userinput>"
msgstr ""
"<userinput>./configure --prefix=/usr       \\\n"
"            --sysconfdir=/etc    \\\n"
"            --localstatedir=/var \\\n"
"            --disable-static     &amp;&amp;\n"
"make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: blfs-en/networking/netlibs/libndp.xml:87
#, xml-text
msgid "This package does not come with a test suite."
msgstr "Esse pacote não vem com uma suíte de teste."

#. type: Content of: <sect1><sect2><para>
#: blfs-en/networking/netlibs/libndp.xml:91
#, xml-text
msgid "Now, as the <systemitem class=\"username\">root</systemitem> user:"
msgstr ""
"Agora, como o(a) usuário(a) &quot;<systemitem "
"class=\"username\">root</systemitem>&quot;:"

#. type: Content of: <sect1><sect2><screen>
#: blfs-en/networking/netlibs/libndp.xml:94
#, no-wrap, xml-text
msgid "<userinput>make install</userinput>"
msgstr "<userinput>make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: blfs-en/networking/netlibs/libndp.xml:99
#, xml-text
msgid "Contents"
msgstr "Conteúdo"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/networking/netlibs/libndp.xml:102
#, xml-text
msgid "Installed Program"
msgstr "Aplicativo Instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/networking/netlibs/libndp.xml:103
#, xml-text
msgid "Installed Library"
msgstr "Biblioteca Instalada"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: blfs-en/networking/netlibs/libndp.xml:104
#, xml-text
msgid "Installed Directory"
msgstr "Diretório Instalado"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: blfs-en/networking/netlibs/libndp.xml:107
#: blfs-en/networking/netlibs/libndp.xml:125
#, xml-text
msgid "ndptool"
msgstr "ndptool"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/networking/netlibs/libndp.xml:108
#, xml-text
msgid "libndp.so"
msgstr "libndp.so"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: blfs-en/networking/netlibs/libndp.xml:109
#, xml-text
msgid "None"
msgstr "Nenhum(a)"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: blfs-en/networking/netlibs/libndp.xml:114
#, xml-text
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: blfs-en/networking/netlibs/libndp.xml:115
#, xml-text
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/networking/netlibs/libndp.xml:119
#, xml-text
msgid "<command>ndptool</command>"
msgstr "<command>ndptool</command>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:122
#, xml-text
msgid "is a tool for sending and receiving NDP messages"
msgstr "é uma ferramenta para enviar e receber mensagens &quot;NDP&quot;"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: blfs-en/networking/netlibs/libndp.xml:131
#, xml-text
msgid "<filename class=\"libraryfile\">libndp.so</filename>"
msgstr "<filename class=\"libraryfile\">libndp.so</filename>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: blfs-en/networking/netlibs/libndp.xml:134
#, xml-text
msgid "provides a wrapper for the IPv6 Neighbor Discovery Protocol"
msgstr ""
"fornece um envólucro para o &quot;Neighbor Discovery Protocol&quot; do "
"&quot;IPv6&quot;"

#, xml-text
#~ msgid "c7e775fd5a9d676e8cba9c3732c4df93"
#~ msgstr "c7e775fd5a9d676e8cba9c3732c4df93"

#, xml-text
#~ msgid "360 KB"
#~ msgstr "360 KB"

#, xml-text
#~ msgid "User Notes: <ulink url=\"&blfs-wiki;/libndp\"/>"
#~ msgstr "Observações de Usuário(a): <ulink url=\"&blfs-wiki;/libndp\"/>"
