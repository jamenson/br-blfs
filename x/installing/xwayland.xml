<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY xwayland-download-http "&xorg-download-http;/xserver/xwayland-&xwayland-version;.tar.xz">
  <!ENTITY xwayland-download-ftp  "">
  <!ENTITY xwayland-md5sum        "78067c218323fe2a496ca5f2145fe7ab">
  <!ENTITY xwayland-size          "1,2 MB">
  <!ENTITY xwayland-buildsize     "28 MB (adicionar 362 MB para os testes)">
  <!ENTITY xwayland-time          "0,2 UPC (com paralelismo=4; adicionar 1,7 UPC para testes, não incluindo o
tempo de clonagem)">
]>

<sect1 id="xwayland" xreflabel="Xwayland-&xwayland-version;">
  <?dbhtml filename="xwayland.html"?>


  <title>Xwayland-&xwayland-version;</title>

  <indexterm zone="xwayland">
    <primary sortas="a-xwayland">xwayland</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Xwayland</title>

    <para>
      O pacote &quot;<application>Xwayland</application>&quot; é um servidor
&quot;Xorg&quot; executando sobre o servidor &quot;wayland&quot;. Ele foi
separado do pacote principal do servidor &quot;Xorg&quot;. Permite executar
clientes do &quot;X&quot; dentro de uma sessão &quot;wayland&quot;.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&xwayland-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&xwayland-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &xwayland-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &xwayland-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &xwayland-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &xwayland-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do &quot;Xwayland&quot;</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="libxcvt"/>, <xref linkend="pixman"/>, <xref
linkend="wayland-protocols"/>, <xref role='runtime' linkend="xorg7-app"/>
(tempo de execução) e <xref linkend="xorg7-font"/> (somente font-util)
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="libepoxy"/>, <xref linkend="libtirpc"/> e <xref
linkend="mesa"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="git"/> (para baixar pacotes necessários para os testes),
<xref linkend="libei"/>, <xref linkend="libgcrypt"/>, <xref
linkend="nettle"/>, <xref linkend="xmlto"/>, <xref linkend="xorg7-legacy"/>
(somente bdftopcf, para construir fontes exigidas para os testes), <ulink
url="https://gitlab.freedesktop.org/xorg/test/rendercheck">rendercheck</ulink>
(para testes) e <ulink
url="https://wayland.pages.freedesktop.org/weston/">weston</ulink> (para
testes)
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Xwayland</title>

    <para>
      Instale o &quot;<application>xwayland</application>&quot; executando os
seguintes comandos:
    </para>

<screen><userinput>sed -i '/install_man/,$d' meson.build &amp;&amp;

mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..                       \
      --prefix=$XORG_PREFIX          \
      --buildtype=release            \
      -D xkb_output_dir=/var/lib/xkb &amp;&amp;
ninja</userinput></screen>

    <para>
      <!-- keep 22.1 above: they used to build it in gitlab-ci because debian
     had an obsolete version of weston, but now they take weston from
     debian so the command to build it has been removed. -->
Construir a estrutura de teste precisa de algum trabalho. Primeiro, <ulink
url="https://wayland.pages.freedesktop.org/weston/">weston</ulink> traz
várias dependências, mas o número pode ser reduzido desabilitando recursos
desnecessários. O comando <command>meson</command> para uma construção
simplificada do <application>weston</application> é mostrado em <ulink
url="https://gitlab.freedesktop.org/xorg/xserver/-/blob/xwayland-22.1/.gitlab-ci/debian-install.sh">
construção da integração contínua do(a) desenvolvedor(a)</ulink>.

    </para>

    <para>
      Executar os testes envolve transferir outras duas estruturas, em adição às
mencionadas dependências opcionais:
    </para>

<screen remap="test"><userinput>mkdir tools &amp;&amp;
pushd tools &amp;&amp;

git clone https://gitlab.freedesktop.org/mesa/piglit.git --depth 1 &amp;&amp;
cat &gt; piglit/piglit.conf &lt;&lt; EOF                                    &amp;&amp;
<literal>[xts]
path=$(pwd)/xts
EOF</literal>

git clone https://gitlab.freedesktop.org/xorg/test/xts --depth 1   &amp;&amp;

export DISPLAY=:22           &amp;&amp;
../hw/vfb/Xvfb $DISPLAY &amp;
VFB_PID=$!                   &amp;&amp;
cd xts                      &amp;&amp;
CFLAGS=-fcommon ./autogen.sh &amp;&amp;
make                         &amp;&amp;
kill $VFB_PID                &amp;&amp;
unset DISPLAY VFB_PID        &amp;&amp;
popd</userinput></screen>

    <para>
      Então os testes podem ser executados com:
    </para>

<!-- The xfontset tests fail on my system -renodr -->
<screen remap="test"><userinput>XTEST_DIR=$(pwd)/tools/xts PIGLIT_DIR=$(pwd)/tools/piglit ninja test</userinput></screen>

    
    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root" revision="sysv"><userinput>ninja install &amp;&amp;
cat &gt;&gt; /etc/sysconfig/createfiles &lt;&lt; "EOF"
<literal>/tmp/.X11-unix dir 1777 root root</literal>
EOF</userinput></screen>

<screen role="root" revision="systemd"><userinput>ninja install</userinput></screen>

    <para>
      Se o &quot;<xref linkend='xorg-server'/>&quot; não estiver instalado e você
não planeja instalá-lo mais tarde, [então] você pode instalar o
&quot;<command>Xvfb</command>&quot; a partir deste pacote. Como o(a)
usuário(a) &quot;&root;&quot;:
    </para>

<screen role="nodump"><userinput>install -vm755 hw/vfb/Xvfb /usr/bin</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command>sed -i '/install_man/,$d' meson.build</command>: Impede a
instalação de uma página de manual para o
&quot;<command>Xserver</command>&quot;, que também é fornecida pelo
&quot;<xref linkend='xorg-server'/>&quot;. Remova esse comando se o
&quot;<xref linkend='xorg-server'/>&quot; não estiver instalado e você não
planejar instalá-lo mais tarde.
    </para>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <option>-D secure-rpc=false</option>: Essa opção desabilita construir
suporte RPC se <xref linkend="libtirpc"/> não estiver instalado.
    </para>

    <para revision="sysv">
      <command>cat &gt;&gt; /etc/sysconfig/createfiles...</command>: Esse comando
cria o diretório &quot;<filename
class="directory">/tmp/.X11-unix</filename>&quot; na inicialização e garante
que as permissões e titularidade da propriedade estejam corretas conforme
exigido pelos aplicativos que usam o &quot;Xwayland&quot;.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          Xwayland
        </seg>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          Nenhum(a)
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="Xwayland">
        <term><command>Xwayland</command></term>
        <listitem>
          <para>
            Permite que clientes do &quot;X&quot; executem sob o &quot;wayland&quot;
          </para>
          <indexterm zone="xwayland Xwayland">
            <primary sortas="b-Xwayland">Xwayland</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
