<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY mesa-download-http "https://mesa.freedesktop.org/archive/mesa-&mesa-version;.tar.xz">
  <!ENTITY mesa-download-ftp  "">
  <!ENTITY mesa-md5sum        "c64b7e2b4f1c7782c41bf022edbb365c">
  <!ENTITY mesa-size          "30 MB">
  <!ENTITY mesa-buildsize     "1,3 GB (com documentos, adicionar 569 MB para testes)">
  <!ENTITY mesa-time          "3,8 UPC (com documentos; adicionar 1,2 UPC para testes; ambos com
paralelismo=4)">
]>

<sect1 id="mesa" xreflabel="Mesa-&mesa-version;">
  <?dbhtml filename="mesa.html"?>

  <title>Mesa-&mesa-version;</title>

  <indexterm zone="mesa">
    <primary sortas="a-Mesa">Mesa</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Mesa</title>

    <para>
      <application>Mesa</application> é uma biblioteca gráfica 3D compatível com
OpenGL.
    </para>

    <note>
      <para>
        <application>Mesa</application> é atualizada relativamente muitas
vezes. Você possivelmente queira usar a versão &mesa-major-minor;.x do Mesa
mais recente disponível.
      </para>
    </note>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&mesa-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&mesa-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &mesa-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &mesa-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &mesa-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &mesa-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>
    <itemizedlist spacing="compact">
    
      <!-- 9 Apr 23.  Change to unversioned patch since it never changes. -->
<listitem>
        <para>
          Remendo recomendado: <ulink url="&patch-root;/mesa-add_xdemos-4.patch"/>
(instala dois programas de demonstração para testagem do Mesa - não
necessário se você instalar o pacote <ulink
url="https://archive.mesa3d.org/demos/"> mesa-demos</ulink>)
        </para>
      </listitem>
      
    <!--
      <listitem>

        <para>
          Recommended patch:
          <ulink url="&patch-root;/mesa-&mesa-version;-seizure_prevention-1.patch"/>
          (prevents severe flashing when using
          <application>Firefox</application> and
          <application>Thunderbird</application> with the Zink gallium driver)
        </para>
      </listitem>
      -->
</itemizedlist>

    <bridgehead renderas="sect3">Dependências do Mesa</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="xorg7-lib"/>, <xref linkend="libdrm"/>, <xref
linkend="Mako"/> e <xref linkend="PyYAML"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">

      <itemizedlist spacing="compact">
        <listitem>
          <para>
            <xref linkend="glslang"/> (exigido para suporte a Vulkan)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="libva"/> (para fornecer suporte VA-API para alguns
controladores gallium. Observe que existe uma dependência circular. Você
precisa construir <application>libva</application> primeiro sem suporte EGL
e GLX do mesa, instalar esse pacote e reconstruir
<application>libva</application>)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="libvdpau"/> (para construir controladores VDPAU)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="llvm"/> (exigido para o llvmpipe, r300, r600, e controladores
radeonsi)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="wayland-protocols"/> (exigido para <xref role="nodep"
linkend='plasma-build'/>, GNOME e recomendado para <xref role="nodep"
linkend='gtk3'/>)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="libclc"/> (exigido para o controlador iris gallium da Intel)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="vulkan-loader"/> (exigido para suporte a Vulkan)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="ply"/> (exigido para o controlador vulkan da Intel)
          </para>
        </listitem>
        <listitem>
          <para>
            <xref linkend="cbindgen"/>, <xref linkend="make-ca"/> e <xref
linkend="rust-bindgen"/> (exigido para o controlador Nouveau Vulkan)
          </para>
        </listitem>
      </itemizedlist>
    </para>

    <note>
      <para>
        Uma conexão de Internet é necessária para construir o controlador Nouveau
Vulkan.
      </para>
    </note>

    <note>
      <para>
        <emphasis role="bold">Escolhendo Controladores Mesa</emphasis>
      </para>

      <para>
        Nas instruções abaixo, todos os controladores disponíveis são
construídos. Isso quase sempre funcionará. No entanto, não é
eficiente. Dependendo do teu hardware de vídeo, você provavelmente precisará
somente de controladores específicos.
      </para>

      <para>
        A primeira coisa que você precisa saber é qual tipo de dispositivo de vídeo
você tem. Em alguns casos, ele está construído na CPU. Em outros, é uma
placa PCI separada. Em ambos os casos, você pode saber qual hardware de
vídeo tem instalando <xref linkend='pciutils'/> e executando:
      </para>

      <screen role='nodump'><userinput>lspci | grep VGA</userinput></screen>

      <para>
        O dispositivo de vídeo é provavelmente uma das três famílias: AMD, Intel ou
NVIDIA. Vejam-se as Explicações do Comando para <parameter>-D
gallium-drivers=auto</parameter> abaixo para ver quais opções estão
disponíveis para teu hardware de vídeo específico (ou hardware emulado de
vídeo). Você provavelmente deveria adicionar a opção softpipe ou llvmpipe
como um controlador de apoio.
      </para>

      <para>
        Para 'platforms' você pode selecionar x11 e (ou) wayland. Observe que
atualmente no BLFS somente o Gnome e o KDE podem usar o wayland. Se você não
for usar um desses ambientes de área de trabalho, então provavelmente
desejará somente o x11.
      </para>

      <para>
        Para 'vulkan-drivers' você pode querer limitar a seleção ao teu hardware
atual. Esses controladores são usados por alguns aplicativos
específicos. Por exemplo, ffmpeg (incluindo ffplay) usará Vulkan em vez de
OpenGL para renderizar vídeo na GPU. Se você não quiser fazer isso, você
pode ou não querer instalá-los. Vejam-se as Explicações do Comando para
<parameter>-D vulkan-drivers=auto</parameter> abaixo para ver quais opções
estão disponíveis para teu hardware de vídeo específico (ou hardware emulado
de vídeo). Você provavelmente deveria adicionar a opção swrast como um
controlador residual, a menos que não precise do Vulkan.
      </para> 
    </note>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <!-- for libsensors according to Meson -->
<!--<ulink url="https://github.com/KhronosGroup/glslang">
glslang</ulink>
      (for vulkan drivers),-->
<!-- <ulink url="https://github.com/KhronosGroup/Vulkan-Loader">
Vulkan-Loader</ulink> -->
<xref linkend="libgcrypt"/>, <xref linkend="libunwind"/>, <xref
linkend="lm_sensors"/>, <xref linkend="nettle"/>, <xref
linkend="valgrind"/>, <ulink
url="https://archive.mesa3d.org/demos/">mesa-demos</ulink> (fornece mais que
300 extra demos para testar <application>Mesa</application>; isso inclui os
mesmos programas adicionados pelo remendo acima), <ulink
url="https://omxil.sourceforge.net/">Bellagio OpenMAX Integration
Layer</ulink> (para plataformas móveis) e <ulink
url="https://github.com/tizonia/tizonia-openmax-il/wiki/Tizonia-OpenMAX-IL/">libtizonia</ulink>,
      
    </para>


  <!--
    <note>

      <para>
        The instructions below assume that
        <application>LLVM</application> with the r600/amdgpu and host backends
        and run-time type information (RTTI - needed for nouveau) are installed.
        You will need to modify the instructions if you
        choose not to install all of these. For an explanation of Gallium3D see
        <ulink url="https://en.wikipedia.org/wiki/Gallium3D"/>.
      </para>
    </note>
-->
</sect2>

  <sect2 role="kernel" id="mesa-kernel"
         xreflabel='Configuração de Núcleo do Mesa'>
    <title>Configuração do Núcleo</title>

    <para>
      Habilite as seguintes opções na configuração do núcleo e recompile o núcleo
se necessário:
    </para>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="mesa-kernel.xml"/>

    <note>
      <para>
        O correspondente nome do controlador Gallium3D do Mesa é fornecido como
comentário para as entradas de configuração. Se você não sabe o nome do
controlador Gallium3D do Mesa para tua GPU, veja <xref
linkend="mesa-gallium-drivers"/> abaixo.
      </para>

      <para>
        <option>CONFIG_DRM_RADEON</option>, <option>CONFIG_DRM_AMDGPU</option>,
<option>CONFIG_DRM_NOUVEAU</option> e <option>CONFIG_DRM_I915</option>
possivelmente exijam firmware. Veja-se <xref linkend='postlfs-firmware'/>
para detalhes.
      </para>

      <para>
        Selecionar <option>CONFIG_DRM_RADEON</option>,
<option>CONFIG_DRM_AMDGPU</option> ou <option>CONFIG_DRM_NOUVEAU</option>
como <quote><literal>y</literal></quote> não é recomendado. Se for, qualquer
firmware exigido precisa ser construído como parte da imagem do núcleo ou do
initramfs para o controlador funcionar corretamente.
      </para>

      <para>
        As subentradas sob <option>CONFIG_DRM_AMDGPU</option> são usadas para
garantir que o controlador de núcleo AMDGPU suporte todas as GPUs usando o
controlador <literal>radeonsi</literal>. Elas não são necessárias se você
não precisasse da própria <option>CONFIG_DRM_AMDGPU</option>. Elas
possivelmente sejam desnecessárias para alguns modelos de GPU.
      </para>

      <para>
        Para <literal>llvmpipe</literal> ou <literal>softpipe</literal>,
<option>CONFIG_DRM_VGEM</option> é exigido ou os clientes do X podem falhar
ao iniciar com uma mensagem enigmática <computeroutput>Error: couldn't get
an RGB, Double-buffered visual</computeroutput>. Falando estritamente, ele
pode ser compilado como um módulo. Mas o módulo não será carregado
automaticamente, de forma que é mais conveniente construí-lo como parte da
imagem do núcleo.
      </para>
    </note>
    <indexterm zone="mesa mesa-kernel">
      <primary sortas="d-mesa">mesa</primary>
    </indexterm>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do Mesa</title>
    <para>
      Se você baixou o remendo xdemos (necessário se testar a instalação do Xorg
de acordo com as instruções do BLFS), aplique-o executando o seguinte
comando:
    </para>


<!--
    <para>

      If you have downloaded the seizure prevention patch, apply it by running
      the following command:
    </para>

<screen><userinput>patch -Np1 -i ../mesa-&mesa-version;-seizure_prevention-1.patch</userinput></screen>
-->
<!-- Note that freedreno, vc4, and virgl all need special support from
        libdrm - renodr
        For me, libdrm does not mention virgl, but mesa accepts it. AFAICS
        freedreno is for qualcom hardware, libdrm will build for it on suitable
        systems. VC4 is for the Broadcom VC4 used in the raspberry pi - ken -->
<!--
    <note>

      <para>
        The measurements above, and the Contents below, are for a full build.
        Many people will not wish to install drivers they cannot use, so the
        following paragraphs explain how to limit the drivers, and give an
        example which can be be reduced or amended as necessary.
      </para>
    </note>

-->
<!--
    <para>

      The (non-gallium) DRI drivers available in X86 are auto, <emphasis>or
      alternatively a choice from</emphasis> i915, i965, nouveau, r100, r200,
      and swrast. Use 'auto' to build all available DRI drivers, or use an
      empty string (DRI_DRIVERS="") if you wish to only build gallium drivers.
    </para>

    <para>
      The platforms ("window systems") available for X86 linux are x11, wayland,
     drm, and surfaceless. By not specifying anything, the meson
      build-system will build for all these platforms if you have the
      dependencies, identical to if you had specified '-Dplatforms=auto'.
    </para>

    <para>
      Modify the commands below for your desired drivers.  The drivers listed
      below will cover most modern video cards and virtual machines.  For help in
      selecting drivers see <ulink url="https://docs.mesa3d.org/systems.html"/>.
-->
<!-- these two don't seem to be mentioned in that mesa link -->
<!--
      For intel drivers, specify crocus for i965 gen 4 through to haswell, iris
      for broadwell and later.
    </para>


    <note>
      <para>
        Although the nouveau drivers can be built for both gallium and dri, the
        i915 driver can only be built for one or the other.
      </para>
    </note>

<screen><userinput>GALLIUM_DRV="crocus,i915,iris,nouveau,r600,radeonsi,svga,swrast,virgl"
DRI_DRIVERS="i965,nouveau"</userinput></screen>
-->
<!-- https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/22021 -->
<!-- No longer needed
    <para>

      Remove two unneeded references to LLVM headers to avoid a build
      failure with LLVM 17 or later:
    </para>

<screen><userinput>sed '/Scalar.h/d;/Utils.h/d' \
    -i src/gallium/auxiliary/gallivm/lp_bld_init.c</userinput>
</screen>
-->
<screen><!--<screen>
<userinput>patch -Np1 -i ../mesa-&mesa-version;-add_xdemos-1.patch</userinput></screen>-->
<userinput>patch -Np1 -i ../mesa-add_xdemos-4.patch</userinput></screen>

   


      


    

    <para>
      Instale o <application>Mesa</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..                 \
      --prefix=$XORG_PREFIX    \
      --buildtype=release      \
      -D platforms=x11,wayland \
      -D gallium-drivers=auto  \
      -D vulkan-drivers=auto   \
      -D valgrind=disabled     \
      -D video-codecs=all      \
      -D libunwind=disabled    &amp;&amp;

ninja</userinput></screen>

    <warning>
      <para>
        Por favor, consulte teu(tua) advogado(a) ou remova a opção <parameter>-D
video-codecs=all</parameter> se você distribuirá as bibliotecas e
controladores compiladas do Mesa para outros(as).
      </para>
    </warning>

    <para>
      Para testar os resultados, emita: <command>meson configure -D
build-tests=true &amp;&amp; ninja test</command>.
    </para>


    <!-- All 88 tests passed for me for 22.3.3 [pierre]
     All 90 tests passed for me for 23.1.0 [bdubbs]
     89 Tests passed for me, 1 timeout on 23.1.8 [rahul]
     All 98 tests passed for me for 23.3.1 [bdubbs]
     All 106 tests passed for me for 24.3.1 [zeckma]
-->
<para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

    <para>
      Se desejado, instale a documentação opcional executando os seguintes
comandos como o(a) usuário(a) <systemitem
class="username">root</systemitem>:
    </para>

<screen role="root"
        remap="doc"><userinput>cp -rv ../docs -T /usr/share/doc/mesa-&mesa-version;</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>--buildtype=release</parameter>: Essa chave garante uma
construção totalmente otimizada e desabilita asserções de depuração que
desacelerarão severamente as bibliotecas em certos casos de uso. Sem essa
chave, os tamanhos de construção podem atingir a faixa de 2 GB.
    </para>

    <para>
      <anchor id='mesa-gallium-drivers' xreflabel='Mesa Gallium3D Drivers'/>
<parameter>-D gallium-drivers=auto</parameter>: Esse parâmetro controla
quais controladores Gallium3D deveriam ser construídos:

      <itemizedlist spacing="compact">
        <listitem>
          <para>
            <literal>auto</literal> seleciona todos os controladores Gallium3D
disponíveis para x86. Com uma lista separada por vírgulas, somente um
subconjunto desses controladores será construído. Se você souber exatamente
quais controladores precisa, poderá selecioná-los explicitamente. Por
exemplo, <option>-D gallium-drivers=radeonsi,iris,llvmpipe</option>.
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>r300</literal> (para séries Radeon 9000 ou Radeon X da ATI) 
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>r600</literal> (para séries Radeon HD 2000-6000 da AMD/ATI)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>radeonsi</literal> (para Radeon HD 7000 da AMD ou modelos mais
recentes de GPU da AMD) 
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>nouveau</literal> (para GPUs suportadas da NVIDIA, elas estão
listadas como todos os <quote>recursos 3D</quote> ou <quote>DONE</quote> ou
<quote>N/A</quote> na <ulink
url='https://nouveau.freedesktop.org/FeatureMatrix.html'>página de situação
Nouveau</ulink>)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>virgl</literal> (para uma GPU virtual do QEMU com suporte a
<application>virglrender</application>; observe que o <xref linkend='qemu'/>
do BLFS não é construído com <application>virglrender</application>) 
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>svga</literal> (para uma GPU virtual do VMWare)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>llvmpipe</literal> (usar CPU para rasterização 3D. Observe que ele
é muito mais lento que usar uma GPU moderna com capacidade 3D, de forma que
ele deveria ser usado somente se a GPU não for suportada por outros
controladores)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>softpipe</literal> (também usa CPU para rasterização 3D, porém mais
lento que llvmpipe. Não existe muita razão para usá-lo, a menos que LLVM não
esteja disponível)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>iris</literal> (para GPUs da Intel fornecidas com CPUs Broadwell ou
mais recentes, ou como uma placa dedicada PCIe)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>crocus</literal> (para GPUs da Intel GMA 3000, série X3000, série
4000 ou série X4000 fornecidas com chipsets ou GPUs HD da Intel fornecidas
com CPUs pré Broadwell)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>i915</literal> (para GPUs da Intel GMA 900, 950, 3100 ou 3150
fornecidas com chipsets ou CPUs Atom D/N 4xx/5xx)
          </para>
        </listitem>
        <listitem>
          <para>
             <literal>zink</literal> (usa Vulkan para implementar OpenGL e, embora
instável às vezes, pode ser um substituto decente para controladores Gallium
de baixo desempenho, como o Nouveau)
          </para>
        </listitem>
      </itemizedlist>
    </para>

    <para>
      <parameter>-D vulkan-drivers=auto</parameter>: Esse parâmetro controla quais
controladores Vulkan deveriam ser construídos:

      <itemizedlist spacing="compact">
        <listitem>
          <para>
            <literal>auto</literal> seleciona todos os controladores Vulkan disponíveis
para x86. Com uma lista separada por vírgulas, somente um subconjunto desses
controladores será construído. Se você souber exatamente quais controladores
precisa, poderá selecioná-los explicitamente. Por exemplo, <option>-D
vulkan-drivers=amd,nouveau,swrast</option>.
          </para>
        </listitem>

        <listitem>
          <para>
            <literal>amd</literal> (para Radeon HD 7730 da AMD ou GPUs mais recentes da
AMD)
          </para>
        </listitem>

        <listitem>
          <para>
            <literal>intel</literal> (para GPUs da Intel fornecidas com Skylake ou CPUs
mais recentes, ou como uma placa dedicada PCIe)
          </para>
        </listitem>

        <listitem>
          <para>
            <literal>intel_hasvk</literal> (para GPUs da Intel fornecidas com CPUs Ivy
Bridge, Haswell ou Broadwell)
          </para>
        </listitem>

        <listitem>
          <para>
            <literal>nouveau</literal> (para GTX 16XX, RTX 20XX ou GPUs mais recentes da
NVIDIA)
          </para>
        </listitem>

        <listitem>
          <para>
            <literal>swrast</literal> (para usar a CPU para rasterização 3D). Observe
que ele é muito mais lento que usar uma GPU moderna com capacidade 3D, de
forma que ele deveria ser usado somente se a GPU não for suportada por
outros controladores.
          </para>
        </listitem>

        <listitem>
          <para>
            <literal>""</literal> (lista vazia, use <option>-D
vulkan-drivers=""</option> se você não tiver instalado e não planeja
instalar <xref linkend='vulkan-loader'/>)
          </para>
        </listitem>
      </itemizedlist>
    </para>

    <para>
      <parameter>-D platforms=...</parameter>: Esse parâmetro controla quais
sistemas de janelas serão suportados. As plataformas Linux disponíveis são
x11 e wayland.
    </para>

    <para>
      <parameter>-D valgrind=disabled</parameter>: Esse parâmetro desabilita o uso
do Valgrind durante o processo de construção. Remova esse parâmetro se você
tiver o Valgrind instalado e desejar verificar vazamentos de memória.
    </para>

    <para>
      <parameter>-D video-codecs=all</parameter>: Esse parâmetro habilita
construir codificadores/decodificadores para formatos de vídeo cobertos por
patentes de terceiros(as).
    </para>

    <para>
      <parameter>-D libunwind=disabled</parameter>: Esse parâmetro desabilita o
uso da libunwind.
    </para>

    <para>
      <command>meson configure -D build-tests=true</command>: Esse comando
reconfigurará a construção para configurar <option>-D
build-tests=true</option>, mas manterá as outras opções especificadas no
comando <command>meson setup</command> sem mudanças. Ele permite que
<command>ninja test</command> construa e execute testes unitários.
    </para>

    <para>
      <option>-D egl-native-platform="..."</option>: Esse parâmetro controla qual
suporte da Biblioteca de Gráficos Incorporados será construído. As opções de
Linux disponíveis são auto (padrão), x11, wayland, surfaceless e drm.
    </para>

    <para>
      <option>-D legacy-x11="..."</option> Esse parâmetro controla quais recursos
legados do X11 recebem suporte integrado. As opções disponíveis do Linux são
none (padrão) e dri2. Esses recursos raramente são mais necessários.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <!-- in /usr/lib -->
<segtitle>Sobras Instaladas de Controlador DRI</segtitle>
      <!-- in /usr/lib/dri -->
<segtitle>Controladores VA-API Instalados</segtitle>
      <!-- in /usr/lib/dri -->
<segtitle>Estruturas GBM de Retaguarda Instaladas</segtitle>
      <!-- in /usr/lib/gbm -->
<segtitle>Controladores VDPAU Instalados</segtitle>
      <!-- in /usr/lib/vdpau -->
<segtitle>Controladores Vulkan Instalados</segtitle>
      <!-- in /usr/lib -->
<segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          glxgears, glxinfo, mme_fermi_sim_hw_test e mme_tu104_sim_hw_test
        </seg>
        <seg>
          libEGL.so, libGL.so, libGLESv1_CM.so, libGLESv2.so, libgbm.so, libglapi.so,
libgallium-&mesa-version;.so e libxatracker.so
        </seg>
        <seg>
          
          <!-- Begin DRI driver stubs -->
<!-- End DRI driver stubs -->
libdril_dri.so e links simbólicos para ela: crocus_dri.so, i915_dri.so,
iris_dri.so, kms_swrast_dri.so, nouveau_dri.so, r300_dri.so, r600_dri.so,
r600_drv_video.so, radeonsi_dri.so, swrast_dri.so, virtio_gpu_dri.so,
vmwgfx_dri.so e zink_dri.so
          
        </seg>
        <seg>
          
          <!-- Begin VA-API drivers -->
<!-- End VA-API drivers -->
nouveau_drv_video.so, radeonsi_drv_video.so e virtio_gpu_drv_video.so,
          
        </seg>
        <seg>
          
          <!-- Begin GBM backends -->
<!-- End GBM backends -->
dri_gbm.so
          
        </seg>
        <seg>
          
          <!-- Begin VDPAU drivers -->
<!-- End VDPAU drivers -->
libvdpau_nouveau.so, libvdpau_r600.so, libvdpau_radeonsi.so e
libvdpau_virtio_gpu.so (Muitos desses controladores estão rigidamente
lincados).
        </seg>
        <seg>
          libvulkan_intel_hasvk.so, libvulkan_intel.so, libvulkan_lvp.so,
libvulkan_nouveau.so e libvulkan_radeon.so
        </seg>
        <seg>
          
          <!-- $XORG_PREFIX/include/GL is installed by xorg-protos -->
$XORG_PREFIX/{include/{EGL,GLES,GLES2,GLES3,KHR},
$XORG_PREFIX/lib/{dri,gbm,vdpau}}, $XORG_PREFIX/share/drirc.d (contém
soluções alternativas para vários aplicativos, particularmente navegadores e
jogos), $XORG_PREFIX/share/vulkan e /usr/share/doc/mesa-&mesa-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="glxgears">
        <term><command>glxgears</command></term>
        <listitem>
          <para>
            é uma demonstração GL útil para solucionar problemas gráficos
          </para>
          <indexterm zone="mesa glxgears">
            <primary sortas="b-glxgears">glxgears</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="glxinfo">
        <term><command>glxinfo</command></term>
        <listitem>
          <para>
            é um aplicativo de diagnóstico que exibe informações relativas ao hardware
gráfico e bibliotecas GL instaladas
          </para>
          <indexterm zone="mesa glxinfo">
            <primary sortas="b-glxinfo">glxinfo</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="dri_gbm">
        <term><filename class='libraryfile'>dri_gbm.so</filename></term>
        <listitem>
          <para>
            implementa funções de Graphics Buffer Management necessitadas pela
<systemitem class='library'>libgbm</systemitem> em cima da <systemitem
class='library'>libdrm</systemitem>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry id="libdril_dri">
        <term><filename class='libraryfile'>libdril_dri.so</filename></term>
        <listitem>
          <para>
            é uma sobra que permite que o servidor Xorg coopere com os controladores
Gallium3D
          </para>
        </listitem>
      </varlistentry>

      <varlistentry id="libEGL">
        <term><filename class="libraryfile">libEGL.so</filename></term>
        <listitem>
          <para>
            fornece uma interface gráfica de plataforma nativa conforme definido pela
especificação EGL-1.4
          </para>
          <indexterm zone="mesa libEGL">
            <primary sortas="c-libGL">libEGL.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgallium">
        <term><filename class='libraryfile'>libgallium-&mesa-version;.so</filename></term>
        <listitem>
          <para>
            contém todos os controladores Gallium3D
          </para>
        </listitem>
      </varlistentry>

      <varlistentry id="libgbm">
        <term><filename class="libraryfile">libgbm.so</filename></term>
        <listitem>
          <para>
            é a biblioteca Graphics Buffer Manager do <application>Mesa</application>
          </para>
          <indexterm zone="mesa libgbm">
            <primary sortas="c-libgbm">libgbm.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libglapi">
        <term><filename class="libraryfile">libglapi.so</filename></term>
        <listitem>
          <para>
            é a implementação <application>Mesa</application> da API OpenGL
          </para>
          <indexterm zone="mesa libglapi">
            <primary sortas="c-libglapi">libglapi.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libGLESv1_CM">
        <term><filename class="libraryfile">libGLESv1_CM.so</filename></term>
        <listitem>
          <para>
            é a biblioteca OpenGL ES 1.1 do <application>Mesa</application>
          </para>
          <indexterm zone="mesa libGLESv1_CM">
            <primary sortas="c-libGLESv1_CM">libGLESv1_CM.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libGLES2">
        <term><filename class="libraryfile">libGLES2.so</filename></term>
        <listitem>
          <para>
            é a biblioteca OpenGL ES 2.0 do <application>Mesa</application>
          </para>
          <indexterm zone="mesa libGLES2">
            <primary sortas="c-libGLES2">libGLES2.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libGL">
        <term><filename class="libraryfile">libGL.so</filename></term>
        <listitem>
          <para>
            é a principal biblioteca OpenGL do <application>Mesa</application>
          </para>
          <indexterm zone="mesa libGL">
            <primary sortas="c-libGL">libGL.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libxatracker">
        <term><filename class="libraryfile">libxatracker.so</filename></term>
        <listitem>
          <para>
            é o rastreador de estado XA para o controlador VMWare vmwgfx
          </para>
          <indexterm zone="mesa libxatracker">
            <primary sortas="c-libxatracker">libxatracker.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
