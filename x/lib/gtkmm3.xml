<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY gtkmm3-download-http "&gnome-download-http;/gtkmm/&gnome-minor-24;/gtkmm-&gtkmm3-version;.tar.xz">
  <!ENTITY gtkmm3-download-ftp  "">
  <!ENTITY gtkmm3-md5sum        "47871a7973e186c1189b2145b507de15">
  <!ENTITY gtkmm3-size          "14 MB">
  <!ENTITY gtkmm3-buildsize     "200 MB (com testes)">
  <!ENTITY gtkmm3-time          "1,2 UPC (Usando paralelismo=4; com testes)">
]>

<sect1 id="gtkmm3" xreflabel="Gtkmm-&gtkmm3-version;">
  <?dbhtml filename="gtkmm3.html"?>


  <title>Gtkmm-&gtkmm3-version;</title>

  <indexterm zone="gtkmm3">
    <primary sortas="a-Gtkmm">Gtkmm</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Gtkmm</title>

    <para>
      O pacote &quot;<application>Gtkmm</application>&quot; fornece uma interface
&quot;C++&quot; para o &quot;<application>GTK+ 3</application>&quot;.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&gtkmm3-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&gtkmm3-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &gtkmm3-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &gtkmm3-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &gtkmm3-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &gtkmm3-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do Gtkmm</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="atkmm"/>, <xref linkend="gtk3"/> e <xref linkend="pangomm"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="doxygen"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Gtkmm</title>

    <para>
      Instale o &quot;<application>Gtkmm</application>&quot; executando os
seguintes comandos:
    </para>

<screen><userinput>mkdir gtkmm3-build &amp;&amp;
cd    gtkmm3-build &amp;&amp;

meson setup --prefix=/usr --buildtype=release .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja test</command>. Observe que
você precisa estar em um ambiente gráfico, pois os testes tentam abrir
algumas janelas.
    </para>

    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

    <para>
      Se você tiver construído a documentação (vejam-se as Explicações dos
Comandos abaixo), [então] ela foi instalada em &quot;<filename
class="directory">/usr/share/doc/gtkmm-3.0</filename>&quot;. Para
consistência, mova-a para um diretório versionado como o(a) usuário(a)
&quot;<systemitem class="username">root</systemitem>&quot;:
    </para>

<screen role="nodump"><userinput>mv -v /usr/share/doc/gtkmm-3.0 /usr/share/doc/gtkmm-&gtkmm3-version;</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <option>-D build-documentation=true</option>: Se você tiver instalado <xref
linkend="doxygen"/>, essa definição construirá e instalará a documentação.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libgdkmm-3.0.so e libgtkmm-3.0.so
        </seg>
        <seg>
          /usr/include/gdkmm-3.0, /usr/include/gtkmm-3.0, /usr/lib/gdkmm-3.0,
/usr/lib/gtkmm-3.0 e, opcionalmente,
/usr/share/{devhelp/books/gtkmm-3.0,doc/gtkmm-&gtkmm3-version;}
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libgdkmm-3">
        <term><filename class="libraryfile">libgdkmm-3.0.so</filename></term>
        <listitem>
        <para>
          contém as classes da API do GDK
        </para>
          <indexterm zone="gtkmm3 libgdkmm-3">
            <primary sortas="c-libgdkmm-3">libgdkmm-3.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgtkmm-3">
        <term><filename class="libraryfile">libgtkmm-3.0.so</filename></term>
        <listitem>
        <para>
          contém as classes da &quot;API&quot; do &quot;<application>GTK+
3</application>&quot;
        </para>
          <indexterm zone="gtkmm3 libgtkmm-3">
            <primary sortas="c-libgtkmm-3">libgtkmm-3.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
