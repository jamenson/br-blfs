<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libgedit-amtk-download-http "https://gitlab.gnome.org/World/gedit/libgedit-amtk/-/archive/5.9.0/libgedit-amtk-&libgedit-amtk-version;.tar.bz2">
  <!ENTITY libgedit-amtk-download-ftp  "">
  <!ENTITY libgedit-amtk-md5sum        "8eaca94c4808fb00e6057ebf731f4daa">
  <!ENTITY libgedit-amtk-size          "60 KB">
  <!ENTITY libgedit-amtk-buildsize     "2,4 MB (com testes)">
  <!ENTITY libgedit-amtk-time          "0,1 UPC (com testes)">
]>

<sect1 id="libgedit-amtk" xreflabel="libgedit-amtk-&libgedit-amtk-version;">
  <?dbhtml filename="libgedit-amtk.html"?>


  <title>libgedit-amtk-&libgedit-amtk-version;</title>

  <indexterm zone="libgedit-amtk">
    <primary sortas="a-libgedit-amtk">libgedit-amtk</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libgedit-amtk</title>

    <para>
      O pacote <application>libgedit-amtk</application> contém uma substituição
básica do GTKUIManager baseada no GAction.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libgedit-amtk-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libgedit-amtk-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libgedit-amtk-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libgedit-amtk-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libgedit-amtk-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libgedit-amtk-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libgedit-amtk</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="gtk3"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="gtk-doc"/> (para documentação) e <xref linkend="valgrind"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libgedit-amtk</title>

    <para>
      Instale o <application>libgedit-amtk</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir amtk-build &amp;&amp;
cd    amtk-build &amp;&amp;

meson setup ..              \
      --prefix=/usr         \
      --buildtype=release   \
      -D gtk_doc=false      &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja test</command>. Um teste,
test-action-map, é conhecido por falhar.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>
  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D gtk_doc=false</parameter>: Essa chave impede o sistema de
construção de gerar e instalar a documentação da API. Omita essa chave se
você tiver o <xref linkend="gtk-doc" role="nodep"/> instalado e desejar
gerar e instalar a documentação da API.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libgedit-amtk-5.so
        </seg>
        <seg>
          /usr/include/libgedit-amtk-5 e /usr/share/gtk-doc/html/libgedit-amtk-5.0
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libgedit-amtk-5">
        <term><filename class="libraryfile">libgedit-amtk-5.so</filename></term>
        <listitem>
          <para>
            fornece uma API básica de substituição do GTKUIManager baseada em GAction
          </para>
          <indexterm zone="libgedit-amtk libgedit-amtk-5">
            <primary sortas="c-libgedit-amtk-5">libgedit-amtk-5.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>

</sect1>
