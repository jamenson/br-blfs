<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY gtk-vnc-download-http "&gnome-download-http;/gtk-vnc/1.5/gtk-vnc-&gtk-vnc-version;.tar.xz">
  <!ENTITY gtk-vnc-download-ftp  "">
  <!ENTITY gtk-vnc-md5sum        "6e9815e7960636e95f626a3f164eb01d">
  <!ENTITY gtk-vnc-size          "224 KB">
  <!ENTITY gtk-vnc-buildsize     "11 MB">
  <!ENTITY gtk-vnc-time          "0,2 UPC">
]>

<sect1 id="gtk-vnc" xreflabel="gtk-vnc-&gtk-vnc-version;">
  <?dbhtml filename="gtk-vnc.html"?>


  <title>gtk-vnc-&gtk-vnc-version;</title>

  <indexterm zone="gtk-vnc">
    <primary sortas="a-gtk-vnc">gtk-vnc</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao VNC do Gtk</title>

    <para>
      O pacote &quot;<application>Gtk VNC</application>&quot; contém uma pequena
engenhoca visualizadora &quot;VNC&quot; para o
&quot;<application>GTK+</application>&quot;. Ela é construída usando co
rotinas, permitindo que seja completamente assíncrona enquanto permanece com
camada única.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&gtk-vnc-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&gtk-vnc-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &gtk-vnc-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &gtk-vnc-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &gtk-vnc-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &gtk-vnc-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do &quot;Gtk VNC&quot;</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="gnutls"/>, <xref linkend="gtk3"/> e <xref
linkend="libgcrypt"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      &gobject-introspection; e <xref linkend="vala"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="cyrus-sasl"/>, <xref linkend="gi-docgen"/> (para gerar
documentação) e <xref linkend="pulseaudio"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do VNC do Gtk</title>

    <para>
      Instale o &quot;<application>Gtk VNC</application>&quot; executando os
seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr --buildtype=release .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja test</command>
    </para>

    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <option>-D with-vala=false</option>: Essa chave desabilita construção das
ligações Vala. Adicione isso se você decidir construir o gtk-vnc sem o vala
instalado.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          gvnccapture
        </seg>
        <seg>
          libgtk-vnc-2.0.so, libgvnc-1.0.so e libgvncpulse-1.0.so
        </seg>
        <seg>
          /usr/include/gtk-vnc-2.0, /usr/include/gvnc-1.0 e /usr/include/gvncpulse-1.0
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="gvnccapture">
        <term><command>gvnccapture</command></term>
        <listitem>
          <para>
            é usado para capturar imagem a partir do servidor &quot;VNC&quot;
          </para>
          <indexterm zone="gtk-vnc gvnccapture">
            <primary sortas="b-gvnccapture">gvnccapture</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgtk-vnc-2">
        <term><filename class="libraryfile">libgtk-vnc-2.0.so</filename></term>
        <listitem>
          <para>
            contém as ligações do &quot;<application>GTK+ 3</application>&quot; para o
&quot;<application>Gtk VNC</application>&quot;
          </para>
          <indexterm zone="gtk-vnc libgtk-vnc-2">
            <primary sortas="c-libgtk-vnc-2.0">libgtk-vnc-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgvnc-1a">
        <term><filename class="libraryfile">libgvnc-1.0.so</filename></term>
        <listitem>
          <para>
            contém as ligações &quot;GObject&quot; para o &quot;<application>Gtk
VNC</application>&quot;
          </para>
          <indexterm zone="gtk-vnc libgvnc-1a">
            <primary sortas="c-libgvnc-1.0">libgvnc-1.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgvncpulse-1">
        <term><filename class="libraryfile">libgvncpulse-1.0.so</filename></term>
        <listitem>
          <para>
            é a ponte do &quot;<application>PulseAudio</application>&quot; para o
&quot;<application>Gtk VNC</application>&quot;
          </para>
          <indexterm zone="gtk-vnc libgvncpulse-1">
            <primary sortas="c-libgvncpulse-1.0">libgvncpulse-1.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
