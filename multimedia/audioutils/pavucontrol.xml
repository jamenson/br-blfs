<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY pavucontrol-download-http "https://www.freedesktop.org/software/pulseaudio/pavucontrol/pavucontrol-&pavucontrol-version;.tar.xz">
  <!ENTITY pavucontrol-download-ftp  "">
  <!ENTITY pavucontrol-md5sum        "51743b9bc9eb01959bf3c770facc6555">
  <!ENTITY pavucontrol-size          "168 KB">
  <!ENTITY pavucontrol-buildsize     "5,2 MB">
  <!ENTITY pavucontrol-time          "0,2UPC">
]>

<sect1 id="pavucontrol" xreflabel="pavucontrol-&pavucontrol-version;">
  <?dbhtml filename="pavucontrol.html"?>


  <title>pavucontrol-&pavucontrol-version;</title>

  <indexterm zone="pavucontrol">
    <primary sortas="a-pavucontrol">pavucontrol</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao pavucontrol</title>

    <para>
      PulseAudio Volume Control (pavucontrol) é uma ferramenta simples de controle
de volume baseada em GTK ("mixer") para o servidor de som PulseAudio. Em
contraste com as ferramentas clássicas de mixagem, essa te permite controlar
o volume dos dispositivos de hardware e de cada fluxo de reprodução
separadamente.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&pavucontrol-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&pavucontrol-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &pavucontrol-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &pavucontrol-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &pavucontrol-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &pavucontrol-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do pavucontrol</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="gtkmm4"/>, <xref linkend="json-glib"/>, <xref
linkend="libsigc3"/> e <xref linkend="pulseaudio"/>
    </para>

    <bridgehead renderas="sect3">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="libcanberra"/> e <xref linkend="lynx"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do pavucontrol</title>

    <para>
       Instale o <application>pavucontrol</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr --buildtype=release -D lynx=false .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install &amp;&amp;
mv /usr/share/doc/pavucontrol /usr/share/doc/pavucontrol-&pavucontrol-version;
</userinput></screen>
  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>-D lynx=false</parameter>: Essa chave desabilita gerar o arquivo
README em formato de texto. Remova essa chave se você quiser o arquivo
README em formato de texto e tiver <xref linkend="lynx"/> instalado.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          pavucontrol
        </seg>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          /usr/share/pavucontrol e /usr/share/doc/pavucontrol-&pavucontrol-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="pavucontrol-prog">
        <term><command>pavucontrol</command></term>
        <listitem>
          <para>
            é uma ferramenta de configuração GUI para configurações de som usando o
<application>pulsaudio</application>
          </para>
          <indexterm zone="pavucontrol pavucontrol-prog">
            <primary sortas="b-pavucontrol">pavucontrol</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
