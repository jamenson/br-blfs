<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libplacebo-download-http "https://github.com/haasn/libplacebo/archive/v&libplacebo-version;/libplacebo-&libplacebo-version;.tar.gz">
  <!ENTITY libplacebo-download-ftp  "">
  <!ENTITY libplacebo-md5sum        "8dd72edf2ec5f1918770a317ef8107a5">
  <!ENTITY libplacebo-size          "828 KB">
  <!ENTITY libplacebo-buildsize     "36 MB">
  <!ENTITY libplacebo-time          "0,1 UPC (Com testes, ambos usando paralelismo=4)">
]>

<sect1 id="libplacebo" xreflabel="libplacebo-&libplacebo-version;">
  <?dbhtml filename="libplacebo.html"?>

  <title>libplacebo-&libplacebo-version;</title>

  <indexterm zone="libplacebo">
    <primary sortas="a-libplacebo">libplacebo</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libplacebo</title>

    <para>
      O pacote <application>libplacebo</application> contém uma biblioteca para
processamento de primitivos e sombreadores de imagem e vídeo. Também inclui
um pipeline de renderização de alta qualidade que suporta OpenGL e Vulkan.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libplacebo-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libplacebo-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libplacebo-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libplacebo-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libplacebo-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libplacebo-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libplacebo</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="ffmpeg"/> e <xref linkend="glad"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="glslang"/> e <xref linkend="vulkan-loader"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="lcms2"/> <xref linkend="libunwind"/>, <ulink
url="https://github.com/quietvoid/dovi_tool/">dovi_tool</ulink>, <ulink
url="https://github.com/Immediate-Mode-UI/Nuklear">Nuklear</ulink> e <ulink
url="https://github.com/Cyan4973/xxHash">xxHash</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libplacebo</title>

    <para>
      Primeiro, corrija uma falha de construção que ocorre com glslang-15.0.0:
    </para>

<screen><userinput remap="pre">sed -e "20s/$/,/"                                         \
    -e "21i cxx.find_library('glslang', required: false)" \
    -i src/glsl/meson.build</userinput></screen>

    <para>
      Instale <application>libplacebo</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..            \
      --prefix=/usr       \
      --buildtype=release \
      -D tests=true       \
      -D demos=false      &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja test</command>. Um teste,
opengl_surfaceless.c, é conhecido por falhar.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D demos=false</parameter>: Essa chave desabilita construir os
programas de demonstração, porque construir
<application>plplay</application> atualmente está quebrado.
    </para>

    <para>
      <parameter>-D tests=true</parameter>: Essa chave habilita construir o código
necessário para executar os testes.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libplacebo.so
        </seg>
        <seg>
          /usr/include/libplacebo
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libplacebo-lib">
        <term><filename class="libraryfile">libplacebo.so</filename></term>
        <listitem>
          <para>
            processa primitivos e sombreadores de imagem e vídeo e fornece um pipeline
de renderização de alta qualidade para OpenGL e Vulkan
          </para>
          <indexterm zone="libplacebo libplacebo-lib">
            <primary sortas="c-libplacebo">libplacebo.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
