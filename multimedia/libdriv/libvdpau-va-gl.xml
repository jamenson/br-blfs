<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libvdpau-va-gl-version       "0.4.2">

  <!ENTITY libvdpau-va-gl-download-http "https://github.com/i-rinat/libvdpau-va-gl/archive/v&libvdpau-va-gl-version;/libvdpau-va-gl-&libvdpau-va-gl-version;.tar.gz">
  <!ENTITY libvdpau-va-gl-download-ftp  "">
  <!ENTITY libvdpau-va-gl-md5sum        "8db21dcfd5cd14c6ec51b992e20369dc">
  <!ENTITY libvdpau-va-gl-size          "120 KB">
  <!ENTITY libvdpau-va-gl-buildsize     "4,9 MB">
  <!ENTITY libvdpau-va-gl-time          "menos que 0,1 UPC (adicionar 1,1 UPC para testes)">
]>

  <sect1 id="libvdpau-va-gl" xreflabel="libvdpau-va-gl-&libvdpau-va-gl-version;">
  <?dbhtml filename="libvdpau-va-gl.html"?>

    <sect1info>
<date>$Date$</date></sect1info>

    <title>libvdpau-va-gl-&libvdpau-va-gl-version;</title>

    <indexterm zone="libvdpau-va-gl">
      <primary sortas="a-libvdpau-va-gl">libvdpau-va-gl</primary>
    </indexterm>

    <sect2 role="package">
      <title>Introdução ao libvdpau-va-gl</title>

      <para>
        O pacote <application>libvdpau-va-gl</application> contém uma biblioteca que
implementa a biblioteca VDPAU. Libvdpau_va_gl usa OpenGL nos bastidores para
acelerar o desenho e o dimensionamento e a VA-API (se disponível) para
acelerar a decodificação de vídeo. Por enquanto, a VA-API está disponível em
alguns chips da Intel e em alguns adaptadores de vídeo da AMD com a ajuda do
controlador libvdpau.
      </para>

      &lfs123_checked;

      <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
      <itemizedlist spacing="compact">
        <listitem>
          <para>
            Transferência do Controlador Libvdpau-va-gl (HTTP): <ulink
url="&libvdpau-va-gl-download-http;"/>
          </para>
        </listitem>
        <listitem>
          <para>
            Transferência do Controlador Libvdpau-va-gl (FTP): <ulink
url="&libvdpau-va-gl-download-ftp;"/>
          </para>
        </listitem>
        <listitem>
          <para>
            Soma de verificação MD5 da Transferência do Controlador Libvdpau-va-gl:
&libvdpau-va-gl-md5sum;
          </para>
        </listitem>
        <listitem>
          <para>
            Tamanho da Transferência do Controlador Libvdpau-va-gl:
&libvdpau-va-gl-size;
          </para>
        </listitem>
        <listitem>
          <para>
            Espaço em disco estimado exigido: &libvdpau-va-gl-buildsize;
          </para>
        </listitem>
        <listitem>
          <para>
            Tempo de construção estimado: &libvdpau-va-gl-time;
          </para>
        </listitem>
      </itemizedlist>

      <bridgehead renderas="sect3">Dependências do libvdpau-va-gl</bridgehead>

      <bridgehead renderas="sect4">Exigidas</bridgehead>
      <para role="required">
        <xref linkend="cmake"/>, <xref linkend="libvdpau"/>, <xref linkend="libva"/>
e <xref linkend="mesa"/>
      </para>

      <bridgehead renderas="sect4">Opcionais</bridgehead>
      <para role="optional">
        <xref linkend="doxygen"/>, <xref linkend="graphviz"/> e <xref
linkend="texlive"/> ou <xref linkend="tl-installer"/>
      </para>

    </sect2>

    <sect2 role="installation">
      <title>Instalação do libvdpau-va-gl</title>

      <para>
        Instale o <application>libvdpau-va-gl</application> executando os seguintes
comandos:
      </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=$XORG_PREFIX .. &amp;&amp;
make</userinput></screen>

      <para>
        Para testar os resultados, emita: <command>make check</command>. Os testes
precisam ser executados a partir de um ambiente do Xorg.
      </para>

      <para>
        Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
      </para>

<screen role="root"><userinput>make install</userinput></screen>

    </sect2>



    <!--
    <sect2 role="commands">

      <title>Command Explanations</title>

      <para>
        <command>sed -e '/#include &lt;stdlib.h&gt;/a #include &lt;string&gt;' ...</command>:
        fix the build for the C++ Standard Library header changes in gcc-13.
      </para>

    </sect2>
-->
<sect2 role="configuration">
      <title>Configuração</title>

      <para>
        Para permitir que a libvdpau encontre libvdpau-va-gl, configure uma variável
de ambiente. Como o(a) usuário(a) <systemitem
class="username">root</systemitem>:
      </para>

<screen role="root"><userinput>echo "export VDPAU_DRIVER=va_gl" >> /etc/profile.d/xorg.sh</userinput></screen>

    </sect2>

    <sect2 role="content">
      <title>Conteúdo</title>

      <segmentedlist>
        <segtitle>Aplicativos Instalados</segtitle>
        <segtitle>Biblioteca Instalada</segtitle>
        <segtitle>Diretórios Instalados</segtitle>

        <seglistitem>
          <seg>
            Nenhum(a)
          </seg>
          <seg>
            libvdpau_va_gl.so
          </seg>
          <seg>
            Nenhum(a)
          </seg>
        </seglistitem>
      </segmentedlist>

      <variablelist>
        <bridgehead renderas="sect2">Descrições Curtas</bridgehead>
        <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

        <varlistentry id="libvdpau-va-gl-lib">
          <term><filename class="libraryfile">libvdpau_va_gl.so</filename></term>
          <listitem>
            <para>
              contém funções para implementar a estrutura de retaguarda do OpenGL para a
API do VDPAU (Video Decode and Presentation API for Unix)
            </para>
            <indexterm zone="libvdpau-va-gl libvdpau-va-gl-lib">
              <primary sortas="c-libvdpau-va-gl">libvdpau.so</primary>
            </indexterm>
          </listitem>
        </varlistentry>

      </variablelist>

    </sect2>

  </sect1>
