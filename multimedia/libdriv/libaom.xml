<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libaom-download-http "https://storage.googleapis.com/aom-releases/libaom-&libaom-version;.tar.gz">
  <!ENTITY libaom-download-ftp  "">
  <!ENTITY libaom-md5sum        "aabd9c71c7d52c837ec276156d5a953f">
  <!ENTITY libaom-size          "5,2 MB">
  <!ENTITY libaom-buildsize     "121 MB (adicionar 1,2 GB para os testes)">
  <!ENTITY libaom-time          "1,2 UPC (com paralelismo=4, adicionar 155 UPC para testes)">
]>




<!-- Tests not run for version 3.6.1. -->
<!-- Tests not run for version 3.9.0. -->
<sect1 id="libaom" xreflabel="libaom-&libaom-version;">
  <?dbhtml filename="libaom.html"?>

  <title>libaom-&libaom-version;</title>

  <indexterm zone="libaom">
    <primary sortas="a-libaom">libaom</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libaom</title>

    <para>
      O pacote <application>libaom</application> contém uma versão de referência
do codificador de vídeo Alliance for Open Media. Esse codificador é uma
alternativa livre de patente ao H.265 e está começando a ser usado em toda a
Internet.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informações do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libaom-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libaom-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libaom-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libaom-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libaom-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libaom-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libaom</bridgehead>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="yasm"/> (ou <xref role='nodep' linkend="nasm"/>)
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="doxygen"/>
    </para>

    &test-use-internet;

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libaom</title>

    <para>
      Instale o <application>libaom</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir aom-build &amp;&amp;
cd    aom-build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr \
      -D CMAKE_BUILD_TYPE=Release  \
      -D BUILD_SHARED_LIBS=1       \
      -D ENABLE_DOCS=no            \
      -G Ninja .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja testdata &amp;&amp; ninja
runtests</command>. Observe que os testes tomam um tempo extremamente longo
para executar.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install &amp;&amp;
rm -v /usr/lib/libaom.a</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>-D BUILD_SHARED_LIBS=1</parameter>: Essa chave constrói versões
compartilhadas das bibliotecas.
    </para>

    <para>
      <parameter>-D ENABLE_DOCS=no</parameter>: Essa chave desabilita a construção
da documentação porque ela falha devido a uma incompatibilidade com a versão
mais recente do <xref role="nodep" linkend="doxygen"/>.
    </para>

    <para>
      <option>-D ENABLE_NASM=yes</option>: Use essa chave se você tiver ambos,
<xref linkend="yasm"/> e <xref role="nodep" linkend="nasm"/>, instalados e
desejar usar nasm em vez do yasm.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libaom.so
        </seg>
        <seg>
          /usr/include/aom
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libaom-lib">
        <term><filename class="libraryfile">libaom.so</filename></term>
        <listitem>
          <para>
            contém funções que fornecem uma implementação de referência do codificador
AV1
          </para>
          <indexterm zone="libaom libaom-lib">
            <primary sortas="c-libaom">libaom.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
