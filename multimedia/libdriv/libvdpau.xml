<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libvdpau-version        "1.5">

  <!ENTITY libvdpau-download-http "https://gitlab.freedesktop.org/vdpau/libvdpau/-/archive/&libvdpau-version;/libvdpau-&libvdpau-version;.tar.bz2">
  <!ENTITY libvdpau-download-ftp  "">
  <!ENTITY libvdpau-md5sum        "148a192110e7a49d62c0bf9ef916c099">
  <!ENTITY libvdpau-size          "140 KB">
  <!ENTITY libvdpau-buildsize     "4,6 MB (com testes)">
  <!ENTITY libvdpau-time          "menos que 0,1 UPC (com testes)">
]>

  <sect1 id="libvdpau" xreflabel="libvdpau-&libvdpau-version;">
  <?dbhtml filename="libvdpau.html"?>

    <sect1info>
<date>$Date$</date></sect1info>

    <title>libvdpau-&libvdpau-version;</title>

    <indexterm zone="libvdpau">
      <primary sortas="a-libvdpau">libvdpau</primary>
    </indexterm>

    <sect2 role="package">
      <title>Introdução ao libvdpau</title>

      <para>
        O pacote <application>libvdpau</application> contém uma biblioteca que
implementa a biblioteca VDPAU.
      </para>

      <para>
        Video Decode and Presentation API for Unix (VDPAU) é uma biblioteca de fonte
aberto (libvdpau) e API originalmente projetada pela NVIDIA para a série
GeForce 8 dela e hardware de GPU posterior direcionado ao Sistema de Janelas
X. Essa API da VDPAU permite que programas de vídeo transfiram partes do
processo de decodificação de vídeo e pós-processamento de vídeo para o
hardware de vídeo da GPU.
     </para>

     <para>
        Atualmente, as partes capazes de serem transferidas pela VDPAU para a GPU
são compensação de movimento (mo comp), transformação discreta inversa de
cosseno (iDCT), decodificação de comprimento variável (VLD) e desbloqueio
para vídeos codificados com MPEG-1, MPEG-2, MPEG-4 ASP (MPEG-4 Parte 2),
H.264/MPEG-4 AVC e VC-1, WMV3/WMV9. Quais codificadores específicos desses
podem ser transferidos para a GPU depende da versão do hardware da GPU;
especificamente, para também decodificar os formatos MPEG-4 ASP (MPEG-4
Parte 2), Xvid/OpenDivX (DivX 4) e DivX 5, uma série GeForce 200M (2xxM) (a
décima primeira geração de unidades de processamento gráfico GeForce da
NVIDIA) ou mais recente hardware de GPU é exigido.
      </para>

      &lfs123_checked;

      <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
      <itemizedlist spacing="compact">
        <listitem>
          <para>
            Transferência (HTTP): <ulink url="&libvdpau-download-http;"/>
          </para>
        </listitem>
        <listitem>
          <para>
            Transferência (FTP): <ulink url="&libvdpau-download-ftp;"/>
          </para>
        </listitem>
        <listitem>
          <para>
            Soma de verificação MD5 da transferência: &libvdpau-md5sum;
          </para>
        </listitem>
        <listitem>
          <para>
            Tamanho da transferência: &libvdpau-size;
          </para>
        </listitem>
        <listitem>
          <para>
            Espaço em disco estimado exigido: &libvdpau-buildsize;
          </para>
        </listitem>
        <listitem>
          <para>
            Tempo de construção estimado: &libvdpau-time;
          </para>
        </listitem>
      </itemizedlist>

      <bridgehead renderas="sect3">Dependências do libvdpau</bridgehead>

      <bridgehead renderas="sect4">Exigidas</bridgehead>
      <para role="required">
        <xref linkend="xorg7-lib"/>
      </para>

      <bridgehead renderas="sect4">Recomendadas (tempo de execução)</bridgehead>
      
      <!-- "nodep" to prevent BLFS tool from blindly installing all the
           drivers -->
<para role='nodep'>
        O controlador VDPAU adequado para o hardware em teu sistema: <xref
linkend='libvdpau-va-gl'/> (para GPUs da Intel) e <xref linkend='mesa'/>
(fornecendo os controladores VDPAU <literal>r600</literal> ,
<literal>radeonsi</literal> e <literal>nouveau</literal>, para as GPUs
Radeon HD 2xxx e posteriores da ATI/AMD, e <ulink
url='https://nouveau.freedesktop.org/VideoAcceleration.html'>GPUs suportadas
da NVIDIA</ulink>; o Mesa precisa ser construído depois desse pacote para
esses controladores)
      </para>

      <bridgehead renderas="sect4">Opcionais</bridgehead>
      <para role="optional">
        <xref linkend="doxygen"/>, <xref linkend="graphviz"/> e <xref
linkend="texlive"/> ou <xref linkend="tl-installer"/>
      </para>

    </sect2>

    <sect2 role="installation">
      <title>Instalação do libvdpau</title>

      <para>
        Instale o <application>libvdpau</application> executando os seguintes
comandos:
      </para>

<screen><userinput>mkdir build &amp;&amp;
cd   build &amp;&amp;

meson setup --prefix=$XORG_PREFIX .. &amp;&amp;
ninja</userinput></screen>

      <para>
        Para testar os resultados, emita: <command>ninja test</command>. Existe
somente um teste para esse pacote, dlclose, e ele é conhecido por falhar em
alguns sistemas.
      </para>

      <para>
        Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
      </para>

<screen role="root"><userinput>ninja install</userinput></screen>

      <para>
        Se o <application>doxygen</application> estiver presente ao tempo da
construção, coloque a documentação em um diretório versionado como o(a)
usuário(a) <systemitem class="username">root</systemitem>:
      </para>

<screen role="root"><userinput>[ -e $XORG_PREFIX/share/doc/libvdpau ] &amp;&amp; mv -v $XORG_PREFIX/share/doc/libvdpau{,&libvdpau-version;}</userinput></screen>

    </sect2>

    <sect2 role="content">
      <title>Conteúdo</title>

      <segmentedlist>
        <segtitle>Aplicativos Instalados</segtitle>
        <segtitle>Biblioteca Instalada</segtitle>
        <segtitle>Diretórios Instalados</segtitle>

        <seglistitem>
          <seg>
            Nenhum(a)
          </seg>
          <seg>
            libvdpau.so
          </seg>
          <seg>
            $XORG_PREFIX/{include,lib}/vdpau
          </seg>
        </seglistitem>
      </segmentedlist>

      <variablelist>
        <bridgehead renderas="sect2">Descrições Curtas</bridgehead>
        <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

        <varlistentry id="libvdpau-lib">
          <term><filename class="libraryfile">libvdpau.so</filename></term>
          <listitem>
            <para>
              contém funções para transferir partes do processo de decodificação de vídeo
e pós-processamento de vídeo para o hardware de vídeo da GPU
            </para>
            <indexterm zone="libvdpau libvdpau-lib">
              <primary sortas="c-libvdpau">libvdpau.so</primary>
            </indexterm>
          </listitem>
        </varlistentry>

      </variablelist>

    </sect2>

  </sect1>
