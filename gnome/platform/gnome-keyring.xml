<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY gnome-keyring-download-http
           "&gnome-download-http;/gnome-keyring/&gnome-46;/gnome-keyring-&gnome-keyring-version;.tar.xz">
  <!ENTITY gnome-keyring-download-ftp  "">
  <!ENTITY gnome-keyring-md5sum        "7a8ab16a87f03ca05fc176925fcce649">
  <!ENTITY gnome-keyring-size          "1,3 MB">
  <!ENTITY gnome-keyring-buildsize     "129 MB">
  <!ENTITY gnome-keyring-time          "0,2 UPC (Usando paralelismo=4; adicionar 0,2 UPC para testes)">
]>

<sect1 id="gnome-keyring" xreflabel="gnome-keyring-&gnome-keyring-version;">
  <?dbhtml filename="gnome-keyring.html"?>


  <title>gnome-keyring-&gnome-keyring-version;</title>

  <indexterm zone="gnome-keyring">
    <primary sortas="a-gnome-keyring">gnome-keyring</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao GNOME Keyring</title>

    <para>
      O pacote <application>GNOME Keyring</application> contém um processo de
segundo plano que mantém senhas e outros segredos para usuários(as).
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&gnome-keyring-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&gnome-keyring-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &gnome-keyring-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &gnome-keyring-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &gnome-keyring-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &gnome-keyring-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do GNOME Keyring</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="dbus"/> e <xref linkend="gcr"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="linux-pam"/>, <xref linkend="libxslt"/> e <xref
linkend="openssh"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="gnupg2"/>, <xref linkend="valgrind"/>, <ulink
url="https://github.com/linux-test-project/lcov">LCOV</ulink> e <ulink
url="https://people.redhat.com/sgrubb/libcap-ng/">libcap-ng</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do GNOME Keyring</title>



    <!--
    <para>

      Regenerate the <command>configure</command> script to make it work
      with libgcrypt-1.11 or newer:
    </para>

<screen><userinput>autoreconf</userinput></screen>
-->
<para>
      Instale o <application>GNOME Keyring</application> executando os seguintes
comandos:
    </para>

<screen><userinput>sed -i 's:"/desktop:"/org:' schema/*.xml &amp;&amp;

./configure --prefix=/usr      \
            --sysconfdir=/etc  \
            --enable-ssh-agent &amp;&amp;
make</userinput></screen>

    <para>
      Um endereço de barramento de sessão é necessário para executar os
testes. Para testar os resultados, emita: <command>make check</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command>sed ... schema/*.xml</command>: Esse comando corrige uma entrada
obsoleta no modelo de esquema.
    </para>

    <para>
      <!--https://bugzilla.redhat.com/show_bug.cgi?id=2250704-->
<parameter>--enable-ssh-agent</parameter>: Essa chave habilita o agente SSH
integrado ao gnome-keyring em vez daquele no GCR. Isso é feito devido a
defeitos no gcr que fazem com que o agente não funcione corretamente.
      
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          gnome-keyring (link simbólico), gnome-keyring-3 e gnome-keyring-daemon
        </seg>
        <seg>
          gnome-keyring-pkcs11.so (módulo PKCS#11) e pam_gnome_keyring.so (módulo PAM)
        </seg>
        <seg>
          /usr/lib/gnome-keyring e /usr/share/xdg-desktop-portal
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="gnome-keyring-daemon">
        <term><command>gnome-keyring-daemon</command></term>
        <listitem>
          <para>
            é um processo de segundo plano de sessão que mantém senhas para usuários(as)
          </para>
          <indexterm zone="gnome-keyring gnome-keyring-daemon">
            <primary sortas="b-gnome-keyring-daemon">gnome-keyring-daemon</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
