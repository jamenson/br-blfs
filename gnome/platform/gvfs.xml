<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY gvfs-download-http "&gnome-download-http;/gvfs/1.56/gvfs-&gvfs-version;.tar.xz">
  <!ENTITY gvfs-download-ftp  "">
  <!ENTITY gvfs-md5sum        "5ba12cd02b17c058aa71adc39c5eb6cf">
  <!ENTITY gvfs-size          "1,2 MB">
  <!ENTITY gvfs-buildsize     "29 MB">
  <!ENTITY gvfs-time          "0,1 UPC (usando paralelismo = 4)">
]>

<sect1 id="gvfs" xreflabel="Gvfs-&gvfs-version;">
  <?dbhtml filename="gvfs.html"?>


  <title>Gvfs-&gvfs-version;</title>

  <indexterm zone="gvfs">
    <primary sortas="a-Gvfs">Gvfs</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Gvfs</title>

    <para>
      O pacote <application>Gvfs</application> é um sistema de arquivos virtual de
espaço de usuário(a) projetado para funcionar com as abstrações de E/S da
biblioteca GIO da GLib.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&gvfs-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&gvfs-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &gvfs-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &gvfs-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &gvfs-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &gvfs-time;
        </para>
      </listitem>
    </itemizedlist>

    <!--
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Required patch:
          <ulink url="&patch-root;/gvfs-&gvfs-version;-fix_security_vulnerabilities-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do Gvfs</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="dbus"/>, <xref linkend="glib2"/>, <xref linkend="gcr4"/>,
<xref linkend="libusb"/> e <xref linkend="libsecret"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="gtk3"/>, <xref linkend="libcdio"/>, <xref
linkend="libgudev"/>, <xref linkend="libsoup3"/>, <phrase
revision="sysv"><xref linkend="elogind"/></phrase> <phrase
revision="systemd"><xref role='runtime' linkend="systemd"/> (tempo de
execução)</phrase> e <xref linkend="udisks2"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="apache"/>, <xref linkend="avahi"/>, <xref linkend="bluez"/>,
<xref linkend="fuse3"/>, <xref linkend="gnome-online-accounts"/>, <xref
linkend="gtk-doc"/>, <xref linkend="libarchive"/>, <xref
linkend="libgcrypt"/>, <xref linkend="libgdata"/>, <xref
linkend="libxml2"/>, <xref linkend="libxslt"/>, <xref linkend="openssh"/>,
<xref linkend="samba"/>, <ulink
url="&gnome-download-http;/gnome-desktop-testing/">gnome-desktop-testing</ulink>
(para testes), <ulink
url="https://www.videolan.org/developers/libbluray.html">libbluray</ulink>,
<ulink url="http://www.gphoto.org/">libgphoto2</ulink>, <ulink
url="https://www.libimobiledevice.org/">libimobiledevice</ulink>, <ulink
url="https://gitlab.gnome.org/GNOME/msgraph">libmsgraph</ulink>, <ulink
url="https://libmtp.sourceforge.net/">libmtp</ulink>, <ulink
url="https://github.com/sahlberg/libnfs/">libnfs</ulink> e <ulink
url="https://twisted.org/">Twisted</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Gvfs</title>

    <para>
      Instale o <application>Gvfs</application> executando os seguintes comandos:
    </para>

<screen revision='sysv'><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup               \
      --prefix=/usr       \
      --buildtype=release \
      -D onedrive=false   \
      -D fuse=false       \
      -D gphoto2=false    \
      -D afc=false        \
      -D bluray=false     \
      -D nfs=false        \
      -D mtp=false        \
      -D smb=false        \
      -D tmpfilesdir=no   \
      -D dnssd=false      \
      -D goa=false        \
      -D google=false     \
      -D systemduserunitdir=no .. &amp;&amp;
ninja</userinput></screen>

<screen revision='systemd'><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup               \
      --prefix=/usr       \
      --buildtype=release \
      -D onedrive=false   \
      -D fuse=false       \
      -D gphoto2=false    \
      -D afc=false        \
      -D bluray=false     \
      -D nfs=false        \
      -D mtp=false        \
      -D smb=false        \
      -D dnssd=false      \
      -D goa=false        \
      -D google=false     .. &amp;&amp;
ninja</userinput></screen>

    <para>
      A suíte de teste exige <application>gnome-desktop-testing</application>, que
está além do escopo do BLFS.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/gsettings-destdir.xml"/>
  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D &lt;option&gt;=false</parameter>: Essas chaves são exigidas se
a dependência correspondente não estiver instalada. Remova aquelas onde você
tiver instalado o aplicativo correspondente e desejar usá-lo com o
<application>Gvfs</application>. A opção dnssd exige avahi e, ambos, goa e
google exigem GNOME Online Accounts. A opção google também exige libgdata. A
opção google também exige libgdata. O suporte a onedrive exige o pacote
libmsgraph.
    </para>

    <para revision="sysv">
      <parameter>-D tmpfilesdir=no</parameter> <parameter>-D
systemduserunitdir=no</parameter>: Essas chaves fornecem locais de diretório
do systemd. Configurá-las para <quote>no</quote> desabilita a dependência do
systemd e do logind.
    </para>

    <para>
      <option>-D cdda=false</option>: Essa chave é exigida se a libcdio não
estiver instalada. A estrutura de retaguarda cdda é inútil em máquinas sem
uma unidade de CDROM/DVD.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libgvfscommon.so, libgvfsdaemon.so e algumas sob /usr/lib/gio/modules/
        </seg>
        <seg>
          /usr/include/gvfs-client e /usr/{lib,share}/gvfs
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libgvfscommon">
        <term><filename class="libraryfile">libgvfscommon.so</filename></term>
        <listitem>
          <para>
            contém as funções comuns de API usadas em aplicativos
<application>Gvfs</application>
          </para>
          <indexterm zone="gvfs libgvfscommon">
            <primary sortas="c-libgvfscommon">libgvfscommon.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
