<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY spidermonkey-download-http "&mozilla-http;/firefox/releases/&spidermonkey-version;esr/source/firefox-&spidermonkey-version;esr.source.tar.xz">
  <!ENTITY spidermonkey-download-ftp  "">
  <!-- size and md5sum are in packages.ent -->
  <!ENTITY spidermonkey-buildsize     "3,6 GB (40 MB instalado depois de remover 36 MB de bibliotecas estáticas;
adicionar 34 MB para os testes principais e 37 MB para os testes jit)">
  <!ENTITY spidermonkey-time          "1,9 UPC (com paralelismo=4; adicionar 1,1 UPC para testes principais e 3,7
UPC para os testes jit)">
]>

<sect1 id="spidermonkey" xreflabel="SpiderMonkey oriundo de Firefox-&spidermonkey-version;">
  <?dbhtml filename="spidermonkey.html"?>


  <title>SpiderMonkey oriundo de firefox-&spidermonkey-version;</title>

  <indexterm zone="spidermonkey">
    <primary sortas="a-spidermonkey">SpiderMonkey</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao SpiderMonkey</title>

    <para>
      <application>SpiderMonkey</application> é o mecanismo JavaScript e
WebAssembly da Mozilla, escrito em C++ e Rust. No BLFS, o código-fonte do
SpiderMonkey é retirado do Firefox.
    </para>

    
    <!-- To editors: make sure polkit works with mozjs when
         tagging SpiderMonkey or upgrading it to a new major version.  -->
&lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&spidermonkey-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&spidermonkey-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &spidermonkey-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &spidermonkey-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &spidermonkey-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &spidermonkey-time;
        </para>
      </listitem>
    </itemizedlist>

    <!--
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Required patch:
          <ulink url="&patch-root;/js-&JS91-version;-python_3_10-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do SpiderMonkey</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <!--<xref linkend="six"/>
, and-->
<xref linkend="cbindgen"/>, <xref linkend="icu"/>, <xref linkend="which"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      
      <!-- If clang is installed, it will be used instead of gcc.
           gcc does not work for 32-bit system w/o -msse2 -mfpmath=sse:
           https://bugzilla.mozilla.org/show_bug.cgi?id=1729459 -->
<xref linkend="llvm"/> (com <application>Clang</application>, exigido para
sistemas de 32 bits sem recursos de SSE2)
    </para>

    <important>
      <para>
        Se você estiver construindo esse pacote em um sistema de 32 bits e o Clang
não estiver instalado ou você estiver substituindo a opção padrão do
compilador com a variável de ambiente <envar>CXX</envar>, por favor, leia
primeiro a seção Explicações do Comando.
      </para>
    </important>

    
    

  <!-- It seems nasm is only used for aarch64-win64. -->
<!--bridgehead renderas="sect4">
Optional</bridgehead>
    <para role="optional">
      <xref linkend="nasm"/>
    </para-->
</sect2>

  <sect2 role="installation">
    <title>Instalação do SpiderMonkey</title>

    <note>
      <para>
        Ao contrário da maioria dos outros pacotes no BLFS, as instruções abaixo
exigem que você desempacote
<filename>firefox-&spidermonkey-version;esr.tar.xz</filename> e mude para o
diretório <filename>firefox-&spidermonkey-version;</filename>.
      </para>

      <para>
        Extrair o tarball reconfigurará as permissões do diretório atual para 0755,
se você tiver permissão para fazer isso. Se você fizer isso em um diretório
onde o bit sticky estiver configurado, como <filename
class="directory">/tmp</filename>, ela terminará com mensagens de erro:
      </para>

<literallayout>tar: .: Cannot utime: Operation not permitted
tar: .: Cannot change mode to rwxr-xr-t: Operation not permitted
tar: Exiting with failure status due to previous errors
</literallayout>

      <para>
        Isso finaliza com situação diferente de zero, mas <emphasis>NÃO</emphasis>
significa que existe um problema real. Não desempacote como o(a) usuário(a)
<systemitem class="username">root</systemitem> em um diretório onde o bit
sticky estiver configurado - isso irá desconfigurá-lo.
      </para>

    </note>

    <para>
      Se você estiver usando ICU-76.1 ou posterior, adapte o sistema de construção
para usar a biblioteca correta:
    </para>

<screen><userinput remap="pre">sed -i 's/icu-i18n/icu-uc &amp;/' js/moz.configure</userinput></screen>

    <para>
      Instale <application>SpiderMonkey</application> executando os seguintes
comandos:
    </para>

    <note>
      <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
         href="../../xincludes/mozshm.xml"/>
      <para>
        Compilar o código C++ respeita $MAKEFLAGS e o padrão é 'j1'; o código do
rust usará todos os processadores.
      </para>
    </note>

<screen><userinput>mkdir obj &amp;&amp;
cd    obj &amp;&amp;

../js/src/configure --prefix=/usr            \
                    --disable-debug-symbols  \
                    --disable-jemalloc       \
                    --enable-readline        \
                    --enable-rust-simd       \
                    --with-intl-api          \
                    --with-system-icu        \
                    --with-system-zlib       &amp;&amp;
make</userinput></screen>

    <para>
      Se você desejar executar a suíte de teste, adapte-a para ser compatível com
Python 3.13.0 ou mais recente:
    </para>

    <screen remap='test'><userinput>sed 's/pipes/shlex/' -i ../js/src/tests/lib/results.py</userinput></screen>

    <para>
      Para executar a suíte de teste SpiderMonkey, emita:
    </para>

    <screen remap='test'><userinput>make -C js/src check-jstests \
     JSTESTS_EXTRA_ARGS="--timeout 300 --wpt=disabled" | tee jstest.log</userinput></screen>

    <para>
      Como nós estamos construindo com o ICU do sistema, 155 testes (de um total
de mais de 50.000) são conhecidos por falharem. A lista de testes falhos
pode ser extraída via <command>grep 'UNEXPECTED-FAIL'
jstest.log</command>. Passe a opção <option>-c</option> para
<command>grep</command> se você quiser somente o número total de testes
falhos.
    </para>

    <para>
      A suíte de teste é executada com todos os núcleos de CPU disponíveis: mesmo
em um cgroup com menos núcleos atribuídos, ela ainda tenta gerar tantas
tarefas de teste quanto o número de <emphasis>todos</emphasis> os núcleos no
sistema; felizmente, o núcleo ainda não executará essas tarefas em núcleos
não atribuídos ao cgroup, de forma que o uso da CPU ainda é controlado.
    </para>

    <para>
      Para executar a suíte de teste JIT, emita o comando a seguir. Observe que 6
testes na suíte 'timezone.js' são conhecidos por falharem devido ao ICU-76.
    </para>

    <screen remap='test'><userinput>make -C js/src check-jit-test</userinput></screen>

    <para>
      <!-- TL;DR: DO NOT REMOVE MEMORY USAGE NOTE W/O MY CONFIRMATION!

           "six tests": large-arraybuffers/arraybuffer-transfer.js, it's ran
           with 6 different configurations.

           "may": this is stochasitic (like all parallelization issue),
           don't remove the note about memory usage simply because "I cannot
           reproduce it".

           "peak": the time period using so much memory is very short, so
           don't just watch the output of "top" or "free" with eyesight.
           Run the test in a cgroup and read the "memory.peak" pseudo file
           for a proper measurement.

           Q: Why not just document some test failures?
           A: This *really* can cause stability issue because the kernel
              may OOM kill another process if the test is not ran in a
              cgroup with memory.max set.  Even if running it in a cgroup,
              the kernel may still OOM kill the "main" process controlling
              the test process instead of a single test job, causing a
              incomplete test.

                                                                 - xry111
      -->
Assim como a suíte de teste do SpiderMonkey, o número de tarefas de teste é
o mesmo que o número de todos os núcleos de CPU no sistema, mesmo se um
cgroup for usado. Para piorar as coisas, alguns casos de teste podem usar
até 4 GB de memória do sistema, de forma que o pico de uso de memória
possivelmente seja muito grande se tua CPU tiver vários núcleos. Executar a
suíte de teste JIT sem memória suficiente possivelmente invoque o OOM killer
do núcleo e cause problemas de estabilidade. Se você não tiver memória de
sistema suficiente disponível, posponha
<option>JITTEST_EXTRA_ARGS=-j<replaceable>N</replaceable></option> ao
comando e substitua <replaceable>N</replaceable> pelo número de tarefas de
teste que tua memória de sistema disponível consiga conter. Por exemplo, se
você tiver 15 GB de memória de sistema disponível e 4 núcleos de CPU,
posponha <option>JITTEST_EXTRA_ARGS=-j3</option> para executar a suíte de
teste com 3 tarefas paralelas de forma que o uso de memória não exceda 12
GB.
      
    </para>

    <caution>
      <para>
        Um problema no processo de instalação faz com que qualquer programa em
execução que se vincule à biblioteca compartilhada do SpiderMonkey (por
exemplo, GNOME Shell) trave se o SpiderMonkey for reinstalado, atualizado ou
rebaixado sem uma mudança do número da versão principal
(&spidermonkey-major; em &spidermonkey-version;). Para contornar esse
problema, remova a versão antiga da biblioteca compartilhada do SpiderMonkey
antes da instalação:
      </para>

<screen role="root"><userinput>rm -fv /usr/lib/libmozjs-&spidermonkey-major;.so</userinput></screen>
    </caution>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install &amp;&amp;
rm -v /usr/lib/libjs_static.ajs &amp;&amp;
sed -i '/@NSPR_CFLAGS@/d' /usr/bin/js&spidermonkey-major;-config</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>--disable-debug-symbols</parameter>: Não gere símbolos de
depuração, pois eles são muito grandes e a maioria dos(as) usuários(as) não
precisará deles. Remova-o se você quiser depurar o SpiderMonkey.
    </para>

    <para>
      <parameter>--disable-jemalloc</parameter>: Essa chave desabilita o alocador
de memória interna usado no SpiderMonkey. jemalloc destina-se somente para o
ambiente do navegador Firefox. Para outros aplicativos que usam o
SpiderMonkey, o aplicativo pode travar à medida que os itens alocados no
alocador jemalloc forem liberados no alocador do sistema (glibc).
    </para>

    <para>
      <parameter>--enable-readline</parameter>: Essa chave habilita suporte
Readline na interface de linha de comando do SpiderMonkey.
    </para>

    <para>
      <parameter>--enable-rust-simd</parameter>: Essa chave habilita otimização do
SIMD na caixa encoding_rs enviada.
    </para>

    <para>
      <parameter>--with-intl-api</parameter>: Isso habilita as funções de
internacionalização exigidas pelo <application>Gjs</application>.
    </para>

    <para>
      <parameter>--with-system-*</parameter>: Esses parâmetros permitem que o
sistema de construção use versões de sistema das bibliotecas acima. Eles são
necessários para estabilidade.
    </para>

    <para>
      <command>rm -v /usr/lib/libjs_static.ajs</command>: Remove uma grande
biblioteca estática que não é usada por nenhum pacote do BLFS.
    </para>

    <para>
      <command>sed -i '/@NSPR_CFLAGS@/d'
/usr/bin/js&spidermonkey-major;-config</command>: Impede que
<command>js&spidermonkey-major;-config</command> use CFLAGS defeituosas.
    </para>

    <para>
      <option><envar>CC=gcc CXX=g++</envar></option>: O BLFS costumava preferir
usar gcc e g++ em vez dos padrões do fluxo de desenvolvimento dos programas
<application>clang</application>. Com o lançamento do gcc-12 a construção
demora mais tempo com gcc e g++, principalmente por causa de avisos extras,
e é maior. Passe essas variáveis de ambiente para o conjunto de comandos
sequenciais de configuração se você desejar continuar a usar gcc, g++
(exportando-as e desconfigurando-as depois da instalação ou simplesmente
acrescentando-as antes do comando
<command>../js/src/configure</command>). Se você estiver construindo em um
sistema de 32 bits, veja também abaixo.
    </para>

    <para>
      <option><envar>CXXFLAGS="-msse2 -mfpmath=sse"</envar></option>: Use SSE2 em
vez de 387 para operações de ponto flutuante de precisão dupla. É necessário
ao GCC para satisfazer as expectativas dos(as) desenvolvedores(as) do fluxo
de desenvolvimento (Mozilla) com aritmética de ponto flutuante. Use-o se
você estiver construindo esse pacote em um sistema de 32 bits com GCC (se o
Clang não estiver instalado ou o GCC for especificado
explicitamente). Observe que isso fará com que o SpiderMonkey trave em um
processador sem capacidade SSE2. Se você estiver executando o sistema em um
processador tão antigo, o Clang será estritamente necessário. Essa
configuração não é necessária em sistemas de 64 bits porque todos os
processadores x86 de 64 bits suportam SSE2 e os compiladores de 64 bits
(ambos, Clang e GCC) usam SSE2 por padrão.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          js&spidermonkey-major; e js&spidermonkey-major;-config
        </seg>
        <seg>
          libmozjs-&spidermonkey-major;.so
        </seg>
        <seg>
          /usr/include/mozjs-&spidermonkey-major;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="js&spidermonkey-major;">
        <term><command>js&spidermonkey-major;</command></term>
        <listitem>
          <para>
            fornece uma interface de linha de comando para o mecanismo do
<application>JavaScript</application>
          </para>
          <indexterm zone="spidermonkey js&spidermonkey-major;">
            <primary sortas="b-js&spidermonkey-major;">
              js&spidermonkey-major;
            </primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="js&spidermonkey-major;-config">
        <term><command>js&spidermonkey-major;-config</command></term>
        <listitem>
          <para>
            é usado para encontrar o compilador SpiderMonkey e os sinalizadores do
vinculador
          </para>
          <indexterm zone="spidermonkey js&spidermonkey-major;-config">
            <primary sortas="b-js&spidermonkey-major;-config">
              js&spidermonkey-major;-config
            </primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libmozjs-&spidermonkey-major;">
        <term>
          <filename class="libraryfile"> libmozjs-&spidermonkey-major;.so </filename>
        </term>
        <listitem>
          <para>
            contém as funções da API de JavaScript da Mozilla
          </para>
          <indexterm zone="spidermonkey libmozjs-&spidermonkey-major;">
            <primary sortas="c-libmozjs&spidermonkey-major;">
              libmozjs-&spidermonkey-major;.so
            </primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>

</sect1>
