<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY glib2-download-http "&gnome-download-http;/glib/&glib2-minor;/glib-&glib2-version;.tar.xz">
  <!ENTITY glib2-download-ftp  "">
  <!ENTITY glib2-md5sum        "87c7641e80b23a05b8ab506d52c970e3">
  <!ENTITY glib2-size          "5,3 MB">
  <!ENTITY glib2-buildsize     "229 MB (adicionar 22 MB para os testes)">
  <!ENTITY glib2-time          "0,7 UPC (adicionar 0,4 UPC para testes; ambos usando paralelismo=4)">

  <!ENTITY gobject-introspection-download-http
    "&gnome-download-http;/gobject-introspection/1.82/gobject-introspection-&gobject-introspection-version;.tar.xz">
  <!ENTITY gobject-introspection-md5sum        "50beb465bc81f33395b5e0e3bbe364ec">
  <!ENTITY gobject-introspection-size          "1,1 MB">
]>

<sect1 id="glib2" xreflabel="GLib-&glib2-version;">
  <?dbhtml filename="glib2.html"?>


  <title>GLib-&glib2-version;</title>

  <indexterm zone="glib2">
    <primary sortas="a-GLib2">GLib2</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao GLib</title>

    <para>
      O pacote <application>GLib</application> contém bibliotecas de baixo nível
úteis para fornecer manipulação de estrutura de dados para C, amarradores de
portabilidade e interfaces para funcionalidades em tempo de execução, como
um loop de eventos, camadas, carregamento dinâmico e um sistema de objetos.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&glib2-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&glib2-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &glib2-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &glib2-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &glib2-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &glib2-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>

    <para>
      <emphasis role="strong">GObject Introspection (Recomendado)</emphasis>
    </para>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência: <ulink url="&gobject-introspection-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &gobject-introspection-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &gobject-introspection-size;
        </para>
      </listitem>
    </itemizedlist>

    <para>
      <emphasis role="strong">Remendo para Seleção de Nível de Registro
(Opcional)</emphasis>
    </para>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Remendo opcional: <ulink url="&patch-root;/glib-skip_warnings-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências de GLib</bridgehead>

    <!--
    <bridgehead renderas="sect4">
Required</bridgehead>
    <para role="required">
      <xref linkend='packaging'/>
    </para>
-->
<bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend='docutils'/>, <xref linkend="libxslt"/> e <xref
linkend="pcre2"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <!--<xref linkend="sysprof"/>
-->
<xref linkend='cairo'/> (para alguns testes do GObject Introspection), <xref
linkend="dbus"/> (para alguns testes), <xref linkend="fuse3"/> e <ulink
url="https://bindfs.org/">bindfs</ulink> (ambos necessários para um teste),
<xref linkend="gdb"/> (para ligações), <xref linkend='gjs'/> (para alguns
testes do GObject Introspection), <xref linkend='gtk-doc'/> (para
documentação do GObject Introspection), <xref linkend="DocBook"/>, <xref
linkend="docbook-xsl"/>, <xref linkend="gi-docgen"/> (para construir
documentação de API), <xref linkend="glib-networking"/> (para alguns testes,
mas isso é uma dependência circular), <xref linkend="Mako"/> e <xref
linkend="markdown"/> (ambos para <command>g-ir-doc-tool</command>) e <ulink
url="&sysprof-url;">sysprof</ulink>
    </para>

    <bridgehead renderas="sect4">Dependências Adicionais de Tempo de Execução</bridgehead>
    <para role="recommended">
      Citado diretamente a partir do arquivo <filename>INSTALL</filename>:
<quote>Algumas das funcionalidades relacionadas ao tipo mime no GIO exigem
os utilitários <command>update-mime-database</command> e
<command>update-desktop-database</command></quote>, os quais são parte de
<xref role="runtime" linkend="shared-mime-info"/> e <xref role="runtime"
linkend="desktop-file-utils"/>, respectivamente. Esses dois utilitários
também são necessários para alguns testes.
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do GLib</title>

    <para>
      Se desejado, aplique o remendo opcional. Em muitos casos, os aplicativos que
usam essa biblioteca, direta ou indiretamente, por meio de outras
bibliotecas, tais como <xref linkend="gtk3"/>, emitem numerosos avisos
quando executados a partir da linha de comando. Esse remendo habilita o uso
de uma variável de ambiente, <envar>GLIB_LOG_LEVEL</envar>, que suprime
mensagens indesejadas. O valor da variável é um dígito que corresponde a:
    </para>

    <simplelist>
      <member>1 Alerta</member>
      <member>2 Crítico</member>
      <member>3 Erro</member>
      <member>4 Aviso</member>
      <member>5 Informe</member>
    </simplelist>

    <para>
      Por exemplo, <userinput>export GLIB_LOG_LEVEL=4</userinput> pulará a saída
gerada de mensagens Warning e Notice (e mensagens de Informação/Depuração se
estiverem ativadas). Se <envar>GLIB_LOG_LEVEL</envar> não estiver definida,
a saída gerada normal de mensagem não será afetada.
    </para>

<screen><userinput>patch -Np1 -i ../glib-skip_warnings-1.patch</userinput></screen>

    <warning>
      <para>
        Se uma versão anterior da glib estiver instalada, [então] mova os cabeçalhos
para fora do caminho, de modo que os pacotes posteriores não encontrem
conflitos:
      </para>

<screen role="root"><userinput remap="pre">if [ -e /usr/include/glib-2.0 ]; then
    rm -rf /usr/include/glib-2.0.old &amp;&amp;
    mv -vf /usr/include/glib-2.0{,.old}
fi</userinput></screen>

    </warning>

    <para>
      Instale <application>GLib</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..                  \
      --prefix=/usr             \
      --buildtype=release       \
      -D introspection=disabled \
      -D glib_debug=disabled    \
      -D man-pages=enabled      \
      -D sysprof=disabled       &amp;&amp;
ninja</userinput></screen>

    <para>
      A suíte de teste da <application>GLib</application> exige
<application>desktop-file-utils</application> para alguns testes. No
entanto, <application>desktop-file-utils</application> exige a
<application>GLib</application> para a finalidade de compilar; portanto,
você precisa primeiro instalar a <application>GLib</application> e então
executar a suíte de teste.
    </para>

    <para>
      Como o(a) usuário(a) <systemitem class="username">root</systemitem>, instale
esse pacote pela primeira vez para permitir construir o GObject
Introspection:
    </para>

    <screen role="root"><userinput>ninja install</userinput></screen>

    <para>
      Construa GObject Introspection:
    </para>

    <screen><userinput>tar xf ../../gobject-introspection-&gobject-introspection-version;.tar.xz &amp;&amp;

meson setup gobject-introspection-&gobject-introspection-version; gi-build \
            --prefix=/usr --buildtype=release     &amp;&amp;
ninja -C gi-build</userinput></screen>

    <para>
      Para testar os resultados do GObject Introspection, emita: <command>ninja -C
gi-build test</command>.
    </para>

    <para>
      Como o(a) usuário(a) &root;, instale o GObject Introspection para gerar os
dados de introspecção das bibliotecas GLib (requeridos por vários pacotes
usantes do Glib, especialmente alguns pacotes do GNOME):
    </para>

    <screen role='root'><userinput>ninja -C gi-build install</userinput></screen>

    <para>
      Agora gere os dados de introspecção:
    </para>

    <screen><userinput>meson configure -D introspection=enabled &amp;&amp;
ninja</userinput></screen>

    <para>
      Se você tiver <xref linkend='gi-docgen'/> instalado e desejar construir a
documentação da API para esse pacote, emita:
    </para>

    <screen remap='doc'><userinput>sed "/docs_dir =/s|$| / 'glib-' + meson.project_version()|" \
    -i ../docs/reference/meson.build                        &amp;&amp;
meson configure -D documentation=true                       &amp;&amp;
ninja</userinput></screen>

    <para>
      Como o(a) usuário(a) &root;, instale esse pacote novamente para os dados de
introspecção (e, opcionalmente, a documentação):
    </para>

    <screen role='root'><userinput>ninja install</userinput></screen>

    <para>
      Você deveria agora instalar <xref linkend="desktop-file-utils"/> e <xref
linkend="shared-mime-info"/> e proceder à execução da suíte de teste.
    </para>

    <warning>
      <para>
        Não execute a suíte de teste como &root; ou alguns testes falharão
inesperadamente e deixarão alguns diretórios não compatíveis com FHS na
hierarquia <filename class='directory'>/usr</filename>.
      </para>
    </warning>
    
    <!-- I cannot reproduce these issues.
    <note>

      <para>
        If you have installed the glib-skip_warnings-1.patch and have 
        the environment variable GLIB_LOG_LEVEL set, unset it before running 
        the tests.  It will cause several tests to fail.
      </para>
    </note>

    <note>
      <para>
        If you have used the -D glib_debug=disabled option, 17 tests will 
        fail.  
      </para>
    </note>
    -->
<para>
      Para testar os resultados, depois de ter instalado os pacotes, emita:
<command>LC_ALL=C ninja test</command> como um(a) usuário(a) não-&root;.
    </para>
  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D man-pages=enabled</parameter>: Essa chave faz com que a
construção crie e instale as páginas de manual do pacote.
    </para>

    <para>
      <parameter>-D glib_debug=disabled</parameter>: Essa chave faz com que a
construção não inclua algumas verificações caras para depuração nos
programas e bibliotecas construídos.
    </para>

    <para>
      <parameter>-D sysprof=disabled</parameter>: Essa chave desabilita o suporte
de rastreamento para sysprof. Remova essa opção se você quiser o suporte de
rastreamento. Observe que se o sysprof não estiver instalado, remover essa
opção fará com que o sistema de construção baixe uma cópia do sysprof a
partir da Internet.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <bridgehead renderas="sect3">Conteúdo do GLib</bridgehead>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          gapplication, gdbus, gdbus-codegen, gi-compile-repository,
gi-decompile-typelib, gi-inspect-typelib gio, gio-querymodules,
glib-compile-resources, glib-compile-schemas, glib-genmarshal,
glib-gettextize, glib-mkenums, gobject-query, gresource, gsettings, gtester
e gtester-report
        </seg>
        <seg>
          libgio-2.0.so, libgirepository-2.0.so, libglib-2.0.so, libgmodule-2.0.so,
libgobject-2.0.so e libgthread-2.0.so
        </seg>
        <seg>
          /usr/include/gio-unix-2.0, /usr/include/glib-2.0, /usr/lib/gio,
/usr/lib/glib-2.0, /usr/share/glib-2.0 e /usr/share/doc/glib-&glib2-version;
(opcional)
        </seg>
      </seglistitem>
    </segmentedlist>

    <bridgehead renderas="sect3">Conteúdo do GObject Introspection</bridgehead>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      
      <!-- g-ir-doc-tool is installed if Mako and Markdown are present -->
<seglistitem>
        <seg>
          g-ir-annotation-tool, g-ir-compiler, g-ir-doc-tool (opcional),
g-ir-generate, g-ir-inspect e g-ir-scanner
        </seg>
        <seg>
          libgirepository-1.0.so e _giscanner.&python3-lib-suffix;.so
        </seg>
        <seg>
          /usr/include/gobject-introspection-1.0, /usr/lib/girepository-1.0,
/usr/lib/gobject-introspection, /usr/share/gir-1.0 e
/usr/share/gobject-introspection-1.0
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="gapplication">
        <term><command>gapplication</command></term>
        <listitem>
          <para>
            pode ser usado para iniciar aplicativos e para enviar mensagens para
instâncias já em execução de outros aplicativos
          </para>
          <indexterm zone="glib2 gapplication">
            <primary sortas="b-gapplication">aplicativo</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gdbus">
        <term><command>gdbus</command></term>
        <listitem>
          <para>
            é uma ferramenta simples usada para trabalhar com objetos
<application>D-Bus</application>
          </para>
          <indexterm zone="glib2 gdbus">
            <primary sortas="b-gdbus">gdbus</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gdbus-codegen">
        <term><command>gdbus-codegen</command></term>
        <listitem>
          <para>
            é usado para gerar código e (ou) documentação para uma ou mais interfaces
<application>D-Bus</application>
          </para>
          <indexterm zone="glib2 gdbus-codegen">
            <primary sortas="b-gdbus-codegen">gdbus-codegen</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gi-compile-repository">
        <term><command>gi-compile-repository</command></term>
        <listitem>
          <para>
            converte um ou mais arquivos GIR em um ou mais arquivos typelib
          </para>
          <indexterm zone="glib2 gi-compile-repository">
            <primary sortas="b-gi-compile-repository">gi-compile-repository</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gi-decompile-typelib">
        <term><command>gi-decompile-typelib</command></term>
        <listitem>
          <para>
            é um descompilador GIR que usa a API do repositório
          </para>
          <indexterm zone="glib2 gi-decompile-typelib">
            <primary sortas="b-gi-decompile-typelib">gi-decompile-typelib</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gi-inspect-typelib">
        <term><command>gi-inspect-typelib</command></term>
        <listitem>
          <para>
            é um utilitário que fornece informações acerca de um typelib do GI
          </para>
          <indexterm zone="glib2 gi-inspect-typelib">
            <primary sortas="b-gi-inspect-typelib">gi-inspect-typelib</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gio">
        <term><command>gio</command></term>
        <listitem>
          <para>
            é um utilitário que torna muitos recursos do <application>GIO</application>
disponíveis a partir da linha de comando
          </para>
          <indexterm zone="glib2 gio">
            <primary sortas="b-gio">gio</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gio-querymodules">
        <term><command>gio-querymodules</command></term>
        <listitem>
          <para>
            é usado para criar um arquivo <filename>giomodule.cache</filename> nos
diretórios listados. Esse arquivo lista os pontos implementados de extensão
para cada módulo que tenha sido encontrado
          </para>
          <indexterm zone="glib2 gio-querymodules">
            <primary sortas="b-gio-querymodules">gio-querymodules</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="glib-compile-resources">
        <term><command>glib-compile-resources</command></term>
        <listitem>
          <para>
            é usado para ler a descrição do recurso a partir de um arquivo e os arquivos
aos quais ele referencia para criar um pacote binário de recurso que seja
adequado para uso com a API GResource
          </para>
          <indexterm zone="glib2 glib-compile-resources">
            <primary sortas="b-glib-compile-resources">glib-compile-resources</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="glib-compile-schemas">
        <term><command>glib-compile-schemas</command></term>
        <listitem>
          <para>
            é usado para compilar todos os arquivos de esquema XML do GSettings em um
diretório para um arquivo binário com o nome
<filename>gschemas.compiled</filename> que pode ser usado pelo GSettings
          </para>
          <indexterm zone="glib2 glib-compile-schemas">
            <primary sortas="b-glib-compile-resources">glib-compile-schemas</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="glib-genmarshal">
        <term><command>glib-genmarshal</command></term>
        <listitem>
          <para>
            é um utilitário de geração de código marechal C para encerramentos GLib
          </para>
          <indexterm zone="glib2 glib-genmarshal">
            <primary sortas="b-glib-genmarshal">glib-genmarshal</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="glib-gettextize">
        <term><command>glib-gettextize</command></term>
        <listitem>
          <para>
            é uma variante do utilitário de internacionalização
<application>gettext</application>
          </para>
          <indexterm zone="glib2 glib-gettextize">
            <primary sortas="b-glib-gettextize">glib-gettextize</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="glib-mkenums">
        <term><command>glib-mkenums</command></term>
        <listitem>
          <para>
            é um utilitário de geração de descrição de enumeração em linguagem C
          </para>
          <indexterm zone="glib2 glib-mkenums">
            <primary sortas="b-glib-mkenums">glib-mkenums</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gobject-query">
        <term><command>gobject-query</command></term>
        <listitem>
          <para>
            é um pequeno utilitário que desenha uma árvore de tipos
          </para>
          <indexterm zone="glib2 gobject-query">
            <primary sortas="b-gobject-query">gobject-query</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gresource">
        <term><command>gresource</command></term>
        <listitem>
          <para>
            oferece uma interface simples de linha de comando para GResource
          </para>
          <indexterm zone="glib2 gresource">
            <primary sortas="b-gresource">gresource</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gsettings">
        <term><command>gsettings</command></term>
        <listitem>
          <para>
            oferece uma interface simples de linha de comando para GSettings
          </para>
          <indexterm zone="glib2 gsettings">
            <primary sortas="b-gsettings">gsettings</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gtester">
        <term><command>gtester</command></term>
        <listitem>
          <para>
            é um utilitário de execução de teste
          </para>
          <indexterm zone="glib2 gtester">
            <primary sortas="b-gtester">gtester</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gtester-report">
        <term><command>gtester-report</command></term>
        <listitem>
          <para>
            é um utilitário de formatação de informe de teste
          </para>
          <indexterm zone="glib2 gtester-report">
            <primary sortas="b-gtester-report">gtester-report</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgio">
        <term><filename class='libraryfile'>libgio-2.0.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca que fornece classes úteis para E/S de uso geral, rede de
intercomunicação, IPC, configurações e outras funcionalidades de aplicativos
de alto nível
          </para>
          <indexterm zone="glib2 libgio">
            <primary sortas="c-libgio">libgio-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgirepository">
        <term><filename class='libraryfile'>libgirepository-2.0.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca que fornece acesso a typelibs e dados de introspecção que
descrevem APIs da C
          </para>
          <indexterm zone="glib2 libgirepository">
            <primary sortas="c-libgirepository2">libgirepository-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libglib">
        <term><filename class='libraryfile'>libglib-2.0.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca de utilitários portável de uso geral, que fornece muitos
tipos úteis de dados, macros, conversões de tipo, utilitários de sequências
de caracteres, utilitários de arquivo, uma abstração de mainloop e assim por
diante
          </para>
          <indexterm zone="glib2 libglib">
            <primary sortas="c-libglib">libglib-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgmodule">
        <term><filename class='libraryfile'>libgmodule-2.0.so</filename></term>
        <listitem>
          <para>
            fornece API portável para carregar dinamicamente módulos
          </para>
          <indexterm zone="glib2 libgmodule">
            <primary sortas="c-libgmodule">libgmodule-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgobject">
        <term><filename class='libraryfile'>libgobject-2.0.so</filename></term>
        <listitem>
          <para>
            fornece o sistema de tipo base do GLib e a classe de objeto
          </para>
          <indexterm zone="glib2 libgobject">
            <primary sortas="c-libgobject">libgobject-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgthread">
        <term><filename class='libraryfile'>libgthread-2.0.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca básica para retro compatibilidade; costumava ser a
biblioteca de camadas do GLib, mas as funcionalidades foram mescladas na
<systemitem class='library'>libglib-2.0</systemitem>
          </para>
          <indexterm zone="glib2 libgthread">
            <primary sortas="c-libgthread">libgthread-2.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="g-ir-annotation-tool">
        <term><command>g-ir-annotation-tool</command></term>
        <listitem>
          <para>
            cria ou extrai dados de anotação a partir de typelibs do GI
          </para>
          <indexterm zone="glib2 g-ir-annotation-tool">
            <primary sortas="b-g-ir-annotation-tool">g-ir-annotation-tool</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="g-ir-compiler">
        <term><command>g-ir-compiler</command></term>
        <listitem>
          <para>
            é uma contraparte de <command>gi-compile-repository</command> para a antiga
API <systemitem class='library'>libgirepository-1.0</systemitem>
          </para>
          <indexterm zone="glib2 g-ir-compiler">
            <primary sortas="b-g-ir-compiler">g-ir-compiler</primary>
          </indexterm>
        </listitem>
      </varlistentry>

     <varlistentry id="g-ir-doc-tool">
        <term><command>g-ir-doc-tool</command></term>
        <listitem>
          <para>
            gera arquivos Mallard que podem ser visualizados com <command>yelp</command>
ou renderizados para HTML com <command>yelp-build</command> originários das
<ulink url="&gnome-download-http;/yelp-tools">ferramentas yelp</ulink>
          </para>
          <indexterm zone="glib2 g-ir-doc-tool">
            <primary sortas="b-g-ir-doc-tool">g-ir-doc-tool</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="g-ir-inspect">
        <term><command>g-ir-inspect</command></term>
        <listitem>
          <para>
            é uma contraparte de <command>gi-inspect-typelib</command> para a antiga API
<systemitem class='library'>libgirepository-1.0</systemitem>
          </para>
          <indexterm zone="glib2 g-ir-inspect">
            <primary sortas="b-g-ir-inspect">g-ir-inspect</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="g-ir-generate">
        <term><command>g-ir-generate</command></term>
        <listitem>
          <para>
            é uma contraparte de <command>gi-decompile-typelib</command> para a antiga
API <systemitem class='library'>libgirepository-1.0</systemitem>
          </para>
          <indexterm zone="glib2 g-ir-generate">
            <primary sortas="b-g-ir-generate">g-ir-generate</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="g-ir-scanner">
        <term><command>g-ir-scanner</command></term>
        <listitem>
          <para>
            é uma ferramenta que gera arquivos XML GIR analisando cabeçalhos e
introspectando bibliotecas baseadas em GObject
          </para>
          <indexterm zone="glib2 g-ir-scanner">
            <primary sortas="b-g-ir-scanner">g-ir-scanner</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgirepository-1.0">
        <term><filename class='libraryfile'>libgirepository-1.0.so</filename></term>
        <listitem>
          <para>
            é uma contraparte de <systemitem
class='library'>libgirepository-2.0</systemitem> com a antiga API 1.0
          </para>
          <indexterm zone="glib2 libgirepository-1.0">
            <primary sortas="c-libgirepository1">libgirepository-1.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>

  </sect2>

</sect1>
