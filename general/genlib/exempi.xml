<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY exempi-download-http "https://libopenraw.freedesktop.org/download/exempi-&exempi-version;.tar.xz">
  <!ENTITY exempi-download-ftp  "">
  <!ENTITY exempi-md5sum        "51fe14c2a5fa44816ba8187c6ad87d78">
  <!ENTITY exempi-size          "2,7 MB">
  <!ENTITY exempi-buildsize     "289 MB (adicionar 236 MB para testes)">
  <!ENTITY exempi-time          "0,4 UPC (adicionar 0,6 UPC para testes; ambos usando paralelismo=4)">
]>

<sect1 id="exempi" xreflabel="Exempi-&exempi-version;">
  <?dbhtml filename="exempi.html"?>


  <title>Exempi-&exempi-version;</title>

  <indexterm zone="exempi">
    <primary sortas="a-Exempi">Exempi</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Exempi</title>

    <para>
      <application>Exempi</application> é uma implementação do XMP (Extensible
Metadata Platform da Adobe).
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&exempi-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&exempi-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &exempi-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &exempi-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &exempi-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &exempi-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências de Exempi</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="boost"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="required">
      <xref linkend="valgrind"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Exempi</title>

    <para>
      Se você pretende executar os testes de regressão, primeiro remova um teste
que depende de um SDK proprietário da Adobe:
    </para>

<screen><userinput>sed -i -r '/^\s?testadobesdk/d' exempi/Makefile.am &amp;&amp;
autoreconf -fiv</userinput></screen>


    <para>
      Instale <application>Exempi</application> executando os seguintes comandos:
    </para>

<screen><userinput>./configure --prefix=/usr --disable-static &amp;&amp;
make</userinput></screen>

    <para>
      <!--
      If valgrind is installed, mosts tests will indicate a failure, but
      that is a test problem and not a package problem.-->
Para testar os resultados, emita: <command>make check</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/static-libraries.xml"/>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>exempi</seg>
        <seg>libexempi.so</seg>
        <seg>/usr/include/exempi-2.0</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

     <varlistentry id="exempi-prog">
        <term><command>exempi</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para manipular metadados XMP
          </para>
          <indexterm zone="exempi exempi-prog">
            <primary sortas="b-exempi">exempi</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libexempi">
        <term><filename class="libraryfile">libexempi.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca usada para analisar os metadados XMP
          </para>
          <indexterm zone="exempi libexempi">
            <primary sortas="c-libexempi">libexempi.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
