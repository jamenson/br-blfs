<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libportal-download-http "https://github.com/flatpak/libportal/releases/download/&libportal-version;/libportal-&libportal-version;.tar.xz">
  <!ENTITY libportal-download-ftp  "">
  <!ENTITY libportal-md5sum        "0c63ee25d2e2986ce4df544fe8149046">
  <!ENTITY libportal-size          "108 KB">
  <!ENTITY libportal-buildsize     "4,0 MB (com testes; adicionar 9 MB para documentos)">
  <!ENTITY libportal-time          "menos que 0,1 UPC (adicionar 0,6 UPC para testes)">
]>

<sect1 id="libportal" xreflabel="libportal-&libportal-version;">
  <?dbhtml filename="libportal.html"?>


  <title>libportal-&libportal-version;</title>

  <indexterm zone="libportal">
    <primary sortas="a-libportal">libportal</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libportal</title>

    <para>
      O pacote <application>libportal</application> fornece uma biblioteca que
contém APIs assíncronas estilo GIO para a maioria dos portais Flatpak.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libportal-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libportal-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libportal-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libportal-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libportal-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libportal-time;
        </para>
      </listitem>
    </itemizedlist>

    <!--  No longer needed
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Optional patch (required for running the test suite):
          <ulink url="&patch-root;/libportal-&libportal-version;-testsuite_fix-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do libportal</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      &gobject-introspection;
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="gtk3"/> e <xref linkend="gtk4"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas (tempo de execução)</bridgehead>
    <para role="runtime">
      Para tornar esse pacote realmente útil, instale <xref
linkend="xdg-desktop-portal"/>, <xref linkend="xdg-desktop-portal-gtk"/>,
<xref role="nodep" linkend="xdg-desktop-portal-gnome"/> (se executar um
ambiente de área de trabalho GNOME) e <xref role="nodep"
linkend="xdg-desktop-portal-lxqt"/> (se executar um ambiente de área de
trabalho LXQt). Eles não são necessários se usar esse pacote somente para
satisfazer uma dependência de construção.
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="gi-docgen"/> (para documentação), <xref
linkend="python-dbusmock"/> e <xref linkend="pytest"/> (para testagem),
<xref linkend="qt6"/> e <xref linkend="vala"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libportal</title>

    <warning>
      <para>
        Se uma versão anterior do "libportal" estiver instalada, [então] mova os
cabeçalhos para fora do caminho, de forma que os pacotes posteriores não
encontrem conflitos (como o(a) usuário(a) <systemitem
role="username">root</systemitem>):
      </para>
<screen role="root"><userinput>if [ -e /usr/include/libportal ]; then
   rm -rf /usr/include/libportal.old &amp;&amp;
   mv -vf /usr/include/libportal{,.old}
fi</userinput></screen>
    </warning>

    <para>
      Instale <application>libportal</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr --buildtype=release -D docs=false .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Se você tiver <xref linkend='gi-docgen'/> instalado e desejar construir a
documentação da API para esse pacote, emita:
    </para>

<screen role='nodump'><userinput>sed -i "/output/s/-1/-&libportal-version;/" ../doc/meson.build &amp;&amp;
meson configure -D docs=true                     &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja test</command>. Observe que
processos adicionais <filename>dbus-daemon</filename> possivelmente precisem
ser eliminados depois que os testes forem executados.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D docs=false</parameter>: &gi-doc-disable;
    </para>

  </sect2>
  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libportal.so, libportal-gtk3.so, libportal-gtk4.so e libportal-qt6.so
        </seg>
        <seg>
          /usr/include/libportal e /usr/share/gtk-doc/html/libportal
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libportal-lib">
        <term><filename class="libraryfile">libportal.so</filename></term>
        <listitem>
          <para>
            fornece APIs assíncronas no estilo GIO para a maioria dos portais Flatpak
          </para>
          <indexterm zone="libportal libportal-lib">
            <primary sortas="c-libportal">libportal.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libportal-gtk3">
        <term><filename class="libraryfile">libportal-gtk3.so</filename></term>
        <listitem>
          <para>
            fornece funções específicas do GTK+-3 para interagir com portais Flatpak
          </para>
          <indexterm zone="libportal libportal-gtk3">
            <primary sortas="c-libportal-gtk3">libportal-gtk3.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libportal-gtk4">
        <term><filename class="libraryfile">libportal-gtk4.so</filename></term>
        <listitem>
          <para>
            fornece funções específicas do GTK-4 para interagir com portais Flatpak
          </para>
          <indexterm zone="libportal libportal-gtk4">
            <primary sortas="c-libportal-gtk4">libportal-gtk4.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libportal-qt6">
        <term><filename class="libraryfile">libportal-qt6.so</filename></term>
        <listitem>
          <para>
            fornece funções específicas do Qt6 para interagir com portais Flatpak
          </para>
          <indexterm zone="libportal libportal-qt6">
            <primary sortas="c-libportal-qt6">libportal-qt6.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>

</sect1>
