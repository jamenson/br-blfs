<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY fmt-download-http "https://github.com/fmtlib/fmt/archive/&fmt-version;/fmt-&fmt-version;.tar.gz">
  <!ENTITY fmt-download-ftp  "">
  <!ENTITY fmt-md5sum        "10c2ae163accd3b82e6b8b4dff877645">
  <!ENTITY fmt-size          "688 KB">
  <!ENTITY fmt-buildsize     "44 MB (com testes)">
  <!ENTITY fmt-time          "0,4 UPC (usando paralelismo = 4; com os testes)">
]>

<sect1 id="fmt" xreflabel="fmt-&fmt-version;">
  <?dbhtml filename="fmt.html"?>

  <title>fmt-&fmt-version;</title>

  <indexterm zone="fmt">
    <primary sortas="a-fmt">fmt</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao fmt</title>

    <para>
      O pacote <application>fmt</application> é uma biblioteca de formatação de
fonte aberto que fornece uma alternativa rápida e segura ao stdio do C e ao
iostreams do C++.</para>
    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&fmt-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&fmt-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &fmt-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &fmt-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &fmt-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &fmt-time;
        </para>
      </listitem>
    </itemizedlist>

  <!--
    <bridgehead renderas="sect3">
Fmt Dependencies</bridgehead>

    <bridgehead renderas="sect4">Required</bridgehead>
    <para role="required">
      <xref linkend="glib2"/> and
      <xref linkend="libsigc"/>
    </para>

    <bridgehead renderas="sect4">Optional</bridgehead>
    <para role="optional">
      <xref linkend="doxygen"/>,
      <xref linkend="glib-networking"/> (for tests),
      <xref linkend="gnutls"/> (for tests),
      <xref linkend="libxslt"/>, and
      <ulink url="&gnome-download-http;/mm-common">mm-common</ulink>
    </para>
-->
</sect2>

  <sect2 role="installation">
    <title>Instalação do fmt</title>

    <para>
      Instale <application>fmt</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr     \
      -D CMAKE_INSTALL_LIBDIR=/usr/lib \
      -D BUILD_SHARED_LIBS=ON          \
      -D FMT_TEST=OFF                  \
      -G Ninja ..                      &amp;&amp;
ninja</userinput></screen>

    <para>
      Se você tiver habilitado testes, emita: <command>ninja test</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>-D FMT_TEST=OFF</parameter>: Essa chave inicializa os testes do
pacote. Configure para ON se você desejar executar testes.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libfmt.so
        </seg>
        <seg>
          /usr/include/fmt e /usr/lib/cmake/fmt
        </seg>
      </seglistitem>
    </segmentedlist>

  </sect2>

</sect1>
