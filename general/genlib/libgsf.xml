<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libgsf-download-http "&gnome-download-http;/libgsf/1.14/libgsf-&libgsf-version;.tar.xz">
  <!ENTITY libgsf-download-ftp  "">
  <!ENTITY libgsf-md5sum        "baf36a3e89293652f67cded4b1608cc5">
  <!ENTITY libgsf-size          "700 KB">
  <!ENTITY libgsf-buildsize     "14 MB (com testes)">
  <!ENTITY libgsf-time          "0,2 UPC (com testes)">
]>

<sect1 id="libgsf" xreflabel="libgsf-&libgsf-version;">
  <?dbhtml filename="libgsf.html"?>


  <title>libgsf-&libgsf-version;</title>

  <indexterm zone="libgsf">
    <primary sortas="a-libgsf">libgsf</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libgsf</title>

    <para>
      O pacote <application>libgsf</application> contém uma biblioteca usada para
fornecer uma camada extensível de abstração de entrada/saída para formatos
estruturados de arquivo.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libgsf-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libgsf-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libgsf-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libgsf-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libgsf-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libgsf-time;
        </para>
      </listitem>
    </itemizedlist>

    <!--
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Required patch:
          <ulink url="&patch-root;/libgsf-&libgsf-version;-upstream_fixes-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do libgsf</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="glib2"/> e <xref linkend="libxml2"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="gdk-pixbuf"/> (Para construir
<command>gsf-office-thumbnailer</command>)
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend='seven-zip'/> (para testes), <xref linkend="gtk-doc"/>, <xref
linkend="valgrind"/> (para testes) e <ulink
url='https://sourceforge.net/projects/infozip/files/UnZip%206.x%20%28latest%29/'>unzip</ulink>
(para testes)
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libgsf</title>

    <!--
    <para>

      First, fix building <application>libgsf</application> with libxml2-2.13:
    </para>

<screen><userinput remap="pre">patch -Np1 -i ../libgsf-&libgsf-version;-upstream_fixes-1.patch</userinput></screen>
-->
<para>
      Instale <application>libgsf</application> executando os seguintes comandos:
    </para>

<screen><userinput>./configure --prefix=/usr --disable-static &amp;&amp;
make</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make check</command>. Dois testes
relacionados ao <xref linkend="valgrind"/> são conhecidos por falharem.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/static-libraries.xml"/>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/gtk-doc-rebuild.xml"/>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          gsf, gsf-office-thumbnailer e gsf-vba-dump
        </seg>
        <seg>
          libgsf-1.so
        </seg>
        <seg>
          /usr/include/libgsf-1, /usr/share/gtk-doc/html/gsf e /usr/share/thumbnailers
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="gsf">
        <term><command>gsf</command></term>
        <listitem>
          <para>
            é um utilitário simples de arquivamento, um pouco semelhante ao <ulink
role='man' url='&man;tar.1'>tar(1)</ulink>
          </para>
          <indexterm zone="libgsf gsf">
            <primary sortas="b-gsf">gsf</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gsf-office-thumbnailer">
        <term><command>gsf-office-thumbnailer</command></term>
        <listitem>
          <para>
            é usado internamente por aplicativos <application>GNOME</application>, como
<application>Nautilus</application>, para gerar miniaturas de vários tipos
de arquivos de aplicativos de escritório
          </para>
          <indexterm zone="libgsf gsf-office-thumbnailer">
            <primary sortas="b-gsf-office-thumbnailer">gsf-office-thumbnailer</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gsf-vba-dump">
        <term><command>gsf-vba-dump</command></term>
        <listitem>
          <para>
            é usado para extrair macros do Visual Basic for Applications a partir de
arquivos
          </para>
          <indexterm zone="libgsf gsf-vba-dump">
            <primary sortas="b-gsf-vba-dump">gsf-vba-dump</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libgsf-1">
        <term><filename class="libraryfile">libgsf-1.so</filename></term>
        <listitem>
          <para>
            contém as funções da API <application>libgsf</application>
          </para>
          <indexterm zone="libgsf libgsf-1">
            <primary sortas="c-libgsf-1">libgsf-1.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
