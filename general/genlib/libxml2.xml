<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!-- Also update the download, md5, size entities in libxml2py2.xml -->
  <!ENTITY libxml2-download-http "https://download.gnome.org/sources/libxml2/2.13/libxml2-&libxml2-version;.tar.xz">
<!--  <!ENTITY libxml2-download-http "http://xmlsoft.org/sources/libxml2-&libxml2-version;.tar.xz">-->
  <!ENTITY libxml2-download-ftp  "">
  <!ENTITY libxml2-md5sum        "85dffa2387ff756bdf8b3b247594914a">
  <!ENTITY libxml2-size          "2,3 MB">
  <!ENTITY libxml2-buildsize     "113 MB (com testes)">
  <!ENTITY libxml2-time          "0,4 UPC (Usando paralelismo=4; com testes)">
  <!ENTITY testsuite-version     "20130923">
]>

<sect1 id="libxml2" xreflabel="libxml2-&libxml2-version;">
  <?dbhtml filename="libxml2.html"?>


  <title>libxml2-&libxml2-version;</title>

  <indexterm zone="libxml2">
    <primary sortas="a-libxml2">libxml2</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libxml2</title>

    <para>
      O pacote <application>libxml2</application> contém bibliotecas e utilitários
usados para analisar arquivos "XML".
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libxml2-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libxml2-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libxml2-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libxml2-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libxml2-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libxml2-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>
    <itemizedlist spacing="compact">
      
      <!-- 2.13.4:  all changed appied
      <listitem>

        <para>
          Required patch:
          <ulink url="&patch-root;/libxml2-&libxml2-version;-upstream_fix-2.patch"/>
        </para>
      </listitem>
      -->
<listitem>
        <para>
          Suíte de Teste Opcional: <ulink
url="https://www.w3.org/XML/Test/xmlts&testsuite-version;.tar.gz"/> - Isso
habilita <command>make check</command> para se fazer uma testagem completa.
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libxml2</bridgehead>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="icu"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="valgrind"/> (possivelmente seja usado nos testes)
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do libxml2</title>

    <para>
      Instale <application>libxml2</application> executando os seguintes comandos:
    </para>

<screen><userinput>./configure --prefix=/usr           \
            --sysconfdir=/etc       \
            --disable-static        \
            --with-history          \
            --with-icu              \
            PYTHON=/usr/bin/python3 \
            --docdir=/usr/share/doc/libxml2-&libxml2-version; &amp;&amp;
make</userinput></screen>

    <para>
      Se você baixou a suíte de teste, [então] emita o seguinte comando:
    </para>

<screen><userinput>tar xf ../xmlts&testsuite-version;.tar.gz</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make check &gt;
check.log</command>. Esse comando imprimirá várias linhas de mensagens de
erro como <quote><computeroutput>Failed to parse
xstc/...</computeroutput></quote> porque alguns arquivos de teste estão
ausentes e essas mensagens podem ser seguramente ignoradas. Um sumário dos
resultados pode ser obtido com <command>grep -E '^Total|expected|Ran'
check.log</command>. Se <xref linkend="valgrind"/> estiver instalado e você
desejar verificar vazamentos de memória, substitua <command>check</command>
por <command>check-valgrind</command>.
    </para>

    <note>
      <para>
        Os testes usam <ulink url="http://localhost/">http://localhost/</ulink> para
testar a análise de entidades externas. Se a máquina onde você executar os
testes servir como um sítio da Web, [então] os testes possivelmente travem,
dependendo do conteúdo do arquivo servido. Portanto, é recomendado desligar
o servidor durante os testes; como o(a) usuário(a) <systemitem
class="username">root</systemitem>:
      </para>

<screen role="nodump" revision="sysv"><userinput>/etc/init.d/httpd stop</userinput></screen>
<screen role="nodump" revision="systemd"><userinput>systemctl stop httpd.service</userinput></screen>

    </note>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

    <para>
      Finalmente, evite que alguns pacotes desnecessariamente se vinculem ao ICU
usando os seguintes comandos como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>rm -vf /usr/lib/libxml2.la &amp;&amp;
sed '/libs=/s/xml2.*/xml2"/' -i /usr/bin/xml2-config</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/static-libraries.xml"/>

    <para>
      <parameter>--with-history</parameter>: Essa chave habilita o suporte
<application>Readline</application> ao executar
<command>xmlcatalog</command> ou <command>xmllint</command> no modo shell.
    </para>

    <para>
      <parameter>--with-icu</parameter>: Essa chave habilita suporte para
<application>ICU</application>, o qual fornece suporte adicional
Unicode. Isso é necessário para alguns pacotes no BLFS, como para
QtWebEngine.
    </para>

    <para>
      <parameter>PYTHON=/usr/bin/python3</parameter>: Permite construir o módulo
"libxml2" com "Python3" em vez de "Python2".
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          xml2-config, xmlcatalog e xmllint
        </seg>
        <seg>
          libxml2.so
        </seg>
        <seg>
          /usr/include/libxml2, /usr/lib/cmake/libxml2,
/usr/share/doc/libxml2-&libxml2-version; e /usr/share/gtk-doc/html/libxml2
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="xml2-config">
        <term><command>xml2-config</command></term>
        <listitem>
          <para>
            determina os sinalizadores de compilação e vinculador que deveriam ser
usados para compilar e vincular aplicativos que usam a <filename
class="libraryfile">libxml2</filename>
          </para>
          <indexterm zone="libxml2 xml2-config">
            <primary sortas="b-xml2-config">xml2-config</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="xmlcatalog">
        <term><command>xmlcatalog</command></term>
        <listitem>
          <para>
            é usado para monitorar e manipular catálogos "XML" e "SGML"
          </para>
          <indexterm zone="libxml2 xmlcatalog">
            <primary sortas="b-xmlcatalog">xmlcatalog</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="xmllint">
        <term><command>xmllint</command></term>
        <listitem>
          <para>
            analisa arquivos "XML" e gera informes (baseados nas opções) para detectar
erros na codificação "XML"
          </para>
          <indexterm zone="libxml2 xmllint">
            <primary sortas="b-xmllint">xmllint</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libxml2-lib">
        <term><filename class="libraryfile">libxml2.so</filename></term>
        <listitem>
          <para>
            fornece funções para aplicativos para analisar arquivos que usam o formato
"XML"
          </para>
          <indexterm zone="libxml2 libxml2-lib">
            <primary sortas="c-libxml2">libxml2.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
