<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libpaper-download-http "https://github.com/rrthomas/libpaper/releases/download/v&libpaper-version;/libpaper-&libpaper-version;.tar.gz">
  <!ENTITY libpaper-download-ftp  "">
  <!ENTITY libpaper-md5sum        "794552e5e5b9796c15bc222fefd9e1ff">
  <!ENTITY libpaper-size          "1,1 MB">
  <!ENTITY libpaper-buildsize     "12 MB (com testes)">
  <!ENTITY libpaper-time          "0,1 UPC (com testes)">
]>

<sect1 id="libpaper" xreflabel="libpaper-&libpaper-version;">
  <?dbhtml filename="libpaper.html"?>


  <title>libpaper-&libpaper-version;</title>

  <indexterm zone="libpaper">
    <primary sortas="a-libpaper">libpaper</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libpaper</title>

    <para>
      Esse pacote destina-se a fornecer uma maneira simples para os aplicativos
executarem ações baseadas em um sistema ou tamanho de papel especificado
pelo(a) usuário(a).
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libpaper-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libpaper-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libpaper-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libpaper-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libpaper-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libpaper-time;
        </para>
      </listitem>
    </itemizedlist>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libpaper</title>

    <para>
      Instale o <application>libpaper</application> executando os seguintes
comandos:
    </para>

<!-- All tests "PASS" but the log just contains:
         "sysconfdir does not start with prefix, cannot run test!"
    <para>

      To test the results, issue: <command>make check</command>.
    </para> -->
<screen><userinput>./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --disable-static     \
            --docdir=/usr/share/doc/libpaper-&libpaper-version; &amp;&amp;
make</userinput></screen>

    

    <para>
      A suíte de teste desse pacote não funciona com uma configuração usando o
local padrão de instalação.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/static-libraries.xml"/>
    
  <!--
    <para>

      <parameter>- -enable-relocatable</parameter>:
      This switch is needed to run the tests.
    </para>
    -->
</sect2>

  <sect2 role="configuration">
    <title>Configurando o libpaper</title>

    <sect3>
      <title>Informação de Configuração</title>

      <para>
        O tamanho do papel é determinado automaticamente a partir da localidade do
sistema; veja-se <literal>LC_PAPER</literal> em <ulink role='man'
url='&man;locale.7'>locale(7)</ulink>. Se você quiser substituí-lo, crie um
arquivo <filename>papersize</filename> no diretório de configuração do(a)
usuário(a). Por exemplo:
      </para>

<screen role="nodump"><userinput>mkdir -pv ~/.config &amp;&amp;
echo "a4" &gt; ~/.config/papersize</userinput></screen>

      <para>
        Se você quiser substituir o tamanho do papel globalmente (para todos(as)
os(as) usuários(as)), configure a variável de ambiente
<envar>PAPERSIZE</envar>. Por exemplo:
      </para>

<screen role="nodump"><userinput>echo "PAPERSIZE=a4" &gt; /etc/profile.d/libpaper.sh</userinput></screen>

      <para>
        Você pode usar um tamanho diferente de papel, como
<literal>letter</literal>.
      </para>

    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          paper e paperconf
        </seg>
        <seg>
          libpaper.so
        </seg>
        <seg>
          /usr/share/doc/libpaper-&libpaper-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="paper">
        <term><command>paper</command></term>
        <listitem>
          <para>
            imprime informação de configuração de papel
          </para>
          <indexterm zone="libpaper paper">
            <primary sortas="b-paper">paper</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="paperconf">
        <term><command>paperconf</command></term>
        <listitem>
          <para>
            imprime informação de configuração de papel em um modo de compatibilidade
          </para>
          <indexterm zone="libpaper paperconf">
            <primary sortas="b-paperconf">paperconf</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libpaper-lib">
        <term><filename class="libraryfile">libpaper.so</filename></term>
        <listitem>
          <para>
            contém funções para interrogar a biblioteca de papéis
          </para>
          <indexterm zone="libpaper libpaper-lib">
            <primary sortas="c-libpaper">libpaper.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
