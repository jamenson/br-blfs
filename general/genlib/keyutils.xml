<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY keyutils-download-http "https://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git/snapshot/keyutils-&keyutils-version;.tar.gz">
  <!ENTITY keyutils-download-ftp  "">
  <!ENTITY keyutils-md5sum        "6b70b2b381c1b6d9adfaf66d5d3e7c00">
  <!ENTITY keyutils-size          "136 KB">
  <!ENTITY keyutils-buildsize     "2,6 MB (com testes)">
  <!ENTITY keyutils-time          "menos que 0,1 UPC (adicionar 0,4 UPC para testes)">
]>

<sect1 id="keyutils" xreflabel="keyutils-&keyutils-version;">
  <?dbhtml filename="keyutils.html"?>


  <title>keyutils-&keyutils-version;</title>

  <indexterm zone="keyutils">
    <primary sortas="a-keyutils">keyutils</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao keyutils</title>

    <para>
      <application>Keyutils</application> é um conjunto de utilitários para
gerenciar o recurso de retenção de chave no núcleo, que pode ser usado por
sistemas de arquivos, dispositivos de bloco e muito mais para obter e reter
as chaves de autorização e de encriptação exigidas para realizar operações
seguras.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&keyutils-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&keyutils-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &keyutils-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &keyutils-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &keyutils-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &keyutils-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do Keyutils</bridgehead>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="lsb-tools"/> (referido pela suíte de teste)
    </para>
  </sect2>

  <sect2 role="kernel" id="keyutils-test-kernel"
         xreflabel="Configuração de Núcleo do Keyutils">
    <title>Configuração do Núcleo</title>

    <para>
      Se executar a suíte de teste, alguns testes precisarão dos seguintes
recursos do núcleo habilitados:
    </para>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="keyutils-test-kernel.xml"/>

    <indexterm zone="keyutils keyutils-test-kernel">
      <primary sortas="d-keyutils">keyutils (testagem)</primary>
    </indexterm>
  </sect2>
  <sect2 role="installation">
    <title>Instalação do keyutils</title>

    <para>
      Instale <application>keyutils</application> executando os seguintes
comandos:
    </para>

<screen><userinput>make</userinput></screen>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make NO_ARLIB=1 LIBDIR=/usr/lib BINDIR=/usr/bin SBINDIR=/usr/sbin install</userinput></screen>

    <para>
      A suíte de teste só pode executar depois de instalar-se esse pacote. Para
testar os resultados, emita, como o(a) usuário(a) <systemitem
class="username">root</systemitem>:
    </para>

<screen role="root" remap="test"><userinput>make -k test</userinput></screen>

    <para>
      Se <xref linkend='lsb-tools'/> não estiver instalado, a suíte de teste
produzirá algumas linhas reclamando que o comando
<command>lsb_release</command> não está disponível, mas não afetará o
resultado do teste.
    </para>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>



    <!--
    <para>

      <command>sed ... Makefile</command>: This command ensures the pkgconfig
      file is placed in the correct directory.
    </para>

    <para>
      <command>sed ... tests/toolbox.inc.sh</command>: In LFS, GCC has been
      configured with <option>- -enable-default-pie</option> so
      <command>/usr/bin/bash</command> is a PIE, but the test script does
      not anticipate it.  Fix this oversight so the test can run on a LFS
      system.
    </para>
-->
<para>
      <parameter>NO_ARLIB=1</parameter>: Esse sinalizador do "make" desabilita a
instalação da biblioteca estática.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando keyutils</title>

    <sect3 id="keyutils-config">
      <title>Arquivos de Configuração</title>

      <para>
        <filename>/etc/request-key.conf</filename> e
<filename>/etc/request-key.d/*</filename>
      </para>

      <indexterm zone="keyutils keyutils-config">
        <primary sortas="e-etc-request-key.conf">/etc/request-key.conf</primary>
      </indexterm>

      <indexterm zone="keyutils keyutils-config">
        <primary sortas="e-etc-request-key.d">/etc/request-key.d/*</primary>
      </indexterm>

    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>keyctl, key.dns_resolver e request-key</seg>
        <seg>libkeyutils.so</seg>
        <seg>/etc/keyutils, /etc/request-key.d e /usr/share/keyutils</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="keyctl">
        <term><command>keyctl</command></term>
        <listitem>
          <para>
            controla o recurso de gerenciamento de chave com uma variedade de
subcomandos
          </para>
          <indexterm zone="keyutils keyctl">
            <primary sortas="b-keyctl">keyctl</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="key.dns_resolver">
        <term><command>key.dns_resolver</command></term>
        <listitem>
          <para>
            é invocado por <command>request-key</command> em nome do núcleo quando os
serviços do núcleo (como NFS, CIFS e AFS) precisam realizar uma pesquisa de
nome de dispositivo e o núcleo não tem a chave armazenada em cache. Não é
destinado, ordinariamente, a ser chamado diretamente
          </para>
          <indexterm zone="keyutils key.dns_resolver">
            <primary sortas="b-key.dns_resolver">key.dns_resolver</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="request-key">
        <term><command>request-key</command></term>
        <listitem>
          <para>
            é invocado pelo núcleo quando o núcleo é solicitado por uma chave que não
tem imediatamente disponível. O núcleo cria uma chave temporária e, em
seguida, chama esse aplicativo para instanciá-lo. Não é destinado a ser
chamado diretamente
          </para>
          <indexterm zone="keyutils request-key">
            <primary sortas="b-request-keyt-key">request-key</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libkeyutils">
        <term><filename class="libraryfile">libkeyutils.so</filename></term>
        <listitem>
          <para>
            contém a instanciação da API da biblioteca "keyutils"
          </para>
          <indexterm zone="keyutils libkeyutils">
            <primary sortas="c-libkeyutils">libkeyutils.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
