<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY qca-download-http "https://download.kde.org/stable/qca/&qca-version;/qca-&qca-version;.tar.xz">
  <!ENTITY qca-download-ftp  "">
  <!ENTITY qca-md5sum        "d8aaa46356a322464f65b04d00d2bac6">
  <!ENTITY qca-size          "748 KB">
  <!ENTITY qca-buildsize     "57 MB (com testes)">
  <!ENTITY qca-time          "1,0UPC (usando paralelismo = 4; com os testes)">
]>

<sect1 id="qca" xreflabel="qca-&qca-version;">
  <?dbhtml filename="qca.html"?>


  <title>Qca-&qca-version;</title>

  <indexterm zone="qca">
    <primary sortas="a-qca">qca</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Qca</title>

    <para>
      O <application>Qca</application> visa a fornecer uma API criptográfica
direta e multiplataforma, usando tipos de dados e convenções
<application>Qt</application>. <application>Qca</application> separa a API
da implementação, usando "plugins" conhecidos como "Providers".
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&qca-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&qca-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &qca-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &qca-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &qca-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &qca-time;
        </para>
      </listitem>
    </itemizedlist>

    <!--
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Required patch:
          <ulink url="&patch-root;/qca-&qca-version;-openssl-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do Qca</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
    <xref linkend="make-ca"/>, <xref linkend="cmake"/>, <xref linkend="qt6"/> e
<xref linkend="which"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="Optional">
    <xref linkend="cyrus-sasl"/>, <xref linkend="gnupg2"/>, <xref
linkend="libgcrypt"/>, <xref linkend="libgpg-error"/>, <xref
linkend="nss"/>, <xref linkend="nspr"/>, <xref linkend="p11-kit"/>, <xref
linkend="doxygen"/> e <ulink
url="https://botan.randombit.net/">Botan</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Qca</title>

    <para>
      Corrija o local dos certificados de AC:
    </para>

<screen><userinput>sed -i 's@cert.pem@certs/ca-bundle.crt@' CMakeLists.txt</userinput></screen>

    <para>
      Instale <application>Qca</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=$QT6DIR            \
      -D CMAKE_BUILD_TYPE=Release                \
      -D QT6=ON                                  \
      -D QCA_INSTALL_IN_QT_PREFIX=ON             \
      -D QCA_MAN_INSTALL_DIR:PATH=/usr/share/man \
      .. &amp;&amp;
make</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make test</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>-D CMAKE_BUILD_TYPE=Release</parameter>: Essa chave é usada para
aplicar um nível mais alto de otimização à compilação.
    </para>

    <para>
      <parameter>-D QT6=ON</parameter>: Assegura que os pacotes sejam construídos
com <xref linkend="qt6"/>.
    </para>

    <para>
      <parameter>-D QCA_MAN_INSTALL_DIR:PATH=/usr/share/man</parameter>: Instala a
página de manual do qca no local normal.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>mozcerts-qt6 e qcatool-qt6</seg>
        <seg>
          libqca-qt6.so, libqca-cyrus-sasl.so, libqca-gcrypt.so, libqca-gnupg.so,
libqca-logger.so, libqca-nss.so, libqca-ossl.so e libqca-softstore.so
        </seg>
        <seg>
          &qt6-dir;/include/Qca-qt6, &qt6-dir;/lib/cmake/Qca-qt6 e
&qt6-dir;/lib/qca-qt6
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="mozcerts">
        <term><command>mozcerts-qt6</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para converter "certdata.txt" em
arquivos "arquivo_saida_gerada.pem"
          </para>
          <indexterm zone="qca mozcerts">
            <primary sortas="b-mozcerts">mozcerts-qt6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="qcatool">
        <term><command>qcatool-qt6</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para realizar várias operações
criptográficas com o "Qca"
          </para>
          <indexterm zone="qca qcatool">
            <primary sortas="b-qcatool">qcatool-qt6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libqca">
        <term><filename class="libraryfile">libqca-qt6.so</filename></term>
        <listitem>
          <para>
            é a biblioteca "Qt Cryptography Architecture" (Qca)
          </para>
          <indexterm zone="qca libqca">
            <primary sortas="c-libqca">libqca-qt6.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
