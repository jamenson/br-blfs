<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY ibus-download-http "https://github.com/ibus/ibus/archive/&ibus-version;/ibus-&ibus-version;.tar.gz">
  <!ENTITY ibus-download-ftp  "">
  <!ENTITY ibus-md5sum        "3d685af1010d871bb858dc8a8aabb5c4">
  <!ENTITY ibus-size          "1,5 MB">
  <!ENTITY ibus-buildsize     "55 MB (adicionar 1 MB para os testes)">
  <!ENTITY ibus-time          "0,3 UPC (Usando paralelismo=4; adicionar 1,0 UPC para testes)">
<!-- Since UCD.zip can be used by gucharmap too, let's use the same version:
     This prevents version mismatches, since UCD.zip filename is not versioned.
-->
  <!ENTITY ucd-download-http  "https://www.unicode.org/Public/zipped/&gucharmap-major-version;.0/UCD.zip">
]>

<sect1 id="ibus" xreflabel="ibus-&ibus-version;">
  <?dbhtml filename="ibus.html"?>


  <title>ibus-&ibus-version;</title>

  <indexterm zone="ibus">
    <primary sortas="a-ibus">ibus</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao ibus</title>

    <para>
      <application>ibus</application> é um barramento de entrada inteligente. É
uma nova estrutura essencial de suporte de entrada para o sistema
operacional Linux. Ele fornece uma interface de usuário de método de entrada
amigável e com todos os recursos.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&ibus-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&ibus-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &ibus-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &ibus-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &ibus-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &ibus-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>

    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Base de dados de caracteres "Unicode": <ulink url="&ucd-download-http;">
&ucd-download-http;</ulink>
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do ibus</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      
      <!-- needed since we're using autogen.sh -->
<xref linkend="iso-codes"/> e <xref linkend="vala"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="dconf"/>, &gobject-introspection;, <xref linkend="gtk3"/>,
<xref linkend="gtk4"/> e <xref linkend="libnotify"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <!--<xref linkend="pyxdg" role="runtime"/>

      (runtime, for the <command>ibus-setup</command>), and
      NOTE: The reference to the 'xdg' python module is commented out in the source -->
<xref linkend="gtk-doc"/> (para gerar documentação da API), <xref
linkend="dbus-python"/> e <xref linkend="pygobject3"/> (ambos para construir
a biblioteca de suporte a Python), <xref linkend="libxkbcommon"/>, <xref
linkend="wayland"/> (ambos para construir os programas de suporte a
Wayland), <ulink url="https://www.joypixels.com/">EmojiOne</ulink> e <ulink
url="https://github.com/AyatanaIndicators/libdbusmenu">libdbusmenu</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do ibus</title>

    <para>
      Primeiro, instale a base de dados de caracteres Unicode como o(a) usuário(a)
&root;:
    </para>

<screen role="root"><userinput>mkdir -p               /usr/share/unicode/ucd &amp;&amp;
unzip -o ../UCD.zip -d /usr/share/unicode/ucd</userinput></screen>

    <para>
      Corrija um problema com entradas de esquema obsoletas:
    </para>

<screen><userinput>sed -e 's@/desktop/ibus@/org/freedesktop/ibus@g' \
    -i data/dconf/org.freedesktop.ibus.gschema.xml</userinput></screen>

    <para>
      Se <xref linkend='gtk-doc'/> não estiver instalado, remova as referências a
ele:
    </para>

<screen><userinput>if ! [ -e /usr/bin/gtkdocize ]; then
  sed '/docs/d;/GTK_DOC/d' -i Makefile.am configure.ac
fi</userinput></screen>

    <para>
      Instale <application>ibus</application> executando os seguintes comandos:
    </para>

<screen revision='sysv'><userinput>SAVE_DIST_FILES=1 NOCONFIGURE=1 ./autogen.sh &amp;&amp;

PYTHON=python3                     \
./configure --prefix=/usr          \
            --sysconfdir=/etc      \
            --disable-python2      \
            --disable-appindicator \
            --disable-emoji-dict   \
            --disable-gtk2         \
            --disable-systemd-services &amp;&amp;
make</userinput></screen>

<screen revision='systemd'><userinput>SAVE_DIST_FILES=1 NOCONFIGURE=1 ./autogen.sh &amp;&amp;

PYTHON=python3                     \
./configure --prefix=/usr          \
            --sysconfdir=/etc      \
            --disable-python2      \
            --disable-appindicator \
            --disable-gtk2         \
            --disable-emoji-dict   &amp;&amp;
make</userinput></screen>

    <para>
      <!-- The log says "No idea to simulate key events in Wayland"
           in a Mutter wayland session.  DO NOT REMOVE UNLESS TESTED
           IN WAYLAND.  -->
Para testar os resultados, emita: <command>make -k check</command>. O teste
chamado ibus-compose falha porque usa algumas localidades não instaladas no
LFS. O teste chamado ibus-keypress falhará se executar em uma sessão do
Wayland. O teste xkb-latin-layouts também é conhecido por falhar em alguns
sistemas.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>--disable-appindicator</parameter>: Essa chave desabilita usar a
libdbusmenu. Omita se você instalou a dependência opcional.
    </para>

    <para>
      <parameter>--disable-emoji-dict</parameter>: Essa chave desabilita o uso de
dicionários de emoticons. Omita se você instalou o pacote opcional.
    </para>

    <para>
      <parameter>--disable-gtk2</parameter>: Essa chave remove a dependência do
<application>GTK+-2</application>.
    </para>

    <para>
      <option>--disable-gtk4</option>: Essa chave desabilita construir o immodule
do <application>GTK 4</application>. Use-a se você não quiser instalar o
<application>GTK 4</application>.
    </para>

    <para>
      <option>--enable-python-library</option>: Essa chave habilita construir a
biblioteca de suporte do <application>Python</application>. Use-a se você
tiver instalado os módulos opcionais do Python.
    </para>

    <para>
      <option>--enable-wayland</option>: Essa chave habilita construir os
programas de suporte do <application>Wayland</application>. Ela é habilitada
automaticamente se <xref linkend='libxkbcommon'/> e <xref
linkend='wayland'/> estiverem instalados.
    </para>

    <para>
      <!-- GCC security policy: "Libraries like libvtv and the sanitizers
           are intended to be used in diagnostic cases and not intended for
           use in sensitive environments."  In my build it also causes
           "compose/sequences-little-endian" bail out with a sanitizer
           warning and the package to FTBFS.  -->
<envar>NOCONFIGURE=1</envar>: Impede que <command>autogen.sh</command>
execute o conjunto de comandos sequenciais <command>configure</command>
gerado. Nós executaremos o conjunto de comandos sequenciais manualmente em
vez de depender do <command>autogen.sh</command> para executá-lo, porque
<command>autogen.sh</command> configuraria <option>-fsanitize=address
-fsanitize=leak</option> em <envar>CFLAGS</envar> executando
<command>configure</command>, mas essas opções do compilador não são
adequadas para uso produtivo e também podem causar uma falha da construção.
      
    </para>

    <para>
      <envar>PYTHON=python3</envar>: Essa variável de ambiente faz com que o
conjunto de comandos sequenciais <command>configure</command> procure por
<application>Python 3</application>. Use-o se quiser construir a biblioteca
de suporte do <application>Python 3</application>.
    </para>

    <para>
      <!-- This is needed because we do not have the Emoji dictionary.-->
<envar>SAVE_DIST_FILES=1</envar>: Essa variável de ambiente faz com que o
conjunto de comandos sequenciais <command>autogen.sh</command> salve alguns
arquivos de cabeçalho pré gerados em vez de sobrescrevê-los quando for
executado. Isso evita falhas de construção ao gerar
<filename>ibusemojigen.h</filename>.
      
    </para>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/gtk-doc-rebuild.xml"/>

  <!--
    <para>

      <command>gzip -dfv ...</command>: Decompress installed man pages
      in accordance with other man pages.
    </para>
-->
</sect2>

  <sect2 role="configuration">
    <title>Configurando Ibus</title>

    <para>
      Se o GTK+-3 estiver instalado e <option>--disable-gtk3</option> não for
usado, o módulo IM do ibus para GTK+-3 será instalado. Como o(a) usuário(a)
&root;, atualize um arquivo de cache do GTK+-3, de forma que os aplicativos
baseados em GTK possam encontrar o módulo IM recém-instalado e usar o ibus
como um método de entrada:
    </para>

    <screen role='nodump'><userinput>gtk-query-immodules-3.0 --update-cache</userinput></screen>

    <para>
      O comando acima atualiza o arquivo de cache para GTK+-3. O GTK-4 não exige
um arquivo de cache para módulos IM.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          ibus, ibus-daemon e ibus-setup
        </seg>
        <seg>
          libibus-1.0.so e im-ibus.so (módulo "IM" <application>GTK+</application>)
        </seg>
        <seg>
          /etc/dconf/db/ibus.d, /usr/include/ibus-1.0, /usr/share/gtk-doc/html/ibus e
/usr/share/ibus
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="ibus-daemon">
        <term><command>ibus-daemon</command></term>
        <listitem>
          <para>
            é o processo de segundo plano de barramento de entrada inteligente
          </para>
          <indexterm zone="ibus ibus-daemon">
            <primary sortas="b-ibus-daemon">ibus-daemon</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="ibus-setup">
        <term><command>ibus-setup</command></term>
        <listitem>
          <para>
            é o aplicativo <application>GTK+</application> usado para configurar o
<command>ibus-daemon</command>
          </para>
          <indexterm zone="ibus ibus-setup">
            <primary sortas="b-ibus-setup">ibus-setup</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libibus-1.0">
        <term><filename class="libraryfile">libibus-1.0.so</filename></term>
        <listitem>
          <para>
            contém as funções da "API" <application>ibus</application>
          </para>
          <indexterm zone="ibus libibus-1.0">
            <primary sortas="c-libibus-1.0">libibus-1.0.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
