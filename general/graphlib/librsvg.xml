<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY librsvg-download-http "&gnome-download-http;/librsvg/&librsvg-minor;/librsvg-&librsvg-version;.tar.xz">
  <!ENTITY librsvg-download-ftp  "">
  <!ENTITY librsvg-md5sum        "6d495c8bb2ee0cb0a62856c790a67298">
  <!ENTITY librsvg-size          "6,3 MB">
  <!ENTITY librsvg-buildsize     "1,2 GB (17 MB instalado), adicionar 492 MB para testes">
  <!ENTITY librsvg-time          "0,6 UPC (adicionar 0,4 UPC para testes; ambos usando paralelismo=4)">
]>

<sect1 id="librsvg" xreflabel="librsvg-&librsvg-version;">
  <?dbhtml filename="librsvg.html"?>


  <title>librsvg-&librsvg-version;</title>

  <indexterm zone="librsvg">
    <primary sortas="a-librsvg">librsvg</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao librsvg</title>

    <para>
      O pacote <application>librsvg</application> contém uma biblioteca e
ferramentas usadas para manipular, converter e visualizar imagens "Scalable
Vector Graphic" ("SVG").
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&librsvg-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&librsvg-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &librsvg-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &librsvg-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &librsvg-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &librsvg-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do librsvg</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="cairo"/>, <xref linkend="cargo-c"/>, <xref
linkend="gdk-pixbuf"/>, <xref linkend="pango"/> e <xref linkend="rust"/>
    </para>

    &build-use-internet;

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      &gobject-introspection; e <xref linkend="vala"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="docutils"/> (para páginas de manual), <xref
linkend="gi-docgen"/> (para documentação) e <xref linkend="xorg7-font"/>
(para testes)
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do librsvg</title>

    <para>
      Primeiro, corrija o caminho de instalação da documentação da API:
    </para>

<screen><userinput>sed -e "/OUTDIR/s|,| / 'librsvg-&librsvg-version;', '--no-namespace-dir',|" \
    -e '/output/s|Rsvg-2.0|librsvg-&librsvg-version;|'                      \
    -i doc/meson.build</userinput></screen>

    <para>
      Instale <application>librsvg</application> executando os seguintes comandos:
    </para>

<!-- Be sure to unset GLIB_LOG_LEVEL.  If set, it interferes with the 
         tests. 

         With 2.59.1 one test of 5 fails. (I cannot see any failures here,
         but using rustc-1.82.0.)
     -->
<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr --buildtype=release .. &amp;&amp;
ninja</userinput></screen>

    

    <para>
      Para testar os resultados, emita:
    </para>

    <screen remap='test'><userinput>ninja test</userinput></screen>

    <para>
      Um teste, Rust tests (rsvg), é conhecido por falhar.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<!--
    <note>

      <para>
        If you installed the package on to your system using a
        <quote>DESTDIR</quote> method, an important file was not installed and
        should be copied and/or generated. Generate it using the following
        command as the <systemitem class="username">root</systemitem> user:
      </para>

<screen role="root"><userinput>gdk-pixbuf-query-loaders - -update-cache</userinput></screen>
    </note>
-->
<screen role="root"><userinput>ninja install</userinput></screen>


  </sect2>

  <!--
  <sect2 role="commands">

    <title>Command Explanations</title>

    <para>
      <parameter>- -enable-vala</parameter>: This switch enables
      building of the Vala bindings. Remove this switch if you don't have
      <xref linkend="vala"/> installed.
    </para>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/static-libraries.xml"/>

    <para>
      <parameter>DOC_INSTALL_DIR='$(docdir)'</parameter>: This override
      ensures installing the API documentation into the expected location
      if <xref linkend='gi-docgen'/> is installed.
    </para>

    <para>
      <command>cargo update - -precise=0.3.36 time</command>: This updates
      the <filename>Cargo.lock</filename> file to refer to version 0.3.36
      of the time crate for the test suite.  Originally it refers to
      version 0.3.34, which fails to build with Rustc-1.80.0 or later.
    </para>

    <para>
      <option>- -disable-introspection</option>: Use this switch if you have
      not installed <application>Gobject Introspection</application>.
    </para>

    <para>
      <option>- -disable-gtk-doc</option>: This switch prevents building
      the API documentation, even if <xref linkend="gi-docgen"/> (despite
      the name of the option) is available.
    </para>

  </sect2>
-->
<sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          rsvg-convert
        </seg>
        <seg>
          librsvg-2.so e libpixbufloader-svg.so (instalada em
/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders)
        </seg>
        <seg>
          /usr/include/librsvg-2.0 e /usr/share/doc/librsvg-&librsvg-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="rsvg-convert">
        <term><command>rsvg-convert</command></term>
        <listitem>
          <para>
            é usado para converter imagens em "PNG", "PDF", "PS", "SVG" e outros
formatos
          </para>
          <indexterm zone="librsvg rsvg-convert">
            <primary sortas="b-rsvg-convert">rsvg-convert</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="librsvg-2">
        <term><filename class="libraryfile">librsvg-2.so</filename></term>
        <listitem>
          <para>
            fornece as funções para renderizar Gráficos Escaláveis Vetoriais
          </para>
          <indexterm zone="librsvg librsvg-2">
            <primary sortas="c-librsvg-2">librsvg-2.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libpixbufloader-svg">
        <term><filename class="libraryfile">libpixbufloader-svg.so</filename></term>
        <listitem>
          <para>
            é o plug-in <application>Gdk Pixbuf</application> que permite que
aplicativos <application>GTK+</application> renderizem imagens de Gráficos
Escaláveis Vetoriais
          </para>
          <indexterm zone="librsvg libpixbufloader-svg">
            <primary sortas="c-libpixbufloader-svg">libpixbufloader-svg.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
