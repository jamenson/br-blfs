<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY jasper-download-http "https://github.com/jasper-software/jasper/archive/version-&jasper-version;/jasper-version-&jasper-version;.tar.gz">
  <!ENTITY jasper-download-ftp  "">
  <!ENTITY jasper-md5sum        "aa4df693b90223fe6848b34cf1208624">
  <!ENTITY jasper-size          "1,9 MB">
  <!ENTITY jasper-buildsize     "9,4 MB (com testes)">
  <!ENTITY jasper-time          "0,3 UPC (com testes)">
]>

<sect1 id="jasper" xreflabel="jasper-&jasper-version;">
  <?dbhtml filename="jasper.html"?>


  <title>jasper-&jasper-version;</title>

  <indexterm zone="jasper">
    <primary sortas="a-jasper-&jasper-version;">jasper</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao jasper</title>

    <para>
      O Projeto <application>jasper</application> é uma iniciativa de fonte aberto
para fornecer uma implementação de referência baseada em software livre do
codec JPEG-2000.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&jasper-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&jasper-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &jasper-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &jasper-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &jasper-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &jasper-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do jasper</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="cmake"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="libjpeg"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="freeglut"/> (necessário para <command>jiv</command>), <xref
linkend="doxygen"/> (necessário para gerar documentação html) e <xref
linkend="texlive"/> ( necessário para regerar a documentação em pdf)
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do jasper</title>


    <!--
    <para>

      First, fix a problem that happens due to cmake-3.22:
    </para>

<screen><userinput remap="pre">sed -i '/GLUT_glut_LIBRARY/s/^/#/' build/cmake/modules/JasOpenGL.cmake</userinput></screen>
-->
<para>
      Instale <application>jasper</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir BUILD &amp;&amp;
cd    BUILD &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr    \
      -D CMAKE_BUILD_TYPE=Release     \
      -D CMAKE_SKIP_INSTALL_RPATH=ON  \
      -D JAS_ENABLE_DOC=NO            \
      -D ALLOW_IN_SOURCE_BUILD=YES    \
      -D CMAKE_INSTALL_DOCDIR=/usr/share/doc/jasper-&jasper-version; \
      ..  &amp;&amp;
make</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make test</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/cmake-skip-install-rpath.xml"/>

    <para>
      <parameter>-D JAS_ENABLE_DOC=NO</parameter>: Essa opção desabilita a
reconstrução da documentação em PDF se <xref linkend="texlive"/> estiver
instalado.
    </para>

    <para>
      <parameter>-D ALLOW_IN_SOURCE_BUILD=YES</parameter>: Essa chave permite
construir a partir da árvore do fonte. No nosso caso, isso é necessário para
nos permitir construir dentro do diretório BUILD em vez de precisar criar
outro diretório fora da árvore do fonte.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>imgcmp, imginfo, jasper e jiv</seg>
        <seg>libjasper.so</seg>
        <seg>/usr/include/jasper e /usr/share/doc/jasper-&jasper-version;</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="imgcmp">
        <term><command>imgcmp</command></term>
        <listitem>
          <para>
            compara duas imagens da mesma geometria
          </para>
          <indexterm zone="jasper imgcmp">
            <primary sortas="b-imgcmp">imgcmp</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="imginfo">
        <term><command>imginfo</command></term>
        <listitem>
          <para>
            exibe informações a respeito de uma imagem
          </para>
          <indexterm zone="jasper imginfo">
            <primary sortas="b-imginfo">imginfo</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="jasper-command">
        <term><command>jasper</command></term>
        <listitem>
          <para>
            converte imagens entre formatos (BMP, JPS, JPC, JPG, PGX, PNM, MIF e RAS)
          </para>
          <indexterm zone="jasper jasper">
            <primary sortas="b-jasper">jasper</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="jiv">
        <term><command>jiv</command></term>
        <listitem>
          <para>
            exibe imagens
          </para>
          <indexterm zone="jasper jiv">
            <primary sortas="b-jiv">jiv</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="jasper-lib">
        <term><filename class="libraryfile">libjasper.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca usada por aplicativos para ler e gravar arquivos no formato
"JPEG2000"
          </para>
          <indexterm zone="jasper jasper-lib">
            <primary sortas="c-libjasper">libjasper.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
