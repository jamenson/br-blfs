<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY pixman-download-http "https://www.cairographics.org/releases/pixman-&pixman-version;.tar.gz">
  <!ENTITY pixman-download-ftp  "">
  <!ENTITY pixman-md5sum        "0825cd6bfc488d5177f2f013a06ef240">
  <!ENTITY pixman-size          "796 KB">
  <!ENTITY pixman-buildsize     "29 MB (Com testes)">
  <!ENTITY pixman-time          "0,2 UPC (Usando paralelismo=4; com testes)">
]>

<sect1 id="pixman" xreflabel="Pixman-&pixman-version;">
  <?dbhtml filename="pixman.html"?>


  <title>Pixman-&pixman-version;</title>

  <indexterm zone="pixman">
    <primary sortas="a-Pixman">Pixman</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Pixman</title>

    <para>
      O pacote <application>Pixman</application> contém uma biblioteca que fornece
recursos de manipulação de pixel de baixo nível, como composição de imagem e
rasterização trapezoidal.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&pixman-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&pixman-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &pixman-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &pixman-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &pixman-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &pixman-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do Pixman</bridgehead>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="libpng"/> e <xref linkend="gtk3"/> (para testes e
demonstrações)
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Pixman</title>

    <para>
      Instale <application>Pixman</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr --buildtype=release .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>ninja test</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libpixman-1.so
        </seg>
        <seg>
          /usr/include/pixman-1
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libpixman">
        <term><filename class="libraryfile">libpixman-1.so</filename></term>
        <listitem>
          <para>
            contém funções que fornecem recursos de baixo nível de manipulação de pixel
          </para>
          <indexterm zone="pixman libpixman">
            <primary sortas="c-libpixman">libpixman-1.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
