<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libavif-download-http "https://github.com/AOMediaCodec/libavif/archive/v&libavif-version;/libavif-&libavif-version;.tar.gz">
  <!ENTITY libavif-download-ftp  "">
  <!ENTITY libavif-md5sum        "ec292cb8d51c0aa02f9fd5ef2419c853">
  <!ENTITY libavif-size          "13 MB">
  <!ENTITY libavif-buildsize     "22 MB">
  <!ENTITY libavif-time          "menos que 0,1 UPC">
]>

<sect1 id="libavif" xreflabel="libavif-&libavif-version;">
  <?dbhtml filename="libavif.html"?>

  <title>libavif-&libavif-version;</title>

  <indexterm zone="libavif">
    <primary sortas="a-libavif">libavif</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libavif</title>

    <para>
      O pacote <application>libavif</application> contém uma biblioteca usada para
codificar e decodificar arquivos AVIF.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libavif-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libavif-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libavif-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libavif-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libavif-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libavif-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libavif</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="libaom"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="gdk-pixbuf"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <ulink url="https://github.com/google/googletest">gtest</ulink>, <ulink
url="https://code.videolan.org/videolan/dav1d">libdav1d</ulink>, <ulink
url="https://chromium.googlesource.com/libyuv/libyuv/">libyuv</ulink>,
<ulink url="https://github.com/xiph/rav1e">rav1e</ulink> e <ulink
url="https://gitlab.com/AOMediaCodec/SVT-AV1">svt-av1</ulink>
    </para>

    &test-use-internet;

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libavif</title>

    <para>
      Instale o <application>libavif</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr \
      -D CMAKE_BUILD_TYPE=Release  \
      -D AVIF_CODEC_AOM=SYSTEM     \
      -D AVIF_BUILD_GDK_PIXBUF=ON  \
      -D AVIF_LIBYUV=OFF           \
      -G Ninja .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar o pacote (observe que isso fará com que o sistema de construção
baixe uma cópia do <ulink
url="https://github.com/google/googletest">gtest</ulink> e construa a suíte
de teste com a cópia), emita:
    </para>

<screen remap='test'><userinput>cmake .. -D AVIF_GTEST=LOCAL -D AVIF_BUILD_TESTS=ON &amp;&amp;
ninja &amp;&amp; ninja test</userinput></screen>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

    <para>
      O formato AV1 precisa ser adicionado ao cache dos carregadores. Como o(a)
usuário(a) &root;:
    </para>

<screen role="root"><userinput>gdk-pixbuf-query-loaders --update-cache</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>-D AVIF_CODEC_AOM=ON</parameter>: Essa chave habilita usar o
codificador AOM. Esse pacote é inútil sem pelo menos um codificador
integrado.
    </para>

    <para>
      <parameter>-D AVIF_BUILD_GDK_PIXBUF=ON</parameter>: Essa chave constrói o
carregador AVIF para aplicativos que usam gdk-pixbuf. Remova-a se você não
tiver instalado o <xref role="nodep" linkend="gdk-pixbuf"/>.
    </para>

    <para>
      <option>-D AVIF_LIBYUV=OFF</option>: Use essa chave se você não tiver
instalado <ulink
url="https://chromium.googlesource.com/libyuv/libyuv/">libyuv</ulink>.
    </para>

    <para>
      <option>-D AVIF_CODEC_DAV1D=SYSTEM</option>: Use essa chave se você tiver
instalado <ulink
url="https://code.videolan.org/videolan/dav1d">libdav1d</ulink> e desejar
usá-lo como um codificador.
    </para>

    <para>
      <option>-D AVIF_CODEC_RAV1E=SYSTEM</option>: Use essa chave se você tiver
instalado <ulink url="https://github.com/xiph/rav1e">rav1e</ulink> e desejar
usá-lo como um codificador.
    </para>

    <para>
      <option>-D AVIF_CODEC_SVT=SYSTEM</option>: Use essa chave se você tiver
instalado <ulink
url="https://gitlab.com/AOMediaCodec/SVT-AV1">svt-av1</ulink> e desejar
usá-lo como um codificador.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libavif.so e libpixbbufloader-avif.so (em
/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders)
        </seg>
        <seg>
          /usr/include/avif e /usr/lib/cmake/libavif
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libavif-lib">
        <term><filename class="libraryfile">libavif.so</filename></term>
        <listitem>
          <para>
            contém funções que fornecem uma implementação C portável do formato de
imagem AV1
          </para>
          <indexterm zone="libavif-lib libavif">
            <primary sortas="c-libavif">libavif.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libpixbufloader-avif">
        <term><filename class="libraryfile">libpixbufloader-avif.so</filename></term>
        <listitem>
          <para>
            permite que aplicativos que usam gdk-pixbuf leiam imagens AVIF
          </para>
          <indexterm zone="libavif libpixbufloader-avif">
            <primary sortas="c-libpixbufloader-avif">libpixbufloader-avif.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
