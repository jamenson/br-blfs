<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY rust-bindgen-download-http "https://github.com/rust-lang/rust-bindgen/archive/v&rust-bindgen-version;/rust-bindgen-&rust-bindgen-version;.tar.gz">
  <!ENTITY rust-bindgen-md5sum        "b59ecb112ad52cbba2297e650f507764">
  <!ENTITY rust-bindgen-size          "2,3 MB">
  <!ENTITY rust-bindgen-buildsize     "161 MB">
  <!ENTITY rust-bindgen-time          "0,3 UPC (com paralelismo=8)">
]>

<sect1 id="rust-bindgen" xreflabel="rust-bindgen-&rust-bindgen-version;">
  <?dbhtml filename="rust-bindgen.html"?>

  <title>rust-bindgen-&rust-bindgen-version;</title>

  <indexterm zone="rust-bindgen">
    <primary sortas="a-rust-bindgen">rust-bindgen</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao rust-bindgen</title>

    <para>
      O pacote <application>rust-bindgen</application> contém um utilitário que
gera ligações Rust a partir de cabeçalhos C/C++.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&rust-bindgen-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &rust-bindgen-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &rust-bindgen-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &rust-bindgen-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &rust-bindgen-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências de rust-bindgen</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="rust"/> e <xref role='runtime' linkend="llvm"/> (com Clang,
tempo de execução)
    </para>

    &build-use-internet;

  </sect2>

  <sect2 role="installation">
    <title>Instalação do rust-bindgen</title>

    <para>
      Instale <application>rust-bindgen</application> executando os seguintes
comandos:
    </para>

<screen><userinput>cargo build --release</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>cargo test --release</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>install -v -m755 target/release/bindgen /usr/bin</userinput></screen>

    <para>
      Ainda como o(a) usuário(a) &root;, instale os arquivos de suporte à
completação do Bash e do Zsh:
    </para>

<screen role='root'><userinput>bindgen --generate-shell-completions bash \
    &gt; /usr/share/bash-completion/completions/bindgen
bindgen --generate-shell-completions zsh  \
    &gt; /usr/share/zsh/site-functions/_bindgen</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          bindgen
        </seg>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          Nenhum(a)
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="bindgen">
        <term><command>bindgen</command></term>
        <listitem>
          <para>
            gera ligações para Rust a partir de cabeçalhos C/C++
          </para>
          <indexterm zone="rust-bindgen bindgen">
            <primary sortas="b-bindgen">bindgen</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
