<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY doxygen-download-http "https://doxygen.nl/files/doxygen-&doxygen-version;.src.tar.gz">
  <!ENTITY doxygen-download-ftp  "">
  <!ENTITY doxygen-md5sum        "1c3bfaaeda1544bf2b33b73ceb4d2ea4">
  <!ENTITY doxygen-size          "8,1 MB">
  <!ENTITY doxygen-buildsize     "253 MB (com testes)">
  <!ENTITY doxygen-time          "1,6 UPC (com testes; ambos usando paralelismo=4)">
]>

<sect1 id="doxygen" xreflabel="Doxygen-&doxygen-version;">
  <?dbhtml filename="doxygen.html"?>


  <title>Doxygen-&doxygen-version;</title>

  <indexterm zone="doxygen">
    <primary sortas="a-Doxygen">Doxygen</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao "Doxygen"</title>

    <para>
      O pacote <application>Doxygen</application> contém um sistema de
documentação para "C++", "C", "Java", "Objective-C", "Corba IDL" e, até
certo ponto, "PHP", "C#" e "D". É útil para gerar documentação "HTML" e(ou)
manual de referência fora de linha a partir de um conjunto de arquivos fonte
documentados. Também existe suporte para gerar saída em "RTF", "PostScript",
"PDF" com hiperlink, "HTML" compactado e páginas de manual "Unix". A
documentação é extraída diretamente dos fontes, o que torna muito mais fácil
manter a documentação consistente com o código-fonte.
    </para>

    <para>
      Você também pode configurar o <application>Doxygen</application> para
extrair a estrutura do código a partir de arquivos fonte não
documentados. Isso é muito útil para encontrar rapidamente seu caminho em
grandes distribuições de fontes. Usado junto com
<application>Graphviz</application>, você também consegue visualizar as
relações entre os vários elementos por meio de gráficos de dependência,
diagramas de herança e diagramas de colaboração, todos gerados
automaticamente.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&doxygen-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&doxygen-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &doxygen-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &doxygen-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &doxygen-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &doxygen-time;
        </para>
      </listitem>
    </itemizedlist>


    <!--
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing='compact'>
      <listitem>
        <para>
          Required patch: <ulink
          url="&patch-root;/doxygen-&doxygen-version;-flex_2_6_0_fix-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>-->
<bridgehead renderas="sect3">Dependências do "Doxygen"</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="cmake"/> e <xref linkend="git"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="qt6"/> (para doxywizard)
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <!-- Can someone check this? With LLVM7, it fails to build. -->
<xref linkend="graphviz"/>, <xref linkend="gs"/>, <xref linkend="libxml2"/>
(exigido para os testes), <xref linkend="llvm"/> (com clang), <xref
linkend="texlive"/> (ou <xref linkend="tl-installer"/>), <xref
linkend="xapian"/> (para doxyindexer) e <ulink
url="https://javacc.github.io/javacc/">javacc</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do "Doxygen"</title>

    <!--  At version 1.11.0, I don't see this.  -bdubbs
    <note>
      <para>
        When untarring this package, you may see messages such as
        <quote>Ignoring unknown extended header keyword 'LIBARCHIVE.xattr.com.apple.TextEncoding'</quote>
.
        These messages are harmless, and can be safely ignored.
      </para>
    </note>
-->
<para>
      Primeiro, corrija alguns conjuntos de comandos sequenciais python:
    </para>

<screen><userinput>grep -rl '^#!.*python$' | xargs sed -i '1s/python/&amp;3/'</userinput></screen>

    <para>
      Instale <application>Doxygen</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir -v build &amp;&amp;
cd       build &amp;&amp;

cmake -G "Unix Makefiles"          \
      -D CMAKE_BUILD_TYPE=Release  \
      -D CMAKE_INSTALL_PREFIX=/usr \
      -D build_wizard=ON           \
      -D force_qt=Qt6              \
      -W no-dev .. &amp;&amp;
make</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make testes</command>. Um teste,
012_cite.dox, é conhecido por falhar se <xref linkend="texlive"/> ou <xref
linkend="tl-installer"/> não estiver instalado.
    </para>

    <para>
      Se você deseja gerar a documentação do pacote, [então] você precisa ter
<application>Python</application>, <application>TeX Live</application> (para
documentos "HTML") e <application>Ghostscript</application> (para documentos
"PDF") instalados, então emita o seguinte comando:
    </para>

<screen remap="doc"><userinput>cmake  -D build_doc=ON \
       -D DOC_INSTALL_DIR=share/doc/doxygen-&doxygen-version; \
       .. &amp;&amp;
make docs</userinput></screen>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install &amp;&amp;
install -vm644 ../doc/*.1 /usr/share/man/man1</userinput></screen>

    <para>
      Se você gerou a documentação do pacote, então as páginas de manual serão
instaladas automaticamente e você não precisará executar o último comando
<command>install ...</command>.
    </para>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <option>-D build_wizard=OFF</option>: Use essa chave se
<application>Qt6</application> não estiver instalado.
    </para>

    <para>
      <option>-D build_search=ON</option>: Use essa chave se
<application>xapian</application> estiver instalado e você desejar construir
ferramentas de pesquisa externas (<command>doxysearch.cgi</command> e
<command>doxyindexer</command>).
    </para>

    <para>
      <option>-D force_qt6=ON</option>: Use essa chave para construir
<command>doxywizard</command> com Qt6, mesmo se Qt5 estiver instalado.
    </para>

    <para>
      <option>-D use_libclang=ON</option>: Use essa chave se
<application>llvm</application> com <application>clang</application>
estiverem instalados, para adicionar suporte para análise da libclang.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando o "Doxygen"</title>

    <para>
      Não existe nenhuma configuração real necessária para o pacote
<application>Doxygen</application>, embora três pacotes adicionais sejam
exigidos se você desejar usar recursos estendidos. Se demandar fórmulas para
criar documentação em PDF, então você precisa ter o <xref
linkend="texlive"/> instalado. Se demandar fórmulas para converter arquivos
PostScript em bitmaps, então você precisa ter o <xref linkend="gs"/>
instalado.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          doxygen e opcionalmente, doxywizard, doxyindexer e doxysearch.cgi
        </seg>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          /usr/share/doc/doxygen-&doxygen-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="doxygen-prog">
        <term><command>doxygen</command></term>
        <listitem>
          <para>
            é um utilitário baseado em linha de comando usado para gerar arquivos de
configuração de modelo e, em seguida, gerar documentação a partir desses
modelos. Use <command>doxygen --help</command> para uma explicação dos
parâmetros da linha de comando
          </para>
          <indexterm zone="doxygen doxygen-prog">
            <primary sortas="b-doxygen">doxygen</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="doxywizard">
        <term><command>doxywizard</command></term>
        <listitem>
          <para>
            é uma estrutura "GUI" de interação direta com o(a) usuário(a) para
configurar e executar <command>doxygen</command>
          </para>
          <indexterm zone="doxygen doxywizard">
            <primary sortas="b-doxywizard">doxywizard</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="doxyindexer">
        <term><command>doxyindexer</command></term>
        <listitem>
          <para>
            gera um índice de pesquisa chamado <filename>doxysearch.db</filename> a
partir de um ou mais arquivos de dados de pesquisa produzidos por
<command>doxygen</command>. Veja-se, por exemplo, <ulink
url="https://javacc.github.io/javacc/"/>
          </para>
          <indexterm zone="doxygen doxyindexer">
            <primary sortas="b-doxyindexer">doxyindexer</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="doxysearch.cgi">
        <term><command>doxysearch.cgi</command></term>
        <listitem>
          <para>
            é um aplicativo "CGI" para pesquisar os dados indexados por
<command>doxyindexer</command>
          </para>
          <indexterm zone="doxygen doxysearch.cgi">
            <primary sortas="b-doxysearch.cgi">doxysearch.cgi</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
