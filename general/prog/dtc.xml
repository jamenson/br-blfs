<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY dtc-download-http "https://kernel.org/pub/software/utils/dtc/dtc-&dtc-version;.tar.xz">
  <!ENTITY dtc-md5sum        "0f193be84172556027da22d4fe3464e0">
  <!ENTITY dtc-size          "168 KB">
  <!ENTITY dtc-buildsize     "13 MB (com testes)">
  <!ENTITY dtc-time          "0,2 UPC (com testes)">
]>


<!-- Try to keep the indentation used in this file-->
<sect1 id="dtc" xreflabel="dtc-&dtc-version;">
  <?dbhtml filename="dtc.html"?>

  <title>dtc-&dtc-version;</title>

  <!--Required section-->
<indexterm zone="dtc">
    <primary sortas="a-dtc">dtc</primary>
  </indexterm>

  
  <sect2 role="package">
    <title>Introdução ao Dtc</title>

    <para>
      O pacote <application>dtc</application> contém o Device Tree Compiler para
trabalhar com fontes de árvores de dispositivos e arquivos binários e também
libfdt, uma biblioteca de utilitários para ler e manipular árvores de
dispositivos no formato binário.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&dtc-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &dtc-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &dtc-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &dtc-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &dtc-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências de Dtc</bridgehead>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend='libyaml'/>, <xref linkend='swig'/> e <xref
linkend='texlive'/>
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do Dtc</title>

    <para>
      Instale <application>dtc</application> executando os seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr         \
            --buildtype=release   \
            -D python=disabled .. &amp;&amp;
ninja</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>CC='gcc -Wl,-z,noexecstack' meson
test -v</command>.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

    <screen role="root"><userinput>ninja install</userinput></screen>

    <para>
      Ainda como o(a) usuário(a) &root;, remova a biblioteca estática inútil:
    </para>

    <screen role="root"><userinput>rm /usr/lib/libfdt.a</userinput></screen>

    <para>
      Se você tiver <xref linkend='texlive'/> instalado, poderá construir o
formato PDF da documentação emitindo o seguinte comando:
    </para>

    <screen remap="doc"><userinput>pushd ../Documentation
  latexmk -bibtex --pdf dtc-paper &amp;&amp;
  latexmk -bibtex --pdf dtc-paper -c
popd</userinput></screen>

    <para>
      Para instalar a documentação, como o(a) usuário(a) &root; emita o seguinte
comando:
    </para>

    <screen role="root"><userinput>cp -R ../Documentation -T /usr/share/doc/dtc-&dtc-version;</userinput></screen>

    <para>
      Se você tiver instalado <xref linkend='swig'/> e desejar instalar as
ligações Python 3 desse pacote, construa o módulo Python 3:
    </para>

    <screen role="nodump"><userinput>&build-wheel-cmd; ..</userinput></screen>

    <para>
      Como o(a) usuário(a) &root;, instale o módulo Python 3:
    </para>

    <screen role="nodump"><userinput>&install-wheel; libfdt</userinput></screen>
  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D python=disabled</parameter>: Essa chave impede construir a
ligação do Python 3 com o método obsoleto (executando
<command>setup.py</command> diretamente). Nós construiremos a ligação do
Python 3 com o comando <command>pip3 wheel</command> separadamente, se
desejado.
    </para>

    
    <!-- https://github.com/dgibson/dtc/issues/163 -->
<para>
      <envar>CC='gcc -Wl,-z,noexecstack'</envar>: Essa variável impede marcar as
bibliotecas compartilhadas na suíte de teste como exigindo pilha
executável. A Glibc 2.41 ou posterior parou de permitir abrir com
<function>dlopen</function> tal biblioteca compartilhada, de forma que a
suíte de teste falharia. Mas essas bibliotecas compartilhadas não precisam
realmente de uma pilha executável, de modo que nós podemos usar
<parameter>-Wl,-z,noexecstack</parameter> para consertar a suíte de teste. É
necessário no ambiente <envar>CC</envar> para o <command>meson
test</command> porque essas bibliotecas compartilhadas são construídas por
um conjunto de comandos sequenciais de teste em vez do sistema de construção
<command>meson</command>/<command>ninja</command>, e o conjunto de comandos
sequenciais de teste não reconhece outras variáveis de ambiente
<quote>comuns</quote> como <envar>LDFLAGS</envar>.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          convert-dtsv0, dtc, dtdiff, fdtdump, fdtget, fdtoverlay e fdtput
        </seg>
        <seg>
          libfdt.so
        </seg>
        <seg>
          /usr/lib/python&python3-majorver;/site-packages/libfdt-&dtc-version;.dist-info
e /usr/share/doc/dtc-&dtc-version; (opcionalmente)
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="convert-dtsv0">
        <term><command>convert-dtsv0</command></term>
        <listitem>
          <para>
            converte a fonte da árvore de dispositivos v0 em árvore de dispositivos v1
          </para>
          <indexterm zone="dtc convert-dtsv0">
            <primary sortas="b-convert-dtsv0">convert-dtsv0</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="dtc-prog">
        <term><command>dtc</command></term>
        <listitem>
          <para>
            compila a fonte da árvore de dispositivos (dts) para o blob binário da
árvore de dispositivos (dtb) ou descompila dtb para dts
          </para>
          <indexterm zone="dtc dtc-prog">
            <primary sortas="b-dtc">dtc</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="dtdiff">
        <term><command>dtdiff</command></term>
        <listitem>
          <para>
            compara duas árvores de dispositivos
          </para>
          <indexterm zone="dtc dtdiff">
            <primary sortas="b-dtdiff">dtdiff</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="fdtdump">
        <term><command>fdtdump</command></term>
        <listitem>
          <para>
            imprime uma versão legível de um arquivo simples de árvore de dispositivos
          </para>
          <indexterm zone="dtc fdtdump">
            <primary sortas="b-fdtdump">fdtdump</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="fdtget">
        <term><command>fdtget</command></term>
        <listitem>
          <para>
            lê valores a partir da árvore de dispositivos
          </para>
          <indexterm zone="dtc fdtget">
            <primary sortas="b-fdtget">fdtget</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="fdtoverlay">
        <term><command>fdtoverlay</command></term>
        <listitem>
          <para>
            aplica uma série de sobreposições a um blob de árvore de dispositivo base
          </para>
          <indexterm zone="dtc fdtoverlay">
            <primary sortas="b-fdtoverlay">fdtoverlay</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="fdtput">
        <term><command>fdtput</command></term>
        <listitem>
          <para>
            escreve um valor de propriedade em uma árvore de dispositivos
          </para>
          <indexterm zone="dtc fdtput">
            <primary sortas="b-fdtput">fdtput</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libfdt">
        <term><filename class="libraryfile">libfdt.so</filename></term>
        <listitem>
          <para>
            é uma biblioteca de utilitários para ler e manipular árvores de dispositivos
no formato binário
          </para>
          <indexterm zone="dtc libfdt">
            <primary sortas="c-libfdt">libfdt.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
