<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;
  <!-- Remove once mozilla programs can use python 3.12 -->
  <!ENTITY python311-download-http
           "https://www.python.org/ftp/python/&python311-version;/Python-&python311-version;.tar.xz">
  <!ENTITY python311-md5sum        "4efe92adf28875c77d3b9b2e8d3bc44a">
  <!ENTITY python311-size          "19 MB">
  <!ENTITY python311-buildsize     "301 MB">
  <!ENTITY python311-time          "1,1 UPC (Usando paralelismo=4)">
]>

<sect1 id="python311" xreflabel="Python-&python311-version;">
  <?dbhtml filename="python311.html" ?>

  <title>Python-&python311-version;</title>

  <indexterm zone="python311">
    <primary sortas="a-Python3.11">Python3.11</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Python 3.11</title>

    <para>
      O pacote <application>Python 3.11</application> contém uma versão mais
antiga do ambiente de desenvolvimento do
<application>Python</application>. Isso é necessário <emphasis
role="bold">somente</emphasis> para construir <xref linkend='seamonkey'/>,
pois o sistema de construção dele não tem sido atualizado para suportar
<xref linkend="python3"/>.
    </para>

    <note>
      <para>
        NÃO instale esse pacote se você não estiver instalando Seamonkey.
      </para>
    </note>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&python311-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &python311-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &python311-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &python311-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &python311-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do Python 3.11</bridgehead>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <!-- (required if building firefox or thunderbird)
           commented out because it's obviously conflicting with
           "Do NOT install this package if you are not installing
           Seamonkey."  Maybe we should just demote this to optional but
           the package is already tagged for 12.3.  FIXME.  -->
<xref linkend="sqlite"/>
      
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Python 3.11</title>

    <para>
      Instale <application>Python 3.11</application> executando os seguintes
comandos:
    </para>

<screen><userinput>CXX="/usr/bin/g++"                     \
./configure --prefix=/opt/python3.11   \
            --disable-shared           \
            --with-system-expat        &amp;&amp;
make</userinput></screen>

    <para>
      Como esse pacote é usado somente em situações muito limitadas, testes não
são recomendados.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command> CXX="/usr/bin/g++" ./configure ...</command>: Evite uma mensagem
irritante durante a configuração.
    </para>

    <para>
      <parameter>--prefix=/opt/python3.11</parameter>: Isso instala Python 3.11 em
/opt para a finalidade de evitar conflitos com a versão do sistema do Python
e permitir fácil remoção/isolamento quando programas atualizarem para Python
3.12 ou posterior.
    </para>

    <para>
      <parameter>--disable-shared</parameter>: Essa chave desabilita construir
bibliotecas compartilhadas. Como todos os pacotes que precisam do
python-3.11 foram construídos em módulos e não se vinculam diretamente ao
python, é seguro desativar o suporte de biblioteca compartilhada.
    </para>

    <para>
      <parameter>--with-system-expat</parameter>: Essa chave habilita vinculação à
versão do sistema do <application>Expat</application>.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          python3.11
        </seg>
        <seg>
          libpython3.11.a
        </seg>
        <seg>
          /opt/python3.11/include/python3.11 e /opt/python3.11/lib/python3.11,
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="python311-ver">
        <term><command>python3.11</command></term>
        <listitem>
          <para>
            é um nome específico da versão para o programa <command>python</command>
          </para>
          <indexterm zone="python311 python311-ver">
            <primary sortas="b-python3.11">python3.11</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>

  </sect2>

</sect1>
