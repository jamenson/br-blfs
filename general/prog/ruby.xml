<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY ruby-download-http "https://cache.ruby-lang.org/pub/ruby/&ruby-minor-version;/ruby-&ruby-version;.tar.xz">
  <!ENTITY ruby-download-ftp  "">
  <!ENTITY ruby-md5sum        "63a56b170246fcf0198a5eaba57dd46c">
  <!ENTITY ruby-size          "16 MB">
  <!ENTITY ruby-buildsize     "697 MB (com documentos de API da C e testes)">
  <!ENTITY ruby-time          "1,0 UPC (com documentos da API da C; adicionar 0,5 UPC para testes; todos
usando paralelismo=4)">
]>

<sect1 id="ruby" xreflabel="Ruby-&ruby-version;">
  <?dbhtml filename="ruby.html"?>


  <title>Ruby-&ruby-version;</title>

  <indexterm zone="ruby">
    <primary sortas="a-Ruby">Ruby</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Ruby</title>

    <para>
      O pacote &quot;<application>Ruby</application>&quot; contém o ambiente de
desenvolvimento &quot;<application>Ruby</application>&quot;. Isso é útil
para &quot;scripts&quot; orientados a objetos.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&ruby-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&ruby-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &ruby-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &ruby-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &ruby-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &ruby-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do Ruby</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="libyaml"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="doxygen"/>, <xref linkend="graphviz"/>, <xref
linkend="rust"/>, <xref linkend="tk"/>, <xref linkend="valgrind"/>,
&berkeley-db; e <ulink url="https://dtrace.org/about/">DTrace</ulink>
    </para>

    &test-use-internet;

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Ruby</title>

    <note>
      <para>
        Se tiver instalado um pacote que fornece uma gema de Ruby, como <xref
linkend="asciidoctor"/>, você precisa reinstalar esses pacotes ao atualizar
para uma nova versão secundária do Ruby.
      </para>
    </note>

    <para>
      Instale &quot;<application>Ruby</application>&quot; executando o seguinte
comando:
    </para>

<screen><userinput>./configure --prefix=/usr         \
            --disable-rpath       \
            --enable-shared       \
            --without-valgrind    \
            --without-baseruby    \
            ac_cv_func_qsort_r=no \
            --docdir=/usr/share/doc/ruby-&ruby-version; &amp;&amp;
make</userinput></screen>

    <para>
      Opcionalmente, construa os documentos da &quot;API&quot; &quot;C&quot;
executando os seguintes comandos:
    </para>

<screen><userinput>make capi</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make -k check</command>. Se o
ambiente tiver variáveis relacionadas às configurações de proxy
(<envar>all_proxy</envar>, <envar>ALL_PROXY</envar>,
<envar>http_proxy</envar> e etc.) configuradas, a suíte de teste sairá mais
cedo com mensagens como <computeroutput>net/ftp is not
found</computeroutput>. Certifique-se de que você desconfigurou essas
variáveis para a suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

    <note>
      <para>
        Se você tem aplicativos &quot;Ruby-On-Rails&quot; instalados e fez uma
atualização do &quot;<application>Ruby</application>&quot; instalando esse
pacote, [então] você pode precisar executar uma atualização lá também (como
o(a) usuário(a) &quot;&root;&quot;):
      </para>

<screen role="nodump"><userinput>cd /caminho/para/aplicativo/web/
bundle update rake</userinput></screen>

      <para>
        e reinicie o servidor &quot;web&quot; que atende o aplicativo.
      </para>

    </note>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>--disable-rpath</parameter>: Essa chave desabilita a incorporação
de <filename class='directory'>/usr/lib</filename> como um caminho de
pesquisa de biblioteca no aplicativo <command>ruby</command>. Fazer isso não
é necessário (porque <filename class='directory'>/usr/lib</filename> é um
caminho de biblioteca do sistema) e possivelmente faça com que a suíte de
teste executar com a <filename class='libraryfile'>libruby. so</filename> do
sistema em vez da recém construída quando o Ruby foi instalado.
    </para>

    <para>
      <parameter>--enable-shared</parameter>: Essa chave habilita construir a
biblioteca compartilhada &quot;<filename
class="libraryfile">libruby</filename>&quot;.
    </para>

    <para>
      <parameter>--without-baseruby</parameter>: Essa chave impede usar o
<command>ruby</command> do sistema se ele já estiver instalado. O sistema de
construção usará a versão recém-construída.
    </para>

    <para>
      <parameter>ac_cv_func_qsort_r=no</parameter>: Essa chave impede usar a
função <function>qsort_r</function> proveniente da Glibc. Ruby faz algumas
otimizações agressivas assumindo alguns detalhes de implementação do
algoritmo de ordenação, mas a suposição não é verdadeira com a implementação
da Glibc. Com essa chave, Ruby usará a própria implementação dela de
ordenação.
    </para>

    <para>
      <option>--disable-install-doc</option>: Essa chave desabilita construir e
instalar índices &quot;rdoc&quot; e documentos da &quot;API&quot;
&quot;C&quot;.
    </para>

    <para>
      <option>--disable-install-rdoc</option>: Essa chave desabilita construir e
instalar índices &quot;rdoc&quot;.
    </para>

    <para>
      <option>--disable-install-capi</option>: Essa chave desabilita construir e
instalar documentos da &quot;API&quot; &quot;C&quot;.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          bundle, bundler, erb, gem, irb, racc, rake, rbs, rdbg, rdoc, ri, ruby e
typeprof
        </seg>
        <seg>
          libruby.so
        </seg>
        <seg>
          /usr/include/ruby-&ruby-minor-version;.0, /usr/lib/ruby,
/usr/share/doc/ruby-&ruby-version; e /usr/share/ri
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="bundle">
        <term><command>bundle</command></term>
        <listitem>
          <para>
            cria pacotes de &quot;Ruby Gems&quot;
          </para>
          <indexterm zone="ruby bundle">
            <primary sortas="b-bundle">bundle</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="bundler">
        <term><command>bundler</command></term>
        <listitem>
          <para>
            gerencia as dependências de um aplicativo durante todo o ciclo de vida dele
          </para>
          <indexterm zone="ruby bundler">
            <primary sortas="b-bundler">bundler</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="erb">
        <term><command>erb</command></term>
        <listitem>
          <para>
            é uma estrutura de interação direta com o(a) usuário(a) em linha de comando
para &quot;eRuby&quot;, que fornece um sistema de modelos para
&quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby erb">
            <primary sortas="b-erb">erb</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="gem">
        <term><command>gem</command></term>
        <listitem>
          <para>
            é o comando para &quot;RubyGems&quot;, que é um sofisticado gerenciador de
pacotes para &quot;<application>Ruby</application>&quot;. Isso é semelhante
ao comando &quot;pip&quot; do &quot;Python&quot;
          </para>
          <indexterm zone="ruby gem">
            <primary sortas="b-gem">gem</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="irb">
        <term><command>irb</command></term>
        <listitem>
          <para>
            é a interface interativa para &quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby irb">
            <primary sortas="b-irb">irb</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="rake">
        <term><command>rake</command></term>
        <listitem>
          <para>
            é um utilitário de construção semelhante ao &quot;make&quot; para
&quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby rake">
            <primary sortas="b-rake">rake</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="rdbg">
        <term><command>rdbg</command></term>
        <listitem>
          <para>
            é um depurador interativo para &quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby rdbg">
            <primary sortas="b-rdbg">rdbg</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="rdoc">
        <term><command>rdoc</command></term>
        <listitem>
          <para>
            gera documentação &quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby rdoc">
            <primary sortas="b-rdoc">rdoc</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="ri">
        <term><command>ri</command></term>
        <listitem>
          <para>
            exibe documentação a partir de uma base de dados relacionada a classes,
módulos e métodos &quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby ri">
            <primary sortas="b-ri">ri</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="ruby-prog">
        <term><command>ruby</command></term>
        <listitem>
          <para>
            é uma linguagem de &quot;script&quot; interpretada para programação
orientada a objetos rápida e fácil
          </para>
          <indexterm zone="ruby ruby-prog">
            <primary sortas="b-ruby">ruby</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libruby">
        <term><filename role="libraryfile">libruby.so</filename></term>
        <listitem>
          <para>
            contém as funções da &quot;API&quot; exigidas pelo
&quot;<application>Ruby</application>&quot;
          </para>
          <indexterm zone="ruby libruby">
            <primary sortas="c-libruby">libruby.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
