<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY python3-download-http
           "https://www.python.org/ftp/python/&python3-version;/Python-&python3-version;.tar.xz">
  <!ENTITY python3-download-ftp  "">
  <!ENTITY python3-md5sum        "4c2d9202ab4db02c9d0999b14655dfe5">
  <!ENTITY python3-size          "22 MB">
  <!ENTITY python3-buildsize     "649 MB (adicionar 65 MB para os testes)">
  <!ENTITY python3-time          "2,1 UPC (adicionar 1,1 UPC para testes; ambos usando paralelismo=4)">
  <!--
  Note: Size does not reflect docs that were installed in LFS.
  Test time measured with 60 second timeout.
  -->
  <!ENTITY python3htmldoc-download-http
  "https://www.python.org/ftp/python/doc/&python3-version;/python-&python3-version;-docs-html.tar.bz2">
  <!ENTITY python3-docs-md5sum "d6aede88f480a018d26b3206f21654ae">
]>

<sect1 id="python3" xreflabel="Python-&python3-version;">
  <?dbhtml filename="python3.html" ?>


  <title>Python-&python3-version;</title>

  <indexterm zone="python3">
    <primary sortas="a-Python3">Python3</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Python 3</title>

    <para>
      O pacote <application>Python 3</application> contém o ambiente de
desenvolvimento <application>Python</application>. Isso é útil para
programação orientada a objetos, escrita de conjuntos sequenciais de
comandos, prototipagem de aplicativos grandes ou desenvolvimento de
aplicativos inteiros.
    </para>

    <note>
      <para>
        O <application>Python 3</application> foi instalado no LFS. A única razão
para reconstruí-lo aqui é se módulos opcionais forem necessários ou para
atualizar esse pacote.
      </para>
    </note>

    &lfs123_checked;

    <important>
      <para>
         Se atualizar para uma nova versão secundária do Python-3 (por exemplo, de
Python-3.11.x para Python-3.12.0), você precisará reinstalar quaisquer
módulos do Python3 que tiver instalado. Você também deveria reinstalar
pacotes que geram módulos do Python3, incluindo &gobject-introspection;,
<xref linkend="libxml2"/>, <xref linkend="opencv"/>, <xref
linkend="fontforge"/>, <xref linkend="gnome-tweaks"/>, <xref
linkend="samba"/> e <xref linkend="graphviz"/> (se swig estiver instalado).
      </para>

      <para>Antes de atualizar, você pode obter uma lista de módulos instalados com
<command>pip3 list</command>. A lista pode estar incompleta porque alguns
módulos Python não são instalados com <command>pip3</command>, por exemplo o
módulo <literal>cracklib</literal> instalado por <xref role='nodep'
linkend='cracklib'/>. Use <command>ls
/usr/lib/python3.<replaceable>minor</replaceable>/site-packages</command>
para uma lista abrangente.
      </para>

      <para>
         Os módulos Python oriundos do LFS também terão que ser reinstalados: <ulink
url="&lfs-root;/chapter08/flit-core.html">flit-core</ulink>, <ulink
url="&lfs-root;/chapter08/wheel.html">wheel</ulink>, <ulink
url="&lfs-root;/chapter08/setuptools.html">setuptools</ulink>, <ulink
url="&lfs-root;/chapter08/meson.html">meson</ulink>, <ulink
url="&lfs-root;/chapter08/markupsafe.html">MarkupSafe</ulink> e <ulink
url="&lfs-root;/chapter08/jinja2.html">Jinja2</ulink>.
      </para>
    </important>

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&python3-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&python3-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &python3-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &python3-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &python3-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &python3-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferência Adicional Opcional</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&python3htmldoc-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &python3-docs-md5sum;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do &quot;Python&quot; 3</bridgehead>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="sqlite"/> (exigido se construir Firefox ou Thunderbird)
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="bluez"/>, <xref linkend="gdb"/> (exigido para alguns testes),
<xref linkend="valgrind"/> e <ulink
url="https://www.bytereef.org/mpdecimal/">libmpdec</ulink>
    </para>

    <bridgehead renderas="sect4">Opcionais (Para Módulos Adicionais)</bridgehead>
    <para role="optional">
      <xref linkend="libnsl"/>, <xref linkend="tk"/> e &berkeley-db;
    </para>

    <para condition="html" role="usernotes">Observações de Editor(a): <ulink url="&blfs-wiki;/Python3"/>
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do Python 3</title>

    <para>
      Instale o &quot;<application>Python 3</application>&quot; executando os
seguintes comandos:
    </para>

<screen><userinput>CXX="/usr/bin/g++"               \
./configure --prefix=/usr        \
            --enable-shared      \
            --with-system-expat  \
            --enable-optimizations &amp;&amp;
make</userinput></screen>

    <para>
      Alguns testes são conhecidos por travarem ocasional e
indefinidamente. Portanto, para testar os resultados, execute a suíte de
teste, porém configure um limite de tempo de 2 minutos para cada caso de
teste:
    </para>

<!-- 
         For Python-3.12.3 all tests pass
         Total tests: run=41,778 skipped=1,155
         Total test files: run=485/488 skipped=24 resource_denied=3

         For Python-3.12.4 
         Total tests: run=42,152 failures=6 skipped=1,234
         Total test files: run=487/489 failed=4 skipped=17 resource_denied=6 rerun=4

         For Python-3.13.0
         Total tests: run=44,153 skipped=1,640
         Total test files: run=479/478 failed=3 skipped=20 resource_denied=2 rerun=3

         For Python-3.13.1
         Total tests: run=44,420 failures=2 skipped=1,863
         Total test files: run=480/478 failed=4 skipped=20 resource_denied=2 rerun=4

         For Python-3.13.2
         Total tests: run=46,327 failures=2 skipped=1,716
         Total test files: run=479/480 failed=1 skipped=21 resource_denied=2 rerun=1
    -->
<screen remap='test'><userinput>make test TESTOPTS="--timeout 120"</userinput></screen>

    

    <para>
      Para um sistema relativamente lento, você possivelmente precise aumentar o
limite de tempo e 1 UPC (medido ao construir Binutils  a passagem 1 com um
núcleo de CPU) deveria ser suficiente. Alguns testes são instáveis, de forma
que a suíte de teste automaticamente reexecutará testes falhos. Se um teste
falhou, porém a seguir passou quando reexecutado, ele deveria ser
considerado como passado.
    </para>

    <para>
      De mais de 44.000 testes, o seguinte teste é conhecido por falhar:
test_importlib.
    </para>

    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

    <para>
      Se atualizar e a documentação tiver sido baixada, [então] opcionalmente
instale-a como o(a) usuário(a) &quot;&root;&quot;:
    </para>

<screen role="root"><userinput>install -v -dm755 /usr/share/doc/python-&python3-version;/html

tar --strip-components=1 \
    --no-same-owner       \
    --no-same-permissions \
    -C /usr/share/doc/python-&python3-version;/html \
    -xvf ../python-&python3-version;-docs-html.tar.bz2</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command> CXX="/usr/bin/g++" ./configure ...</command>: Evite uma mensagem
irritante durante a configuração.
    </para>

    <para>
      <parameter>--with-system-expat</parameter>: Essa chave habilita vinculação à
versão do sistema do <application>Expat</application>.
    </para>



    <!--
    <para>

      <parameter>- -with-ensurepip=yes</parameter> : This switch enables building
      the <command>pip</command> and <command>setuptools</command> packaging
      programs. <command>setuptools</command> is needed for building some
      Python modules.
    </para>
-->
<para>
      <parameter>--enable-optimizations</parameter>: Essa chave habilita
otimizações estáveis, mas caras.
    </para>

    <para>
      <option>--with-lto</option>: Essa chave opcional habilita &quot;Link Time
Optimization&quot; denso. Excepcionalmente, ele cria um &quot;<filename
class="libraryfile">/usr/lib/python&python3-majorver;/config-&python3-majorver;-&lt;arch&gt;-linux-gnu/libpython&python3-majorver;.a</filename>&quot;
muito maior com um pequeno aumento no tempo para compilar o
&quot;<application>Python</application>&quot;. Os resultados em tempo de
execução não parecem mostrar nenhum benefício advindo de fazer isso.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando o Python 3</title>

    <para>
      Para a finalidade de que &quot;<command>python3</command>&quot; encontre a
documentação instalada, crie o seguinte link simbólico independente da
versão:
    </para>

<screen role="root"><userinput>ln -svfn python-&python3-version; /usr/share/doc/python-3</userinput></screen>

    <para>
      e adicione a seguinte variável de ambiente ao perfil individual do(a)
usuário(a) ou ao do sistema:
    </para>

<screen role="root"><userinput>export PYTHONDOCS=/usr/share/doc/python-3/html</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          <!--
          easy_install-&python3-majorver;,
          -->
2to3 (link simbólico) e 2to3-&python3-majorver;, idle3 (link simbólico) e
idle&python3-majorver;, pip3 e pip&python3-majorver;, pydoc3 (link
simbólico) e pydoc&python3-majorver;, python3 (link simbólico) e
python&python3-majorver;, e python3-config (link simbólico) e
python&python3-majorver;-config
        </seg>
        <seg>
          libpython&python3-majorver;.so e libpython3.so
        </seg>
        <seg>
          /usr/include/python&python3-majorver;, /usr/lib/python&python3-majorver; e
/usr/share/doc/python-&python3-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <!--
      <varlistentry id="2to3">

        <term><command>2to3</command></term>
        <listitem>
          <para>
            is designed to assist in the transition between python2 and python3
            by automatically converting code to be Python3 compatible.
          </para>
          <indexterm zone="python3 2to3">
            <primary sortas="b-2to3">2to3</primary>
          </indexterm>
        </listitem>
      </varlistentry>
      Put here for easy use when removing Python2 -->
<!--
      <varlistentry id="easy_install">

        <term><command>easy_install</command></term>
        <listitem>
          <para>
            is a frontend to pip3 to make it easier to configure python modules
            through the PIP package manager
          </para>
          <indexterm zone="python3 easy_install">
            <primary sortas="python3 easy_install">easy_install</primary>
          </indexterm>
        </listitem>
      </varlistentry>
      -->
<?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>
      
      
      <varlistentry id="idle3">
        <term><command>idle3</command></term>
        <listitem>
          <para>
            é um conjunto sequencial de comandos envólucros que abrem um editor
&quot;GUI&quot; compatível com
&quot;<application>Python</application>&quot;. Para esse conjunto sequencial
de comandos executar, você precisa ter instalado o
&quot;<application>Tk</application>&quot; antes do &quot;Python&quot;, de
forma que o módulo &quot;Tkinter&quot; do &quot;Python&quot; seja construído
          </para>
          <indexterm zone="python3 idle3">
            <primary sortas="b-idle3">idle3</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="pydoc3">
        <term><command>pydoc3</command></term>
        <listitem>
          <para>
            é a ferramenta de documentação do
&quot;<application>Python</application>&quot;
          </para>
          <indexterm zone="python3 pydoc3">
            <primary sortas="b-pydoc3">pydo3c</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="python3-prog">
        <term><command>python3</command></term>
        <listitem>
          <para>
            é uma linguagem de programação interpretada, interativa e orientada a
objetos
          </para>
          <indexterm zone="python3 python3-prog">
            <primary sortas="b-python3">python3</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="python-ver">
        <term><command>python&python3-majorver;</command></term>
        <listitem>
          <para>
            é um nome específico da versão para o programa <command>python</command>
          </para>
          <indexterm zone="python3 python-ver">
            <primary sortas="b-python&python3-majorver;">python&python3-majorver;</primary>
          </indexterm>
        </listitem>
      </varlistentry>
<!--
      <varlistentry id="pyvenv">

        <term><command>pyvenv</command></term>
        <listitem>
          <para>
            creates virtual <application>Python</application> environments in
            one or more target directories.
          </para>
          <indexterm zone="python3 pyvenv">
            <primary
            sortas="b-python&python3-majorver;">python&python3-majorver;</primary>
          </indexterm>
        </listitem>
      </varlistentry>
      -->
      
    </variablelist>

  </sect2>

</sect1>
