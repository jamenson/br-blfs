<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect2 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../../general.ent">
  %general-entities;

  <!ENTITY numpy-download-http "https://files.pythonhosted.org/packages/source/n/numpy/numpy-&numpy-version;.tar.gz">
  <!ENTITY numpy-download-ftp  "">
  <!ENTITY numpy-md5sum        "&numpy-md5sum;">
  <!ENTITY numpy-size          "19 MB">
  <!ENTITY numpy-buildsize     "107 MB (add 23 MB for tests)">
  <!ENTITY numpy-time          "0,4 UPC (adicionar 1,1 UPC para testes)">
]>

  <sect2 id="numpy" xreflabel="NumPy-&numpy-version;">

    <title>NumPy-&numpy-version;</title>

    <indexterm zone="numpy">
      <primary sortas="a-numpy">numpy</primary>
    </indexterm>

    <sect3 role="package">
      <title>Introdução ao Módulo NumPy</title>

      <para>
        &quot;<application>NumPy</application>&quot; é o pacote fundamental para
computação científica com &quot;Python&quot;.
      </para>

      &lfs123_checked;

      <bridgehead renderas="sect4">Informação do Pacote</bridgehead>
      <itemizedlist spacing="compact">
        <listitem>
          <para>
            Transferência (HTTP): <ulink url="&numpy-download-http;"/>
          </para>
        </listitem>
        <listitem>
          <para>
            Transferência (FTP): <ulink url="&numpy-download-ftp;"/>
          </para>
        </listitem>
        <listitem>
          <para>
            Soma de verificação MD5 da transferência: &numpy-md5sum;
          </para>
        </listitem>
        <listitem>
          <para>
            Tamanho da transferência: &numpy-size;
          </para>
        </listitem>
        <listitem>
          <para>
            Espaço em disco estimado exigido: &numpy-buildsize;
          </para>
        </listitem>
        <listitem>
          <para>
            Tempo de construção estimado: &numpy-time;
          </para>
        </listitem>
      </itemizedlist>

      <bridgehead renderas="sect4">Dependências do NumPy</bridgehead>

      <bridgehead renderas="sect5">Exigidas</bridgehead>
      <para role="required">
        <xref linkend="cython"/>, <xref linkend="meson_python"/> e <xref
linkend="pyproject-metadata"/>
      </para>



      <!--  <bridgehead renderas="sect5">
Recommended</bridgehead>
      <para role="recommended">
        <xref linkend="setuptools_scm"/>
      </para>-->
<bridgehead renderas="sect5">Opcionais</bridgehead>
      <para role="optional">
        fortran oriundo do <xref linkend="gcc"/>, <ulink
url="https://www.netlib.org/lapack/">lapack e cblas</ulink> e <ulink
url="https://www.openblas.net/">openblas</ulink>
      </para>

      <bridgehead renderas="sect5">Opcionais (para testagem)</bridgehead>
      <para role="optional">
        <xref linkend="attrs"/>, <xref linkend="pytest"/>, <xref linkend="pytz"/> e
<ulink url="https://pypi.org/project/hypothesis/">hypothesis</ulink>
      </para>

    </sect3>

    <sect3 role="installation">
      <title>Instalação do NumPy</title>

      <para> Construa o módulo: </para>

<screen><userinput>pip3 wheel -w dist --no-build-isolation --no-deps --no-cache-dir -C setup-args=-Dallow-noblas=true $PWD</userinput></screen>

      <para>
        Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
      </para>

<screen role="root"><userinput>&install-wheel; numpy</userinput></screen>

      <para>
        A instalação pode ser testada com os seguintes comandos:
      </para>

<!-- no && for actual test because of a possible error -->
<!--
      <para>

        Two tests, out of over 48000 tests, are known to fail.
      </para>
-->
<screen remap="test"><userinput>mkdir -p test                                  &amp;&amp;
cd       test                                  &amp;&amp;
python3 -m venv --system-site-packages testenv &amp;&amp;
source testenv/bin/activate                    &amp;&amp;
pip3 install hypothesis                        &amp;&amp;
python3 -c "import numpy, sys; sys.exit(numpy.test() is False)"
deactivate</userinput></screen>





    </sect3>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../../xincludes/pip3-cmd-explain.xml"/>

    <sect3 role="content">
      <title>Conteúdo</title>

      <segmentedlist>
        <segtitle>Aplicativos Instalados</segtitle>
        <segtitle>Bibliotecas Instaladas</segtitle>
        <segtitle>Diretórios Instalados</segtitle>

        <seglistitem>
          <seg>
            f2py, f2py3 e f2py&python3-majorver; (três cópias do mesmo
&quot;script&quot;)
          </seg>
          
          <!-- we seem not to record the solibs, see e.g. lxml, cairo
           actually, this one has a lot, all with awkward
           * .cpython-311-x86_64-linux-gnu.so names -->
<seg>Nenhum(a)</seg>
          <seg>
            /usr/lib/python&python3-majorver;/site-packages/numpy
          </seg>
        </seglistitem>
      </segmentedlist>

      <variablelist>
        <bridgehead renderas="sect5">Descrições Curtas</bridgehead>
        <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

        <varlistentry id="f2py">
          <term><command>f2py</command></term>
          <listitem>
            <para>
              é o utilitário gerador de interface &quot;Fortran&quot; para
&quot;Python&quot;.
            </para>
            <indexterm zone="numpy f2py">
              <primary sortas="b-f2py">f2py</primary>
            </indexterm>
          </listitem>
        </varlistentry>

      </variablelist>

    </sect3>

  </sect2>
