<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY usbutils-download-http "https://kernel.org/pub/linux/utils/usb/usbutils/usbutils-&usbutils-version;.tar.xz">
  <!ENTITY usbutils-download-ftp  "">
  <!ENTITY usbutils-md5sum        "0a351e2241c50a1f026a455dccf24d73">
  <!ENTITY usbutils-size          "120 KB">
  <!ENTITY usbutils-buildsize     "1,9 MB">
  <!ENTITY usbutils-time          "menos que 0,1 UPC">
]>

<sect1 id="usbutils" xreflabel="usbutils-&usbutils-version;">
  <?dbhtml filename="usbutils.html"?>


  <title>usbutils-&usbutils-version;</title>

  <indexterm zone="usbutils">
    <primary sortas="a-usbutils">usbutils</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao "USB Utils"</title>

    <para>
      O pacote <application>USB Utils</application> contém utilitários usados para
exibir informações relativas a barramentos "USB" no sistema e os
dispositivos conectados a eles.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&usbutils-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&usbutils-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &usbutils-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &usbutils-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &usbutils-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &usbutils-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do "USB Utils"</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="libusb"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref role="runtime" linkend="hwdata"/> (tempo de execução)
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do "USB Utils"</title>

    <para>
      Instale <application>USB Utils</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..            \
      --prefix=/usr       \
      --buildtype=release &amp;&amp;

ninja</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

    <para>
      Para o arquivo de dados <filename>usb.ids</filename>, instale o pacote <xref
linkend='hwdata'/>.
    </para>

    <para>
      O script <command>lsusb.py</command> exibe informações em um formato mais
facilmente legível que <command>lsusb</command>. Para encontrar as opções,
use <command>lsusb.py -h</command>. Uma forma de uso recomendada pelo(a)
desenvolvedor(a) é <command>lsusb.py -ciu</command>.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          lsusb, lsusb.py, usb-devices e usbhid-dump
        </seg>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          Nenhum(a)
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="lsusb">
        <term><command>lsusb</command></term>
        <listitem>
          <para>
            é um utilitário para exibir informações relativas a todos os barramentos
"USB" no sistema e todos os dispositivos conectados a eles, mas não de forma
amigável a humanos(as)
          </para>
          <indexterm zone="usbutils lsusb">
            <primary sortas="b-lsusb">lsusb</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="lsusb.py">
        <term><command>lsusb.py</command></term>
        <listitem>
          <para>
            exibe informações relativas a todos os barramentos "USB" no sistema e todos
os dispositivos conectados a eles em forma razoavelmente amigável a
humanos(as)
          </para>
          <indexterm zone="usbutils lsusb.py">
            <primary sortas="b-lsusb.py">lsusb.py</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="usb-devices">
        <term><command>usb-devices</command></term>
        <listitem>
          <para>
            é um script de shell que exibe detalhes dos barramentos "USB" e dos
dispositivos conectados a eles. Ele foi projetado para ser usado se
"/proc/bus/usb/devices" não estiver disponível em seu sistema
          </para>
          <indexterm zone="usbutils usb-devices">
            <primary sortas="b-usb-devices">usb-devices</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="usbhid-dump">
        <term><command>usbhid-dump</command></term>
        <listitem>
          <para>
            é usado para despejar descritores de informes e fluxos a partir de
interfaces "HID" (dispositivo de interface humana) de dispositivos "USB"
          </para>
          <indexterm zone="usbutils usbhid-dump">
            <primary sortas="b-usbhid-dump">usbhid-dump</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
