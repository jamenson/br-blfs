<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY mc-download-http "http://ftp.midnight-commander.org/mc-&mc-version;.tar.xz">
  <!ENTITY mc-download-ftp  "">
  <!ENTITY mc-md5sum        "b3596c1f092b9822a6cd9c9a1aef8dde">
  <!ENTITY mc-size          "2,3 MB">
  <!ENTITY mc-buildsize     "71 MB (adicionar 97 MB para os testes)">
  <!ENTITY mc-time          "0,3 UPC (usando paralelismo=4; adicionar 0,1 UPC para testes)">
]>

<sect1 id="mc" xreflabel="MC-&mc-version;">
  <?dbhtml filename="mc.html"?>


  <title>MC-&mc-version;</title>

  <indexterm zone="mc">
    <primary sortas="a-MC">MC</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao "MC"</title>

    <para>
      <application>MC</application> (Midnight Commander) é um gerenciador de
arquivos em tela cheia em modo texto e shell visual. Ele fornece uma
interface clara, amigável e um tanto protegida para um sistema Unix, ao
mesmo tempo que torna muitas operações frequentes de arquivos mais
eficientes e preserva todo o poder do prompt de comando.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&mc-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&mc-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &mc-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &mc-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &mc-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &mc-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do "MC"</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="glib2"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="slang"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <!--<xref linkend="samba"/>
,-->
<xref linkend="doxygen"/>, <xref linkend="gpm"/>, <xref
linkend="graphviz"/>, <xref linkend="libarchive"/>, <xref
linkend="libssh2"/>, <xref linkend="pcre2"/>, <xref linkend="ruby"/>, <xref
linkend="x-window-system"/> e <xref linkend="zip"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do "MC"</title>

    <para>
      Instale <application>MC</application> executando os seguintes comandos:
    </para>

<screen><userinput>./configure --prefix=/usr \
           --sysconfdir=/etc \
           --enable-charset &amp;&amp;
make</userinput></screen>

    <para>
      Para testar os resultados, emita: <command>make check</command>.

    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>--sysconfdir=/etc</parameter>: Essa chave coloca o diretório
global de configuração em <filename class="directory">/etc</filename>.
    </para>

    <para>
      <parameter>--enable-charset</parameter>: Essa chave adiciona suporte ao
<command>mcedit</command> para edição de arquivos em codificações diferentes
daquela implícita na localidade atual.
    </para>

    <para>
      <parameter>--with-screen=ncurses</parameter>: Use isso se você não tiver
<xref linkend="slang"/> instalado.
    </para>

    <para>
      <option>--with-search-engine=pcre2</option>: Use essa chave se você
preferisse usar <xref linkend="pcre2" role="nodep"/> em vez de GLib para o
mecanismo de pesquisa integrado.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando o "MC"</title>

    <sect3 id="mc-config">
      <title>Arquivos de Configuração</title>

      <para>
        <filename>~/.config/mc/*</filename>
      </para>

      <indexterm zone="mc mc-config">
        <primary sortas="e-AA.config/.mc/*">~/.config/.mc/*</primary>
      </indexterm>

    </sect3>

    <sect3>
      <title>Informação de Configuração</title>

      <para>
        O diretório <filename class="directory">~/.config/mc</filename> e o conteúdo
dele são criados quando você inicia o <command>mc</command> pela primeira
vez. Então você pode editar o arquivo principal de configuração
<filename>~/.config/mc/ini </filename> manualmente ou por meio do shell do
<application> MC</application>. Consulte-se a página de manual do <ulink
role='man' url='&man;mc.1'>mc(1)</ulink> para detalhes.
      </para>

    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>"mc" e os links simbólicos "mcdiff", "mcedit" e "mcview"</seg>
        <seg>Nenhum(a)</seg>
        <seg>/etc/mc e /usr/{libexec,share}/mc</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <!-- This program is installed in /usr/libexec
      <varlistentry id="conssaver">

        <term><command>cons.saver</command></term>
        <listitem>
          <para>
            is used internally by <command>mc</command> for saving and
            restoring the text behind the panels on Linux text console
          </para>
          <indexterm zone="mc conssaver">
            <primary sortas="b-conssaver">cons.saver</primary>
          </indexterm>
        </listitem>
      </varlistentry>
      -->
<?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      

      <varlistentry id="mc-prog">
        <term><command>mc</command></term>
        <listitem>
          <para>
            é um shell visual
          </para>
          <indexterm zone="mc mc-prog">
            <primary sortas="b-mc">mc</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="mcdiff">
        <term><command>mcdiff</command></term>
        <listitem>
          <para>
            é uma ferramenta interna visual de comparação
          </para>
          <indexterm zone="mc mcdiff">
            <primary sortas="b-mcdiff">mcdiff</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="mcedit">
        <term><command>mcedit</command></term>
        <listitem>
          <para>
            é um editor interno de arquivos
          </para>
          <indexterm zone="mc mcedit">
            <primary sortas="b-mcedit">mcedit</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="mcview">
        <term><command>mcview</command></term>
        <listitem>
          <para>
            é um visualizador interno de arquivos
          </para>
          <indexterm zone="mc mcview">
            <primary sortas="b-mcview">mcview</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
