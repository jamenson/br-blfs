<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY lm-sensors-download-http "https://github.com/lm-sensors/lm-sensors/archive/V&lm-sensors-version;/lm-sensors-&lm-sensors-version;.tar.gz">
  <!ENTITY lm-sensors-download-ftp  "">
  <!ENTITY lm-sensors-md5sum        "f60e47b5eb50bbeed48a9f43bb08dd5e">
  <!ENTITY lm-sensors-size          "268 KB">
  <!ENTITY lm-sensors-buildsize     "2,6 MB">
  <!ENTITY lm-sensors-time          "menos que 0,1 UPC">
]>

<sect1 id="lm_sensors" xreflabel="lm-sensors-&lm-sensors-version;">
  <?dbhtml filename="lm-sensors.html"?>


  <title>lm-sensors-&lm-sensors-version;</title>

  <indexterm zone="lm_sensors">
    <primary sortas="a-lm_sensors">lm-sensors</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao lm-sensors</title>

    <para>
      O pacote <application>lm-sensors</application> fornece suporte de espaço de
usuário(a) para os controladores de monitoramento de hardware no núcleo
Linux. Isso é útil para monitorar a temperatura da CPU e para ajustar o
desempenho de alguns hardwares (como ventiladores de resfriamento).
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&lm-sensors-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&lm-sensors-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &lm-sensors-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &lm-sensors-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &lm-sensors-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &lm-sensors-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do lm-sensors</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="which"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <ulink url="https://oss.oetiker.ch/rrdtool/">RRDtool</ulink> (exigido para
construir o aplicativo <command>sensord</command>) e <ulink role="runtime"
url="https://www.nongnu.org/dmidecode/">dmidecode</ulink> (tempo de
execução)
    </para>

  </sect2>

  <sect2 role="kernel" id="lm_sensors-kernel">
    <title>Configuração do Núcleo</title>

    <para>
      As opções de configuração a seguir tentam abranger os dispositivos de
monitoramento de hardware mais comuns em um sistema típico de área de
trabalho ou laptop. Veja-se a ajuda de cada uma (pressionando o botão
<keycap>H</keycap> com a opção focada em <command>make menuconfig</command>)
para saber se você precisa dela. Existem muitos dispositivos de
monitoramento de hardware específicos da plataforma, de forma que é
impossível listar a configuração de todos eles aqui. Você pode investigar o
conteúdo de <filename class='directory'>/sys/class/hwmon</filename> dentro
de uma distribuição <quote>mainstream</quote> em execução no sistema para
saber quais controladores você precisa.
    </para>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="lm_sensors-kernel.xml"/>

    <para>
      Recompile teu núcleo e reinicialize no novo núcleo.
    </para>

    <indexterm zone="lm_sensors lm_sensors-kernel">
      <primary sortas="d-lm_sensors">lm_sensors</primary>
    </indexterm>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do lm-sensors</title>

    <para>
      Instale <application>lm-sensors</application> executando os seguintes
comandos:
    </para>

<screen><userinput>make PREFIX=/usr           \
     BUILD_STATIC_LIB=0    \
     MANDIR=/usr/share/man \
     EXLDFLAGS=</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make PREFIX=/usr        \
     BUILD_STATIC_LIB=0 \
     MANDIR=/usr/share/man install &amp;&amp;

install -v -m755 -d /usr/share/doc/lm-sensors-&lm-sensors-version; &amp;&amp;
cp -rv              README INSTALL doc/* \
                    /usr/share/doc/lm-sensors-&lm-sensors-version;</userinput></screen>
  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>BUILD_STATIC_LIB=0</parameter>: Esse parâmetro desabilita a
compilação e instalação da versão estática da <filename
class="libraryfile">libsensors</filename>.
    </para>

    <para>
      <parameter>EXLDFLAGS=</parameter>: Esse parâmetro desabilita codificar
rigidamente caminhos de pesquisa de biblioteca (rpath) nos arquivos binários
executáveis e bibliotecas compartilhadas. Esse pacote não precisa do rpath
para uma instalação no local padrão, e o rpath às vezes pode causar efeitos
indesejados ou até mesmo problemas de segurança.
    </para>

    <para>
      <option>PROG_EXTRA=sensord</option>: Esse parâmetro habilita compilar o
<command>sensord</command>, um processo de segundo plano que consegue
monitorar teu sistema em intervalos regulares. Compilar
<command>sensord</command> exige <ulink
url="https://oss.oetiker.ch/rrdtool/">RRDtool</ulink>. Certifique-se de
instalar o RRDtool em <filename class="directory">/usr</filename> executando
<command>make prefix=/usr</command> ao construí-lo. Caso contrário,
lm-sensors não o encontrará facilmente.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando lm-sensors</title>

    <sect3 id="lm_sensors-config">
      <title>Arquivo de Configuração</title>

      <para>
        <filename>/etc/sensors3.conf</filename>
      </para>

      <indexterm zone="lm_sensors lm_sensors-config">
        <primary
        sortas="e-etc-path-Configfilename2">/etc/sensors3.conf</primary>
      </indexterm>

    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          fancontrol, isadump, isaset, pwmconfig, sensors, sensors-conf-convert,
sensors-detect e, opcionalmente, sensord
        </seg>
        <seg>
          libsensors.so
        </seg>
        <seg>
          /etc/sensors.d, /usr/include/sensors e
/usr/share/doc/lm-sensors-&lm-sensors-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="fancontrol">
        <term><command>fancontrol</command></term>
        <listitem>
          <para>
            é um conjunto de comandos sequenciais de shell para uso com
<application>lm-sensors</application>. Ele lê a configuração dele a partir
de um arquivo (/etc/sensors3.conf por padrão), então calcula velocidades do
ventilador a partir das temperaturas e configura as correspondentes saídas
geradas de PWM para os valores computados
          </para>
          <indexterm zone="lm_sensors fancontrol">
            <primary sortas="b-fancontrol">fancontrol</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="isadump">
        <term><command>isadump</command></term>
        <listitem>
          <para>
            é um pequeno aplicativo auxiliar para examinar registros visíveis por meio
do barramento "ISA". Destina-se a testar qualquer chip que resida no
barramento "ISA" trabalhando com um registrador de endereços e um
registrador de dados (acesso tipo "I2C") ou um intervalo plano (de até 256
bytes)
          </para>
          <indexterm zone="lm_sensors isadump">
            <primary sortas="b-isadump">isadump</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="isaset">
        <term><command>isaset</command></term>
        <listitem>
          <para>
            é um pequeno aplicativo auxiliar para configurar registros visíveis por meio
do barramento "ISA"
          </para>
          <indexterm zone="lm_sensors isaset">
            <primary sortas="b-isaset">isaset</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="pwmconfig">
        <term><command>pwmconfig</command></term>
        <listitem>
          <para>
            testa as saídas geradas de modulação por largura de pulso ("PWM") dos
sensores e configura o controle do ventilador
          </para>
          <indexterm zone="lm_sensors pwmconfig">
            <primary sortas="b-pwmconfig">pwmconfig</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="sensors">
        <term><command>sensors</command></term>
        <listitem>
          <para>
            imprime as leituras atuais de todos os "chips" sensores
          </para>
          <indexterm zone="lm_sensors sensors">
            <primary sortas="b-sensors">sensors</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="sensors-conf-convert">
        <term><command>sensors-conf-convert</command></term>
        <listitem>
          <para>
            é um script <application>Perl</application> para converter arquivos de
configuração de versão 2 do <application>lm-sensors</application> para
funcionar com a versão 3
          </para>
          <indexterm zone="lm_sensors sensors-conf-convert">
            <primary sortas="b-sensors-conf-convert">sensors-conf-convert</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="sensors-detect">
        <term><command>sensors-detect</command></term>
        <listitem>
          <para>
            é um conjunto de comandos sequenciais <application>Perl</application> que te
guiará ao longo do processo de varredura do teu sistema em busca de vários
chips de monitoramento de hardware (sensores) suportados pela <filename
class="libraryfile">libsensors</filename>, ou mais geralmente pela suíte de
ferramentas do <application>lm-sensors</application>
          </para>
          <indexterm zone="lm_sensors sensors-detect">
            <primary sortas="b-sensors-detect">sensors-detect</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="sensord">
        <term><command>sensord</command></term>
        <listitem>
          <para>
            (opcional) é um processo de segundo plano que consegue ser usado para
registrar periodicamente as leituras do sensor
          </para>
          <indexterm zone="lm_sensors sensord">
            <primary sortas="b-sensord">sensord</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libsensors">
        <term><filename class="libraryfile">libsensors.so</filename></term>
        <listitem>
          <para>
            contém as funções de API do <application>lm-sensors</application>
          </para>
          <indexterm zone="lm_sensors libsensors">
            <primary sortas="c-libsensors">libsensors.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
