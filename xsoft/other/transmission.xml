<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY transmission-download-http "https://github.com/transmission/transmission/releases/download/&transmission-version;/transmission-&transmission-version;.tar.xz">
  <!ENTITY transmission-download-ftp  "">
  <!ENTITY transmission-md5sum        "8132b9f012b8e6309911c80ee9fd00f7">
  <!ENTITY transmission-size          "11 MB">
  <!ENTITY transmission-buildsize     "226 MB (com ambas interfaces GUI)">
  <!ENTITY transmission-time          "2,3 UPC (com ambas interfaces GUI; usando paralelismo=4)">
]>

<sect1 id="transmission" xreflabel="Transmission-&transmission-version;">
  <?dbhtml filename="transmission.html"?>


  <title>Transmission-&transmission-version;</title>

  <indexterm zone="transmission">
    <primary sortas="a-Transmission">Transmission</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Transmission</title>

    <para>
      <application>Transmission</application> é um cliente BitTorrent
multiplataforma e de fonte aberto. Isso é útil para baixar arquivos grandes
(como ISOs do Linux) e reduz a necessidade para os(as) distribuidores(as)
fornecerem largura de banda de servidor.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&transmission-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&transmission-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &transmission-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &transmission-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &transmission-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &transmission-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Remendo exigido: <ulink
url="&patch-root;/transmission-&transmission-version;-build_fix-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do Transmission</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="curl"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="libevent"/>, <xref linkend="libpsl"/> (para usar bibliotecas
do sistema, em vez das agrupadas) e <xref linkend="librsvg"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas (para construir uma GUI)</bridgehead>
    <para role="recommended">
      <xref linkend="gtkmm4"/> ou <xref linkend="qt6"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="nodejs"/> (para construir o cliente web, não necessário em
tempo de execução), <ulink
url="https://github.com/ubuntu/gnome-shell-extension-appindicator">appindicator</ulink>,
<ulink url="https://github.com/jech/dht">dht</ulink>, <ulink
url="https://github.com/libb64/libb64">libb64</ulink>, <ulink
url="https://github.com/ebiggers/libdeflate">libdeflate</ulink>, <ulink
url="https://github.com/miniupnp/libnatpmp">libnatpmp</ulink>, <ulink
url="https://github.com/bittorrent/libutp">libutp</ulink> e <ulink
url="https://github.com/miniupnp/miniupnp">miniupnp</ulink>
    </para>

    <para condition="html" role="usernotes">
      Observações de Editor(a): <ulink url="&blfs-wiki;/transmission"/>
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do Transmission</title>

    <para>
      Primeiro, corrija uma falha de construção devido a declarações incorretas em
CMakeLists.txt:
    </para>

<screen><userinput>patch -Np1 -i ../transmission-&transmission-version;-build_fix-1.patch</userinput></screen>

    <para>
      Instale o <application>Transmission</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr \
      -D CMAKE_BUILD_TYPE=Release  \
      -D CMAKE_INSTALL_DOCDIR=/usr/share/doc/transmission-&transmission-version; \
      .. &amp;&amp;

make</userinput></screen>


    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

    <para>
      A seguir, se a dependência <xref linkend="gtkmm4"/> foi instalada, crie
transmission.png a partir do arquivo SVG. Como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>rsvg-convert                                               \
   /usr/share/icons/hicolor/scalable/apps/transmission.svg \
   -o /usr/share/pixmaps/transmission.png</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <option>-D ENABLE_QT=OFF</option>: Essa chave desabilita construir a
interface <application>Qt</application>. O padrão é o de construí-la se
<xref linkend="qt6"/> estiver instalado.
    </para>

    <para>
      <option>-D ENABLE_GTK=OFF</option>: Essa chave desabilita construir a
interface GTK-4. O padrão é o de construí-la se o <xref linkend="gtkmm4"/>
estiver instalado.
    </para>

    <para>
      <option>-D ENABLE_WEB=OFF</option>: Essa chave desabilita a construção do
cliente web. O padrão é o de construí-lo se o <xref linkend="nodejs"/>
estiver instalado.
    </para>

    <para>
      <option>-D REBUILD_WEB=ON</option>: Essa chave força reconstruir o cliente
web. Essa opção precisa de <xref linkend="nodejs"/> para ser instalado bem
como de uma conexão de internet. O padrão é o de não reconstruir o cliente.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>
          
          <!--transmission-cli,-->
transmission-create, transmission-daemon, transmission-edit,
transmission-gtk, transmission-qt, transmission-remote e transmission-show
        </seg>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          /usr/share/transmission (contém o cliente web) e
/usr/share/doc/transmission-&transmission-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <!-- Not seen in 4.0.4 or 4.0.5
      <varlistentry id="transmission-cli">

        <term><command>transmission-cli</command></term>
        <listitem>
          <para>
            is a lightweight, command line BitTorrent client with scripting
            capabilities
          </para>
          <indexterm zone="transmission transmission-cli">
            <primary sortas="b-transmission-cli">transmission-cli</primary>
          </indexterm>
        </listitem>
      </varlistentry>
-->
<?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>



      <varlistentry id="transmission-create">
        <term><command>transmission-create</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando usada para criar arquivos .torrent
          </para>
          <indexterm zone="transmission transmission-create">
            <primary sortas="b-transmission-create">transmission-create</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="transmission-daemon">
        <term><command>transmission-daemon</command></term>
        <listitem>
          <para>
            é uma sessão do Transmission baseada em processo de segundo plano que pode
ser controlada por meio de comandos RPC a partir da interface web do
Transmission ou do <command>transmission-remote</command>
          </para>
          <indexterm zone="transmission transmission-daemon">
            <primary sortas="b-transmission-daemon">transmission-daemon</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="transmission-edit">
        <term><command>transmission-edit</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para modificar URLs de anúncio de
arquivos .torrent
          </para>
          <indexterm zone="transmission transmission-edit">
            <primary sortas="b-transmission-edit">transmission-edit</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="transmission-gtk">
        <term><command>transmission-gtk</command></term>
        <listitem>
          <para>
            é um cliente bittorrent GTK+
          </para>
          <indexterm zone="transmission transmission-gtk">
            <primary sortas="b-transmission-gtk">transmission-gtk</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="transmission-qt">
        <term><command>transmission-qt</command></term>
        <listitem>
          <para>
            é um cliente bittorrent baseado em Qt
          </para>
          <indexterm zone="transmission transmission-qt">
            <primary sortas="b-transmission-qt">transmission-qt</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="transmission-remote">
        <term><command>transmission-remote</command></term>
        <listitem>
          <para>
            é um utilitário de controle remoto para transmission-daemon e transmission
          </para>
          <indexterm zone="transmission transmission-remote">
            <primary sortas="b-transmission-remote">transmission-remote</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="transmission-show">
        <term><command>transmission-show</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para exibir metadados de arquivo
.torrent do BitTorrent
          </para>
          <indexterm zone="transmission transmission-show">
            <primary sortas="b-transmission-show">transmission-show</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
