<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY seamonkey-download-http "https://archive.seamonkey-project.org/releases/&seamonkey-version;/source/seamonkey-&seamonkey-version;.source.tar.xz">
  <!ENTITY seamonkey-download-ftp  "">
  <!ENTITY seamonkey-md5sum        "1b506ff751f39b2480ba4e16afb86d6d">
  <!ENTITY seamonkey-size          "237 MB">
  <!ENTITY seamonkey-buildsize     "4,4 GB (150 MB instalado)">
  <!ENTITY seamonkey-time          "6,8 UPC (com paralelismo=8)">
]>

<sect1 id="seamonkey" xreflabel="seamonkey-&seamonkey-version;">
  <?dbhtml filename="seamonkey.html" ?>


  <title>Seamonkey-&seamonkey-version;</title>

  <indexterm zone="seamonkey">
    <primary sortas="a-seamonkey">seamonkey</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Seamonkey</title>

    <para>
      <application>Seamonkey</application> é uma suíte de navegadores, um
descendente do <application>Netscape</application>. Ele inclui o navegador,
o compositor, clientes de mensagens e notícias e um cliente de IRC.
    </para>

    <para>
      É a continuação do Mozilla Application Suite, controlado pela comunidade,
criado depois que a Mozilla decidiu focar em aplicativos separados para
navegação e mensagens eletrônicas. Esses aplicativos são <xref
linkend="firefox"/> e <xref linkend="thunderbird"/>.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&seamonkey-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&seamonkey-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &seamonkey-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &seamonkey-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &seamonkey-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &seamonkey-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Remendo recomendado (exigido para construir com ICU-75 do sistema ou mais
recente): <ulink
url="&patch-root;/seamonkey-&seamonkey-version;-cxx17-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>

    <note>
      <para>
        O tarball <emphasis>seamonkey-&seamonkey-version;.source.tar.xz</emphasis>
desempacotará para o diretório
<emphasis>seamonkey-&seamonkey-version;</emphasis>.
      </para>
    </note>

    <bridgehead renderas="sect3">Dependências do Seamonkey</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="cbindgen"/>, <xref linkend="gtk3"/>, <xref
linkend="libarchive"/>, <xref linkend="python311"/>, <xref linkend="yasm"/>
e <xref linkend="zip"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <!--<xref linkend="libvpx"/>
, Causes build failures similar to Firefox -->
<xref linkend="icu"/>, <xref linkend="libevent"/>, <xref
linkend="libwebp"/>, <xref linkend="llvm"/> (com clang), <xref
linkend="nasm"/>, <xref linkend="nspr"/>, <xref linkend="nss"/> e <xref
linkend="pulseaudio"/>
    </para>

    <note>
      <para>
        Se você não instalar as dependências recomendadas, então cópias internas
desses pacotes serão usadas. Elas podem ter sido testadas para funcionar,
mas podem estar desatualizadas ou conter falhas de segurança.
      </para>
    </note>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="alsa-lib"/>, <xref linkend="dbus-glib"/>, <xref
linkend="nodejs"/>, <xref linkend="valgrind"/>, <xref linkend="wget"/>,
<xref linkend="wireless_tools"/>, <ulink
url="https://hunspell.sourceforge.net/">Hunspell</ulink> e <ulink
url="https://facebook.github.io/watchman/">Watchman</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Seamonkey</title>

    <para>
      A configuração do <application>Seamonkey</application> é realizada
criando-se um arquivo <filename>mozconfig</filename> contendo as opções
desejadas de configuração. Um arquivo <filename>mozconfig</filename> padrão
é criado abaixo. Para ver a lista completa de opções disponíveis de
configuração (e uma descrição abreviada de cada uma), emita <command>python3
configure.py --help</command>. Você também possivelmente revise o arquivo
inteiro e descomente quaisquer outras opções desejadas. Crie o arquivo
emitindo o seguinte comando:
    </para>

<screen><!-- With Firefox ESR 128 we'd replace the following part with
"- -enable-elf-hack=relr", or remove it if relr becomes the default.
But I've no idea if SeaMonkey will have this change backported.  -->
<?dbfo keep-together="auto"?><userinput>cat &gt; mozconfig &lt;&lt; "EOF"
<literal># If you have a multicore machine, all cores will be used

# If you have installed DBus-Glib comment out this line:
ac_add_options --disable-dbus

# If you have installed dbus-glib, and you have installed (or will install)
# wireless-tools, and you wish to use geolocation web services, comment out
# this line
ac_add_options --disable-necko-wifi

# Uncomment these lines if you have installed optional dependencies:
#ac_add_options --enable-system-hunspell

# Uncomment the following option if you have not installed PulseAudio
#ac_add_options --disable-pulseaudio
# and uncomment this if you installed alsa-lib instead of PulseAudio
#ac_add_options --enable-alsa

# Comment out the following option

# Comment out following options if you have not installed
# recommended dependencies:
ac_add_options --with-system-icu
ac_add_options --with-system-libevent
ac_add_options --with-system-nspr
ac_add_options --with-system-nss
ac_add_options --with-system-webp

# Disabling debug symbols makes the build much smaller and a little
# faster. Comment this if you need to run a debugger.
ac_add_options --disable-debug-symbols

# The elf-hack is reported to cause failed installs (after successful builds)
# on some machines. It is supposed to improve startup time and it shrinks
# libxul.so by a few MB.  With recent Binutils releases the linker already
# supports a much safer and generic way for this.
ac_add_options --disable-elf-hack
ac_add_options --enable-linker=bfd
export LDFLAGS="$LDFLAGS -Wl,-z,pack-relative-relocs"

# Seamonkey has some additional features that are not turned on by default,
# such as an IRC client, calendar, and DOM Inspector. The DOM Inspector
# aids with designing web pages. Comment these options if you do not
# desire these features.
ac_add_options --enable-calendar
ac_add_options --enable-dominspector
ac_add_options --enable-irc

# The BLFS editors recommend not changing anything below this line:
ac_add_options --prefix=/usr
ac_add_options --enable-application=comm/suite

ac_add_options --disable-crashreporter
ac_add_options --disable-updater
ac_add_options --disable-tests

# The SIMD code relies on the unmaintained packed_simd crate which
# fails to build with Rustc >= 1.78.0.  We may re-enable it once
# Mozilla ports the code to use std::simd and std::simd is stabilized.
ac_add_options --disable-rust-simd

ac_add_options --enable-strip
ac_add_options --enable-install-strip

# You cannot distribute the binary if you do this.
ac_add_options --enable-official-branding

ac_add_options --enable-system-ffi
ac_add_options --enable-system-pixman
ac_add_options --with-system-jpeg
ac_add_options --with-system-png
ac_add_options --with-system-zlib

export CC=clang CXX=clang++</literal>
EOF</userinput></screen>

    <note>
      <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
       href="../../xincludes/mozshm.xml"/>
    </note>

    <para>
      Primeiro, se você estiver construindo com o ICU do sistema, adapte o
mapeamento de quebra de linha para o ICU-74 ou posterior, aplique um remendo
para construir esse pacote com o C++17 padrão porque os cabeçalhos do ICU-75
ou posterior exigem alguns recursos do C++17 e adapte o sistema de
construção para usar a biblioteca correta com o ICU-76 ou posterior:
    </para>

<!-- https://bugzilla.mozilla.org/show_bug.cgi?id=1894423 -->
<screen><userinput remap="pre">(for i in {43..47}; do
   sed '/ZWJ/s/$/,CLASS_CHARACTER/' -i intl/lwbrk/LineBreaker.cpp || exit $?
done) &amp;&amp;

patch -Np1 -i ../seamonkey-&seamonkey-version;-cxx17-1.patch &amp;&amp;

sed -i 's/icu-i18n/icu-uc &amp;/' js/moz.configure</userinput></screen>

    
    <para>
      Em seguida, remova um pedaço de código não usado que viola o padrão C++. O
Clang começou a rejeitá-lo, embora não seja usado desde o lançamento 19.1.0:
    </para>

<screen><userinput>sed -e '/ExclusiveData(ExclusiveData&amp;&amp;/,/^ *}/d' \
    -i js/src/threading/ExclusiveData.h</userinput></screen>

    <para>
      A seguir, corrija um problema com o módulo incluído 'distro' do python:
    </para>

<screen><userinput remap="pre">sed -e '1012 s/stderr=devnull/stderr=subprocess.DEVNULL/' \
    -e '1013 s/OSError/(OSError, subprocess.CalledProcessError)/' \
    -i third_party/python/distro/distro.py</userinput></screen>

    <para>
      Compile <application>Seamonkey</application> executando os seguintes
comandos:
    </para>

<screen><userinput>export PATH_PY311=/opt/python3.11/bin:$PATH &amp;&amp;
PATH=$PATH_PY311 AUTOCONF=true MACH_USE_SYSTEM_PYTHON=1 ./mach build</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Instale <application>Seamonkey</application> emitindo os seguintes comandos
como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

    <note>
      <para>
        Se <command>sudo</command> ou <command>su</command> for invocado para abrir
um shell executando como o(a) usuário(a) &root;, certifique-se de que
<envar>PATH_PY311</envar> foi passado corretamente ou o comando a seguir
falhará. Para <command>sudo</command>, use a opção
<option>--preserve-env=PATH_PY311</option>. Para <command>su</command>,
<emphasis>não</emphasis> use as opções <option>-</option> ou
<option>--login</option>.
      </para>
    </note>

<screen role="root"><userinput>PATH=$PATH_PY311 MACH_USE_SYSTEM_PYTHON=1 ./mach install &amp;&amp;
chown -R 0:0 /usr/lib/seamonkey                          &amp;&amp;

cp -v $(find -name seamonkey.1 | head -n1) /usr/share/man/man1</userinput></screen>

    <para>
      Finalmente, desconfigure a variável <envar>PATH_PY311</envar>:
    </para>

<screen><userinput>unset PATH_PY311</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command>export CC=clang CXX=clang++</command>: Com a introdução do
<application>gcc-12</application>, muitos mais avisos são gerados ao
compilar-se aplicativos Mozilla e isso resulta em uma construção muito mais
lenta e maior. Além disso, construir com GCC no i?86 atualmente está
quebrado. Embora o código de fluxo de desenvolvimento da Mozilla tenha como
padrão usar o <application>llvm</application>, a menos que substituído, o
código de configuração mais antigo no <application>Seamonkey</application>
tem como padrão o gcc.
    </para>



    <!--
    <para>

      <command>AUTOCONF=true ./mach configure</command>: This validates
      the supplied dependencies and the <filename>mozconfig</filename>.
      The building system always checks for an old version (2.13) of
      autoconf, but it's not really needed because the necessary
      <command>configure</command> scripts are already shipped in the
      tarball and we've not modified the <filename>configure.in</filename>
      files.  So we pass <envar>AUTOCONF=true</envar> so the building system
      will skip the checking for autoconf-2.13 and allow building this
      package without autoconf-2.13 installed.
    </para>
-->
<para>
      <option>./mach build --verbose</option>: Use essa alternativa se você
precisar de detalhes de quais arquivos estão sendo compilados, juntamente
com quaisquer sinalizadores C ou C++ sendo usados. Mas não adicione
'--verbose' ao comando de instalação; não é aceito lá.
    </para>

    <para>
      <option>./mach build -jN</option>: A construção deveria, por padrão, usar
todos os Elementos de Processamento de CPU online. Se usar todos os
Elementos de Processamento fizer com que a construção use a área de troca
porque você tem memória insuficiente, usar menos Elementos de Processamento
poderá ser mais rápido.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando Seamonkey</title>

    <para>
      Para instalar vários complementos do <application>Seamonkey</application>,
consulte <ulink
url="https://addons.thunderbird.net/en-US/seamonkey/">Complementos para
Seamonkey</ulink>.
    </para>

    <para>
      Juntamente com usar o menu <quote>Preferências</quote> para configurar
opções e preferências do <application>Seamonkey</application> para atender
aos gostos individuais, um controle mais refinado de muitas opções somente
está disponível usando-se uma ferramenta não disponível a partir do sistema
geral de menus. Para acessar essa ferramenta, você precisará abrir uma
janela do navegador e digitar <systemitem
role="url">about:config</systemitem> na barra de endereço. Isso exibirá uma
lista das preferências de configuração e informações relacionadas a cada
uma. Você pode usar a barra <quote>Pesquisar:</quote> para inserir critérios
de pesquisa e restringir os itens listados. Mudar uma preferência pode ser
feito usando-se dois métodos. Primeiro, se a preferência tiver um valor
booleano (Verdadeiro/Falso), simplesmente clique duas vezes na preferência
para alternar o valor; e dois, para outras preferências, simplesmente clique
com o botão direito na linha desejada, escolha <quote>Modificar</quote> no
menu e mude o valor. Criar novos itens de preferência é realizado da mesma
maneira, exceto que escolha <quote>Novo</quote> no menu e forneça os dados
desejados nos campos quando solicitado(a).
    </para>

    <para>
      Se você usar um ambiente de área de trabalho, como
<application>Gnome</application> ou <application>KDE</application>, você
possivelmente deseje criar um arquivo
<filename>seamonkey.desktop</filename>, de forma que
<application>Seamonkey</application> apareça nos menus do painel. Se você
não habilitou <application>Startup-Notification</application> em teu
mozconfig, mude a linha StartupNotify para false. Como o(a) usuário(a)
<systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>mkdir -pv /usr/share/{applications,pixmaps}              &amp;&amp;

cat &gt; /usr/share/applications/seamonkey.desktop &lt;&lt; "EOF"
<literal>[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=Seamonkey
Comment=A Suíte Mozilla
Icon=seamonkey
Exec=seamonkey
Categories=Network;GTK;Application;Email;Browser;WebBrowser;News;
StartupNotify=true
Terminal=false</literal>
EOF

ln -sfv /usr/lib/seamonkey/chrome/icons/default/default128.png \
        /usr/share/pixmaps/seamonkey.png</userinput></screen>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>seamonkey</seg>
        <seg>
          Numerosas bibliotecas, navegadores e componentes de mensagens
eletrônicas/grupo de notícias, plug-ins, extensões e módulos auxiliares
instalados em <filename class="directory">/usr/lib/seamonkey</filename>
        </seg>
        <seg>
          /usr/lib/seamonkey
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="seamonkey-prog">
        <term><command>seamonkey</command></term>
        <listitem>
          <para>
            é a suíte de clientes de navegador/mensagens eletrônicas/grupo de
notícias/chat da Mozilla
          </para>
          <indexterm zone="seamonkey seamonkey-prog">
            <primary sortas="b-seamonkey">seamonkey</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>

</sect1>
