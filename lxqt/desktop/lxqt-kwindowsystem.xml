<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY kwindowsystem-download-http "&kf6-download-http;/kwindowsystem-&kf6-version;.tar.xz">
  <!ENTITY kwindowsystem-download-ftp  "">
  <!ENTITY kwindowsystem-md5sum        "0fe4524579013c8e9fcf7adf43ea844e">
  <!ENTITY kwindowsystem-size          "2,3 MB">
  <!ENTITY kwindowsystem-buildsize     "76 MB">
  <!ENTITY kwindowsystem-time          "0,3 UPC (Usando paralelismo=4)">
]>

<sect1 id="lxqt-kwindowsystem" xreflabel="kwindowsystem-&kf6-version; para lxqt">
  <?dbhtml filename="lxqt-kwindowsystem.html"?>


  <title>kwindowsystem-&kf6-version; para lxqt</title>

  <indexterm zone="lxqt-kwindowsystem">
    <primary sortas="a-kwindowsystem">kwindowsystem</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao kwindowsystem</title>

    <para>
      O <application>kwindowsystem</application> fornece informações e permite a
interação com o sistema de janelas. Ele fornece uma API de alto nível que é
independente do sistema de janelas e tem implementações específicas de
plataforma.
    </para>

    &lfs123_checked;

    <important>
      <para>
        Esse pacote é extraído a partir do conjunto de pacotes do KF6. Se o <xref
linkend="kf6-frameworks"/> for construído, <emphasis
role="bold">NÃO</emphasis> construa também esse pacote conforme apresentado
aqui.
      </para>
    </important>

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&kwindowsystem-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&kwindowsystem-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &kwindowsystem-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &kwindowsystem-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &kwindowsystem-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &kwindowsystem-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do kwindowsystem</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="extra-cmake-modules"/>, <xref
linkend="plasma-wayland-protocols"/>, <xref linkend="qt6"/> e <xref
linkend="xorg7-lib"/> 
    </para>


  <!--
    <para condition="html" role="usernotes">

      User Notes: <ulink url="&blfs-wiki;/kwindowsystem"/>
    </para>
-->
</sect2>

  <sect2 role="installation">
    <title>Instalação do kwindowsystem</title>

    <para>
      Instale o <application>kwindowsystem</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr \
      -D CMAKE_BUILD_TYPE=Release  \
      -D BUILD_TESTING=OFF         \
      -W no-dev ..                 &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libKF6WindowSystem.so
        </seg>
        <seg>
          /usr/include/KF6, /usr/lib/cmake/KF6WindowSystem, /usr/lib/plugins/kf6,
/usr/lib/qml/org/kde e /usr/share/qlogging-categories6
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="lxqt-kwindowsystem-lib">
        <term><filename class="libraryfile">libKF6WindowSystem.so</filename></term>
        <listitem>
          <para>
            contém as funções de API do <application>KF6 Windowing</application>
          </para>
          <indexterm zone="lxqt-kwindowsystem-lib">
            <primary sortas="c-libKF6WindowSystem">libKF6WindowSystem.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
