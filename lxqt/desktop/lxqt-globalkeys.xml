<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY lxqt-globalkeys-download-http "https://github.com/lxqt/lxqt-globalkeys/releases/download/&lxqt-version;/lxqt-globalkeys-&lxqt-version;.tar.xz">
  <!ENTITY lxqt-globalkeys-download-ftp  "">
  <!ENTITY lxqt-globalkeys-md5sum        "9a8d73be17f61157fa67f36c06a9967c">
  <!ENTITY lxqt-globalkeys-size          "76 KB">
  <!ENTITY lxqt-globalkeys-buildsize     "10 MB">
  <!ENTITY lxqt-globalkeys-time          "0,6 UPC">
]>

<sect1 id="lxqt-globalkeys" xreflabel="lxqt-globalkeys-&lxqt-version;">
  <?dbhtml filename="lxqt-globalkeys.html"?>


  <title>lxqt-globalkeys-&lxqt-version;</title>

  <indexterm zone="lxqt-globalkeys">
    <primary sortas="a-lxqt-globalkeys">lxqt-globalkeys</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao lxqt-globalkeys</title>

    <para>
      O pacote <application>lxqt-globalkeys</application> contém um processo de
segundo plano usado para registrar atalhos globais de teclado, bem como um
editor para atalhos de teclado.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&lxqt-globalkeys-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&lxqt-globalkeys-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &lxqt-globalkeys-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &lxqt-globalkeys-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &lxqt-globalkeys-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &lxqt-globalkeys-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do lxqt-globalkeys</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="liblxqt"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do lxqt-globalkeys</title>

    <para>
      Instale o <application>lxqt-globalkeys</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr \
      -D CMAKE_BUILD_TYPE=Release  \
      ..                           &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          lxqt-config-globalkeyshortcuts e lxqt-globalkeysd
        </seg>
        <seg>
          liblxqt-globalkeys.so e liblxqt-globalkeys-ui.so
        </seg>
        <seg>
          /usr/include/lxqt-globalkeys{,-ui} e /usr/share/cmake/lxqt-globalkeys{,-ui}
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="lxqt-config-globalkeyshortcuts">
        <term><command>lxqt-config-globalkeyshortcuts</command></term>
        <listitem>
          <para>
            é um editor para atalhos de teclado
          </para>
          <indexterm zone="lxqt-globalkeys lxqt-config-globalkeyshortcuts">
            <primary sortas="b-lxqt-config-globalkeyshortcuts">lxqt-config-globalkeyshortcuts</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="lxqt-globalkeysd">
        <term><command>lxqt-globalkeysd</command></term>
        <listitem>
          <para>
            é o processo global de segundo plano de atalhos de teclado
          </para>
          <indexterm zone="lxqt-globalkeys lxqt-globalkeysd">
            <primary sortas="b-lxqt-globalkeysd">lxqt-globalkeysd</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="lxqt-globalkeys-lib">
        <term><filename class="libraryfile">lxqt-globalkeys.so</filename></term>
        <listitem>
          <para>
            contém funções para registrar atalhos de teclado
          </para>
          <indexterm zone="lxqt-globalkeys lxqt-globalkeys-lib">
            <primary sortas="c-lxqt-globalkeys">lxqt-globalkeys.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="liblxqt-globalkeys-ui-lib">
        <term><filename class="libraryfile">liblxqt-globalkeys-ui.so</filename></term>
        <listitem>
          <para>
            contém funções que permitem que uma IU configure atalhos de teclado
          </para>
          <indexterm zone="lxqt-globalkeys liblxqt-globalkeys-ui-lib">
            <primary sortas="c-liblxqt-globalkeys-ui">liblxqt-globalkeys-ui.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
