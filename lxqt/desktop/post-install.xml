<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;
]>

<sect1 id="lxqt-post-install" xreflabel="Pós-instalação do LXQt">
  <?dbhtml filename="post-install.html"?>

  <title>Instruções finais da área de trabalho do LXQt</title>

  <indexterm zone="lxqt-post-install">
    <primary sortas="g-lxqt-post-install">LXQt-post-install</primary>
  </indexterm>

  <sect2 role="package">
  <title>Instruções pós-instalação</title>

    <para>
      Por favor, siga estas instruções antes de iniciar o
<application>LXQt</application> pela primeira vez.
    </para>

    <bridgehead renderas="sect3">Dependências para iniciar o LXQt</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="openbox"/>, ou outro gerenciador de janelas, como o <xref
role="nodep" linkend="xfwm4"/>, ou o kwin originário do <xref role="nodep"
linkend="plasma-build"/>. Observe que o <xref role="nodep" linkend="icewm"/>
não é adequado para o LXQt.
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="breeze-icons"/> e <xref linkend="desktop-file-utils"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="lightdm"/> ou outro Gerenciador de Telas, por exemplo, <xref
role="nodep" linkend="sddm"/>, ou <xref linkend="xscreensaver"/>
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configuração final</title>

    <sect3 id="lxqt-final-updates" xreflabel="Atualizações finais da base de dados do LXQt">
      <title>Atualizações finais da base de dados do LXQt</title>

      <para>
        As bases de dados de área de trabalho precisam ser criadas ou atualizadas
neste ponto. Execute os seguintes comandos como o(a) usuário(a) &root;:
      </para>

<screen role="root"><userinput>ldconfig                             &amp;&amp;
update-mime-database /usr/share/mime &amp;&amp;
xdg-icon-resource forceupdate        &amp;&amp;
update-desktop-database -q</userinput></screen>

    </sect3>

  </sect2>

  <sect2 role="starting">
    <title>Iniciando o LXQt</title>

    <para revision="sysv">
      Você consegue iniciar o <application>LXQt</application> a partir do nível de
execução 3, usando o <xref linkend="xinit"/>, ou a partir do nível de
execução 5, usando um Gerenciador de Telas, como o <xref
linkend="lightdm"/>.
    </para>

    <para revision="systemd">
      Você consegue iniciar o <application>LXQt</application> a partir de um TTY,
usando o <xref linkend="xinit"/> ou usando um gerenciador gráfico de telas,
como o <xref linkend="lightdm"/>.
    </para>

    <para>
      Para iniciar o <application>LXQt</application> usando o <xref
linkend="xinit"/>, execute os seguintes comandos:
    </para>

<screen role="nodump"><userinput>cat &gt; ~/.xinitrc &lt;&lt; "EOF"
<literal>exec startlxqt</literal>
EOF

startx</userinput></screen>

    <para>
      A sessão do X inicia no primeiro terminal virtual não usado, normalmente
vt1. Você pode comutar para outro vt<emphasis>n</emphasis> pressionando
simultaneamente as teclas Ctrl-Alt-F<emphasis>n</emphasis>
(<emphasis>n</emphasis>=2, 3, ...) . Para comutar de volta para a sessão do
X, normalmente iniciada em vt1, use Ctrl-Alt-F1. O vt onde o comando
<command>startx</command> foi executado exibirá muitas mensagens, incluindo
mensagens de iniciação do X, aplicativos iniciados automaticamente com a
sessão e, eventualmente, algumas mensagens de aviso e erro, mas essas são
ocultadas pela interface gráfica. Você possivelmente prefira redirecionar
essas mensagens para um arquivo de registro, que pode ser usado para fins de
depuração. Isso pode ser feito iniciando o X com:
    </para>

    <screen role="nodump"><userinput>startx &amp;&gt; ~/.x-session-errors</userinput></screen>

  </sect2>

  <sect2>
    <title>Configuração inicial</title>

    <para>
      Quando o LXQt inicia pela primeira vez, ele te solicitará o gerenciador de
janelas para usar. Para começar, os(as) editores(as) do BLFS recomendam usar
o <application>openbox</application>. Neste ponto, tanto o plano de fundo
quanto o painel estarão pretos. Clicar com o botão direito no plano de fundo
abrirá um menu e selecionar "Preferências da área de trabalho" te permitirá
mudar a cor do plano de fundo ou configurar uma imagem de plano de fundo.
    </para>

    <para>
      O painel estará na parte inferior da tela. Clicar com o botão direito no
painel abrirá um menu que te permitirá personalizar o painel, incluindo
adicionar pequenas engenhocas e configurar a cor do plano de fundo. Os(As)
editores(as) do BLFS recomendam instalar, no mínimo, as pequenas engenhocas
de Gerenciador de Aplicativos e Gerenciador de Tarefas.
    </para>

    <para>
      Depois que o LXQt for iniciado pela primeira vez, os(as) editores(as) do
BLFS recomendam passar pelas configurações apresentadas no Centro de
Configuração do LXQt, o qual podem ser encontrado sob Configurações do LXQt
no menu Preferências do inicializador de aplicativos.
    </para>

    <note>
      <para>
         Os arquivos de configuração de usuário(a) serão criados no diretório
$HOME/.config/lxqt/. Para conseguir que os ícones das pequenas engenhocas
exibam corretamente, o arquivo lxqt.conf possivelmente precise ser editado
manualmente para incluir a linha "icon_theme=oxygen".
      </para>
    </note>

  </sect2>
</sect1>
