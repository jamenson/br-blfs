<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libkscreen-download-http "https://download.kde.org/stable/plasma/&plasma-version;/libkscreen-&plasma-version;.tar.xz">
  <!ENTITY libkscreen-download-ftp  "">
  <!ENTITY libkscreen-md5sum        "1a23a3c130d5a73e851556638e05c86a">
  <!ENTITY libkscreen-size          "117 KB">
  <!ENTITY libkscreen-buildsize     "20 MB">
  <!ENTITY libkscreen-time          "0,4 UPC (usando paralelismo = 4)">
]>

<sect1 id="lxqt-libkscreen" xreflabel="libkscreen-&plasma-version; para lxqt">
  <?dbhtml filename="lxqt-libkscreen.html"?>


  <title>libkscreen-&plasma-version; para lxqt</title>

  <indexterm zone="lxqt-libkscreen">
    <primary sortas="a-libkscreen">libkscreen</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libkscreen</title>

    <para>
      O pacote <application>libkscreen</application> contém a biblioteca de
gerenciamento de telas do KDE.
    </para>

    &lfs123_checked;

    <important>
      <para>
        Esse pacote é extraído a partir do conjunto de pacotes do plasma. Se o <xref
linkend="plasma-build"/> for construído, <emphasis
role="bold">NÃO</emphasis> construa também esse pacote conforme apresentado
aqui.
      </para>
    </important>

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libkscreen-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libkscreen-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libkscreen-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libkscreen-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libkscreen-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libkscreen-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libkscreen</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="lxqt-kwayland"/>, <xref linkend="plasma-wayland-protocols"/>
e <xref linkend="qt6"/> 
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do libkscreen</title>

    <para>
      Instale o <application>libkscreen</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr        \
      -D CMAKE_BUILD_TYPE=Release         \
      -D CMAKE_INSTALL_LIBEXECDIR=libexec \
      -D KDE_INSTALL_USE_QT_SYS_PATHS=ON  \
      -D BUILD_TESTING=OFF                \
      -W no-dev ..                        &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

    <para revision="sysv">
      Em seguida, remova uma unidade do systemd que não serve para nada em um
sistema SysV, como o(a) usuário(a) &root;:
    </para>

<screen role="root" revision="sysv"><userinput>rm -v /usr/lib/systemd/user/plasma-kscreen.service</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          kscreen-doctor
        </seg>
        <seg>
          libKF6Screen.so e libKF6ScreenDpms.so
        </seg>
        <seg>
          /usr/lib/cmake/KF6Screen, /usr/include/KF6/KScreen e
$QT6DIR/lib/plugins/kf6/kscreen
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="lxqt-libkscreen-kscreen-doctor">
        <term><command>kscreen-doctor</command></term>
        <listitem>
          <para>
            permite modificar a configuração da tela a partir da linha de comando
          </para>
          <indexterm zone="lxqt-libkscreen lxqt-libkscreen-kscreen-doctor">
            <primary sortas="b-kscreen-doctor">kscreen-doctor</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="lxqt-libkscreen-lib">
        <term><filename class="libraryfile">libKF6Screen.so</filename></term>
        <listitem>
          <para>
            contém a biblioteca de gerenciamento de telas do KDE
          </para>
          <indexterm zone="lxqt-libkscreen-lib">
            <primary sortas="c-libKF6Screen">libKF6Screen.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

        <varlistentry id="lxqt-libkscreen-dpms-lib">
        <term><filename class="libraryfile">libKF6ScreenDpms.so</filename></term>
        <listitem>
          <para>
            contém funções de API para lidar com DPMS
          </para>
          <indexterm zone="lxqt-libkscreen-dpms-lib">
            <primary sortas="c-libKF6ScreenDpms">libKF6ScreenDpms.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>

  </sect2>

</sect1>
