<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libfm-qt-download-http "https://github.com/lxqt/libfm-qt/releases/download/&libfm-qt-version;/libfm-qt-&libfm-qt-version;.tar.xz">
  <!ENTITY libfm-qt-download-ftp  "">
  <!ENTITY libfm-qt-md5sum        "3ba7dc9e68e222e0180c1a86ee95e92c">
  <!ENTITY libfm-qt-size          "412 KB">
  <!ENTITY libfm-qt-buildsize     "27 MB">
  <!ENTITY libfm-qt-time          "0,4 UPC (Usando paralelismo=4)">
]>

<sect1 id="libfm-qt" xreflabel="libfm-qt-&libfm-qt-version;">
  <?dbhtml filename="libfm-qt.html"?>


  <title>libfm-qt-&libfm-qt-version;</title>

  <indexterm zone="libfm-qt">
    <primary sortas="a-libfm-qt">libfm-qt</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libfm-qt</title>

    <para>
      <application>libfm-qt</application> é a porta Qt da libfm, uma biblioteca
que fornece componentes para construir gerenciadores de arquivos da área de
trabalho. No LXQt, libfm-qt também lida com ícones e planos de fundo da área
de trabalho.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libfm-qt-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libfm-qt-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libfm-qt-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libfm-qt-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libfm-qt-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libfm-qt-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libfm-qt</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="cmake"/>, <xref linkend="libexif"/>, <xref
linkend="menu-cache"/> e <xref linkend="qt6"/>
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do libfm-qt</title>

    <para>
      Instale o <application>libfm-qt</application> executando os seguintes
comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

cmake -D CMAKE_INSTALL_PREFIX=/usr \
      -D CMAKE_BUILD_TYPE=Release  \
      ..                           &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libfm-qt6.so
        </seg>
        <seg>
          /usr/include/libfm-qt6, /usr/share/cmake/fm-qt6 e /usr/share/libfm-qt6
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="libfm-qt-lib">
        <term><filename class="libraryfile">libfm-qt6.so</filename></term>
        <listitem>
          <para>
            contém funções para implementar um gerenciador gráfico de arquivos, bem como
configurar ícones e planos de fundo da área de trabalho
          </para>
          <indexterm zone="libfm-qt libfm-qt-lib">
            <primary sortas="c-libfm-qt">libfm-qt6.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
