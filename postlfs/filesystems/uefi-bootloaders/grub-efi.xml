<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../../general.ent">
  %general-entities;

  <!ENTITY grub-efi-download-http "https://ftp.gnu.org/gnu/grub/grub-&grub-version;.tar.xz">
  <!ENTITY grub-efi-download-ftp  "">
  <!ENTITY grub-efi-md5sum        "60c564b1bdc39d8e43b3aab4bc0fb140">
  <!ENTITY grub-efi-size          "6,4 MB">
  <!ENTITY grub-efi-buildsize     "183 MB">
  <!ENTITY grub-efi-time          "0,4 UPC (no LFS de 64 bits, usando paralelismo=4)">

  <!ENTITY unifont-download-http  "https://unifoundry.com/pub/unifont/unifont-&unifont-version;/font-builds/unifont-&unifont-version;.pcf.gz">
  <!ENTITY unifont-md5sum         "007ffa7aab47ed3f270caee84d12148b">
  <!ENTITY unifont-size           "1,3 MB">
]>

<sect1 id="grub-efi" xreflabel="GRUB-&grub-version; para EFI">
  <?dbhtml filename="grub-efi.html"?>


  <title>GRUB-&grub-version; para EFI</title>

  <indexterm zone="grub-efi">
    <primary sortas="a-grub-efi">grub-efi</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao GRUB</title>

    <para>
      O pacote <application>GRUB</application> fornece o GRand Unified
Bootloader. Nesta página, ele será construído com suporte UEFI, que não está
habilitado para o GRUB construído no LFS.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&grub-efi-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&grub-efi-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &grub-efi-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &grub-efi-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &grub-efi-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &grub-efi-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>
    
    <!--
    <itemizedlist spacing="compact">

      <listitem>
        <para>
          Required patch:
          <ulink url="&patch-root;/grub-&grub-version;-upstream_fixes-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
    -->
<itemizedlist spacing="compact">
      <title>Dados opcionais de fonte Unicode para o menu do GRUB</title>
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&unifont-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &unifont-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &unifont-size;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do GRUB</bridgehead>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref role="runtime" linkend="efibootmgr"/> (tempo de execução)
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="freetype2"/>, <xref linkend="fuse3"/> e <xref
linkend="lvm2"/>
    </para>

    <para role="nodep">
      Se você estiver construindo o GRUB para um sistema LFS de 32 bits para
inicializá-lo em um firmware UEFI de 64 bits, reconstrua <xref
linkend="gcc"/> com a chave <parameter>--enable-targets=all</parameter>
posposta ao comando <command>../configure</command>.
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do GRUB</title>

    <para>
      Primeiro, instale os dados da fonte como o(a) usuário(a) <systemitem
class="username">root</systemitem> se você os tiver baixado e tiver
instalado a dependência opcional <xref linkend='freetype2'/>:
    </para>

<screen role="root"><userinput>mkdir -pv /usr/share/fonts/unifont &amp;&amp;
gunzip -c ../unifont-&unifont-version;.pcf.gz > /usr/share/fonts/unifont/unifont.pcf</userinput></screen>

    <warning>
      <para>Desconfigure quaisquer variáveis de ambiente que possivelmente afetem a
construção:</para>

      <screen><userinput>unset {C,CPP,CXX,LD}FLAGS</userinput></screen>

      <para>Não tente <quote>ajustar</quote> esse pacote com sinalizadores
personalizados de compilação: esse pacote é um carregador de inicialização,
com operações de baixo nível no código-fonte, as quais provavelmente sejam
quebradas por algumas otimizações agressivas.</para>
    </warning>

    <para>
      Adicionar um arquivo ausente proveniente do tarball de lançamento:
    </para>

    <screen><userinput>echo <literal>depends bli part_gpt</literal> > grub-core/extra_deps.lst</userinput></screen>

    <para>
      Construa o <application>GRUB</application> com os seguintes comandos:
    </para>

<screen><userinput>./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --disable-efiemu     \
            --with-platform=efi  \
            --target=x86_64      \
            --disable-werror     &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não tem uma suíte de teste que forneça resultados
significativos.
    </para>

    <para>
      Agora, se você tiver pulado o pacote GRUB do LFS, como o(a) usuário(a)
<systemitem class="username">root</systemitem>, instale o GRUB e pule as
instruções restantes nesta seção:
    </para>

<screen role="nodump"><userinput>make install &amp;&amp;
mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions</userinput></screen>

    <para>
      Se você não tiver ignorado o pacote GRUB do LFS, como o(a) usuário(a)
&root;, instale somente os componentes não instalados a partir do pacote
GRUB do LFS:
    </para>

    <screen role="root"><userinput>make -C grub-core install</userinput></screen>

    <para>
      Se você tiver instalado os dados opcionais de fonte e <xref
linkend='freetype2'/>, instale o programa <command>grub-mkfont</command> e
os arquivos de dados de fonte (sem os arquivos de dados de fonte, o GRUB
ainda pode funcionar normalmente, mas o menu de inicialização será exibido
usando uma fonte grosseira ou em uma região menor na tela).
    </para>

<screen role="nodump"><userinput>install -vm755 grub-mkfont /usr/bin/ &amp;&amp;
install -vm644 ascii.h widthspec.h *.pf2 /usr/share/grub/</userinput></screen>

    <para>
      Se <xref linkend='fuse3'/> e <xref linkend='lvm2'/> estiverem instalados,
instale também o programa <command>grub-mount</command>:
    </para>

<screen role="nodump"><userinput>install -vm755 grub-mount /usr/bin/</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <!--
    <para>

      <parameter>- -enable-grub-mkfont</parameter>: Build the tool named
      <command>grub-mkfont</command> to generate the font file for the boot
      loader from the font data we've installed.
    </para>

    <warning>
      <para>If the recommended dependency <xref linkend="freetype2"/> is not
      installed, it is possible to omit this option and build GRUB.  However,
      if <command>grub-mkfont</command> is not built, or the unicode font
      data is not available at the time GRUB is built, GRUB won't install
      any font for the boot loader.  The GRUB boot menu will be displayed
      using a coarse font or in a smaller region on the screen.</para>
    </warning>
-->
<para>
      <parameter>--with-platform=efi</parameter>: Garante a construção do GRUB com
EFI habilitado.
    </para>

    <para>
      <parameter>--target=x86_64</parameter>: Garante a construção do GRUB para
x86_64, mesmo se construir em um sistema LFS de 32 bits. A maioria do
firmware EFI em x86_64 não suporta carregadores de inicialização de 32 bits.
    </para>

    <para>
      <option>--target=i386</option>: Algumas plataformas x86 de 32 bits tem
suporte EFI. E algumas plataformas x86_64 tem uma implementação EFI de 32
bits, mas são muito antigas e raras. Use isso ao invés de
<parameter>--target=x86_64</parameter> se você estiver <emphasis
role='bold'>absolutamente seguro(a)</emphasis> de que o LFS está executando
em tal sistema.
    </para>

  </sect2>

  <sect2>
    <title>Configurando o GRUB</title>

    <para>
      Usar o GRUB para tornar o sistema LFS inicializável na plataforma UEFI será
discutido em <xref linkend="grub-setup"/>.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <para>
      Uma lista dos arquivos instalados, juntamente com descrições curtas deles,
pode ser encontrada em <ulink
url="&lfs-root;/chapter08/grub.html#contents-gRUB"/>.
    </para>

    <para>
      Listados abaixo estão os programas recém-instalados, juntamente com
descrições curtas.
    </para>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>grub-mkfont e grub-mount (opcional)</seg>
        <seg>/usr/lib/grub/x86_64-efi</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="grub-mkfont">
        <term><command>grub-mkfont</command></term>
        <listitem>
          <para>
            converte formatos de arquivo de fontes comuns em PF2
          </para>
          <indexterm zone="grub-efi grub-mkfont">
            <primary sortas="b-grub-mkfont">grub-mkfont</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="grub-mount">
        <term><command>grub-mount</command></term>
        <listitem>
          <para>
            é uma ferramenta de depuração para controlador de sistema de arquivos
          </para>
          <indexterm zone="grub-efi grub-mount">
            <primary sortas="b-grub-mount">grub-mount</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>

  </sect2>

</sect1>
