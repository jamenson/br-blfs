<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY shadow-download-http "https://github.com/shadow-maint/shadow/releases/download/&shadow-version;/shadow-&shadow-version;.tar.xz">
  <!ENTITY shadow-download-ftp  "">
  <!ENTITY shadow-md5sum        "0da190e53ecee76237e4c8f3f39531ed">
  <!ENTITY shadow-size          "2,3 MB">
  <!ENTITY shadow-buildsize     "103 MB">
  <!ENTITY shadow-time          "0,2 UPC">
]>

<sect1 id="shadow" xreflabel="Shadow-&shadow-version;">
  <?dbhtml filename="shadow.html"?>


  <title>Shadow-&shadow-version;</title>

  <indexterm zone="shadow">
    <primary sortas="a-Shadow">Shadow</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao Shadow</title>

    <para>
      <application>Shadow</application> foi realmente instalado no LFS e não
existe razão para reinstalá-lo, a menos que você instalou
<application>Linux-PAM</application> depois que teu sistema LFS foi
concluído. Com <application>Linux-PAM</application> instalado, reinstalar
<application>Shadow</application> permitirá que programas como
<command>login</command> e <command>su</command> utilizem o PAM.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&shadow-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&shadow-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &shadow-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &shadow-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &shadow-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &shadow-time;
        </para>
      </listitem>
    </itemizedlist>

    <!--
    <bridgehead renderas="sect3">
Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Required patch:
          <ulink url="&patch-root;/shadow-&shadow-version;-useradd_segfault-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do Shadow</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="linux-pam"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <ulink url="https://libbsd.freedesktop.org/wiki/">libbsd</ulink> e <ulink
url="https://www.openwall.com/tcb/">tcb</ulink>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do Shadow</title>

    <important>
      <para>
        Os comandos de instalação mostrados abaixo são para instalações onde o
<application>Linux-PAM</application> tenha sido instalado e o
<application>Shadow</application> esteja sendo reinstalado para suportar a
instalação do <application>Linux-PAM</application>.
      </para>
    </important>

    <!--
    <warning>

      <para>
        If reinstalling shadow for a version update, be sure to 
        reaccomplish the Linux-PAM configuration below.  The installation
        of shadow overwrites many of the files in 
        <filename class="directory">/etc/pam.d/</filename>.
      </para>
    </warning>
-->
<para>
      Reinstale <application>Shadow</application> executando os seguintes
comandos:
    </para>

<screen><!--
<screen>
<userinput>patch -Np1 -i ../shadow-4.10-useradd_segfault-1.patch &amp;&amp;
-->
<!--
This is the default:            - -with-group-name-max-length=32 &amp;&amp;-->
<userinput>sed -i 's/groups$(EXEEXT) //' src/Makefile.in          &amp;&amp;

find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \; &amp;&amp;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \; &amp;&amp;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \; &amp;&amp;

sed -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD YESCRYPT@' \
    -e 's@/var/spool/mail@/var/mail@'                   \
    -e '/PATH=/{s@/sbin:@@;s@/bin:@@}'                  \
    -i etc/login.defs                                   &amp;&amp;

./configure --sysconfdir=/etc   \
            --disable-static    \
            --without-libbsd    \
            --with-{b,yes}crypt &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make exec_prefix=/usr pamddir= install</userinput></screen>

    <para>
      As páginas de manual foram instaladas no LFS, mas se a reinstalação for
desejada, execute (como o(a) usuário(a) <systemitem
class="username">root</systemitem>):
    </para>

<screen role="root"><userinput>make -C man install-man</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command>sed -i 's/groups$(EXEEXT) //' src/Makefile.in</command>: Esse sed é
usado para suprimir a instalação do aplicativo <command>groups</command>,
pois a versão originária do pacote <application>Coreutils</application>
instalado durante o LFS é a preferida.
    </para>

    <para>
      <command>find man -name Makefile.in -exec ... {} \;</command>: O primeiro
comando é usado para suprimir a instalação das páginas de manual do
<command>groups</command>, de modo que as existentes instaladas a partir do
pacote <application>Coreutils</application> não sejam substituídas. Os
outros dois comandos impedem a instalação de páginas de manual que já estão
instaladas pelo <application>Man-pages</application> no LFS.
    </para>

    <para>
      <command>sed -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD YESCRYPT@' -e
's@/var/spool/mail@/var/mail@' -e '/PATH=/{s@/sbin:@@;s@/bin:@@}' -i
etc/login.defs</command>: Em vez de usar o método padrão 'DES ', esse
comando modifica a instalação para usar o método muito mais seguro
'YESCRYPT' de resumo de senhas, que também permite senhas maiores que oito
caracteres. O comando também muda o local obsoleto <filename
class="directory">/var/spool/mail</filename> para caixas de correio de
usuário(a) que <application>Shadow</application> usa por padrão para o local
<filename class="directory">/var/mail</filename>. Ele também muda o caminho
padrão para estar consistente com aquele configurado no LFS.
    </para>

    <para>
      <parameter>--without-libbsd</parameter>: Impede a procura pela função
<command>readpassphrase</command>, que pode ser encontrada somente na
<filename class="libraryfile">libbsd</filename>, a qual nós não temos no
BLFS. Uma implementação interna de <command>readpassphrase</command> é usada
no lugar.
    </para>

    <para>
      <parameter>pamddir=</parameter>: Impede instalação dos arquivos enviados de
configuração do PAM em <filename class='directory'>/etc/pam.d</filename>. A
configuração enviada não funciona com a configuração PAM do BLFS e nós
criaremos esses arquivos de configuração explicitamente.
    </para>



  <!-- This is the default
    <para>

      <parameter>-\-with-group-name-max-length=32</parameter>: The maximum
      user name is 32 characters. Make the maximum group name the same.
    </para>
    -->
<!--
    <para>

      <parameter>-\-without-su</parameter>: Don't reinstall
      <command>su</command> because upstream recommends using the
      <command>su</command> command from <xref linkend='util-linux'/>
      when <application>Linux-PAM</application> is available.
    </para>
-->
</sect2>


  <!-- Now, /etc/default/useradd is not reinstalled anymore, and this
     configuration has been done in lfs
  <sect2 role="configuration">

    <title>Configuring Shadow</title>

    <para>
      <application>Shadow</application>'s stock configuration for the
      <command>useradd</command> utility may not be desirable for your
      installation. One default parameter causes <command>useradd</command> to
      create a mailbox file for any newly created user.
      <command>useradd</command> will make the group ownership of this file to
      the <systemitem class="groupname">mail</systemitem> group with 0660
      permissions. If you would prefer that these mailbox files are not created
      by <command>useradd</command>, issue the following command as the
      <systemitem class="username">root</systemitem> user:
    </para>

<screen role="root"><userinput>sed -i 's/yes/no/' /etc/default/useradd</userinput></screen>
  </sect2>
-->
<sect2 role="configuration">
    <title>Configurando Linux-PAM para Funcionar com Shadow</title>

    <note>
      <para>
        O restante desta página é dedicado para configurar o
<application>Shadow</application> para funcionar corretamente com o
<application>Linux-PAM</application>.
      </para>
    </note>

    <sect3 id="pam.d">
      <title>Arquivos de Configuração</title>

      <para>
        <filename>/etc/pam.d/*</filename> ou, alternativamente,
<filename>/etc/pam.conf</filename>, <filename>/etc/login.defs</filename> e
<filename>/etc/security/*</filename>
      </para>

      <indexterm zone="shadow pam.d">
        <primary sortas="e-etc-pam.d">/etc/pam.d/*</primary>
      </indexterm>

      <indexterm zone="shadow pam.d">
        <primary sortas="e-etc-pam.conf">/etc/pam.conf</primary>
      </indexterm>

      <indexterm zone="shadow pam.d">
        <primary sortas="e-etc-login.defs">/etc/login.defs</primary>
      </indexterm>

      <indexterm zone="shadow pam.d">
        <primary sortas="e-etc-security">/etc/security/*</primary>
      </indexterm>
    </sect3>

    <sect3>
      <title>Informação de Configuração</title>

      <para>
        Configurar seu sistema para usar <application>Linux-PAM</application> pode
ser uma tarefa complexa. A informação abaixo fornecerá uma configuração
básica, de modo que a funcionalidade de login e senha do
<application>Shadow</application> funcione efetivamente com o
<application>Linux-PAM</application>. Revise a informação e os links na
página <xref linkend="linux-pam"/> para informação adicional de
configuração. Para informação específica relativa a integrar o
<application>Shadow</application>, <application>Linux-PAM</application> e o
<application>libpwquality</application>, você pode visitar o seguinte link:
      </para>

      <itemizedlist spacing="compact">
        <listitem>
          
          <!-- Old URL redirects to here. -->
<para>
            <ulink url="https://deer-run.com/users/hal/linux_passwords_pam.html"/>
          </para>
        </listitem>
      </itemizedlist>

      <sect4 id="pam-login-defs">
        <title>Configurando /etc/login.defs</title>

        <para>
          O aplicativo <command>login</command> atualmente realiza muitas funções que
os módulos <application>Linux-PAM</application> agora deveriam manusear. O
seguinte comando <command>sed</command> comentará as linhas apropriadas em
<filename>/etc/login.defs</filename> e impedirá <command>login</command> de
realizar essas funções (um arquivo de cópia de segurança chamado
<filename>/etc/login.defs.orig</filename> também é criado para preservar o
conteúdo do arquivo original). Emita os seguintes comandos como o(a)
usuário(a) <systemitem class="username">root</systemitem>:
        </para>

        <indexterm zone="shadow pam-login-defs">
          <primary sortas="e-etc-login.defs">/etc/login.defs</primary>
        </indexterm>

<screen role="root"><userinput>install -v -m644 /etc/login.defs /etc/login.defs.orig &amp;&amp;
for FUNCTION in FAIL_DELAY               \
                FAILLOG_ENAB             \
                LASTLOG_ENAB             \
                MAIL_CHECK_ENAB          \
                OBSCURE_CHECKS_ENAB      \
                PORTTIME_CHECKS_ENAB     \
                QUOTAS_ENAB              \
                CONSOLE MOTD_FILE        \
                FTMP_FILE NOLOGINS_FILE  \
                ENV_HZ PASS_MIN_LEN      \
                SU_WHEEL_ONLY            \
                PASS_CHANGE_TRIES        \
                PASS_ALWAYS_WARN         \
                CHFN_AUTH ENCRYPT_METHOD \
                ENVIRON_FILE
do
    sed -i "s/^${FUNCTION}/# &amp;/" /etc/login.defs
done</userinput></screen>
      </sect4>

      <sect4>
        <title>Configurando os Arquivos /etc/pam.d/</title>

        <para>
          Conforme mencionado anteriormente nas instruções do
<application>Linux-PAM</application>, o <application>Linux-PAM</application>
tem dois métodos suportados para configuração. Os comandos abaixo assumem
que você tenha escolhido usar uma configuração baseada em diretório, onde
cada aplicativo tem o próprio arquivo de configuração dele. Você pode
opcionalmente usar um arquivo de configuração
<filename>/etc/pam.conf</filename> usando o texto oriundo dos arquivos
abaixo e fornecendo o nome do aplicativo como um primeiro campo adicional
para cada linha.
        </para>

        <para>
          Como o(a) usuário(a) <systemitem class="username">root</systemitem>, crie os
seguintes arquivos de configuração do <application>Linux-PAM</application>
no diretório <filename class="directory">/etc/pam.d/</filename> (ou adicione
o conteúdo ao arquivo <filename>/etc/pam.conf</filename>) usando os
seguintes comandos:
        </para>
      </sect4>

      <sect4>
        <title>'login'</title>

<screen role="root"><userinput>cat &gt; /etc/pam.d/login &lt;&lt; "EOF"
<literal># Início /etc/pam.d/login

# Configura atraso de falha antes do próximo prompt para 3 segundos
auth      optional    pam_faildelay.so  delay=3000000

# Verifica se o(a) usuário(a) tem permissão para se logar
auth      requisite   pam_nologin.so

# Verifica para assegurar que o(a) root tem permissão para se logar.
# Desabilitado por padrão. Você precisará criar o arquivo /etc/securetty
# para que este módulo funcione. Veja-se man 5 securetty.
#auth      required    pam_securetty.so

# Associações adicionais de grupo - desabilitadas por padrão
#auth      optional    pam_group.so

# Inclui configurações de autenticação do sistema
auth      include     system-auth

# Verifica acesso para o(a) usuário(a)
account   required    pam_access.so

# Inclui configurações de conta do sistema
account   include     system-account

# Configura variáveis padrão de ambiente para o(a) usuário(a)
session   required    pam_env.so

# Configura limites de recursos para o(a) usuário(a)
session   required    pam_limits.so

# Exibe a mensagem do dia - Desabilitado por padrão
#session   optional    pam_motd.so

# Verifica mensagens eletrônicas para o(a) usuário(a) - Desabilitado por padrão
#session   optional    pam_mail.so      standard quiet

# inclui configurações da sessão e senha do sistema
session   include     system-session
password  include     system-password

# Fim /etc/pam.d/login</literal>
EOF</userinput></screen>
      </sect4>

      <sect4>
        <title>'passwd'</title>

<screen role="root"><userinput>cat &gt; /etc/pam.d/passwd &lt;&lt; "EOF"
<literal># Início /etc/pam.d/passwd

password  include     system-password

# Fim /etc/pam.d/passwd</literal>
EOF</userinput></screen>
      </sect4>

      <sect4>
        <title>'su'</title>

<screen role="root"><userinput>cat &gt; /etc/pam.d/su &lt;&lt; "EOF"
<literal># Início /etc/pam.d/su

# Sempre permitir root
auth      sufficient  pam_rootok.so

# Permite que usuários(as) do grupo wheel executem su sem uma senha
# Desabilitado por padrão
#auth      sufficient  pam_wheel.so trust use_uid

# Inclui configurações de autenticação do sistema
auth      include     system-auth

# Limita su a usuários(as) no grupo wheel
# Desabilitado por padrão
#auth      required    pam_wheel.so use_uid

# Inclui configurações de conta do sistema
account   include     system-account

# Configura variáveis padrão de ambiente para o(a) usuário(a) do serviço
session   required    pam_env.so

# Inclui configurações de sessão do sistema
session   include     system-session

# Fim /etc/pam.d/su</literal>
EOF</userinput></screen>
      </sect4>

      <sect4>
        <title>'chpasswd' e 'newusers'</title>

<screen role="root"><userinput>cat &gt; /etc/pam.d/chpasswd &lt;&lt; "EOF"
<literal># Início /etc/pam.d/chpasswd

# Sempre permitir root
auth      sufficient  pam_rootok.so

# Inclui configurações do sistema de autenticação e de conta
auth      include     system-auth
account   include     system-account
password  include     system-password

# Fim /etc/pam.d/chpasswd</literal>
EOF

sed -e s/chpasswd/newusers/ /etc/pam.d/chpasswd >/etc/pam.d/newusers</userinput></screen>
      </sect4>

      <sect4>
        <title>'chage'</title>

<screen role="root"><userinput>cat &gt; /etc/pam.d/chage &lt;&lt; "EOF"
<literal># Início /etc/pam.d/chage

# Sempre permitir root
auth      sufficient  pam_rootok.so

# Inclui configurações do sistema de autenticação e de conta
auth      include     system-auth
account   include     system-account

# Fim /etc/pam.d/chage</literal>
EOF</userinput></screen>
      </sect4>

      <sect4>
        <title>Outros utilitários de sombra</title>

<screen role="root"><userinput>for PROGRAM in chfn chgpasswd chsh groupadd groupdel \
              groupmems groupmod useradd userdel usermod
do
    install -v -m644 /etc/pam.d/chage /etc/pam.d/${PROGRAM}
    sed -i "s/chage/$PROGRAM/" /etc/pam.d/${PROGRAM}
done</userinput></screen>

        <warning>
          <para>
            Neste ponto, você deveria fazer um teste simples para ver se o
<application>Shadow</application> está funcionando conforme o esperado. Abra
outro terminal e se logue como <systemitem
class="username">root</systemitem> e, em seguida, execute
<command>login</command> e se logue como outro(a) usuário(a). Se não vir
nenhum erro, então está tudo bem e você deveria prosseguir com o restante da
configuração. Se você recebeu erros, [então] pare agora e verifique
duplamente os arquivos de configuração acima manualmente. Qualquer erro é o
sinal de um erro no procedimento acima. Você também pode executar a suíte de
teste proveniente do pacote <application>Linux-PAM</application> para
ajudá-lo(a) a determinar o problema. Se não conseguir localizar e corrigir o
erro, [então] você deveria recompilar o <application>Shadow</application>
adicionando a chave <option>--without-libpam</option> ao comando
<command>configure</command> nas instruções acima (também mova o arquivo de
cópia de segurança <filename>/etc/login.defs.orig</filename> para
<filename>/etc/login.defs</filename>). Se falhar em fazer isso e os erros
persistirem, [então] você não conseguirá se logar no seu sistema.
          </para>
        </warning>
      </sect4>

      <sect4 id="pam-access">
        <title>Configurando Acesso de Login</title>

        <para>
          Em vez de usar o arquivo <filename>/etc/login.access</filename> para
controlar o acesso ao sistema, o <application>Linux-PAM</application> usa o
módulo <filename class='libraryfile'>pam_access.so</filename> juntamente com
o arquivo <filename>/etc/security/access.conf</filename>. Renomeie o arquivo
<filename>/etc/login.access</filename> usando o seguinte comando:
        </para>

        <indexterm zone="shadow pam-access">
          <primary sortas="e-etc-security-access.conf">/etc/security/access.conf</primary>
        </indexterm>

<screen role="root"><!-- to editors: it is a common belief that:
        if <condition>
; then <command>; fi
     is equivalent to:
        <condition> && <command>
     This is not true in bash; try:
        ([ 0 = 1 ] && echo not reachable); echo $? # echoes 1
     vs
        (if [ 0 = 1 ]; then echo not reachable; fi); echo $? # echoes 0
     So in scripts that may call subshells (for example through sudo) and
     that need error reporting, the outcome _is_ different. In all
     cases, for bash, the "if" form should be preferred.-->
<userinput>if [ -f /etc/login.access ]; then mv -v /etc/login.access{,.SEMUSO}; fi</userinput></screen>
      </sect4>

      <sect4 id="pam-limits">
        <title>Configurando Limites de Recurso</title>

        <para>
          Em vez de usar o arquivo <filename>/etc/limits</filename> para limitar o uso
dos recursos do sistema, o <application>Linux-PAM</application> usa o módulo
<filename class='libraryfile'>pam_limits.so</filename> junto com o arquivo
<filename>/etc/security/limits.conf</filename>. Renomeie o arquivo
<filename>/etc/limits</filename> usando o seguinte comando:
        </para>

        <indexterm zone="shadow pam-limits">
          <primary sortas="e-etc-security-limits.conf">/etc/security/limits.conf</primary>
        </indexterm>

<screen role="root"><userinput>if [ -f /etc/limits ]; then mv -v /etc/limits{,.SEMUSO}; fi</userinput></screen>

        <caution>
          <para>
          Certifique-se de testar os recursos de login do sistema antes de se
deslogar. Erros na configuração podem causar um bloqueio permanente exigindo
uma inicialização a partir de uma fonte externa para corrigir o problema.
          </para>
        </caution>

      </sect4>
    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <para>
      Uma lista dos arquivos instalados, juntamente com as descrições curtas
deles, pode ser encontrada em <ulink
url="&lfs-root;/chapter08/shadow.html#contents-shadow"/>.
    </para>

  </sect2>

</sect1>
