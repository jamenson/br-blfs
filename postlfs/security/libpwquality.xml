<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY libpwquality-download-http "https://github.com/libpwquality/libpwquality/releases/download/libpwquality-&libpwquality-version;/libpwquality-&libpwquality-version;.tar.bz2">
  <!ENTITY libpwquality-download-ftp  "">
  <!ENTITY libpwquality-md5sum        "6b70e355269aef0b9ddb2b9d17936f21">
  <!ENTITY libpwquality-size          "424 KB">
  <!ENTITY libpwquality-buildsize     "5,4 MB">
  <!ENTITY libpwquality-time          "0,1 UPC">
]>

<sect1 id="libpwquality" xreflabel="libpwquality-&libpwquality-version;">
  <?dbhtml filename="libpwquality.html"?>


  <title>libpwquality-&libpwquality-version;</title>

  <indexterm zone="libpwquality">
    <primary sortas="a-libpwquality">biblioteca pwquality</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao libpwquality</title>

    <para>
      O pacote <application>libpwquality</application> fornece funções comuns para
verificação da qualidade da senha e também pontuação com base na aparente
aleatoriedade dela. A biblioteca também fornece uma função para gerar senhas
aleatórias com boa pronúncia.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&libpwquality-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&libpwquality-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &libpwquality-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &libpwquality-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &libpwquality-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &libpwquality-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do libpwquality</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="cracklib"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="linux-pam"/>
    </para>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do libpwquality</title>

    <para>
      Instale <application>libpwquality</application> executando os seguintes
comandos:
    </para>

<screen><userinput>./configure --prefix=/usr                      \
            --disable-static                   \
            --with-securedir=/usr/lib/security \
            --disable-python-bindings          &amp;&amp;
make &amp;&amp;
&build-wheel-cmd; $PWD/python</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install &amp;&amp;
&install-wheel; pwquality</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>--disable-python-bindings</parameter>: Esse parâmetro desabilita
construir ligações Python com o comando obsoleto <command>python3 setup.py
build</command>. A instrução explícita para construir a ligação Python 3 com
o comando <command>pip3 wheel</command> é fornecida.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando libpwquality</title>

    <para>
      <application>libpwquality</application> é destinado a ser um substituto
funcional para o agora obsoleto módulo PAM
<filename>pam_cracklib.so</filename>. Para configurar o sistema para usar o
módulo <filename>pam_pwquality</filename>, execute os seguintes comandos
como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>mv /etc/pam.d/system-password{,.orig} &amp;&amp;
cat &gt; /etc/pam.d/system-password &lt;&lt; "EOF"
<literal># Inicia /etc/pam.d/system-password

# verifique a força das novas senhas (man pam_pwquality)
password  required    pam_pwquality.so   authtok_type=UNIX retry=1 difok=1 \
                                         minlen=8 dcredit=0 ucredit=0 \
                                         lcredit=0 ocredit=0 minclass=1 \
                                         maxrepeat=0 maxsequence=0 \
                                         maxclassrepeat=0 gecoscheck=0 \
                                         dictcheck=1 usercheck=1 \
                                         enforcing=1 badwords="" \
                                         dictpath=/usr/lib/cracklib/pw_dict

# use resumo yescrypt para encriptação; use sombra e tente usar algum
# token de autenticação previamente definido (senha escolhida) configurado
# por algum módulo anterior.
password  required    pam_unix.so        yescrypt shadow try_first_pass

# Termina /etc/pam.d/system-password</literal>
EOF
</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          pwscore e pwmake
        </seg>
        <seg>
          pam_pwquality.so e libpwquality.so
        </seg>
        <seg>
          <!-- /etc/security was installed by Linux-PAM -->
/usr/lib/python3.11/site-packages/pwquality-&libpwquality-version;.dist-info
          
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="pwmake">
        <term><command>pwmake</command></term>
        <listitem>
          <para>
            é uma ferramenta configurável simples para gerar senhas aleatórias e
relativamente fáceis de pronunciar
          </para>
          <indexterm zone="libpwquality pwmake">
            <primary sortas="b-pwmake">pwmake</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="pwscore">
        <term><command>pwscore</command></term>
        <listitem>
          <para>
            é uma ferramenta simples para verificar a qualidade de uma senha
          </para>
          <indexterm zone="libpwquality pwscore">
            <primary sortas="b-pwscore">pwscore</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libpwquality-lib">
        <term><filename class="libraryfile">libpwquality.so</filename></term>
        <listitem>
          <para>
            contém funções de API para verificar a qualidade da senha
          </para>
          <indexterm zone="libpwquality libpwquality-lib">
            <primary sortas="c-libpwquality">libpwquality.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="pam_pwquality">
        <term><filename class="libraryfile">pam_pwquality.so</filename></term>
        <listitem>
          <para>
            é um módulo <application>Linux PAM</application> usado para realizar
verificação da qualidade da senha
          </para>
          <indexterm zone="libpwquality pam_pwquality">
            <primary sortas="c-pam_pwquality">pam_pwquality.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
