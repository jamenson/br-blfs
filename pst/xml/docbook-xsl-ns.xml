<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY path "https://github.com/docbook/xslt10-stylesheets/releases/download/release">

  <!ENTITY docbook-xsl-ns-download-http "&path;/&docbook-xsl-version;/docbook-xsl-&docbook-xsl-version;.tar.bz2">
  <!ENTITY docbook-xsl-ns-download-ftp  "">
  <!ENTITY docbook-xsl-ns-md5sum        "4a400f0264a19329c7f95f69e098744a">
  <!ENTITY docbook-xsl-ns-size          "23 MB">
  <!ENTITY docbook-xsl-ns-buildsize     "49 MB">
  <!ENTITY docbook-xsl-ns-time          "menos que 0,1 UPC">
]>

<sect1 id="docbook-xsl-ns"
xreflabel="docbook-xsl-ns-&docbook-xsl-version;">
  <?dbhtml filename="docbook-xsl-ns.html"?>


  <title>docbook-xsl-ns-&docbook-xsl-version;</title>

  <indexterm zone="docbook-xsl-ns">
    <primary sortas="a-DocBook-XSL-Stylesheets">Folhas de Estilo DocBook XSL (Namespaced)</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao DocBook XSL Stylesheets (Namespaced)</title>

    <para>
      O pacote <application>Folhas de Estilo DocBook XSL
(Namespaced)</application> contém folhas de estilo XSL. Elas são úteis para
realizar transformações sobre arquivos XML DocBook 5.0.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&docbook-xsl-ns-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&docbook-xsl-ns-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &docbook-xsl-ns-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &docbook-xsl-ns-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &docbook-xsl-ns-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &docbook-xsl-ns-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Transferências Adicionais</bridgehead>
    <itemizedlist spacing='compact'>
      <listitem>
        <para>
          Remendo exigido: <ulink url=
"&patch-root;/docbook-xsl-&docbook-xsl-version;-stack_fix-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do DocBook XSL Stylesheets (Namespaced)</bridgehead>

    <bridgehead renderas="sect4">Recomendadas (em tempo de execução)</bridgehead>
    <para role="recommended">
      <xref role="runtime" linkend="libxml2"/>
    </para>

    <bridgehead renderas="sect4">Opcionais (todas usadas em tempo de execução)</bridgehead>
    <para role="optional">
      <xref role="runtime" linkend="apache-ant"/> (para produzir documentos
<quote>webhelp</quote>), <xref role="runtime" linkend="libxslt"/> (ou
qualquer outro processador XSLT), para processar documentos Docbook, <xref
role="runtime" linkend="ruby"/> (para utilizar as folhas de estilo
<quote>epub</quote>), <xref role="runtime" linkend="zip"/> (para produzir
documentos <quote>epub3</quote>), e <ulink
url="https://sourceforge.net/projects/saxon/files/saxon6/"> Saxon6 </ulink>
e <ulink url="http://xerces.apache.org/xerces2-j/">Xerces2 Java</ulink>
(usados com <xref role="runtime" linkend="apache-ant"/> para produzir
documentos <quote>webhelp</quote>)
    </para>


  </sect2>

  <sect2 role="installation">
    <title>Instalação do DocBook XSL Stylesheets (Namespaced)</title>

    <para>
      Primeiro, corrija um problema que causa estouros de pilha ao fazer recursão:
    </para>

<screen><userinput>patch -Np1 -i ../docbook-xsl-&docbook-xsl-version;-stack_fix-1.patch</userinput></screen>

    <para>
      O BLFS não instala os pacotes exigidos para executar a suíte de teste e
fornecer resultados significativos.
    </para>

    <para>
      Instale o <application>DocBook XSL Stylesheets</application> executando os
seguintes comandos como o(a) usuário(a) <systemitem
class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>install -v -m755 -d /usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version; &amp;&amp;

cp -v -R VERSION assembly common eclipse epub epub3 extensions fo        \
         highlighting html htmlhelp images javahelp lib manpages params  \
         profiling roundtrip slides template tests tools webhelp website \
         xhtml xhtml-1_1 xhtml5                                          \
    /usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version; &amp;&amp;

ln -s VERSION /usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;/VERSION.xsl</userinput></screen>

    <para>
      A documentação para esse pacote é compartilhada com a contraparte dele sem
espaço de nomes, o pacote <xref linkend='docbook-xsl'/>. Instale esse último
se você quiser a documentação.
    </para>
  </sect2>

  <sect2 role="configuration">
    <title>Configurando Folhas de Estilo XSL do DocBook</title>

    <sect3 id="docbook-xsl-ns-config">
      <title>Arquivos de Configuração</title>

      <para>
        <filename>/etc/xml/catalog</filename>
      </para>

      <indexterm zone="docbook-xsl docbook-xsl-config">
        <primary sortas="e-etc-xml-catalog">/etc/xml/catalog</primary>
      </indexterm>

    </sect3>

    <sect3>
      <title>Informação de Configuração</title>

      <para>
        Crie (ou posponha) e povoe o arquivo de catálogo XML usando os seguintes
comandos como o(a) usuário(a) <systemitem class="username">root</systemitem>
(ambas as formas <emphasis>http</emphasis> e <emphasis>https</emphasis> são
usadas porque o fluxo de desenvolvimento tem ambas colocadas na documentação
deles(as)):
      </para>

<screen role="root"><userinput>if [ ! -d /etc/xml ]; then install -v -m755 -d /etc/xml; fi &amp;&amp;
if [ ! -f /etc/xml/catalog ]; then
    xmlcatalog --noout --create /etc/xml/catalog
fi &amp;&amp;

xmlcatalog --noout --add "rewriteSystem" \
           "http://cdn.docbook.org/release/xsl/&docbook-xsl-version;" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteSystem" \
           "https://cdn.docbook.org/release/xsl/&docbook-xsl-version;" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteURI" \
           "http://cdn.docbook.org/release/xsl/&docbook-xsl-version;" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteURI" \
           "https://cdn.docbook.org/release/xsl/&docbook-xsl-version;" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteSystem" \
           "http://cdn.docbook.org/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteSystem" \
           "https://cdn.docbook.org/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteURI" \
           "http://cdn.docbook.org/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteURI" \
           "https://cdn.docbook.org/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteSystem" \
           "http://docbook.sourceforge.net/release/xsl-ns/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog &amp;&amp;

xmlcatalog --noout --add "rewriteURI" \
           "http://docbook.sourceforge.net/release/xsl-ns/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;" \
    /etc/xml/catalog</userinput></screen>

    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Arquivos Instalados</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;/*/*.xsl</seg>
        <seg>/usr/share/xml/docbook/xsl-stylesheets-&docbook-xsl-version;</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="xsl-stylesheets-ns">
        <term><filename>Folhas de Estilo XSL (Namespaced)</filename></term>
        <listitem>
          <para>
            são usados para realizar transformações em arquivos XML
          </para>
          <indexterm zone="docbook-xsl-ns xsl-stylesheets-ns">
            <primary sortas="g-XSL-Stylesheets-Namespaced">Folhas de Estilo XSL (Namespaced)</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
