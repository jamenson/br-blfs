<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

<!--Using archive URL, because when a new version is released, older and new
can be found there, no need to change directory -->
  <!ENTITY mupdf-download-http "https://www.mupdf.com/downloads/archive/mupdf-&mupdf-version;-source.tar.gz">
  <!ENTITY mupdf-download-ftp  "">
  <!ENTITY mupdf-md5sum        "e4e46add0485f4cc879ba954b2444fd9">
  <!ENTITY mupdf-size          "52 MB">
  <!ENTITY mupdf-buildsize     "253 MB">
  <!ENTITY mupdf-time          "0,2 UPC (Usando paralelismo=4)">
  <!ENTITY mupdf-lib-major     "25,4">
  <!ENTITY mupdf-lib-minor     "4">
]>

<sect1 id="mupdf" xreflabel="mupdf-&mupdf-version;">
  <?dbhtml filename="mupdf.html"?>


  <title>MuPDF-&mupdf-version;</title>

  <indexterm zone="mupdf">
    <primary sortas="a-mupdf">mupdf</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao MuPDF</title>

    <para>
      <application>MuPDF</application> é um visualizador leve de PDF e XPS.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&mupdf-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&mupdf-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &mupdf-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &mupdf-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &mupdf-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &mupdf-time;
        </para>
      </listitem>
    </itemizedlist>



    <!--
    <bridgehead renderas="sect3">
Required Additional Downloads</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Required patch: <ulink
          url="&patch-root;/mupdf-&mupdf-version;-security_fix-1.patch"/>
        </para>
      </listitem>
    </itemizedlist>
-->
<bridgehead renderas="sect3">Dependências do MuPDF</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <!-- previously recommended via system freeglut -->
<xref linkend="glu"/> e <xref linkend="xorg7-lib"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="harfbuzz"/>, <xref linkend="libjpeg"/>, <xref
linkend="openjpeg2"/> e <xref linkend="curl"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref role="runtime" linkend="xdg-utils"/> (tempo de execução), <ulink
url="https://jbig2dec.com">jbig2dec</ulink> e <ulink
url="https://mujs.com/">MuJS</ulink>
    </para>

    <bridgehead renderas="sect4">Exigidas (tempo de execução)</bridgehead>
    <para role="required">
      <xref role="runtime" linkend="x-window-system"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do MuPDF</title>

    <para>
      Instale o <application>MuPDF</application> executando os seguintes comandos:
    </para>

<screen><userinput>cat &gt; user.make &lt;&lt; EOF &amp;&amp;
USE_SYSTEM_FREETYPE := yes
USE_SYSTEM_HARFBUZZ := yes
USE_SYSTEM_JBIG2DEC := no
USE_SYSTEM_JPEGXR := no # not used without HAVE_JPEGXR
USE_SYSTEM_LCMS2 := no # need lcms2-art fork
USE_SYSTEM_LIBJPEG := yes
USE_SYSTEM_MUJS := no # build needs source anyway
USE_SYSTEM_OPENJPEG := yes
USE_SYSTEM_ZLIB := yes
USE_SYSTEM_GLUT := no # need freeglut2-art fork
USE_SYSTEM_CURL := yes
USE_SYSTEM_GUMBO := no
EOF

export XCFLAGS=-fPIC                      &amp;&amp;
make build=release shared=yes verbose=yes &amp;&amp;
unset XCFLAGS</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &root;:
    </para>

<screen role="root"><userinput>make prefix=/usr                        \
     shared=yes                         \
     docdir=/usr/share/doc/mupdf-&mupdf-version; \
     install                            &amp;&amp;

ln -sfv libmupdf.so.&mupdf-lib-major; /usr/lib/libmupdf.so   &amp;&amp;
ln -sfv libmupdf.so.&mupdf-lib-major; /usr/lib/libmupdf.so.&mupdf-lib-minor; &amp;&amp;
chmod 755 /usr/lib/libmupdf.so.&mupdf-lib-major;             &amp;&amp;

ln -sfv mupdf-x11 /usr/bin/mupdf</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <command>ln -sfv mupdf-x11 /usr/bin/mupdf </command>: Esse link simbólico
escolhe entre <command>mupdf-gl</command> e <command>mupdf-x11</command> ao
executar <command>mupdf</command>.
    </para>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          mupdf (link simbólico), mupdf-gl, mupdf-x11, mupdf-x11-curl, muraster e
mutool
        </seg>
        <seg>
          libmupdf.so
        </seg>
        <seg>
          /usr/include/mupdf, /usr/share/doc/mupdf-&mupdf-version;
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="mupdf-prog">
        <term><command>mupdf</command></term>
        <listitem>
          <para>
            é um aplicativo para visualizar documentos PDF, XPS, EPUB e CBZ e vários
formatos de imagem, como PNG, JPEG, GIFF e TIFF
          </para>
          <indexterm zone="mupdf mupdf-prog">
            <primary sortas="b-mupdf">mupdf</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="mupdf-gl">
        <term><command>mupdf-gl</command></term>
        <listitem>
          <para>
            mesmo que <command>mupdf</command>, usando um renderizador opengl
          </para>
          <indexterm zone="mupdf mupdf-gl">
            <primary sortas="b-mupdf-gl">mupdf-gl</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="mupdf-x11">
        <term><command>mupdf-x11</command></term>
        <listitem>
          <para>
            mesmo que <command>mupdf</command>, usando um renderizador do Janelas X
          </para>
          <indexterm zone="mupdf mupdf-x11">
            <primary sortas="b-mupdf-x11">mupdf-x11</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="muraster">
        <term><command>muraster</command></term>
        <listitem>
          <para>
            é um aplicativo usado para realizar tarefas de rasterização com documentos
PDF
          </para>
          <indexterm zone="mupdf muraster">
            <primary sortas="b-muraster">muraster</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="mutool">
        <term><command>mutool</command></term>
        <listitem>
          <para>
            é um aplicativo para realizar diversas operações em arquivos PDF, como
mesclar e limpar documentos PDF
          </para>
          <indexterm zone="mupdf mutool">
            <primary sortas="b-mutool">mutool</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libmupdf">
        <term><filename class="libraryfile">libmupdf.so</filename></term>
        <listitem>
          <para>
            contém as funções de API do mupdf
          </para>
          <indexterm zone="mupdf libmupdf">
            <primary sortas="c-libmupdf">libmupdf.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
