<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY paps-download-http "https://github.com/dov/paps/releases/download/v&paps-version;/paps-&paps-version;.tar.gz">
  <!ENTITY paps-download-ftp  "">
  <!ENTITY paps-md5sum        "6bd661b8fd224adc3343a91e6521a4f2">
  <!ENTITY paps-size          "220 KB">
  <!ENTITY paps-buildsize     "4,0 MB">
  <!ENTITY paps-time          "menos que 0,1 UPC">
]>

<sect1 id="paps" xreflabel="paps-&paps-version;">
  <?dbhtml filename="paps.html"?>


  <title>paps-&paps-version;</title>

  <indexterm zone="paps">
    <primary sortas="a-paps">paps</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao paps</title>

    <para>
      <application>paps</application> é um conversor de texto para PostScript que
funciona por intermédio do <application>Pango</application>. A entrada dele
é um arquivo de texto codificado em UTF-8 e produz PostScript
vetorizado. Ele pode ser usado para imprimir qualquer conjunto de comandos
sequenciais complexos suportados pelo <application>Pango</application>.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&paps-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&paps-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &paps-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &paps-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &paps-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &paps-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do paps</bridgehead>
    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="fmt"/> e <xref linkend="pango"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="doxygen"/>
    </para>


  </sect2>

  <sect2 role="installation">
    <title>Instalação do paps</title>


    <para>
      Primeiro, corrija um problema com glib-2.81.0 ou posterior:
    </para>

<screen><userinput>sed -i -r 's/g_utf8_(next|offset)/(char*) &amp;/' src/paps.cc</userinput></screen>

    <para>
      Instale o <application>paps</application> executando os seguintes comandos:
    </para>

<screen><userinput>./configure --prefix=/usr    \
            --disable-Werror \
            --mandir=/usr/share/man &amp;&amp;
make</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) <systemitem class="username">root</systemitem>:
    </para>

<screen role="root"><userinput>make install</userinput></screen>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativo Instalado</segtitle>
      <segtitle>Biblioteca Instalada</segtitle>
      <segtitle>Diretório Instalado</segtitle>

      <seglistitem>
        <seg>paps</seg>
        <seg>Nenhum(a)</seg>
        <seg>Nenhum(a)</seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="paps-prog">
        <term><command>paps</command></term>
        <listitem>
          <para>
            é um conversor de texto para PostScript que suporta codificação de
caracteres UTF-8
          </para>
          <indexterm zone="paps paps-prog">
            <primary sortas="b-paps">paps</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
