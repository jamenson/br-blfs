<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY NetworkManager-download-http
"&gnome-download-http;/NetworkManager/&NetworkManager-minor;/NetworkManager-&NetworkManager-version;.tar.xz">
  <!ENTITY NetworkManager-download-ftp  "">
  <!ENTITY NetworkManager-md5sum        "3a95e6ddade18d9a1abb0b86d2b14a36">
  <!ENTITY NetworkManager-size          "5,9 MB">
  <!ENTITY NetworkManager-buildsize     "299 MB (com testes e documentação)">
  <!ENTITY NetworkManager-time          "0,9 UPC (com testes, usando paralelismo=4)">
]>

<sect1 id="NetworkManager" xreflabel="NetworkManager-&NetworkManager-version;">
  <?dbhtml filename="networkmanager.html"?>


  <title>NetworkManager-&NetworkManager-version;</title>

  <indexterm zone="NetworkManager">
    <primary sortas="a-NetworkManager">NetworkManager</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao NetworkManager</title>

    <para>
      &quot;<application>NetworkManager</application>&quot; é um conjunto de
ferramentas cooperativas que tornam a operação interativa de dispositivos
via rede de intercomunicação simples e direta. Quer você use
&quot;WiFi&quot;, com fio, &quot;3G&quot; ou &quot;Bluetooth&quot;, o
&quot;NetworkManager&quot; te permite mudar rapidamente de uma rede de
intercomunicação para outra: depois que uma rede de intercomunicação tiver
sido configurada e conectada uma vez, ela poderá ser detectada e conectada
novamente automaticamente na próxima vez que estiver disponível.
    </para>

    &lfs123_checked;

    <note revision="systemd">
      <para>
        Certifique-se de desabilitar o serviço
&quot;<command>systemd-networkd</command>&quot; ou configurá-lo para não
gerenciar as interfaces que você quiser gerenciar com o
&quot;<application>NetworkManager</application>&quot;.
      </para>
    </note>

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&NetworkManager-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&NetworkManager-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &NetworkManager-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &NetworkManager-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &NetworkManager-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &NetworkManager-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do NetworkManager</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="libndp"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="curl"/>, <xref linkend="dhcpcd"/>, &gobject-introspection;,
<xref linkend="iptables"/>, <xref linkend="libpsl"/>, <xref linkend="newt"/>
(para <command>nmtui</command>), <xref linkend="nss"/>, <xref role='runtime'
linkend="polkit"/> (tempo de execução), <xref linkend="pygobject3"/>,
<phrase revision="sysv"><xref linkend="elogind"/>,</phrase> <phrase
revision="systemd"><xref linkend="systemd"/>,</phrase> <xref
linkend="vala"/> e <xref linkend="wpa_supplicant"/> (tempo de execução,
construído com suporte a D-Bus)
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <!-- <xref linkend="firewalld"/>
 (For whenever firewalld is reintroduced) -->
<xref linkend="bluez"/>, <xref linkend="dbus-python"/> (para a suíte de
teste), <xref linkend="gnutls"/> (pode ser usado em vez de <xref
linkend="nss"/>), <xref linkend="gtk-doc"/>, <xref linkend="jansson"/>,
<xref linkend="ModemManager"/>, <xref linkend="upower"/>, <xref
linkend="valgrind"/>, <ulink
url="https://thekelleys.org.uk/dnsmasq/doc.html">dnsmasq</ulink>, <ulink
url="https://firewalld.org/">firewalld</ulink>, <ulink
url="https://github.com/Distrotech/libaudit">libaudit</ulink>, <ulink
url="https://github.com/jpirko/libteam">libteam</ulink>, <ulink
url="&gnome-download-http;/mobile-broadband-provider-info/">mobile-broadband-provider-info</ulink>,
<ulink url="https://www.samba.org/ftp/ppp/">PPP</ulink> e <ulink
url="https://dianne.skoll.ca/projects/rp-pppoe/">RP-PPPoE</ulink>
    </para>

  </sect2>

  <sect2 role="kernel" id="NetworkManager-kernel">
    <title>Configuração do Núcleo</title>

    <para>
      Se desejar executar os testes, [então] verifique se pelo menos as seguintes
opções estão habilitadas na configuração do núcleo. Essas opções foram
consideradas necessárias, mas podem não ser suficientes. Recompile o núcleo
se necessário:
    </para>

    

    <!-- Ethernet Teaming support is potentially optional, but I didn't
         run the tests again to test that. It was needed to convince one of
         the Linux Platform tests to move farther along because otherwise
         RTNETLINK would respond with an Error 95 - unknown device type.
         This would cause the test to fail early on in the process.

         [pierre, Nov 2022]: I cannot tell whether these options are
         the only ones that are needed. They are the options I had to add
         in order to have some tests pass. But I already had some other
         options set for packet filtering (iptables), that may not be
         available by default and may be necessary. Even with the options
         below still one test (test-route) fails.-->
<xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="networkmanager-test-kernel.xml"/>

    <indexterm zone="NetworkManager NetworkManager-kernel">
      <primary sortas="d-NetworkManager">NetworkManager (teste)</primary>
    </indexterm>
  </sect2>

  <sect2 role="installation">
    <title>Instalação do NetworkManager</title>

    <para>
      Corrija os conjuntos sequenciais de comandos &quot;Python&quot;, de forma
que eles usem &quot;<application>Python 3</application>&quot;:
    </para>

<screen><userinput>grep -rl '^#!.*python$' | xargs sed -i '1s/python/&amp;3/'</userinput></screen>

    <para>
      Instale o <application>NetworkManager</application> executando os seguintes
comandos:
    </para>

<screen revision="sysv"><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..                    \
      --prefix=/usr               \
      --buildtype=release         \
      -D libaudit=no              \
      -D nmtui=true               \
      -D ovs=false                \
      -D ppp=false                \
      -D selinux=false            \
      -D session_tracking=elogind \
      -D modem_manager=false      \
      -D systemdsystemunitdir=no  \
      -D systemd_journal=false    \
      -D qt=false                 &amp;&amp;
ninja</userinput></screen>

<screen revision="systemd"><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup ..                    \
      --prefix=/usr               \
      --buildtype=release         \
      -D libaudit=no              \
      -D nmtui=true               \
      -D ovs=false                \
      -D ppp=false                \
      -D selinux=false            \
      -D qt=false                 \
      -D session_tracking=systemd \
      -D modem_manager=false      &amp;&amp;
ninja</userinput></screen>

    <para>
      Uma sessão gráfica já ativa com endereço de barramento é necessária para
executar os testes. Para testar os resultados, emita &quot;<command>ninja
test</command>&quot;.
    </para>

    <para>
      Uns poucos testes possivelmente falhem, dependendo das opções de núcleo
habilitadas.
    </para>

    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>ninja install &amp;&amp;
mv -v /usr/share/doc/NetworkManager{,-&NetworkManager-version;}</userinput></screen>

    <para>
      Se você não passou a opção <option>-D docs=true</option> para
<command>meson</command>, você consegue instalar as páginas de manual pré
geradas com (como o(a) usuário(a) &root;):
    </para>

<screen role="root"><userinput>for file in $(echo ../man/*.[1578]); do
   section=${file##*.} &amp;&amp;
   install -vdm 755 /usr/share/man/man$section
   install -vm 644 $file /usr/share/man/man$section/
done</userinput></screen>

    <para>
      Se você não tiver usado <option>-D docs=true</option>, a documentação HTML
pré gerada também pode ser instalada com (como o(a) usuário(a) &root;):
    </para>

<screen role="root"
        remap="doc"><userinput>cp -Rv ../docs/{api,libnm} /usr/share/doc/NetworkManager-&NetworkManager-version;</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>


    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <option>-D docs=true</option>: Use essa chave para habilitar construir
páginas de manual e documentação se <xref linkend="gtk-doc"/> estiver
instalado.
    </para>

    <para>
      <parameter>-D nmtui=true</parameter>: Essa chave habilita construir
<command>nmtui</command>.
    </para>

    <para revision="sysv">
      <parameter>-D systemdsystemunitdir=no</parameter> e <parameter>-D
systemd_journal=false</parameter>: systemd não é usado para sistemas de
inicialização SysV, de forma que evite instalar unidades e usar o diário do
systemd.
    </para>

    <para>
      <parameter>-D ovs=false</parameter>: Essa chave desabilita a integração do
Open vSwitch porque ela precisa de <xref linkend='jansson'/>. Remova-a se
você tiver <xref linkend='jansson'/> instalado em teu sistema.
    </para>

    <para>
      <parameter>-D modem_manager=false</parameter>: Essa chave é exigida se o
<application>ModemManager</application> não estiver instalado. Omita essa
chave se você tiver construído <application>ModemManager</application> e
<application>mobile-broadband-provider-info</application>.
    </para>

    <para revision="sysv">
      <parameter>-D session_tracking=elogind</parameter>: Essa chave é usada para
configurar <command>elogind</command> como o aplicativo padrão para
rastreamento de sessão.
    </para>

    <para revision="systemd">
      <parameter>-D session_tracking=systemd</parameter>: Essa chave é usada para
configurar <command>systemd-logind</command> como o aplicativo padrão para
rastreamento de sessão.
    </para>

    <para>
      <parameter>-D ppp=false</parameter>: Essa chave desabilita suporte a
<application>PPP</application> no <application>NetworkManager</application>,
já que os aplicativos necessários para isso não estão instalados. Remova
essa chave se você precisar de suporte a PPP e tiver o
<application>PPP</application> instalado.
    </para>

    <para>
      <parameter>-D libaudit=no</parameter> e <parameter>-D
selinux=false</parameter>: Essa chave desabilita o suporte para libaudit e
SELinux, vez que ele(a) não é usado(a) no BLFS.
    </para>

    <para>
      <parameter>-D qt=false</parameter>: Essa chave desabilita os exemplos do
<application>Qt 5</application>.
    </para>

    <para>
      <option>-D crypto=gnutls</option>: Use essa chave se você tiver GnuTLS
instalado e quiser usá-lo para operações de certificados e chaves no
NetworkManager, em vez de usar o NSS (o padrão).
    </para>

    <para>
      <option>-D crypto=null</option>: Use essa chave se nem NSS nem GnuTLS
estiverem instalados, mas você quiser construir NetworkManager de qualquer
maneira. Essa chave fará com que NetworkManager careça de alguns recursos
(por exemplo, 802.1X).
    </para>

    <para>
      <option>-D suspend_resume=upower</option>: Use essa chave se você tiver
<xref linkend='upower'/> instalado e quiser usá-lo (em vez de &logind;) para
suspender e retomar o suporte.
    </para>
  </sect2>

  <sect2 role="configuration">
    <title>Configurando o NetworkManager</title>

    <sect3 id="NetworkManager-config">
      <title>Arquivos de Configuração</title>
      <para>
        <filename>/etc/NetworkManager/NetworkManager.conf</filename>
      </para>

      <indexterm zone="NetworkManager NetworkManager-config">
        <primary
        sortas="e-etc-NetworkManager-NetworkManager.conf">
        /etc/NetworkManager/NetworkManager.conf</primary>
      </indexterm>

    </sect3>

    <sect3><title>Informação de Configuração</title>

      <para>
        Para o &quot;<application>NetworkManager</application>&quot; funcionar, pelo
menos um arquivo de configuração mínima precisa estar presente. Tal arquivo
não é instalado com &quot;<command>make install</command>&quot;. Emita o
seguinte comando como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot; para criar um arquivo
&quot;<filename>NetworkManager.conf</filename>&quot; mínimo:
      </para>

<screen role="root"><userinput>cat &gt;&gt; /etc/NetworkManager/NetworkManager.conf &lt;&lt; "EOF"
<literal>[main]
plugins=keyfile</literal>
EOF</userinput></screen>

      <para>
        Esse arquivo não deveria ser modificado diretamente pelos(as) usuários(as)
do sistema. Em vez disso, mudanças específicas do sistema deveriam ser
feitas usando arquivos de configuração no diretório &quot;<filename
class="directory">/etc/NetworkManager/conf.d</filename>&quot;.
      </para>

      <para>
        Para permitir que o &quot;Polkit&quot; gerencie autorizações, adicione o
seguinte arquivo de configuração:
      </para>

<screen role="root"><userinput>cat &gt; /etc/NetworkManager/conf.d/polkit.conf &lt;&lt; "EOF"
<literal>[main]
auth-polkit=true</literal>
EOF</userinput></screen>

      <para>
        Para usar algo diferente do cliente DHCP integrado (recomendado se usar
somente o <command>nmcli</command>), use a seguinte configuração (os valores
válidos incluem ou dhcpcd ou internal):
      </para>

<screen role="nodump"><userinput>cat &gt; /etc/NetworkManager/conf.d/dhcp.conf &lt;&lt; "EOF"
<literal>[main]
dhcp=</literal><replaceable>dhcpcd</replaceable>
EOF</userinput></screen>

      <para>
        Para evitar que o &quot;<application>NetworkManager</application>&quot;
atualize o arquivo &quot;<filename>/etc/resolv.conf</filename>&quot;,
adicione o seguinte arquivo de configuração:
      </para>

<screen role="nodump"><userinput>cat &gt; /etc/NetworkManager/conf.d/no-dns-update.conf &lt;&lt; "EOF"
<literal>[main]
dns=none</literal>
EOF</userinput></screen>

      <para>
        Para opções adicionais de configuração, veja-se &quot;<command>man 5
NetworkManager.conf</command>&quot;.
      </para>

      <para>
        Para permitir que usuários(as) regulares configurem conexões de rede de
intercomunicação, você deveria adicioná-los(as) ao grupo &quot;<systemitem
class="groupname">netdev</systemitem>&quot; e criar uma regra do
&quot;<application>polkit</application>&quot; que conceda acesso. Execute os
seguintes comandos como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
      </para>

<screen role="root"><userinput>groupadd -fg 86 netdev &amp;&amp;
/usr/sbin/usermod -a -G netdev <replaceable>&lt;nome_usuário(a)&gt;</replaceable>

cat &gt; /usr/share/polkit-1/rules.d/org.freedesktop.NetworkManager.rules &lt;&lt; "EOF"
<literal>polkit.addRule(function(action, subject) {
    if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 &amp;&amp; subject.isInGroup("netdev")) {
        return polkit.Result.YES;
    }
});</literal>
EOF</userinput></screen>

    </sect3>

    <sect3 id="NetworkManager-init">
      <title><phrase revision="sysv">Conjunto de Comandos Sequenciais de
Inicialização</phrase> <phrase revision="systemd">Unidade do
systemd</phrase></title>

      <para revision="sysv">
        Para iniciar automaticamente o processo de segundo plano
&quot;<command>NetworkManager</command>&quot; quando o sistema for
reinicializado, instale o conjunto sequencial de comandos de inicialização
&quot;<filename>/etc/rc.d/init.d/networkmanager</filename>&quot; a partir do
pacote &quot;<xref linkend="bootscripts"/>&quot;.
      </para>

      <para revision="systemd">
        Para iniciar o processo de segundo plano <command>NetworkManager</command>
na inicialização, habilite a unidade do systemd instalada anteriormente
executando o seguinte comando como o(a) usuário(a) <systemitem
class="username">root</systemitem>:
      </para>

      <note>
        <para>
          Se usar o &quot;<application>Network Manager</application>&quot; para
gerenciar uma interface, [então] qualquer configuração anterior para essa
interface deveria ser removida e a interface ser desativada antes de iniciar
o &quot;<application>Network Manager</application>&quot;.
        </para>
      </note>

      <!-- The below instruction is obsolete. NetworkManager-wait-online is now
     enabled by default when enabling NetworkManager. -->
<!--
      <para revision="systemd">

        <application>NetworkManager</application> also ships a systemd unit
        called <filename>NetworkManager-wait-online.service</filename> which
        can be used to prevent services that require network connectivity
        from starting until <application>NetworkManager</application> has
        established the connection. To enable it, run the following command
        as the <systemitem class="username">root</systemitem> user:
      </para>

<screen role="root" revision="systemd"><userinput>systemctl enable NetworkManager-wait-online</userinput></screen>
   -->
<!-- As such, let's now provide instructions on how to disable that
           behavior, for those who wish to do so. -->
<indexterm zone="NetworkManager NetworkManager-init">
        <primary sortas="f-NetworkManager">NetworkManager</primary>
      </indexterm>

<screen role="root" revision="sysv"><userinput>make install-networkmanager</userinput></screen>

<screen role="root" revision="systemd"><userinput>systemctl enable NetworkManager</userinput></screen>


      
      <para revision="systemd">
        Começando na versão 1.11.2 do
&quot;<application>NetworkManager</application>&quot;, uma unidade do
&quot;systemd&quot; chamada
&quot;<filename>NetworkManager-wait-online.service</filename>&quot; está
habilitada, a qual é usada para evitar que serviços que exigem conectividade
de rede de intercomunicação iniciem até que o
&quot;<application>NetworkManager</application>&quot; estabeleça uma
conexão. Para desabilitar esse comportamento, execute o seguinte comando
como o(a) usuário(a)&quot; <systemitem
class="username">root</systemitem>&quot;:
      </para>

<screen role="root" revision="systemd"><userinput>systemctl disable NetworkManager-wait-online</userinput></screen>

    </sect3>

  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          NetworkManager, nmcli, nm-online, nmtui e simbolicamente vinculado ao nmtui:
nmtui-connect, nmtui-edit e nmtui-hostname
        </seg>
        <seg>
          libnm.so e vários módulos sob /usr/lib/NetworkManager
        </seg>
        <seg>
          /etc/NetworkManager, /usr/include/libnm, /usr/lib/NetworkManager,
/usr/share/doc/NetworkManager-&NetworkManager-version;,
/usr/share/gtk-doc/html/{libnm,NetworkManager} (se a documentação for
construída) e /var/lib/NetworkManager
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="nmcli">
        <term><command>nmcli</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para controlar o
&quot;<application>NetworkManager</application>&quot; e obter a situação
dele
          </para>
          <indexterm zone="NetworkManager nmcli">
            <primary sortas="b-nmcli">nmcli</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="nm-online">
        <term><command>nm-online</command></term>
        <listitem>
          <para>
            é um utilitário para determinar se você está online
          </para>
          <indexterm zone="NetworkManager nm-online">
            <primary sortas="b-nm-online">nm-online</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="nmtui">
        <term><command>nmtui</command></term>
        <listitem>
          <para>
            é uma interface interativa de usuário(a) baseada em &quot;Ncurses&quot; para
o &quot;<application>nmcli</application>&quot;
          </para>
          <indexterm zone="NetworkManager nmtui">
            <primary sortas="b-nmtui">nmtui</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="nmtui-connect">
        <term><command>nmtui-connect</command></term>
        <listitem>
          <para>
            é uma interface interativa de usuário(a) baseada em &quot;Ncurses&quot; para
ativar/desativar conexões
          </para>
          <indexterm zone="NetworkManager nmtui-connect">
            <primary sortas="b-nmtui-connect">nmtui-connect</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="nmtui-edit">
        <term><command>nmtui-edit</command></term>
        <listitem>
          <para>
            é uma interface interativa de usuário(a) baseada em &quot;Ncurses&quot; para
editar conexões
          </para>
          <indexterm zone="NetworkManager nmtui-edit">
            <primary sortas="b-nmtui-edit">nmtui-edit</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="nmtui-hostname">
        <term><command>nmtui-hostname</command></term>
        <listitem>
          <para>
            é uma interface interativa de usuário(a) baseada em &quot;Ncurses&quot; para
editar o nome do dispositivo
          </para>
          <indexterm zone="NetworkManager nmtui-hostname">
            <primary sortas="b-nmtui-hostname">nmtui-hostname</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="NetworkManager-prog">
        <term><command>NetworkManager</command></term>
        <listitem>
          <para>
            é o processo de segundo plano de gerenciamento de rede de intercomunicação
          </para>
          <indexterm zone="NetworkManager NetworkManager-prog">
            <primary sortas="b-NetworkManager">NetworkManager</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libnm">
        <term><filename class="libraryfile">libnm.so</filename></term>
        <listitem>
          <para>
            contém funções usadas pelo
&quot;<application>NetworkManager</application>&quot;
          </para>
          <indexterm zone="NetworkManager libnm">
            <primary sortas="c-libnm">libnm.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>
    </variablelist>

  </sect2>

</sect1>
