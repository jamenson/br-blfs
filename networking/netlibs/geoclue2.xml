<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY geoclue2-download-http
  "https://gitlab.freedesktop.org/geoclue/geoclue/-/archive/&geoclue2-version;/geoclue-&geoclue2-version;.tar.bz2">
  <!ENTITY geoclue2-download-ftp  "">
  <!ENTITY geoclue2-md5sum        "d58d6f3286a6b3ace395fc36468aace2">
  <!ENTITY geoclue2-size          "108 KB">
  <!ENTITY geoclue2-buildsize     "7,2 MB">
  <!ENTITY geoclue2-time          "0,1 UPC">
]>

<sect1 id="geoclue2" xreflabel="GeoClue-&geoclue2-version;">
  <?dbhtml filename="geoclue2.html"?>


  <title>GeoClue-&geoclue2-version;</title>

  <indexterm zone="geoclue2">
    <primary sortas="a-geoclue">GeoClue</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introdução ao GeoClue</title>

    <para>
      &quot;<application>GeoClue</application>&quot; é um serviço modular de geo
informação construído sobre o sistema de mensagens
&quot;<application>D-Bus</application>&quot;. O objetivo do projeto
&quot;<application>GeoClue</application>&quot; é o de tornar a criação de
aplicativos com reconhecimento de localização o mais simples possível.
    </para>

    &lfs123_checked;

    <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Transferência (HTTP): <ulink url="&geoclue2-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Transferência (FTP): <ulink url="&geoclue2-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Soma de verificação MD5 da transferência: &geoclue2-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Tamanho da transferência: &geoclue2-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Espaço em disco estimado exigido: &geoclue2-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Tempo de construção estimado: &geoclue2-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Dependências do &quot;GeoClue&quot;</bridgehead>

    <bridgehead renderas="sect4">Exigidas</bridgehead>
    <para role="required">
      <xref linkend="json-glib"/> e <xref linkend="libsoup3"/>
    </para>

    <bridgehead renderas="sect4">Recomendadas</bridgehead>
    <para role="recommended">
      <xref linkend="avahi"/>, <xref linkend="libnotify"/>, <xref
linkend="ModemManager"/> e <xref linkend="vala"/>
    </para>

    <bridgehead renderas="sect4">Opcionais</bridgehead>
    <para role="optional">
      <xref linkend="gtk-doc"/>
    </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do GeoClue</title>

    <para>
      Instale o &quot;<application>GeoClue</application>&quot; executando os
seguintes comandos:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr       \
            --buildtype=release \
            -D gtk-doc=false    \
            ..                  &amp;&amp;
ninja</userinput></screen>

    <para>
      Esse pacote não vem com uma suíte de teste.
    </para>

    <para>
      Agora, como o(a) usuário(a) &quot;<systemitem
class="username">root</systemitem>&quot;:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Explicações do Comando</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <option>-D 3g-source=false</option>: Essa chave desabilita a estrutura de
retaguarda 3G. Use-a se você não tiver instalado o pacote
<application>ModemManager</application>.
    </para>

    <para>
      <option>-D modem-gps-source=false</option>: Essa chave desabilita a
estrutura de retaguarda GPS do modem. Use-a se você não tiver instalado o
pacote <application>ModemManager</application>.
    </para>

    <para>
      <option>-D cdma-source=false</option>: Essa chave desabilita a estrutura de
retaguarda fonte CDMA. Use-a se você não tiver instalado o pacote
<application>ModemManager</application>.
    </para>

    <para>
      <option>-D nmea-source=false</option>: Essa chave desabilita a fonte
NMEA. Use-a se você não tiver instalado o pacote
<application>Avahi</application>.
    </para>

    <para>
      <option>-D demo-agent=false</option>: Essa chave desabilita a
demonstração. Use-a se você não tiver instalado o pacote
<application>libnotify</application>.
    </para>

  </sect2>

  <sect2 role="configuration">
    <title>Configurando GeoClue</title>

    <sect3 id="geoclue2-config">
      <title>Arquivos de Configuração</title>
      <para>
        <filename>/etc/geoclue/conf.d/90-lfs-google.conf</filename>
      </para>

      <indexterm zone="geoclue2 geoclue2-config">
        <primary sortas="etc-geoclue-conf.d-90-lfs-google.conf">/etc/geoclue/conf.d/90-lfs-google.conf</primary>
      </indexterm>
    </sect3>

    <sect3><title>Informação de Configuração</title>

      <para>
        Em março de 2024, a Mozilla anunciou o encerramento do Mozilla Location
Service. Geoclue usa esse serviço para determinar um local de usuário(a)
quando solicitado por outras aplicações. A única alternativa suportada pelo
fluxo de desenvolvimento é a de usar o Serviço de Geolocalização do Google.
      </para>

      <para>
        Para usar o Serviço de Geolocalização do Google, uma chave de API precisa
ser usada e um arquivo de configuração precisa ser criado. <emphasis
role="bold">Esta chave de API é destinada somente para uso com LFS. Por
favor, não use esta chave de API se você estiver construindo para outra
distribuição ou distribuindo cópias binárias. Se precisar de uma chave de
API, você pode solicitar uma em <ulink
url="https://www.chromium.org/developers/how-tos/api-keys"/>.</emphasis>
      </para>

      <para>
        Crie a configuração necessária para usar o Serviço de Geolocalização do
Google como o(a) usuário(a) &root;:
      </para>

<screen role="root"><userinput>cat &gt; /etc/geoclue/conf.d/90-lfs-google.conf &lt;&lt; "EOF"
<literal># Begin /etc/geoclue/conf.d/90-lfs-google.conf

# This configuration applies for the WiFi source.
[wifi]

# Set the URL to Google's Geolocation Service.
url=https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDxKL42zsPjbke5O8_rPVpVrLrJ8aeE9rQ

# End /etc/geoclue/conf.d/90-lfs-google.conf</literal>
EOF</userinput></screen>

      <para>
        Se não desejar solicitar teu local a partir de um serviço de geolocalização,
você pode rigidamente codificar teu local em
<filename>/etc/geolocation</filename> usando o formato descrito em <ulink
role='man' url='&man;geoclue'>geoclue(5)</ulink>.
      </para>
    </sect3>
  </sect2>

  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          Nenhum(a)
        </seg>
        <seg>
          libgeoclue-2.so
        </seg>
        <seg>
          /etc/geoclue, /usr/include/libgeoclue-2.0, /usr/libexec/geoclue-2.0 e
/usr/share/gtk-doc/html/{geoclue,libgeoclue}
        </seg>
      </seglistitem>
    </segmentedlist>

  </sect2>

</sect1>
