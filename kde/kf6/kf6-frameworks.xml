<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
 <!ENTITY % general-entities SYSTEM "../../general.ent">
 %general-entities;

  <!-- kf6-download-http is defined on packages.ent
  <!ENTITY kf6-download-http "&kf6-download-http;"> -->
  <!ENTITY kf6-download-ftp  "">
  <!ENTITY kf6-md5sum        "Veja-se Abaixo">
  <!ENTITY kf6-size          "108 MB">
  <!ENTITY kf6-buildsize     "2,9 GB (188 MB instalado)">
  <!ENTITY kf6-time          "12 UPC (usando paralelismo = 8)">
]>

<sect1 id="kf6-frameworks" xreflabel="KDE Frameworks-&kf6-version;">
  <?dbhtml filename="frameworks6.html"?>

  <title>Construindo KDE Frameworks &kf6-version; (KF6)</title>

  <indexterm zone="kf6-frameworks">
     <primary sortas="a-kde-frameworks">KDE Frameworks</primary>
  </indexterm>

  <para>
    KDE Frameworks é uma coleção de bibliotecas baseadas em Qt6 e QML derivadas
a partir de bibliotecas anteriores do KDE. Elas podem ser usadas
independentemente do ambiente de exibição do KDE (Plasma 6).
  </para>

  &lfs123_checked;

  <para>
    As instruções abaixo constroem todos os pacotes do KDE Frameworks em uma
etapa, usando um conjunto de comandos sequenciais do bash.
  </para>

  <bridgehead renderas="sect3">Informação do Pacote</bridgehead>
  <itemizedlist spacing="compact">
    <listitem>
      <para>
        Transferência (HTTP): <ulink url="&kf6-download-http;"/>
      </para>
    </listitem>
    <listitem>
      <para>
        Transferência (FTP): <ulink url="&kf6-download-ftp;"/>
      </para>
    </listitem>
    <listitem>
      <para>
        Soma de verificação MD5 da transferência: &kf6-md5sum;
      </para>
    </listitem>
    <listitem>
      <para>
        Tamanho da transferência: &kf6-size;
      </para>
    </listitem>
    <listitem>
      <para>
        Espaço em disco estimado exigido: &kf6-buildsize;
      </para>
    </listitem>
    <listitem>
      <para>
        Tempo de construção estimado: &kf6-time;
      </para>
    </listitem>
  </itemizedlist>

  <bridgehead renderas="sect3">Dependências de KF6</bridgehead>

  <bridgehead renderas="sect4">Exigidas</bridgehead>
  <para role="required">
    
    <!--<xref linkend="boost"/>
,-->
<!--<xref linkend="giflib"/>
,-->
<!--<xref linkend="libepoxy"/>
,-->
<!--<xref linkend="libjpeg"/>
,-->
<!--<xref linkend="libpng"/>
,-->
<!--<xref linkend="phonon"/>
,-->
<xref linkend="extra-cmake-modules"/>, <xref linkend="breeze-icons"/>, <xref
linkend="DocBook"/>, <xref linkend="docbook-xsl"/>, <xref
linkend="libcanberra"/>, <xref linkend="libgcrypt"/>, <xref
linkend="libical"/>, <xref linkend="libxslt"/>, <xref linkend="lmdb"/>,
<xref linkend="qca"/>, <xref linkend="qrencode"/>, <xref
linkend="plasma-wayland-protocols"/>, <xref linkend="PyYAML"/>, <xref
linkend="shared-mime-info"/>, <xref linkend="perl-uri"/> e <xref
linkend="wget"/> (exigido para baixar os pacotes)
  </para>

  <bridgehead renderas="sect4">Recomendadas</bridgehead>
  <para role="recommended">
    <xref linkend="aspell"/> (estrutura de retaguarda de dicionário para
Sonnet), <xref linkend="avahi"/> (estrutura de retaguarda de DNS-SD para
KDNSSD), <xref linkend="ModemManager"/> (necessário para construir
ModemManager-Qt), <xref linkend="NetworkManager"/> (necessário para
construir NetworkManager-Qt), <xref linkend="polkit-qt"/> (estrutura de
retaguarda de autenticação para KAuth), <xref linkend="vulkan-loader"/>
(adiciona suporte para controladores gráficos do Vulkan) e <xref
linkend="zxing-cpp"/> (adicionar suporte necessário para construir
spectacle)
  </para>

  <para role="recommended">
    Além disso, as instruções abaixo pressupõem que o ambiente tenha sido
configurado conforme descrito em <xref linkend="kf6-intro"/>.
  </para>

  <bridgehead renderas="sect4">Opcionais</bridgehead>
  <para role="optional">
    <xref linkend="bluez"/> (necessário para construir Bluez-Qt) e <ulink
url="https://libdmtx.sourceforge.net/">Datamatrix</ulink> (considerado
recomendado para Prison pelo fluxo de desenvolvimento)
  </para>

  <bridgehead renderas="sect4">Dependência de tempo de execução para FrameworkIntegration</bridgehead>
  <para role="optional">
    <xref role="runtime" linkend="noto-fonts"/>
  </para>
  <bridgehead renderas="sect4">Dependências adicionais recomendadas para kapidox</bridgehead>
  <para role="recommended">
    <xref role="runtime" linkend="doxygen"/> (tempo de execução), <xref
linkend="doxypypy"/>, <xref linkend="doxyqml"/> e <xref linkend="requests"/>
  </para>

  <bridgehead renderas="sect4">Suporte a formatos adicionais de imagem no KImageFormats</bridgehead>
  <para role="optional">
    <xref linkend="libavif"/>, <xref linkend="libjxl"/>, <xref
linkend="libraw"/>, <ulink
url="https://github.com/strukturag/libheif">libheif</ulink>, <ulink
url="https://github.com/AcademySoftwareFoundation/openexr">OpenEXR</ulink>
  </para>

  <bridgehead renderas="sect4">Dependências opcionais para Solid</bridgehead>
  <para role="optional">
    <xref linkend="udisks2"/>, <xref linkend="upower"/> e <ulink role="runtime"
url="https://www.freedesktop.org/software/media-player-info/">media-player-info</ulink>
(tempo de execução)
  </para>

  <bridgehead renderas="sect4">Dependência opcional para KWallet</bridgehead>
  <para role="optional">
    <xref linkend="gpgme"/>, construído com ligações C++ (o que é o padrão).
  </para>

  <bridgehead renderas="sect4">Estruturas de retaguarda opcionais de dicionário para Sonnet</bridgehead>
  <para role="optional">
    <ulink url="http://hspell.ivrix.org.il/">Hspell</ulink> e <ulink
url="https://hunspell.sourceforge.net/">Hunspell</ulink>
  </para>

  <sect2>
    <title>Baixando KDE Frameworks</title>

    <para>
      A maneira mais fácil de obter os pacotes do KDE Frameworks é a de usar um
<command>wget</command> para buscá-los todos de uma vez:
    </para>

<screen><userinput>url=https://download.kde.org/stable/frameworks/&kf6-short-version;/
wget -r -nH -nd -A '*.xz' -np $url</userinput>
<literal>
The options used here are:
  -r            recurse through child directories
  -nH           disable generation of host-prefixed directories
  -nd           do not create a hierarchy of directories
  -A '*.xz'     just get the *.xz files
  -np           don't get parent directories</literal></screen>

   </sect2>

  <sect2>
    <title>Configurando a Ordem do Pacote</title>

    <para>
      A ordem de construção dos arquivos é importante devido às dependências
internas. Crie a lista de arquivos na ordem correta conforme segue:
    </para>

<screen><userinput>cat &gt; frameworks-&kf6-version;.md5 &lt;&lt; "EOF"
<literal>9359cfb89031abeeff8616d73e161415  attica-6.11.0.tar.xz
#22bce8bd85c1ead07bda8a00518d1120  extra-cmake-modules-6.11.0.tar.xz
647b2f9cfd55930e86ebb8c1734df140  kapidox-6.11.0.tar.xz
20c3fbf44d196d578bf9b76d0631a1ad  karchive-6.11.0.tar.xz
4b4af41ab285dda00a660b34e21e2f8c  kcodecs-6.11.0.tar.xz
b0e4142ee55abf3cb38fe6cdf5a0fe71  kconfig-6.11.0.tar.xz
6fa75ef6c2e9ce2fc9b7fb7f77e8db53  kcoreaddons-6.11.0.tar.xz
2ee71003f7887b6ac6646abd134e23ba  kdbusaddons-6.11.0.tar.xz
ead6114916c7aa01a8f6c3965315e9ec  kdnssd-6.11.0.tar.xz
7679bb9a2a0e817f9660ec775f93dc04  kguiaddons-6.11.0.tar.xz
27a6809407ea64c9db9bbb595cfe1282  ki18n-6.11.0.tar.xz
16d30e56e423668c27bfcbd2425a19da  kidletime-6.11.0.tar.xz
4f035f9b0466e297bd95ee57eb53f820  kimageformats-6.11.0.tar.xz
7e48fb5e56bcedff3281d64115d85699  kitemmodels-6.11.0.tar.xz
df934558aaa739a50b16677a3cdc97f7  kitemviews-6.11.0.tar.xz
cbe17f3ad215492a6d3d60d1b161a056  kplotting-6.11.0.tar.xz
85234f0053b4cf6ac5492ba92db2d648  kwidgetsaddons-6.11.0.tar.xz
0fe4524579013c8e9fcf7adf43ea844e  kwindowsystem-6.11.0.tar.xz
72fbceb5e5b990d501a1ba3684c78ad0  networkmanager-qt-6.11.0.tar.xz
05d5c3b1277129f4e5fa1e3db9077e10  solid-6.11.0.tar.xz
cc92475aacad8a29de2029e6483ff0b2  sonnet-6.11.0.tar.xz
5236898e85ea7b2b8124ffe21e2e8a51  threadweaver-6.11.0.tar.xz
efe35421eb54433599bb5724bd4b2312  kauth-6.11.0.tar.xz
33067f4f53c2e0e0da9d1324f01af19a  kcompletion-6.11.0.tar.xz
1562644115cb4a3d8f5e847c196a6e1b  kcrash-6.11.0.tar.xz
52d5e6b18613fd23ba09d05459812062  kdoctools-6.11.0.tar.xz
0cadf6b126b5ed62393b99146bbdaec9  kpty-6.11.0.tar.xz
6d306e6f78769cb8405f909e26db3d76  kunitconversion-6.11.0.tar.xz
b0a6afbdee57b249771017be13b5e9cf  kcolorscheme-6.11.0.tar.xz
b2c09a5d3fdcfa3c7fc88617590ac694  kconfigwidgets-6.11.0.tar.xz
71abec8311c27c7b3f8652da220011b8  kservice-6.11.0.tar.xz
357c4a01badb564c4a66ce3503bb7493  kglobalaccel-6.11.0.tar.xz
772720c4f1b51589abd1964750ef96e6  kpackage-6.11.0.tar.xz
6de72206275db095889d5ef02ecfeaae  kdesu-6.11.0.tar.xz
f7c14bb2f32943060553c517956bcd36  kiconthemes-6.11.0.tar.xz
3a502ab55f0ca2f1a5e5ab69de951932  knotifications-6.11.0.tar.xz
e3056aadf29ca9a09f015632f5c851d8  kjobwidgets-6.11.0.tar.xz
fe6ac2b10dcadad94e2b54dfd8788fc1  ktextwidgets-6.11.0.tar.xz
cbecf5de939fa3c2340abf4c1c34a550  kxmlgui-6.11.0.tar.xz
83d780dba76ffdb59b119513a94cfaa0  kbookmarks-6.11.0.tar.xz
c69002e34487b8182a7b248265846a8f  kwallet-6.11.0.tar.xz
e68dc289e81b20bf781ca7920133ae37  kded-6.11.0.tar.xz
76a597b191d8a1ff103dec70137635a7  kio-6.11.0.tar.xz
092619893a0c6e0e9e735e2396f9518b  kdeclarative-6.11.0.tar.xz
90434f367fce9f8ea1cfaccc4642efef  kcmutils-6.11.0.tar.xz
c12b36fecb8f26b48d793fe7e3a29405  kirigami-6.11.0.tar.xz
3227ac57ecdaed4c0d4c6b6b48b81ac4  syndication-6.11.0.tar.xz
096b9d761ee2997f34de83a9cee5062d  knewstuff-6.11.0.tar.xz
e5ba9f692c12dc7f3f4e2291aca802d7  frameworkintegration-6.11.0.tar.xz
138bf9e51cadd7381a9de428345c78c2  kparts-6.11.0.tar.xz
f75164871c6e7594f81c71d0849f90d3  syntax-highlighting-6.11.0.tar.xz
5ece9440ecedae18706884501e8deae4  ktexteditor-6.11.0.tar.xz
cc4b04c3c42a54259d704534b083f991  modemmanager-qt-6.11.0.tar.xz
fb993807e86e8f1679a6ea59aed71c1e  kcontacts-6.11.0.tar.xz
1545793a6da6d4b67f42959dc49d4cef  kpeople-6.11.0.tar.xz
e55224a99439e7bb1abf7eab2cd2ef75  bluez-qt-6.11.0.tar.xz
43e1dba98100aebf27297815a0fb8296  kfilemetadata-6.11.0.tar.xz
5e4b41a45872794d45038ca66125cbf8  baloo-6.11.0.tar.xz
#8989043ad5c6bae49cb2abb3e88490aa  breeze-icons-6.11.0.tar.xz
d336f55f2747377d0b8f388263749fe4  krunner-6.11.0.tar.xz
5291071632d3296b6d1201622fe1f54e  prison-6.11.0.tar.xz
eb72c496e3e6ba0ea66aa96a81683491  qqc2-desktop-style-6.11.0.tar.xz
89a6c13f89e6ac7ae5cd36c28ea51ba4  kholidays-6.11.0.tar.xz
0f72d877a62f5697c08c3649d0dc61cc  purpose-6.11.0.tar.xz
268c7ca1ba88d7cd544914760d1862a6  kcalendarcore-6.11.0.tar.xz
3b5c13512812315c913e936d8c14298d  kquickcharts-6.11.0.tar.xz
29c0bab2e75f093cbfb1630f349959a3  knotifyconfig-6.11.0.tar.xz
eb41b80c24a0768b915dab171d3db5c3  kdav-6.11.0.tar.xz
56e50e418a25fd981a11553f0e313800  kstatusnotifieritem-6.11.0.tar.xz
1e757bef30a30391eafe34ac06b5d281  ksvg-6.11.0.tar.xz
cdb940239552ee0c70fab32f496bc592  ktexttemplate-6.11.0.tar.xz
ee50bf09b87548e416b60826b6086041  kuserfeedback-6.11.0.tar.xz</literal>
EOF</userinput></screen>

  <para>
    Na lista acima, observe que alguns arquivos foram comentados com um
caractere cerquilha (#). 
    
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          A entrada extra-cmake-modules foi comentada porque foi construída
anteriormente no <xref linkend="kde-prereq"/>.
        </para>
      </listitem>
      <listitem>
        <para>
          O pacote de ícones é abordado separadamente em <xref
linkend="breeze-icons"/>.
        </para>
      </listitem>

    <!--
      <listitem>

        <para>
          There is one package downloaded that is superseded by a later version:
          solid-6.9.1. This release fixes a crash that can occur when inserting
          or removing a USB drive or other removable device.
        </para>
      </listitem>
-->
</itemizedlist>
  </para>

  </sect2>

  <sect2 role="installation">
    <title>Instalação do KDE Frameworks</title>

    &as_root;

    <caution>

      <para>
        Se instalar em <filename class="directory">/opt</filename> e existir um
/opt/kf6 existente ou como um diretório regular ou como um link simbólico,
ele deveria ser reinicializado (como <systemitem class="username">
root</systemitem>):
      </para>

      <screen role="root" revision="sysv"><userinput>mv -v /opt/kf6 /opt/kf6.old                         &amp;&amp;
install -v -dm755           $KF6_PREFIX/{etc,share} &amp;&amp;
ln -sfv /etc/dbus-1         $KF6_PREFIX/etc         &amp;&amp;
ln -sfv /usr/share/dbus-1   $KF6_PREFIX/share       &amp;&amp;
ln -sfv /usr/share/polkit-1 $KF6_PREFIX/share</userinput></screen>

      <screen role="root" revision="systemd"><userinput>mv -v /opt/kf6 /opt/kf6.old                         &amp;&amp;
install -v -dm755           $KF6_PREFIX/{etc,share} &amp;&amp;
ln -sfv /etc/dbus-1         $KF6_PREFIX/etc         &amp;&amp;
ln -sfv /usr/share/dbus-1   $KF6_PREFIX/share       &amp;&amp;
ln -sfv /usr/share/polkit-1 $KF6_PREFIX/share       &amp;&amp;
install -v -dm755           $KF6_PREFIX/lib         &amp;&amp;
ln -sfv /usr/lib/systemd    $KF6_PREFIX/lib</userinput></screen>

    </caution>

    <para>
      Primeiro, inicie um sub shell que sairá em caso de erro:
    </para>


<screen><userinput>bash -e</userinput></screen>

    <para>
      Instale todos os pacotes executando os seguintes comandos:
    </para>

<screen><!-- some packages end with files owned by root in $packagedir, so use as_root
     in the following -->
<userinput>while read -r line; do

    # Get the file name, ignoring comments and blank lines
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
    file=$(echo $line | cut -d" " -f2)

    pkg=$(echo $file|sed 's|^.*/||')          # Remove directory
    packagedir=$(echo $pkg|sed 's|\.tar.*||') # Package directory

    name=$(echo $pkg|sed 's|-6.*$||') # Isolate package name

    tar -xf $file
    pushd $packagedir

      # kapidox is a python module
      case $name in
        kapidox)
          &build-wheel-cmd; $PWD
          as_root &install-wheel; kapidox
          popd
          rm -rf $packagedir
          continue
          ;;
      esac

      mkdir build
      cd    build

      cmake -D CMAKE_INSTALL_PREFIX=$KF6_PREFIX \
            -D CMAKE_INSTALL_LIBEXECDIR=libexec \
            -D CMAKE_PREFIX_PATH=$QT6DIR        \
            -D CMAKE_SKIP_INSTALL_RPATH=ON      \
            -D CMAKE_BUILD_TYPE=Release         \
            -D BUILD_TESTING=OFF                \
            -W no-dev ..
      make
      as_root make install
    popd

  as_root rm -rf $packagedir
  as_root /sbin/ldconfig

done &lt; frameworks-&kf6-version;.md5

exit</userinput></screen>

    <note>
      <para>
        Quaisquer módulos que tenham sido omitidos podem ser instalados
posteriormente usando-se o mesmo procedimento <command>mkdir build; cd
build; cmake; make; make install</command> conforme acima.
      </para>
    </note>

    <para revision="sysv">
      Unidades do systemd inúteis foram instaladas em <filename
class="directory">$KF6_PREFIX/lib</filename>. Remova-as agora (como &root;):
    </para>

<screen role="root"
        revision="sysv"><userinput>rm -rf $KF6_PREFIX/lib/systemd</userinput></screen>

    <para>
      Ocasionalmente, os caminhos de instalação estão rigidamente codificados nos
arquivos instalados. Se o diretório instalado não for /usr, renomeie o
diretório e crie um link simbólico:
    </para>

    <screen role="root"><userinput>mv -v /opt/kf6 /opt/kf6-&kf6-version;
ln -sfvn kf6-&kf6-version; /opt/kf6</userinput></screen>

  </sect2>

    <sect2 role="commands">
    <title>Explicações do Comando</title>

    <para>
      <parameter>-D CMAKE_PREFIX_PATH=$QT6DIR</parameter>: Essa chave é usada para
permitir que cmake encontre as bibliotecas Qt adequadas.
    </para>

    <para>
      <parameter>-D CMAKE_BUILD_TYPE=Release</parameter>: Essa chave é usada para
aplicar um nível mais alto de otimização à compilação.
    </para>

    <para>
      <parameter>-D BUILD_TESTING=OFF</parameter>: Essa chave é usada para evitar
construir aplicativos de teste e bibliotecas que não são de uso para um(a)
usuário(a) final.
    </para>

  </sect2>
  <sect2 role="content">
    <title>Conteúdo</title>

    <segmentedlist>
      <segtitle>Aplicativos Instalados</segtitle>
      <segtitle>Bibliotecas Instaladas</segtitle>
      <segtitle>Diretórios Instalados</segtitle>

      <seglistitem>
        <seg>
          balooctl, baloo_file, baloo_file_extractor, baloosearch, balooshow,
checkXML6, depdiagram-generate, depdiagram-generate-all, depdiagram-prepare,
desktoptojson, gentrigrams, kactivities-cli, kapidox_generate,
kate-syntax-highlighter, kbuildsycoca6, kcookiejar6, kdebugdialog6, kded6,
kdeinit6, kdeinit6_shutdown, kdeinit6_wrapper, kf6-config, kf6kross,
kgendesignerplugin, kglobalaccel6, kiconfinder6, kjs6, kjscmd6, kjsconsole,
knewstuff-dialog, kpackagelauncherqml, kpackagetool6, kquitapp6,
kreadconfig6, kshell6, ktelnetservice6, ktrash6, kwalletd6, kwallet-query,
kwrapper6, kwriteconfig6, meinproc6, parsetrigrams, plasmapkg2,
preparetips6, protocoltojson e solid-hardware6
        </seg>
        <seg>
          libkdeinit6_klauncher.so, libKF6Activities.so, libKF6ActivitiesStats.so,
libKF6Archive.so, libKF6Attica.so, libKF6AuthCore.so, libKF6Auth.so,
libKF6Baloo.so, libKF6BluezQt.so, libKF6Bookmarks.so, libKF6CalendarCore.so,
libKF6CalendarEvents.so, libKF6Codecs.so, libKF6Completion.so,
libKF6ConfigCore.so, libKF6ConfigGui.so, libKF6ConfigWidgets.so,
libKF6Contacts.so, libKF6CoreAddons.so, libKF6Crash.so, libKF6DAV.so,
libKF6DBusAddons.so, libKF6Declarative.so, libKF6DNSSD.so,
libKF6DocTools.so, libKF6Emoticons.so, libKF6FileMetaData.so,
libKF6GlobalAccel.so, libKF6GuiAddons.so, libKF6Holidays.so, libKF6I18n.so,
libKF6IconThemes.so, libKF6IdleTime.so, libKF6ItemModels.so,
libKF6ItemViews.so, libKF6JobWidgets.so, libKF6JSApi.so, libKF6JsEmbed.so,
libKF6JS.so, libKF6KCMUtils.so, libKF6KDELibs4Support.so, libKF6KHtml.so,
libKF6KIOCore.so, libKF6KIOFileWidgets.so, libKF6KIOGui.so,
libKF6KIONTLM.so, libKF6KIOWidgets.so, libKF6Kirigami2.so,
libKF6KrossCore.so, libKF6KrossUi.so, libKF6MediaPlayer.so,
libKF6NetworkManagerQt.so, libKF6NewStuffCore.so, libKF6NewStuff.so,
libKF6Notifications.so, libKF6NotifyConfig.so, libKF6Package.so,
libKF6Parts.so, libKF6PeopleBackend.so, libKF6People.so,
libKF6PeopleWidgets.so, libKF6PlasmaQuick.so, libKF6Plasma.so,
libKF6Plotting.so, libKF6Prison.so, libKF6Pty.so, libKF6Purpose.so,
libKF6PurposeWidgets.so, libKF6QuickAddons.so, libKF6Runner.so,
libKF6Service.so, libKF6Solid.so, libKF6SonnetCore.so, libKF6SonnetUi.so,
libKF6Style.so, libKF6Su.so, libKF6SyntaxHighlighting.so,
libKF6TextEditor.so, libKF6TextWidgets.so, libKF6ThreadWeaver.so,
libKF6UnitConversion.so, libKF6Wallet.so, libKF6WaylandClient.so,
libKF6WaylandServer.so, libKF6WidgetsAddons.so, libKF6WindowSystem.so,
libKF6XmlGui.so, libKF6XmlRpcClient.so e libkwalletbackend6.so
        </seg>
        <seg>
          /opt/kf6 (link simbólico para /opt/kf6-&kf6-version;) se instalar em /opt
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Descrições Curtas</bridgehead>
      <?dbfo list-presentation="list"?> <?dbhtml list-presentation="table"?>

      <varlistentry id="checkXML6">
        <term><command>checkXML6</command></term>
        <listitem>
          <para>
            é uma ferramenta para verificar erros de sintaxe em arquivos XML DocBook do
KDE
          </para>
          <indexterm zone="kf6-frameworks checkXML6">
            <primary sortas="b-checkXML6">checkXML6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="depdiagram-generate">
        <term><command>depdiagram-generate</command></term>
        <listitem>
          <para>
            é uma ferramenta para gerar um diagrama de dependências
          </para>
          <indexterm zone="kf6-frameworks depdiagram-generate">
            <primary sortas="b-depdiagram-generate">depdiagram-generate</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="depdiagram-generate-all">
        <term><command>depdiagram-generate-all</command></term>
        <listitem>
          <para>
            é uma ferramenta para gerar um diagrama de dependências para todas as
estruturas essenciais de suporte de uma vez
          </para>
          <indexterm zone="kf6-frameworks depdiagram-generate-all">
            <primary sortas="b-depdiagram-generate-all">depdiagram-generate-all</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="depdiagram-prepare">
        <term><command>depdiagram-prepare</command></term>
        <listitem>
          <para>
            é uma ferramenta para preparar arquivos de ponto
          </para>
          <indexterm zone="kf6-frameworks depdiagram-prepare">
            <primary sortas="b-depdiagram-prepare">depdiagram-prepare</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="desktoptojson">
        <term><command>desktoptojson</command></term>
        <listitem>
          <para>
            é uma ferramenta para converter um arquivo .desktop em um arquivo .json
          </para>
          <indexterm zone="kf6-frameworks desktoptojson">
            <primary sortas="b-desktoptojson">desktoptojson</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kbuildsycoca6">
        <term><command>kbuildsycoca6</command></term>
        <listitem>
          <para>
            reconstrói o cache de configuração do sistema de arquivos da área de
trabalho KService
          </para>
          <indexterm zone="kf6-frameworks kbuildsycoca6">
            <primary sortas="b-kbuildsycoca6">kbuildsycoca6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kcookiejar6">
        <term><command>kcookiejar6</command></term>
        <listitem>
          <para>
            é uma interface de linha de comando para o armazenamento de cookies HTTP
usado pelo KDE; um serviço do D-BUS para armazenar/recuperar/limpar cookies
          </para>
          <indexterm zone="kf6-frameworks kcookiejar6">
            <primary sortas="b-kcookiejar6">kcookiejar6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kded6">
        <term><command>kded6</command></term>
        <listitem>
          <para>
            consolida vários pequenos serviços em um processo
          </para>
          <indexterm zone="kf6-frameworks kded6">
            <primary sortas="b-kded6">kded6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kdeinit6">
        <term><command>kdeinit6</command></term>
        <listitem>
          <para>
            é um iniciador de processos mais ou menos semelhante ao famoso init usado
para inicializar o UNIX
          </para>
          <indexterm zone="kf6-frameworks kdeinit6">
            <primary sortas="b-kdeinit6">kdeinit6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kf6-config">
        <term><command>kf6-config</command></term>
        <listitem>
          <para>
            é um aplicativo de linha de comando usado para recuperar informações a
respeito da instalação do KDE ou de caminhos de usuário(a)
          </para>
          <indexterm zone="kf6-frameworks kf6-config">
            <primary sortas="b-kf6-config">kf6-config</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kf6kross">
        <term><command>kf6kross</command></term>
        <listitem>
          <para>
            executa conjuntos de comandos sequenciais kross escritos em Javascript,
Python, Ruby, Java e Falcon do KDE
          </para>
          <indexterm zone="kf6-frameworks kf6kross">
            <primary sortas="b-kf6kross">kf6kross</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <!--
      <varlistentry id="kgenapidox">

        <term><command>kgenapidox</command></term>
        <listitem>
          <para>
            is a tool to generate API documentation in the KDE style.
          </para>
          <indexterm zone="kf6-frameworks kgenapidox">
            <primary sortas="b-kgenapidox">kgenapidox</primary>
          </indexterm>
        </listitem>
      </varlistentry>
-->
<varlistentry id="kgendesignerplugin">
        <term><command>kgendesignerplugin</command></term>
        <listitem>
          <para>
            gera plugins de pequena engenhoca para o Qt(TM) Designer
          </para>
          <indexterm zone="kf6-frameworks kgendesignerplugin">
            <primary sortas="b-kgendesignerplugin">kgendesignerplugin</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kglobalaccel6">
        <term><command>kglobalaccel6</command></term>
        <listitem>
          <para>
            é um processo de segundo plano usado para registrar as combinações de teclas
e para ser notificado(a) quando a ação for deflagrada
          </para>
          <indexterm zone="kf6-frameworks kglobalaccel6">
            <primary sortas="b-kglobalaccel6">kglobalaccel6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kjs6">
        <term><command>kjs6</command></term>
        <listitem>
          <para>
            é o mecanismo ECMAScript/JavaScript do KDE
          </para>
          <indexterm zone="kf6-frameworks kjs6">
            <primary sortas="b-kjs5">kjs6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kjscmd6">
        <term><command>kjscmd6</command></term>
        <listitem>
          <para>
             é uma ferramenta para iniciar conjuntos de comandos sequenciais KJSEmbed a
partir da linha de comando
          </para>
          <indexterm zone="kf6-frameworks kjscmd6">
            <primary sortas="b-kjscmd6">kjscmd6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kjsconsole">
        <term><command>kjsconsole</command></term>
        <listitem>
          <para>
            é um console para <command>kjs6</command>
          </para>
          <indexterm zone="kf6-frameworks kjsconsole">
            <primary sortas="b-kjsconsole">kjsconsole</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kpackagelauncherqml">
        <term><command>kpackagelauncherqml</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para iniciar o aplicativo QML kpackage
          </para>
          <indexterm zone="kf6-frameworks kpackagelauncherqml">
            <primary sortas="b-kpackagelauncherqml">kpackagelauncherqml</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kpackagetool6">
        <term><command>kpackagetool6</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando do kpackage
          </para>
          <indexterm zone="kf6-frameworks kpackagetool6">
            <primary sortas="b-kpackagetool6">kpackagetool6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kreadconfig6">
        <term><command>kreadconfig6</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para recuperar valores a partir dos
arquivos de configuração do KDE
          </para>
          <indexterm zone="kf6-frameworks kreadconfig6">
            <primary sortas="b-kreadconfig6">kreadconfig6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kshell6">
        <term><command>kshell6</command></term>
        <listitem>
          <para>
            inicia aplicativos via kdeinit
          </para>
          <indexterm zone="kf6-frameworks kshell6">
            <primary sortas="b-kshell6">kshell6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="ktelnetservice6">
        <term><command>ktelnetservice6</command></term>
        <listitem>
          <para>
            é um serviço de telnet
          </para>
          <indexterm zone="kf6-frameworks ktelnetservice6">
            <primary sortas="b-ktelnetservice6">ktelnetservice6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="ktrash6">
        <term><command>ktrash6</command></term>
        <listitem>
          <para>
            é um aplicativo auxiliar para lidar com a lixeira do KDE
          </para>
          <indexterm zone="kf6-frameworks ktrash6">
            <primary sortas="b-ktrash6">ktrash6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kwalletd6">
        <term><command>kwalletd6</command></term>
        <listitem>
          <para>
           é o processo de segundo plano do gerenciador de carteira
          </para>
          <indexterm zone="kf6-frameworks kwalletd6">
            <primary sortas="b-kwalletd6">kwalletd6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="kwriteconfig6">
        <term><command>kwriteconfig6</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para escrever valores nos arquivos de
configuração do KDE
          </para>
          <indexterm zone="kf6-frameworks kwriteconfig6">
            <primary sortas="b-kwriteconfig6">kwriteconfig6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="meinproc6">
        <term><command>meinproc6</command></term>
        <listitem>
          <para>
            converte arquivos DocBook para HTML
          </para>
          <indexterm zone="kf6-frameworks meinproc6">
            <primary sortas="b-meinproc6">meinproc6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="plasmapkg2">
        <term><command>plasmapkg2</command></term>
        <listitem>
          <para>
            é uma ferramenta para instalar, listar e remover pacotes Plasma
          </para>
          <indexterm zone="kf6-frameworks plasmapkg2">
            <primary sortas="b-plasmapkg2">plasmapkg2</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="preparetips6">
        <term><command>preparetips6</command></term>
        <listitem>
          <para>
            é um conjunto de comandos sequenciais para extrair o texto a partir de um
arquivo de dicas
          </para>
          <indexterm zone="kf6-frameworks preparetips6">
            <primary sortas="b-preparetips6">preparetips6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="solid-hardware6">
        <term><command>solid-hardware6</command></term>
        <listitem>
          <para>
            é uma ferramenta de linha de comando para investigar dispositivos
disponíveis
         </para>
          <indexterm zone="kf6-frameworks solid-hardware6">
            <primary sortas="b-solid-hardware6">solid-hardware6</primary>
          </indexterm>
        </listitem>
      </varlistentry>

   </variablelist>

  </sect2>

</sect1>
